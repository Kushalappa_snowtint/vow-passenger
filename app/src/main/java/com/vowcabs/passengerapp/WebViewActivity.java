package com.vowcabs.passengerapp;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.vowcabs.passengerapp.utils.Utilities;

/**
 * Created by Sumanth on 02/09/16.
 */
public class WebViewActivity extends Activity {

    private WebView mWebView;
    String webUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        webUrl = getIntent().getStringExtra("URL");
        mWebView = (WebView) findViewById(R.id.webView);

        if (!TextUtils.isEmpty(webUrl)) {
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.loadUrl(webUrl);
            Utilities.showProgressDialog(this);

            mWebView.setWebViewClient(new WebViewClient() {

                public void onPageFinished(WebView view, String url) {
                    if (mWebView.getProgress() == 100) {
                        Utilities.hideProgressDialog();
                    }
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView webView, String urlNewString) {
                    return true;
                }

                @SuppressWarnings("deprecation")
                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    // Handle the error
                }

                @TargetApi(android.os.Build.VERSION_CODES.M)
                @Override
                public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                    // Redirect to deprecated method, so you can use it in all SDK versions
                    onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
                }

            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Utilities.dismissProgressDialog();
    }


}
