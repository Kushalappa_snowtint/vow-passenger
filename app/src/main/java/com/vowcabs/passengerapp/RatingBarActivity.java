package com.vowcabs.passengerapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONObject;

public class RatingBarActivity extends Activity {
    RatingBar ratingBar;
    TextView submit, userName;
    boolean isRated = false;
    String tag_updatetriprating_obj = "updatetriprating_req";
    String encodedString;
    EditText comments;
    float tripRating = -1;
    String tag_checktriprating_obj = "checktriprating_req";
    ImageView splashScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rateus_screen);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        submit = (TextView) findViewById(R.id.tv_submit);
        userName = (TextView) findViewById(R.id.customerName);
        comments = (EditText) findViewById(R.id.et_comments);
        splashScreen = (ImageView) findViewById(R.id.iv_splash_screen);

        try {
            startAnimation();
        } catch (Exception e) {
        }

       // checkForTripRating();
        comments.setEnabled(false);

        if (!TextUtils.isEmpty(Utilities.getSharedString("USERNAME", this))) {
            userName.setText("Hi ! " + Utilities.getSharedString("USERNAME", this));
        }

        ratingBar.setStepSize(1);
        ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {
                isRated = true;
                tripRating = rating;
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                makeRateLastTripApi();
            }

        });
    }

    private void makeRateLastTripApi() {
        if (Utilities.isNetworkAvailable(this)) {
            Utilities.showProgressDialog(this);
            encodedString = JsonHeaders.createUpdateTripRatingApiJSONHeader(RatingBarActivity.this, tripRating, comments.getText().toString());
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET, APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    if (!response.isNull("RESPONSECODE")) {

                    }
                    // rating screen functinality not implemented from serverside pushing home screen for timebeing
                    launchHomeScreen();
                    //Utilities.dialogdisplay("Thanks", "Your Feedback is important to us, help us serve you better.", RatingBarActivity.this);
                    // Log.d(tag_updatetriprating_obj, "Sucess" + response);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utilities.hideProgressDialog();
                    launchHomeScreen();
                    VolleyLog.d("Parsing " + tag_updatetriprating_obj, "Error: " + error.getMessage());
                    //Utilities.dialogdisplay("Network Error", "Please Try Again", RatingBarActivity.this);
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_updatetriprating_obj);
        }
    }

    public void dialogdisplay(String tilte, String Message) {
        final Dialog customDialog = new Dialog(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_view, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        TextView message = (TextView) dialogView.findViewById(R.id.tv_DialogMessage);
        title.setText(tilte);
        message.setText(Message);
        Button yes = (Button) dialogView.findViewById(R.id.btn_yes);
        Button no = (Button) dialogView.findViewById(R.id.btn_no);
        yes.setText("Ok");
        no.setVisibility(View.GONE);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();

                finish();
            }
        });
        customDialog.show();
    }

    private void checkForTripRating() {
        splashScreen.setVisibility(View.VISIBLE);

        if (Utilities.isNetworkAvailable(this)) {
            encodedString = JsonHeaders.createCheckForTripRatingApiJSONHeader(RatingBarActivity.this);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET, APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    try {
                        if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equalsIgnoreCase("000")) {

                            if (!response.isNull("TRIPID")) {
                                launchHomeScreen();
                                //splashScreen.setVisibility(View.GONE);
                                comments.setEnabled(true);

                                //Log.d(tag_checktriprating_obj, "Sucess" + response);
                            } else {
                                // rating screen functinality not implemented from serverside pushing home screen for timebeing
                                launchHomeScreen();
                            }
                        }
                    } catch (Exception e) {
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // rating screen functinality not implemented from serverside pushing home screen for timebeing
                    launchHomeScreen();
                    VolleyLog.d("Parsing " + tag_checktriprating_obj, "Error: " + error.getMessage());
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_checktriprating_obj);
        }
    }

    private void launchHomeScreen() {
        try {
            Intent intent = new Intent(RatingBarActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        } catch (Exception e) {
        }
    }


    private void startAnimation() {
        final TextView loadingText = (TextView) findViewById(R.id.loading_text);

        Thread splashTimer = new Thread() {
            public void run() {
                try {
                    int splashTime = 0;
                    while (splashTime < 6000) {

                        sleep(100);

                        if (splashTime < 2000) {
                            setText(".", loadingText);
                        } else if (splashTime >= 2000 && splashTime < 4000) {
                            setText("..", loadingText);
                        } else if (splashTime >= 4000) {
                            setText("...", loadingText);
                        }
                        splashTime = splashTime + 100;

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    launchHomeScreen();
                }
            }
        };
        splashTimer.start();
    }

    void setText(final String text, final TextView tv) {
        try {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tv.setText(text);

                }
            });
        } catch (Exception e) {
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            Utilities.dismissProgressDialog();
        } catch (Exception e) {
        }
    }

}
