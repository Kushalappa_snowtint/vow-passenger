package com.vowcabs.passengerapp.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.R;

/**
 * Created by Sumanth on 29/07/16.
 */
@SuppressLint("ValidFragment")
public class MobitelPaymentFragment extends HomeBaseFragment{

    String tokenId;
    String url;

    public MobitelPaymentFragment(String tokenId, String url)
    {
        this.tokenId = tokenId;
        this.url = url.replace("\\","");
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.mobitel_payment_fragment, container,false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) mActivity).pushFragment(new WebUrlViewFragment(url+tokenId), false, false);
    }
}
