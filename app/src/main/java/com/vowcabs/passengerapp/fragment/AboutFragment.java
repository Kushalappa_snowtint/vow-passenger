package com.vowcabs.passengerapp.fragment;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.MyApplication;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.utils.LocationInstance;

public class AboutFragment extends HomeBaseFragment {
    TextView aboutUs, termsUsed, versionNumber;
    ImageView aboutLogo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.aboutscreen, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        aboutUs = (TextView) getView().findViewById(R.id.tv_aboutUs);
        termsUsed = (TextView) getView().findViewById(R.id.tv_terms);
        versionNumber = (TextView) getView().findViewById(R.id.tv_version);
        aboutLogo      = (ImageView) getView().findViewById(R.id.iv_aboutus_logo);
        versionNumber.setText("Version " + getApplicationVersionName(getActivity()));

        ((HomeActivity) getActivity()).updateHeaderText("About Us", mActivity);

        if (LocationInstance.getInstance().getCountryCode().equalsIgnoreCase("LK")) {
            aboutLogo.setImageResource(R.drawable.vowcab_logo);
        }
        else
        {
            aboutLogo.setImageResource(R.drawable.vow_logo);
        }
        aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LocationInstance.getInstance().getCountryCode().equalsIgnoreCase("LK")) {
                    ((HomeActivity) mActivity).pushFragment(new WebUrlViewFragment("http://lk.vowcabs.com/aboutus.html"), false, false);
                }
                ((HomeActivity) mActivity).pushFragment(new WebUrlViewFragment("http://vowcabs.com/aboutus.html"), false, false);
            }
        });
        termsUsed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LocationInstance.getInstance().getCountryCode().equalsIgnoreCase("LK")) {
                    ((HomeActivity) mActivity).pushFragment(new WebUrlViewFragment("http://lk.vowcabs.com/terms.html"), false, false);
                }
                else {
                    ((HomeActivity) mActivity).pushFragment(new WebUrlViewFragment("http://vowcabs.com/terms.html"), false, false);
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).updateHeaderText("About Us", mActivity);
        MyApplication.getInstance().trackScreenView("About us screen");

    }

    public String getApplicationVersionName(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (NameNotFoundException ex) {

        } catch (Exception e) {

        }
        return "";
    }


}
