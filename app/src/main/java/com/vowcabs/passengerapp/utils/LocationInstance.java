package com.vowcabs.passengerapp.utils;

import android.text.TextUtils;

import com.vowcabs.passengerapp.Models.CABMINI;
import com.vowcabs.passengerapp.Models.CABPRIME;
import com.vowcabs.passengerapp.Models.CABSEDAN;
import com.vowcabs.passengerapp.Models.CABTUKTUK;
import com.vowcabs.passengerapp.json.JSONKEYS;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LocationInstance 
{

	public double latitude ;
	public double longitute ;

    public JSONObject tmhInfo;

	public String currentLocation ="";
	public String currentSelectedPackage ="";
	public double selectedPackageFare ;

	public double destiantionLatitude ;
	public double destiantionLongitute ;
	public int selectedCar =0;
	public boolean isDialogShown = false;
	public String countryCode ="";
	public float toleranceAmount = 1.0f;
    public CABMINI  miniCab = new CABMINI();
    public CABSEDAN sedanCab = new CABSEDAN() ;
    public CABPRIME primeCab = new CABPRIME();
    public CABTUKTUK tuktuk = new CABTUKTUK();
    public boolean sHasLatLngChanged = false;


	private static LocationInstance instance = null;

	private LocationInstance()
	{ 

	}

	public static LocationInstance getInstance() 
	{
		if(instance == null)
		{
			instance = new LocationInstance();
		}
		return instance;
	}


	public double getLatitude() 
	{
		return latitude;
	}

	public void setLatitude(double latitude)
	{
		this.latitude = latitude;
	}

	public double getLongitute() 
	{
		return longitute;
	}

	public void setLongitute(double longitute) 
	{
		this.longitute = longitute;
	}
	public String getCurrentLocation() 
	{
		return currentLocation;
	}

	public void setCurrentLocation(String currentLocation) 
	{
		this.currentLocation = currentLocation;
	}

	public double getDestiantionLatitude()
	{
		return destiantionLatitude;
	}

	public void setDestiantionLatitude(double destiantionLatitude)
	{
		this.destiantionLatitude = destiantionLatitude;
	}

	public double getDestiantionLongitute() 
	{
		return destiantionLongitute;
	}

	public void setDestiantionLongitute(double destiantionLongitute)
	{
		this.destiantionLongitute = destiantionLongitute;
	}

	public int getSelectedCar() 
	{
		return selectedCar;
	}

	public void setSelectedCar(int selectedCar) 
	{
		this.selectedCar = selectedCar;
	}
	public String getCurrentSelectedPackage() 
	{
		return currentSelectedPackage;
	}

	public void setCurrentSelectedPackage(String currentSelectedPackage) 
	{
		this.currentSelectedPackage = currentSelectedPackage;
	}
	public boolean isDialogShown()
	{
		return isDialogShown;
	}

	public void setDialogShown(boolean isDialogShown)
	{
		this.isDialogShown = isDialogShown;
	}

	public double getSelectedPackageFare() 
	{
		return selectedPackageFare;
	}

	public void setSelectedPackageFare(String selectedPackageFare) 
	{
		Double FareIncludingServiceTax = Double.parseDouble(selectedPackageFare + ((Double.parseDouble(selectedPackageFare)*JSONKEYS.SERVICETAX)/100));
		this.selectedPackageFare = FareIncludingServiceTax;
	}
	public String getCountryCode()
	{
		return countryCode;
	}
	public void setCountryCode(String countryCode)
	{
		this.countryCode = countryCode;
	}

	public float getToleranceAmount() {
		return toleranceAmount;
	}

	public void setToleranceAmount(float toleranceAmount) {
		this.toleranceAmount = toleranceAmount;
	}

    public JSONObject getTmhInfo() {
        return tmhInfo;
    }

    public void setTmhInfo(JSONObject tmhInfo) {
        this.tmhInfo = tmhInfo;
    }

    public void setCabDetails(JSONObject response)
    {
        try {
            if (response.has("Cabs")) {
                JSONArray cabs = response.getJSONArray("Cabs");
                for (int index=0 ; index <cabs.length(); index++)
                {
                    JSONObject cab = cabs.getJSONObject(index);
                    if (cab.has("VehicleCategory") && !TextUtils.isEmpty(cab.getString("VehicleCategory")) && cab.getString("VehicleCategory").trim().equalsIgnoreCase("0"))
                    {
                        miniCab.setCabInfo(cab);
                    }
                    else if(cab.has("VehicleCategory") && !TextUtils.isEmpty(cab.getString("VehicleCategory")) && cab.getString("VehicleCategory").trim().equalsIgnoreCase("1"))
                    {
                        sedanCab.setCabInfo(cab);
                    }
                    else if(cab.has("VehicleCategory") && !TextUtils.isEmpty(cab.getString("VehicleCategory")) && cab.getString("VehicleCategory").trim().equalsIgnoreCase("2"))
                    {
                        primeCab.setCabInfo(cab);
                    }
                    else if(cab.has("VehicleCategory") && !TextUtils.isEmpty(cab.getString("VehicleCategory")) && cab.getString("VehicleCategory").trim().equalsIgnoreCase("3"))
                    {
                        tuktuk.setCabInfo(cab);
                    }
                }

            }
        }
      catch (JSONException e){}
    }


}
