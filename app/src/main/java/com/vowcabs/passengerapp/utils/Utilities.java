package com.vowcabs.passengerapp.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.vowcabs.passengerapp.AppController;
import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.fragment.BookTripFragment;
import com.vowcabs.passengerapp.fragment.MobitelPaymentFragment;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.Utilities.PolygonTest.PolyLatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Utilities {
    //	static final PolyLatLng bala= new PolyLatLng(12.978412,74.847222);
    //	static final PolyLatLng kulai= new PolyLatLng(12.959677,74.800530);
    //	static final PolyLatLng mulki= new PolyLatLng(13.019893,74.786798);
    //	static final PolyLatLng sash= new PolyLatLng(13.023907,74.822846);
    //	static final PolyLatLng sash1= new PolyLatLng(13.060364,74.779931);
    //	static final PolyLatLng sash2= new PolyLatLng(12.781204, 74.855853);


    static final PolyLatLng sas0 = new PolyLatLng(12.972442, 77.580643);
    static final PolyLatLng sas1 = new PolyLatLng(13.054021, 74.780142);
    static final PolyLatLng sas2 = new PolyLatLng(13.056367, 74.804218);
    static final PolyLatLng sas3 = new PolyLatLng(13.023920, 74.842739);
    static final PolyLatLng sas4 = new PolyLatLng(12.947202, 74.937165);
    static final PolyLatLng sas5 = new PolyLatLng(12.876250, 74.955709);
    static final PolyLatLng sas6 = new PolyLatLng(12.819596, 74.947601);
    static final PolyLatLng sas7 = new PolyLatLng(12.782981, 74.890892);
    static final PolyLatLng sas8 = new PolyLatLng(12.778677, 74.856383);
    static final PolyLatLng sas9 = new PolyLatLng(12.921274, 74.805176);
    static final PolyLatLng sas10 = new PolyLatLng(13.014916, 74.785722);
    static final PolyLatLng sas11 = new PolyLatLng(13.014916, 74.785722);
    static final PolyLatLng sas12 = new PolyLatLng(7.2908895, 79.885255);
    static final PolyLatLng sas13 = new PolyLatLng(7.2905489, 79.9077426);
    static final PolyLatLng sas14 = new PolyLatLng(7.3002544, 79.9551211);
    static final PolyLatLng sas15 = new PolyLatLng(7.3030053, 79.9875598);
    static final PolyLatLng sas16 = new PolyLatLng(7.357871, 80.139834);
    static final PolyLatLng sas17 = new PolyLatLng(7.244083, 80.215725);
    static final PolyLatLng sas18 = new PolyLatLng(7.104742, 80.204867);
    static final PolyLatLng sas19 = new PolyLatLng(6.9539639, 80.2474605);
    static final PolyLatLng sas20 = new PolyLatLng(6.4672298, 80.3813563);
    static final PolyLatLng sas21 = new PolyLatLng(6.3476468, 80.3152667);
    static final PolyLatLng sas22 = new PolyLatLng(6.324495, 80.289946);
    static final PolyLatLng sas23 = new PolyLatLng(6.374505, 80.086248);
    static final PolyLatLng sas24 = new PolyLatLng(6.525502, 79.932680);
    static final PolyLatLng sas25 = new PolyLatLng(6.816203, 79.860648);
    static final PolyLatLng sas26 = new PolyLatLng(6.924066, 79.843925);
    static final PolyLatLng sas27 = new PolyLatLng(7.199497, 79.811889);
    static final PolyLatLng sas28 = new PolyLatLng(7.3232136, 79.7946124);
    static final PolyLatLng sas29 = new PolyLatLng(13.025496, 77.639258);
    static final PolyLatLng sas30 = new PolyLatLng(13.016517, 77.633917);
    static final PolyLatLng sas31 = new PolyLatLng(13.014698, 77.647028);
    static final PolyLatLng sas32 = new PolyLatLng(13.020771, 77.650450);


    //static final PolyLatLng sas11= new PolyLatLng(26.8465108, 80.9466832);

    static PolyLatLng[] edges = {sas0, sas1, sas2, sas3, sas4, sas5, sas6, sas7, sas8, sas9, sas10, sas12, sas13, sas14, sas15, sas16,
            sas17, sas18, sas19, sas20, sas21, sas22, sas23, sas24, sas25, sas26, sas27, sas28, sas29, sas30, sas31, sas32};
    private static String tag_json_get_services = "jobj_get_services";
    private static String tag_json_store_gcm_details = "jobj_store_gcm_details";
    private static String tag_json_getTMH_details = "tag_json_getTMH_details";

    static Dialog customProgressDialog;
    private static String tag_req_getToken = "jobj_req_getToken";
    private static boolean tokenRequestInitiated = false;

    public static int getSharedInt(String key, Context context) {
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return app_preferences.getInt(key, 0);
    }

    public static void putSharedInt(String key, int value, Context context) {
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static String getSharedString(String key, Context context) {
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return app_preferences.getString(key, "");
    }

    public static void putSharedString(String key, String value, Context context) {
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static boolean getSharedBool(String key, Context context) {
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return app_preferences.getBoolean(key, false);
    }

    public static void putSharedBool(String key, Boolean value, Context context) {
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    //Girish
    public static void removeSharedValueForKey(String key, Context context) {
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.remove(key);
        editor.commit();
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            dialogdisplay("Network Error", "We could not find an active Internet connection, Please check your settings.", context);
            return false;
        }
    }

    public static int getDIP(Context context, int pixels) {
        try {
            return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, pixels, context.getResources().getDisplayMetrics());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return -1;
    }

    static Dialog customDialog = null;

    public static void dialogdisplay(String tilte, String Message, Context context) {
        if (context == null)
            return;

        if (customDialog != null) {
            if (customDialog.isShowing()) {
                customDialog.dismiss();
            }
            customDialog = null;
        }

        customDialog = new Dialog(context);
        View dialogView = customDialog.getLayoutInflater().inflate(R.layout.dialog_view, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        TextView message = (TextView) dialogView.findViewById(R.id.tv_DialogMessage);
        title.setText(tilte);
        message.setText(Message);
        Button yes = (Button) dialogView.findViewById(R.id.btn_yes);
        Button no = (Button) dialogView.findViewById(R.id.btn_no);
        yes.setText("Ok");
        no.setVisibility(View.GONE);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });
        customDialog.show();

    }

    public static void dialogdisplay(String tilte, String Message, final Activity context, final boolean isFinish) {
        if (context == null)
            return;

        if (customDialog != null) {
            if (customDialog.isShowing()) {
                customDialog.dismiss();
            }
            customDialog = null;
        }

        customDialog = new Dialog(context);
        View dialogView = customDialog.getLayoutInflater().inflate(R.layout.dialog_view, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        TextView message = (TextView) dialogView.findViewById(R.id.tv_DialogMessage);
        title.setText(tilte);
        message.setText(Message);
        Button yes = (Button) dialogView.findViewById(R.id.btn_yes);
        Button no = (Button) dialogView.findViewById(R.id.btn_no);
        yes.setText("Ok");
        no.setVisibility(View.GONE);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
                if (isFinish)
                    context.finish();
            }
        });
        customDialog.show();

    }

    public static void displayMobitelBookingDialog(final BookTripFragment.BookTrip bookTrip, Context context) {
        if (context == null)
            return;

        if (customDialog != null) {
            if (customDialog.isShowing()) {
                customDialog.dismiss();
            }
            customDialog = null;
        }

        customDialog = new Dialog(context);
        View dialogView = customDialog.getLayoutInflater().inflate(R.layout.dialog_view, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        TextView message = (TextView) dialogView.findViewById(R.id.tv_DialogMessage);
        title.setText("Thank You");
        message.setText("Thanks for choosing M-cash. This will be considered as cash ride till we get a payment confirmation from mobitel.");
        Button yes = (Button) dialogView.findViewById(R.id.btn_yes);
        Button no = (Button) dialogView.findViewById(R.id.btn_no);
        yes.setText("Book");
        no.setVisibility(View.GONE);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
                bookTrip.execute();
            }
        });
        customDialog.show();

    }

    public static String getSHA1(String Value) {
        try {
            MessageDigest md;
            md = MessageDigest.getInstance("SHA-1");
            byte[] md5hash = new byte[32];
            md.update(Value.getBytes("iso-8859-1"), 0, Value.length());
            md5hash = md.digest();
            return convertToHex(md5hash);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getMD5(String Value) {
        try {
            MessageDigest md;
            md = MessageDigest.getInstance("md5");
            byte[] md5hash = new byte[32];
            md.update(Value.getBytes("iso-8859-1"), 0, Value.length());
            md5hash = md.digest();
            return convertToHex(md5hash);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    private static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9))
                    buf.append((char) ('0' + halfbyte));
                else
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }


        return buf.toString();
    }

    public static String Encrypt(String text, String key)
            throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] keyBytes = new byte[16];
        byte[] b = key.getBytes("UTF-8");
        int len = b.length;
        if (len > keyBytes.length) len = keyBytes.length;
        System.arraycopy(b, 0, keyBytes, 0, len);
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

        byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
        String result = Base64.encodeToString(results, 0);
        return result;
    }

    public static String decrypt(String text, String key) {
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] keyBytes = new byte[16];
            byte[] b = key.getBytes("UTF-8");
            int len = b.length;
            if (len > keyBytes.length) len = keyBytes.length;
            System.arraycopy(b, 0, keyBytes, 0, len);
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            byte[] results = cipher.doFinal(Base64.decode(text, 0));
            return new String(results, "UTF-8");
        } catch (Exception e) {
            // TODO: handle exception
            Log.d("decryption", e.getMessage());
            return null;
        }
    }

    public static String getDeviceId(Context context) {
        final String tmDevice;
        String deviceId;
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        tmDevice = "" + tm.getDeviceId();

        if (tmDevice.length() == 0) {
            deviceId = getSharedString("DEVICE_ID", context);

            if (deviceId.length() == 0) {

                String uniqueID = UUID.randomUUID().toString();
                //				String mobileNo = getSharedString("MOBILE_NO", context);

                java.util.Date date = new java.util.Date();
                String timeStamp = new Timestamp(date.getTime()).toString();


                deviceId = getMD5(uniqueID + timeStamp);

                try {
                    putSharedString("DEVICE_ID", deviceId, context);
                } catch (Exception e) {
                    putSharedString("DEVICE_ID", deviceId, context);
                }

            }

        } else {
            deviceId = getMD5(tmDevice);
        }
        return deviceId;

    }

    //	//url encryption
//	public static String getEncryptedString(String json)
//	{
//		String encryptedString = "";
//		try
//		{
//			encryptedString = Encrypt(json+"|checkSum="+Utilities.getMD5(json), friendlyURLString());
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//		return encryptedString;
//	}
    //url encryption
    public static String getEncryptedString(String json) {
        String encryptedString = "";
        try {
            encryptedString = Encrypt(json + "|checkSum=" + Utilities.getMD5(json), "snowtint!@#$");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedString;
    }

//	public static String friendlyURLString()
//	{
//		String  baseurl = "http://api.vowcabs.com/IamYourCabFriend";
//		baseurl = baseurl.substring(5,35);
//        try
//        {
//            baseurl = baseurl.concat(getCurrentDay());
//        }
//        catch (Exception e){}
//
//		return URLUTF8Encoder.encode(baseurl);
//
//	}

    public static String getDecryptedJsonResponse(String encryptedResponseString) {
        String decryptedString = getDecryptedString(encryptedResponseString, "");
        String arg[] = decryptedString.split("\\|checkSum=");
        return arg[0];

    }

    public static String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd-mm-yyyy HH:mm:ss");
        return mdformat.format(calendar.getTime());
    }

    public static String getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("hh:mm a");
        return mdformat.format(calendar.getTime());
    }

    public static String getCurrentDay() {
        String weekDay;
        SimpleDateFormat dayFormat = new SimpleDateFormat("E", Locale.ENGLISH);

        Calendar calendar = Calendar.getInstance();
        weekDay = dayFormat.format(calendar.getTime());

        return weekDay;
    }


    public static String getDecryptedString(String response, String key) {
        String decryptedString = "";
        try {
            decryptedString = decrypt(response, key);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptedString;
    }


    public static void showProgressDialog(Context ctx) {
        if (customProgressDialog != null) {
            if (customProgressDialog.isShowing()) {
                customProgressDialog.dismiss();
            }
        }

        customProgressDialog = new Dialog(ctx);
        View dialogView = customProgressDialog.getLayoutInflater().inflate(R.layout.progress_dialog, null);
        customProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customProgressDialog.setContentView(dialogView);
        customProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        customProgressDialog.setCanceledOnTouchOutside(false);
        customProgressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        customProgressDialog.show();

    }

    public static void hideProgressDialog() {
        if (customProgressDialog != null) {
            customProgressDialog.dismiss();
        }
    }

    public static void dismissProgressDialog() {
        if (customProgressDialog != null && customProgressDialog.isShowing()) {
            customProgressDialog.dismiss();
        }
    }

    public static String calculateEstimatedTravelTime(JSONObject jObject) {
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        //JSONArray jSteps = null;
        //JSONObject jDistance = null;
        JSONObject jDuration = null;
        try {
            jRoutes = jObject.getJSONArray("routes");
            for (int i = 0; i < jRoutes.length(); i++) {
                jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");

                for (int j = 0; j < jLegs.length(); j++) {
                    /** Getting distance from the json data */
                    //jDistance = ((JSONObject) jLegs.get(j)).getJSONObject("distance");

                    jDuration = ((JSONObject) jLegs.get(j)).getJSONObject("duration");
                }
            }
            if (jDuration != null)
                return jDuration.getString("text");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static void setETA(TextView carETA, String miniETA, boolean isAuto) {
        if (miniETA.length() > 0) {
            carETA.setText(miniETA);
        } else {
            if (!isAuto)
                carETA.setText("No Cabs");
            else
                carETA.setText("No Autos");

        }
    }

    @SuppressWarnings("deprecation")
    public static void clickableAndChangeColor(TextView textView, int colorCode, Context ctx, boolean isClickable) {
        if (ctx != null) {
            textView.setTextColor(ctx.getResources().getColor(colorCode));
            textView.setClickable(isClickable);
        }

    }

    public static Bitmap makeCircularBitmap(Bitmap bmp, int targetWidth, int targetHeight) {
        Bitmap circularBitMap = Bitmap.createBitmap(targetWidth, targetHeight,
                Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(circularBitMap);
        Path path = new Path();
        path.addCircle(
                ((float) targetWidth - 1) / 2,
                ((float) targetHeight - 1) / 2,
                (Math.min(((float) targetWidth), ((float) targetHeight)) / 2),
                Path.Direction.CCW);

        canvas.clipPath(path);
        Bitmap sourceBitmap = bmp;
        canvas.drawBitmap(
                sourceBitmap,
                new Rect(0, 0, sourceBitmap.getWidth(), sourceBitmap
                        .getHeight()), new Rect(0, 0, targetWidth,
                        targetHeight), new Paint(Paint.FILTER_BITMAP_FLAG));
        return circularBitMap;

    }

    public static Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static boolean isPointInRegion(double x, double y) {
        int crossings = 0;
        PolyLatLng point = new PolyLatLng(x, y);
        int count = edges.length;
        // for each edge
        for (int i = 0; i < count; i++) {
            PolyLatLng a = edges[i];
            int j = i + 1;
            if (j >= count) {
                j = 0;
            }
            PolyLatLng b = edges[j];
            if (rayCrossesSegment(point, a, b)) {
                crossings++;
            }
        }
        // odd number of crossings?
        return (crossings % 2 == 1);
        //return true;
    }

    static boolean rayCrossesSegment(PolyLatLng point, PolyLatLng a, PolyLatLng b) {
        double px = point.Longitude;
        double py = point.Latitude;
        double ax = a.Longitude;
        double ay = a.Latitude;
        double bx = b.Longitude;
        double by = b.Latitude;
        if (ay > by) {
            ax = b.Longitude;
            ay = b.Latitude;
            bx = a.Longitude;
            by = a.Latitude;
        }
        // alter longitude to cater for 180 degree crossings
        if (px < 0) {
            px += 360;
        }
        ;
        if (ax < 0) {
            ax += 360;
        }
        ;
        if (bx < 0) {
            bx += 360;
        }
        ;

        if (py == ay || py == by) py += 0.00000001;
        if ((py > by || py < ay) || (px > Math.max(ax, bx))) return false;
        if (px < Math.min(ax, bx)) return true;

        double red = (ax != bx) ? ((by - ay) / (bx - ax)) : Float.MAX_VALUE;
        double blue = (ax != px) ? ((py - ay) / (px - ax)) : Float.MAX_VALUE;
        return (blue >= red);
    }

    public static class PolygonTest {
        static class PolyLatLng {
            double Latitude;
            double Longitude;

            PolyLatLng(double lat, double lon) {
                Latitude = lat;
                Longitude = lon;
            }
        }
    }

    private static String tag_add_recentplaces_obj = "add_recentplaces_req";

    public static void makeAddRecentPlacesApi(final String bookingDestinationAdd, Activity ctx) {
        if (ctx == null)
            return;

        if (Utilities.isNetworkAvailable(ctx)) {
            String encodedString = JsonHeaders.createAddRecentPlacesApiJSONHeader(bookingDestinationAdd,
                    String.valueOf(LocationInstance.getInstance().getDestiantionLatitude()), String.valueOf(LocationInstance.getInstance().getDestiantionLongitute()), ctx);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET, APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("Recent Places Added", bookingDestinationAdd);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_add_recentplaces_obj);
        }

    }

    public static String getDirectionsUrl(LatLng origin, LatLng dest, Context context) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        String key = "key=" + context.getResources().getString(R.string.google_places_server_key);

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + key;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    public static Bitmap writeTextOnDrawable(int drawableId, String text, Activity activity) {
        Bitmap bm = BitmapFactory.decodeResource(activity.getResources(), drawableId)
                .copy(Bitmap.Config.ARGB_8888, true);

        Typeface tf = Typeface.create("Helvetica", Typeface.BOLD);

        Paint paint = new Paint();
        paint.setStyle(Style.FILL);
        paint.setColor(Color.WHITE);
        paint.setTypeface(tf);
        paint.setTextAlign(Align.CENTER);
        paint.setTextSize(convertToPixels(activity, 10));

        Rect textRect = new Rect();
        paint.getTextBounds(text, 0, text.length(), textRect);
        Canvas canvas = new Canvas(bm);

        //If the text is bigger than the canvas , reduce the font size
        if (textRect.width() >= (canvas.getWidth() - 4))     //the padding on either sides is considered as 4, so as to appropriately fit in the text
            paint.setTextSize(convertToPixels(activity, 7));        //Scaling needs to be used for different dpi's

        //Calculate the positions
        int xPos = (canvas.getWidth() / 2) - 2;     //-2 is for regulating the x position offset

        //"- ((paint.descent() + paint.ascent()) / 2)" is the distance from the baseline to the center.
        int yPos = (int) ((canvas.getHeight() / 2) - ((paint.descent() + paint.ascent()) / 2));

        canvas.drawText(text, xPos, yPos, paint);

        return bm;
    }


    public static int convertToPixels(Context context, int nDP) {
        final float conversionScale = context.getResources().getDisplayMetrics().density;
        return (int) ((nDP * conversionScale) + 0.5f);
    }

    public static void sendRegistrationIdToServer(String registrationId, final Context activity) {
        if (Utilities.isNetworkAvailable(activity)) {
            String encodedString = JsonHeaders.createStoreGcmDetailsApiJSONHeader(registrationId, activity);

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(tag_json_store_gcm_details, response.toString());
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });

            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_store_gcm_details);
        }
    }

    public static void makeGetServicesApi(final Context activity) {
        if (Utilities.isNetworkAvailable(activity)) {

            String encodedString = JsonHeaders.createGetServicesApiJSONHeader(activity);
            Log.d("Get Services", (APIConstant.BASE_URL + encodedString).toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    writeDataToFile("services.json", BaseConstants.sdCardPath, response.toString());
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utilities.hideProgressDialog();
                }
            });

            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_get_services);
        }

    }

    public static void locationChecker(GoogleApiClient mGoogleApiClient, final Activity activity) {
        LocationRequest locationRequest = LocationRequest.create();

        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        locationRequest.setInterval(30 * 1000);

        locationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()

                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =

                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

            @Override

            public void onResult(LocationSettingsResult result) {

                final Status status = result.getStatus();

                final LocationSettingsStates state = result.getLocationSettingsStates();

                switch (status.getStatusCode()) {

                    case LocationSettingsStatusCodes.SUCCESS:

                        // All location settings are satisfied. The client can initialize location

                        // requests here.

                        break;

                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                        // Location settings are not satisfied. But could be fixed by showing the user

                        // a dialog.

                        try {
                            // Show the dialog by calling startResolutionForResult(),

                            // and check the result in onActivityResult().

                            status.startResolutionForResult(activity, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;

                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        // Location settings are not satisfied. However, we have no way to fix the

                        // settings so we won't show the dialog.

                        break;
                }
            }

        });

    }


    public static void writeDataToFile(String fileName, File folderPath, String response) {
        FileOutputStream fileOutputStream = null;

        if (folderPath == null || TextUtils.isEmpty(fileName))
            return;

        try {
            if (folderPath != null && !folderPath.isDirectory())
                folderPath.mkdirs();

            File jsonFilePath = new File(folderPath, fileName);
            if (jsonFilePath != null && !jsonFilePath.exists())
                jsonFilePath.createNewFile();

            byte[] contentInBytes = response.getBytes();

            fileOutputStream = new FileOutputStream(jsonFilePath.getPath());
            fileOutputStream.write(contentInBytes);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String readDataFromFile(String fileName) {
        String bufferData = "";
        try {
            File file = new File(fileName);
            if (!file.exists())
                return bufferData;

            FileInputStream fileInputStream = new FileInputStream(file);
            BufferedReader bufferReader = new BufferedReader(new InputStreamReader(fileInputStream));
            String strData = "";
            while ((strData = bufferReader.readLine()) != null) {
                bufferData += strData + "\n";
            }
            bufferReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bufferData;
    }

    public static float roundoff(float input) {
        try {
            DecimalFormat df = new DecimalFormat("#.##");
            df.setRoundingMode(RoundingMode.CEILING);
            return Float.valueOf(df.format(input));
        } catch (Exception ex) {
            // whatever
        }
        return input;
    }

    public static double roundOff(double input) {
        try {
            DecimalFormat df = new DecimalFormat("#.##");
            df.setRoundingMode(RoundingMode.CEILING);
            return Double.valueOf(df.format(input));
        } catch (Exception ex) {
            // whatever
        }
        return input;
    }


    public static void getTMHDetails(Context ctx) {
        if (Utilities.isNetworkAvailable(ctx)) {
            String encodedString = JsonHeaders.creategetTMHDetailsApiJSONHeader(ctx);

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    // Log.d(tag_json_getTMH_details,response.toString());
                    try {
                        if (response != null && !TextUtils.isEmpty(response.toString())) {
                            LocationInstance.getInstance().setTmhInfo(response);
                        }

                    } catch (Exception e) {
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });

            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_getTMH_details);
        }
    }

    public static boolean setNumberPickerTextColor(NumberPicker numberPicker, int color) {
        final int count = numberPicker.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = numberPicker.getChildAt(i);
            if (child instanceof EditText) {
                try {
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint) selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText) child).setTextColor(color);
                    numberPicker.invalidate();
                    return true;
                } catch (NoSuchFieldException e) {
                    Log.w("setNumberPicker", e);
                } catch (IllegalAccessException e) {
                    Log.w("setNumberPicker", e);
                } catch (IllegalArgumentException e) {
                    Log.w("setNumberPicker", e);
                }
            }
        }
        return false;
    }

    public static void makeGetTokenApi(String amount, String bookingID, final Context mContext) {
        try {
            if (mContext == null)
                return;

            if (Utilities.isNetworkAvailable(mContext)) {
                tokenRequestInitiated = true;
                String encodedString = JsonHeaders.createGetNEWTokenApiJSONHeader(amount, bookingID, mContext);
                Utilities.showProgressDialog(mContext);

                JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                        APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            tokenRequestInitiated = false;
                            Utilities.hideProgressDialog();
                            if (response.has("TOKEN") && !TextUtils.isEmpty(response.getString("TOKEN")) && response.has("REDIRECTURL")
                                    && !TextUtils.isEmpty(response.getString("REDIRECTURL"))) {

                                ((HomeActivity) mContext).pushFragment(new MobitelPaymentFragment(response.getString("TOKEN"), response.getString("REDIRECTURL")), false, true);
                            } else {
                                Utilities.dialogdisplay("Payment Not Processed", "Sorry your payment cannot be processed right now. Please pay the bill amount by Cash.", mContext);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {

                        tokenRequestInitiated = false;

                        if (mContext == null)
                            return;
                        try {
                            Utilities.hideProgressDialog();
                            Utilities.dialogdisplay("Payment Not Processed", "Sorry your payment can not be processed right now.  Please pay the bill amount by Cash.", mContext);

                        } catch (Exception e) {
                        }

                    }
                });
                jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                        0,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(jsonObjReq, tag_req_getToken);
            }
        } catch (Exception e) {
            tokenRequestInitiated = false;
        }
    }

    public static float getApproximateFare(float distanceInKms) {

        float approximateFare = 0.0f;
        String baseKm = "";
        String baseFare = "";
        String farePerKm = "";


        try {
            if (LocationInstance.getInstance().selectedCar == 0 && !TextUtils.isEmpty(LocationInstance.getInstance().miniCab.getBaseFare())) {

                if (isDayOrNightFare(getCurrentTime()) == 0) {
                    baseKm = LocationInstance.getInstance().miniCab.getBaseKm();
                    baseFare = LocationInstance.getInstance().miniCab.getBaseFare();
                    farePerKm = LocationInstance.getInstance().miniCab.getFarePerKm();
                } else {
                    baseKm = LocationInstance.getInstance().miniCab.getNightBaseKm();
                    baseFare = LocationInstance.getInstance().miniCab.getNightBaseFare();
                    farePerKm = LocationInstance.getInstance().miniCab.getNightFarePerKm();
                }
            } else if (LocationInstance.getInstance().selectedCar == 1) {
                if (isDayOrNightFare(getCurrentTime()) == 0) {
                    baseKm = LocationInstance.getInstance().sedanCab.getBaseKm();
                    baseFare = LocationInstance.getInstance().sedanCab.getBaseFare();
                    farePerKm = LocationInstance.getInstance().sedanCab.getFarePerKm();
                } else {
                    baseKm = LocationInstance.getInstance().sedanCab.getNightBaseKm();
                    baseFare = LocationInstance.getInstance().sedanCab.getNightBaseFare();
                    farePerKm = LocationInstance.getInstance().sedanCab.getNightFarePerKm();
                }
            } else if (LocationInstance.getInstance().selectedCar == 2) {
                if (isDayOrNightFare(getCurrentTime()) == 0) {
                    baseKm = LocationInstance.getInstance().primeCab.getBaseKm();
                    baseFare = LocationInstance.getInstance().primeCab.getBaseFare();
                    farePerKm = LocationInstance.getInstance().primeCab.getFarePerKm();
                } else {
                    baseKm = LocationInstance.getInstance().primeCab.getNightBaseKm();
                    baseFare = LocationInstance.getInstance().primeCab.getNightBaseFare();
                    farePerKm = LocationInstance.getInstance().primeCab.getNightFarePerKm();
                }
            } else if (LocationInstance.getInstance().selectedCar == 3) {
                if (isDayOrNightFare(getCurrentTime()) == 0) {
                    baseKm = LocationInstance.getInstance().tuktuk.getBaseKm();
                    baseFare = LocationInstance.getInstance().tuktuk.getBaseFare();
                    farePerKm = LocationInstance.getInstance().tuktuk.getFarePerKm();
                } else {
                    baseKm = LocationInstance.getInstance().tuktuk.getNightBaseKm();
                    baseFare = LocationInstance.getInstance().tuktuk.getNightBaseFare();
                    farePerKm = LocationInstance.getInstance().tuktuk.getNightFarePerKm();

                }
            }
            float extraKms = 0.0f;
            try {
                if (Float.valueOf(baseKm) < distanceInKms) {
                    extraKms = distanceInKms - Float.valueOf(baseKm);
                }
            } catch (Exception e) {
            }

            approximateFare = roundoff((Float.valueOf(baseFare) + (extraKms * Float.valueOf(farePerKm))));

        } catch (Exception e) {

        }
        return approximateFare;
    }

    private static int isDayOrNightFare(String dateTime) {
        long from = 500;
        long to = 2200;
        long t = 0;
        DateFormat df = new SimpleDateFormat("hh:mm a");
        //Date/time pattern of desired output date
        DateFormat outputformat = new SimpleDateFormat("HHmm");
        Date date = null;
        String output = null;
        try {
            //Conversion of input String to date
            date = df.parse(dateTime);
            //old date format to new date format
            output = outputformat.format(date);
            t = Long.valueOf(output);
            Long.toString(t).length();
        } catch (ParseException pe) {
            pe.printStackTrace();
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (to > from && t >= from && t <= to || to < from && (t >= from || t <= to)) {
            return 0; // day Trip
        } else {
            return 1; // night Trip
        }
    }


    @SuppressLint("SimpleDateFormat")
    public static String parseDateFormat(String inputDate) throws java.text.ParseException {
        String inputPattern = "hh:mm a |  c, dd LLL yyyy";
        String outputPattern = "M-dd-yyyy hh:mma";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(inputDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str.replace(".", "");
    }

    public class SpecialCharFilter implements InputFilter {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            String specialChars = "/*!@#$%^&*()\"{}_[]|\\?/<>,.:-'';§£¥...";
            for (int i = start; i < end; i++) {
                int type = Character.getType(source.charAt(i));
                if (type == Character.SURROGATE || type == Character.OTHER_SYMBOL ||type == Character.MATH_SYMBOL || specialChars.contains("" + source)||  Character.isWhitespace(0)) {
                    return "";
                }
            }
            return null;
        }
    }

}
