package com.vowcabs.passengerapp.utils;

import android.location.Location;


public class DistanceCalculator 
{
	public long distance;

	private static DistanceCalculator instance = null;

	private DistanceCalculator()
	{ 

	}

	public static DistanceCalculator getInstance() 
	{
		if(instance == null)
		{
			instance = new DistanceCalculator();
		}
		return instance;
	}

	public long calculateDistance(double latA, double lngA, double latB, double lngB) 
	{
		Location locationA = new Location("point A");     
		locationA.setLatitude(latA); 
		locationA.setLongitude(lngA);
		Location locationB = new Location("point B");
		locationB.setLatitude(latB); 
		locationB.setLongitude(lngB);
		long distance = (long) locationA.distanceTo(locationB) ;
		return distance;
	}

}
