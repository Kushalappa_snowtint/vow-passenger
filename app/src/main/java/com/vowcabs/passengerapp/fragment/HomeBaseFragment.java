package com.vowcabs.passengerapp.fragment;

import android.app.Activity;
import android.os.Bundle;

import com.vowcabs.passengerapp.HomeActivity;


public class HomeBaseFragment extends BaseFragment
{
	public Activity mActivity;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		if (this.getActivity() instanceof HomeActivity)
			mActivity = (HomeActivity) this.getActivity();
	}

	public boolean onBackPressed()
	{
		return false;
	}

}
