package com.vowcabs.passengerapp.api;

public final class APIConstant
{

	//=================================================================================================

	/** BASE URL */
//
//	public static final String BASE_URL 	             = "http://52.5.202.80:122/CDTransReceiver.aspx?INPUT=";
//	public static final String PROFILEPIC_URL 	         = "http://52.5.202.80:122/Files/PassengerPic/";           // live server
//
//	public static final String BASE_URL 	             = "http://54.172.200.128:122/CDTransReceiver.aspx?INPUT=";   // test server
//	public static final String PROFILEPIC_URL 	         = "http://54.172.200.128:122/Files/PassengerPic/";

//	public static final String BASE_URL 	             = "http://52.76.138.151:1220/CDTransReceiver.aspx?INPUT=";   // test server
//	public static final String PROFILEPIC_URL 	         = "http://52.76.138.151:1220/Files/PassengerPic/";

//	public static final String BASE_URL 	             = "http://passenger.vowcabs.com:1220/CDTransReceiver.aspx?INPUT=";   // test server
//	public static final String PROFILEPIC_URL 	         = "http://passenger.vowcabs.com:1220/Files/PassengerPic/";


    public static final String BASE_URL = "http://passenger.vowcabs.com:1220/CDTransReceiver.aspx?INPUT=";   // live server
    public static final String PROFILEPIC_URL 	         = "https://vowpassimage.s3.amazonaws.com/";

//    public static final String BASE_URL 	             = "http://test.passenger.vowcabs.com:1220/CDTransReceiver.aspx?INPUT=";   // test server
//    public static final String PROFILEPIC_URL 	         = "https://vowpassimage.s3.amazonaws.com/";

	public static final String MOBITEL_PAYMENT_URL       = "https://www.mcash.lk/ipg/payment.html?t_id=";
    public static final String SAMPATHBANK_PAYMENT_URL   = "http://www.payments.vowcabs.com:1250/payment_init?amount=";


}
