package com.vowcabs.passengerapp.fragment;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.utils.Utilities;


@SuppressLint("SetJavaScriptEnabled")
public class WebUrlViewFragment extends BaseFragment {
    private WebView mWebView;
    String webUrl;
    int SAMPATHBANKPAYMENTSUCESS = 0;
    int SAMPATHBANKPAYMENTFAILED = 1;
    int MOBITELPAYMNETURLSUCESS = 2;
    int MOBITELPAYMNETFAILED = 3;

    public WebUrlViewFragment(String url) {
        webUrl = url;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.webview_fragment, container,
                false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mWebView = (WebView) getView().findViewById(R.id.webUrlView);

        if (!TextUtils.isEmpty(webUrl)) {
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.loadUrl(webUrl);
            Utilities.showProgressDialog(getActivity());

            mWebView.setWebViewClient(new WebViewClient() {

                public void onPageFinished(WebView view, String url) {
                    //if (mWebView.getProgress() == 100) {
                        //Log.e("onPageFinished ", url);

                        if (url.contains("http://api.vowcabs.com/PaymentService/Ipg/Complete.aspx?")) {

                            //dialogdisplay("Success", "Payment Sucees", getActivity(), 2);

                            if (url.contains("Message=1000")) {

                                dialogdisplay("Payment Success", "Your payment is successful.", getActivity(), MOBITELPAYMNETURLSUCESS);
                            } else  {

                                dialogdisplay("Payment Failed", "Some thing went, your payment could not be processed.", getActivity(), MOBITELPAYMNETFAILED);
                            }
                        }
                        if (url.contains("http://www.payments.vowcabs.com:1250/paymentOver?")) {
                            if (url.contains("code=0") && url.contains("message=")) {

                                dialogdisplay("Payment Success", url.substring(url.indexOf("message=") + 8, url.length()).trim(), getActivity(), SAMPATHBANKPAYMENTSUCESS);
                            } else if (url.contains("code=1") && url.contains("message=")) {

                                dialogdisplay("Payment Failed", url.substring(url.indexOf("message=") + 8, url.length()).trim(), getActivity(), SAMPATHBANKPAYMENTFAILED);
                            }
                            //Log.e("PROCESSED: ", url);
                        } else if (url.equalsIgnoreCase("http://www.api.vowcabs.com/paymentservice/Ipg/Cancel.aspx")) {
                            Utilities.dialogdisplay("Cancelled", "Payment Cancelled.", getActivity());
                        }
                        Utilities.hideProgressDialog();
                    //}
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView webView, String urlNewString) {
                    webView.loadUrl(urlNewString);
                    Log.d("OverrideUrlLoading ", urlNewString);
                    return true;
                }

                @SuppressWarnings("deprecation")
                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    // Handle the error
                }

                @TargetApi(android.os.Build.VERSION_CODES.M)
                @Override
                public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                    // Redirect to deprecated method, so you can use it in all SDK versions
                    onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
                }

            });
        }
    }


    Dialog customDialog = null;

    public void dialogdisplay(String tilte, String Message, Context context, final int id) {
        if (context == null)
            return;

        if (customDialog != null) {
            if (customDialog.isShowing()) {
                customDialog.dismiss();
            }
            customDialog = null;
        }
        customDialog = new Dialog(context);
        View dialogView = customDialog.getLayoutInflater().inflate(R.layout.dialog_view, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        TextView message = (TextView) dialogView.findViewById(R.id.tv_DialogMessage);
        title.setText(tilte);
        message.setText(Message);
        Button yes = (Button) dialogView.findViewById(R.id.btn_yes);
        Button no = (Button) dialogView.findViewById(R.id.btn_no);
        yes.setText("Ok");
        no.setVisibility(View.GONE);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();

                if ((id == 0)) {
                    ((HomeActivity) getActivity()).pushFragment(new VowCashFragment(), false, false);
                } else if (id == 2) {
                    ((HomeActivity) getActivity()).pushFragment(new MyTripsFragment(), false, false);
                }

            }
        });
        customDialog.show();

    }

}
