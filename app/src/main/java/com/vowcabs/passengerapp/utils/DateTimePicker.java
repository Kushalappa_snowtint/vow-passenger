package com.vowcabs.passengerapp.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ParseException;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.NumberPicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.vowcabs.passengerapp.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateTimePicker extends DialogFragment {
    public static final String TAG_FRAG_DATE_TIME = "fragDateTime";
    private static final String KEY_DIALOG_TITLE = "dialogTitle";
    private static final String KEY_INIT_DATE = "initDate";
    private static final String TAG_DATE = "date";
    private static final String TAG_TIME = "time";
    private Context mContext;
    private ButtonClickListener mButtonClickListener;
    private OnDateTimeSetListener mOnDateTimeSetListener;
    private Bundle mArgument;
    private DatePicker mDatePicker;
    private TimePicker mTimePicker;
    private int TIME_PICKER_INTERVAL = 1;
    NumberPicker minutePicker;
    List<String> displayedValues;
    private static boolean isOutstationBooking = false;

    // DialogFragment constructor must be empty
//	public DateTimePicker()
//	{
//
//	}
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mButtonClickListener = new ButtonClickListener();
    }

    /**
     * @param dialogTitle Title of the DateTimePicker DialogFragment
     * @param initDate    Initial Date and Time set to the Date and Time Picker
     * @return Instance of the DateTimePicker DialogFragment
     */
    public static DateTimePicker newInstance(CharSequence dialogTitle, Date initDate, boolean isOutstation) {
        // Create a new instance of DateTimePicker
        DateTimePicker mDateTimePicker = new DateTimePicker();
        // Setup the constructor parameters as arguments
        Bundle mBundle = new Bundle();
        mBundle.putCharSequence(KEY_DIALOG_TITLE, dialogTitle);
        mBundle.putSerializable(KEY_INIT_DATE, initDate);
        mDateTimePicker.setArguments(mBundle);
        isOutstationBooking = isOutstation;
        // Return instance with arguments

        return mDateTimePicker;
    }

    AlertDialog mDialog = null;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Retrieve Argument passed to the constructor
        mArgument = getArguments();
        // Use an AlertDialog Builder to initially create the Dialog
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext, AlertDialog.THEME_HOLO_DARK);
        // Setup the Dialog
        mBuilder.setTitle(mArgument.getCharSequence(KEY_DIALOG_TITLE));
        mBuilder.setNegativeButton(android.R.string.no, mButtonClickListener);
        mBuilder.setPositiveButton(android.R.string.yes, mButtonClickListener);
        // Create the Alert Dialog
        mDialog = mBuilder.create();
        // Set the View to the Dialog
        mDialog.setView(createDateTimeView(mDialog.getLayoutInflater()));
        // Return the Dialog created

        mDialog.setCancelable(false);
        return mDialog;
    }


    private void showAlertDialog() {
        // Use an AlertDialog Builder to initially create the Dialog
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext, AlertDialog.THEME_HOLO_DARK);

        // Setup the Dialog
        mBuilder.setTitle("Choose pickup Time");
        mBuilder.setNegativeButton(android.R.string.no, mButtonClickListener);
        mBuilder.setPositiveButton(android.R.string.yes, mButtonClickListener);
        // Create the Alert Dialog
        if (mDialog != null)
            mDialog.cancel();

        mDialog = mBuilder.create();
        // Set the View to the Dialog
        mDialog.setView(createDateTimeView(mDialog.getLayoutInflater()));

        mDialog.show();
        // Return the Dialog created
    }

    /**
     * Inflates the XML Layout and setups the tabs
     *
     * @param layoutInflater Layout inflater from the Dialog
     * @return Returns a view that will be set to the Dialog
     */
    private View createDateTimeView(LayoutInflater layoutInflater) {
        // Inflate the XML Layout using the inflater from the created Dialog
        View mView = layoutInflater.inflate(R.layout.date_time_picker, null);
        // Extract the TabHost
        //		TabHost mTabHost = (TabHost) mView.findViewById(R.id.tab_host);
        //		mTabHost.setup();
        //		// Create Date Tab and add to TabHost
        //		TabHost.TabSpec mDateTab = mTabHost.newTabSpec(TAG_DATE);
        //		mDateTab.setIndicator("Date");
        //		mDateTab.setContent(R.id.date_content);
        //		mTabHost.addTab(mDateTab);
        //		// Create Time Tab and add to TabHost
        //		TabHost.TabSpec mTimeTab = mTabHost.newTabSpec(TAG_TIME);
        //		mTimeTab.setIndicator("Time");
        //		mTimeTab.setContent(R.id.time_content);
        //		mTabHost.addTab(mTimeTab);
        // Retrieve Date from Arguments sent to the Dialog
        DateTime mDateTime = new DateTime((Date) mArgument.getSerializable(KEY_INIT_DATE));
        // Initialize Date and Time Pickers
        mDatePicker = (DatePicker) mView.findViewById(R.id.date_picker);
        mTimePicker = (TimePicker) mView.findViewById(R.id.time_picker);
        mTimePicker.setIs24HourView(true);
        mDatePicker.init(mDateTime.getYear(), mDateTime.getMonthOfYear(),
                mDateTime.getDayOfMonth(), null);
        mDatePicker.setMinDate(System.currentTimeMillis() - 1000);
        if (isOutstationBooking) {
            mTimePicker.setCurrentHour(mDateTime.getHourOfDay() + 1);
        } else {
            mTimePicker.setCurrentHour(mDateTime.getHourOfDay());
        }
        mTimePicker.setCurrentMinute(mDateTime.getMinuteOfHour() + 61);

        //setTimePickerInterval(mTimePicker);
        // Return created view
        return mView;
    }

    /**
     * Sets the OnDateTimeSetListener interface
     *
     * @param onDateTimeSetListener Interface that is used to send the Date and Time
     *                              to the calling object
     */
    public void setOnDateTimeSetListener(OnDateTimeSetListener onDateTimeSetListener) {
        mOnDateTimeSetListener = onDateTimeSetListener;
    }

    private class ButtonClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialogInterface, int result) {
            // Determine if the user selected Ok
            if (DialogInterface.BUTTON_POSITIVE == result) {
                DateTime mDateTime = new DateTime(
                        mDatePicker.getYear(),
                        mDatePicker.getMonth(),
                        mDatePicker.getDayOfMonth(),
                        mTimePicker.getCurrentHour(),
                        mTimePicker.getCurrentMinute()
                );
                try {
                    if (validateBookingDateAndTime(parseDateFormat(mDateTime.getDateString()))) {
                        mOnDateTimeSetListener.DateTimeSet(mDateTime.getDate());
                    } else {
                        showAlertDialog();
                    }
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }

            } else {
                Log.e("Error", "");
            }
        }
    }

    private int getMinute(int minute) {
        switch (minute) {
            case 0:
            case 4:
                return 00;

            case 1:
            case 5:
                return 15;

            case 2:
            case 6:
                return 30;

            case 3:
            case 7:
                return 45;
        }

        return minute;
    }

    /**
     * Interface for sending the Date and Time to the calling object
     */
    public interface OnDateTimeSetListener {
        public void DateTimeSet(Date date);
    }

    private void setTimePickerInterval(TimePicker timePicker) {
        try {
            Class<?> classForid = Class.forName("com.android.internal.R$id");
            // Field timePickerField = classForid.getField("timePicker");

            java.lang.reflect.Field field = classForid.getField("minute");
            minutePicker = (NumberPicker) timePicker
                    .findViewById(field.getInt(null));

            String[] minute = new String[8];

            //minutePicker.setMinValue(0);
            //minutePicker.setMaxValue(7);
            displayedValues = new ArrayList<String>();
            int count = 0;
            for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                displayedValues.add(String.format("%02d", i));
                minute[count] = String.format("%02d", i);
                count++;
            }
            for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                displayedValues.add(String.format("%02d", i));
                minute[count] = String.format("%02d", i);
                count++;
            }

            minutePicker.setDisplayedValues(minute);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SimpleDateFormat")
    public boolean validateBookingDateAndTime(String bookingDate) {
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        String currentDate = df.format(Calendar.getInstance().getTime());
        //String dateStart = "01/14/2012 09:29:58";
        //String dateStop = "01/15/2012 10:31:48";

        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(currentDate);
            d2 = format.parse(bookingDate);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);


            if (d2.getTime() < d1.getTime()) {
                Toast.makeText(mContext, "Booking time should be greater than current time", Toast.LENGTH_SHORT).show();
            } else if (d2.getTime() > d1.getTime() && diffDays >= 3 && diffSeconds > 0) {
                showAlertDialog();
                Toast.makeText(mContext, "Bookings are allowed upto 24 hours", Toast.LENGTH_SHORT).show();
            } else if (d2.getTime() > d1.getTime() && diffHours < 2 && isOutstationBooking)
            {
                showAlertDialog();
                Toast.makeText(mContext, "Booking time should be 2 Hours prior to journey time", Toast.LENGTH_SHORT).show();
            } else if (d2.getTime() > d1.getTime() && diffHours < 1 && !isOutstationBooking)
            {
                showAlertDialog();
                Toast.makeText(mContext, "Booking time should be 1 hour prior to journey time", Toast.LENGTH_SHORT).show();
            }
//            else if(d2.getTime() > d1.getTime() && diff > 60*60*1000)
//            {
//                Toast.makeText(mContext, "Booking time should be 1 Hours prior to journey time", Toast.LENGTH_SHORT).show();
//            }

            else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @SuppressLint("SimpleDateFormat")
    public static String parseDateFormat(String inputDate) throws java.text.ParseException {
        String inputPattern = "hh:mm a |  c, dd LLL yyyy";
        String outputPattern = "MM/dd/yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(inputDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
