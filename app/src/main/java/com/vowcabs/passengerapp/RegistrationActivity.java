package com.vowcabs.passengerapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.ErrorReporter;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

public class RegistrationActivity extends Activity implements OnClickListener {
    private String tag_register_obj = "registration_req";
    String encodedString;
    EditText emaildid;
    EditText password;
    EditText confirmPassword;
    EditText username;
    EditText phonenumber;
    EditText inviteCode;
    TextView processes;
    TextView applyCouponCode;
    RadioButton rbSrilanka, rbIndia;
    RelativeLayout processesLayout;
    String deviceId;
    LinearLayout llTerms;
    boolean registerApiCalled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signupscreen);

        try {
            ErrorReporter errReporter = new ErrorReporter();
            errReporter.Init(this);
            errReporter.CheckErrorAndSendMail(this);
        } catch (Exception e) {
        }

        InitializeUI();
    }

    public void InitializeUI() {
        emaildid = (EditText) findViewById(R.id.emailid);
        password = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);
        username = (EditText) findViewById(R.id.user_name);
        phonenumber = (EditText) findViewById(R.id.phone_number);
        inviteCode = (EditText) findViewById(R.id.inviteCode);
        processes = (TextView) findViewById(R.id.processes_button);
        applyCouponCode = (TextView) findViewById(R.id.tv_applyCouponCode);
        processesLayout = (RelativeLayout) findViewById(R.id.processes_layout);
        rbSrilanka = (RadioButton) findViewById(R.id.rb_srilanka);
        rbIndia = (RadioButton) findViewById(R.id.rb_india);
        llTerms = (LinearLayout) findViewById(R.id.ll_terms);

        try {
            password.setTransformationMethod(new PasswordTransformationMethod());
            confirmPassword.setTransformationMethod(new PasswordTransformationMethod());
            deviceId = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
        } catch (Exception e) {
        }

        processesLayout.setOnClickListener(this);
        applyCouponCode.setOnClickListener(this);
        llTerms.setOnClickListener(this);
    }


    private void makeRegistrationApiCall(boolean isFromIndia) {
        if (Utilities.isNetworkAvailable(this)) {
            Utilities.showProgressDialog(this);

            registerApiCalled = true;
            encodedString = JsonHeaders.createRegistrationApiJSONHeader(username.getText().toString(), password.getText().toString(), phonenumber.getText().toString(), deviceId, emaildid.getText().toString(), inviteCode.getText().toString(), isFromIndia, this);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET, APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    registerApiCalled = false;
                    Utilities.hideProgressDialog();
                    //Log.d("Success ", response.toString());
                    try {
                        Utilities.putSharedString("EMAILID", emaildid.getText().toString(), RegistrationActivity.this);
                        Utilities.putSharedString("USERNAME", username.getText().toString(), RegistrationActivity.this);
                        Utilities.putSharedString("MOBILE", phonenumber.getText().toString(), RegistrationActivity.this);

                        if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("000")) {
                            Intent intent = new Intent(RegistrationActivity.this, OTPValidationActivity.class);
                            intent.putExtra("PhoneNumber", phonenumber.getText().toString());
                            startActivity(intent);
                            finish();

                        } else if (!response.isNull("RESPONSECODE") &&  response.getString("RESPONSECODE").equals("101")) {
                            Utilities.dialogdisplay("Account Already Exist", "Please use  Forget Password to generate new Password or Register with a different Mobile Number.", RegistrationActivity.this, true);
                        } else if (!response.isNull("RESPONSECODE") &&  response.getString("RESPONSECODE").equals("102")) {
                            dialogdisplay("Account Already Exist", "Please validate your mobile number to complete the registration process.");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        registerApiCalled = false;
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    registerApiCalled = false;
                    Utilities.hideProgressDialog();
                    Utilities.dialogdisplay("Network Error", "Please Try Again", RegistrationActivity.this);
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                }
            });
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_register_obj);
        }
    }


    @Override
    public void onClick(View v) {
        if (v == processesLayout) {
            boolean isValide = isValidDetails();
            if (isValide && !registerApiCalled)
                makeRegistrationApiCall(rbIndia.isChecked());

        }
        if (v == applyCouponCode) {
            if (!TextUtils.isEmpty(inviteCode.getText().toString())) {
                validateCouponCode();
            } else {
                Utilities.dialogdisplay("Invalid Promo Code", "Please enter the correct promo code", this);
            }
        }
        if (v == llTerms) {
            String url;
            if (rbSrilanka.isChecked()) {
                url = "http://lk.vowcabs.com/terms.html";
            } else {
                url = "http://vowcabs.com/terms.html";
            }
            Intent intent = new Intent(RegistrationActivity.this, WebViewActivity.class);
            intent.putExtra("URL", url);
            startActivity(intent);
        }
    }

    private void validateCouponCode() {
        if (Utilities.isNetworkAvailable(this)) {
            Utilities.showProgressDialog(RegistrationActivity.this);
            encodedString = JsonHeaders.createCheckPromoCodeApiJSONHeader(inviteCode.getText().toString(), RegistrationActivity.this);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Utilities.hideProgressDialog();
                    //Log.d("Success", response.toString());
                    try {
                        if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("000")) {
                            Utilities.dialogdisplay("Sucess", "Promo code has been applied.", RegistrationActivity.this);
                        } else {
                            Utilities.dialogdisplay("Invalid", "Please check the promocode and try again.", RegistrationActivity.this);
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utilities.hideProgressDialog();
                    Utilities.dialogdisplay("Network Error", "Please Try Again", RegistrationActivity.this);
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());

                }
            });
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(jsonObjReq, "tag_checkpromocode_obj");
        }
    }


    boolean isValidDetails() {
        boolean isValid = false;
        String email = emaildid.getText().toString().trim();
        String passwordText = password.getText().toString();
        String confirmPasswordText = confirmPassword.getText().toString();
        String phoneText = phonenumber.getText().toString().trim();

        String phoneNumberPattern = "^[0-9]*$";
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        boolean isValidEmail = false;
        boolean isValidPassword = false;
        boolean isValidConfirmPassword = false;
        String message = "";
        boolean isValidName = false;
        boolean isValidPhone = false;

        if (phoneText.matches(phoneNumberPattern) && (phoneText.length() == 10 || phoneText.length() == 9)) {
            isValidPhone = true;
        } else {
            message = "Please enter a valid Mobile Number\n";
        }
        if (email.matches(emailPattern)) {
            isValidEmail = true;
        } else {
            message = "Enter Valid Email\n";
        }
        if (!(username.getText().toString().length() == 0) && username.getText().toString() != null) {
            isValidName = true;
        } else {
            message = message + "Name field is Empty\n";

        }
        if (passwordText.length() != 0 && passwordText != null) {
            isValidPassword = true;
        } else {
            message = message + "Password field is Empty\n";
        }
        if (confirmPasswordText.length() != 0 && confirmPasswordText != null) {
            isValidConfirmPassword = true;
        } else {
            message = message + "Confirm password field is Empty\n";

        }
        if (passwordText.compareTo(confirmPasswordText) != 0) {
            isValidConfirmPassword = false;
            message = message + "Password and Confirm Password do not match. Please Try Again.\n";

        }

        if (isValidPassword && isValidEmail && isValidName && isValidConfirmPassword && isValidPhone) {
            isValid = true;
        } else {
            Utilities.dialogdisplay("Invalid Input", message, RegistrationActivity.this);
        }

        return isValid;
    }

    public void dialogdisplay(String tilte, String Message) {
        try {
            final Dialog customDialog = new Dialog(this);
            View dialogView = getLayoutInflater().inflate(R.layout.dialog_view, null);
            customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            customDialog.setContentView(dialogView);
            customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
            TextView message = (TextView) dialogView.findViewById(R.id.tv_DialogMessage);
            title.setText(tilte);
            message.setText(Message);
            Button yes = (Button) dialogView.findViewById(R.id.btn_yes);
            Button no = (Button) dialogView.findViewById(R.id.btn_no);
            yes.setText("Ok");
            no.setVisibility(View.GONE);
            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(RegistrationActivity.this, OTPValidationActivity.class);
                    intent.putExtra("PhoneNumber", phonenumber.getText().toString());
                    startActivity(intent);
                    customDialog.dismiss();
                }
            });
            customDialog.show();
        } catch (Exception e) {
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            Utilities.dismissProgressDialog();
        } catch (Exception e) {
        }
    }
}
