package com.vowcabs.passengerapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.Models.VowTransaction;
import com.vowcabs.passengerapp.adapter.TransactionAdapter;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.DividerItemDecoration;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VowCashTransactionsActivity extends Activity {

    private List<VowTransaction> mtransList = new ArrayList<>();
    private RecyclerView mrecyclerView;
    private TransactionAdapter mtransactionAdapter;
    private final String tag_vow_transactions = "json_vow_transactions";
    private ImageView backArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vowcash);
        mrecyclerView =(RecyclerView) findViewById(R.id.recycler_view);
        backArrow = (ImageView)findViewById(R.id.iv_back_arrow);
        mtransactionAdapter = new TransactionAdapter(mtransList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mrecyclerView.setLayoutManager(mLayoutManager);
        mrecyclerView.setItemAnimator(new DefaultItemAnimator());
        mrecyclerView.addItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.VERTICAL));
        mrecyclerView.setAdapter(mtransactionAdapter);
        makeTrackUserApiCall();
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void prepareTransactionData(JSONObject response)
    {
        if (!response.isNull("CUSTOMERACCOUNTS"))
        {
            try {
                JSONArray customerAccounts = response.getJSONArray("CUSTOMERACCOUNTS");

                if (customerAccounts.length() ==0)
                {
                    Utilities.dialogdisplay("No Transcations","Currently there are no vow transactions.", VowCashTransactionsActivity.this);
                }

                for (int i = 0; i < customerAccounts.length(); i++) {
                    JSONObject transactionObj = customerAccounts.getJSONObject(i);
                    VowTransaction vowTransaction = new VowTransaction();
                    vowTransaction.setData(transactionObj);
                    mtransList.add(vowTransaction);
                }
            }catch (JSONException e){}
        }
        mtransactionAdapter.notifyDataSetChanged();

    }

    private void makeTrackUserApiCall()
    {
        try {
            if (Utilities.isNetworkAvailable(this)) {
                String encodedString = JsonHeaders.creategetCustomerAccountApiJSONHeader(this);

                Utilities.showProgressDialog(this);

                JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                        APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        prepareTransactionData(response);
                        // Log.d("Success", response.toString());
                        Utilities.hideProgressDialog();
                    }

                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utilities.hideProgressDialog();
                        VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                        Utilities.dialogdisplay("Network Error", "Please Try Again", VowCashTransactionsActivity.this);

                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjReq, tag_vow_transactions);
            }
        }catch (Exception e){}

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utilities.dismissProgressDialog();
    }

}
