package com.vowcabs.passengerapp.json;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;

import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.LoginActivity;
import com.vowcabs.passengerapp.OTPValidationActivity;
import com.vowcabs.passengerapp.encryption.URLUTF8Encoder;
import com.vowcabs.passengerapp.utils.DistanceCalculator;
import com.vowcabs.passengerapp.utils.LocationInstance;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonHeaders {
    public static String createLoginApiJSONHeader(String deviceId, String emailorphone, String password, Context context) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        JSONObject deviceinfo = new JSONObject();
        JSONObject customer = new JSONObject();
        try {
            json.put(JSONKEYS.OPCODE, "LOGINUSER");
            deviceinfo.put(JSONKEYS.DEVICETYPE, "ANDROID");
            deviceinfo.put(JSONKEYS.DEVICEID, deviceId);
            if (Utilities.getSharedString("FCM_TOKEN", context) != null && !Utilities.getSharedString("FCM_TOKEN", context).trim().isEmpty()) {
                deviceinfo.put(JSONKEYS.PUSHTOKEN, Utilities.getSharedString("FCM_TOKEN", context));
            } else {
                deviceinfo.put(JSONKEYS.PUSHTOKEN, "");
            }

            if (isValidDetailEmail(emailorphone)) {
                customer.put(JSONKEYS.EMAIL, emailorphone);
            } else
                customer.put(JSONKEYS.PHONE, emailorphone);

            customer.put(JSONKEYS.PASSWORD, Utilities.getMD5(password));
            json.put(JSONKEYS.CUSTOMER, customer);
            json.put(JSONKEYS.DEVICEINFO, deviceinfo);
            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);
            Log.d("Login", "info : "+json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return encodedString;
    }

    static boolean isValidDetailEmail(String emailorphone) {
        boolean isValid = false;
        String email = emailorphone.trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        boolean isValidEmail = false;
        if (email.matches(emailPattern)) {
            isValidEmail = true;
        }
        if (isValidEmail) {
            isValid = true;
        }

        return isValid;
    }

    public static String createValidateOtpApiJSONHeader(String inputCode, String phoneNumber, OTPValidationActivity otpValidationActivity) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        JSONObject deviceinfo = new JSONObject();
        JSONObject customer = new JSONObject();
        try {
            json.put(JSONKEYS.OPCODE, "VALIDATEOTP");
            deviceinfo.put(JSONKEYS.DEVICETYPE, "ANDROID");
            deviceinfo.put(JSONKEYS.DEVICEID, Secure.getString(otpValidationActivity.getContentResolver(), Secure.ANDROID_ID));
            if (!TextUtils.isEmpty(phoneNumber)) {
                customer.put(JSONKEYS.PHONE, (phoneNumber));
            }
            if (!TextUtils.isEmpty(inputCode)) {
                customer.put(JSONKEYS.OTP, inputCode);
            }
            json.put(JSONKEYS.CUSTOMER, customer);
            json.put(JSONKEYS.DEVICEINFO, deviceinfo);
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);
        } catch (Exception e) {

        }
        return encodedString;

    }

    public static String createGetNearByDriverApiJSONHeader(Context ctx) {
        String encodedString = "";

        JSONObject reqObject = new JSONObject();
        JSONObject picupLocObject = new JSONObject();
        try {
            reqObject.put("OPCODE", "GETNEARBYDRIVER");
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", ctx))) {
                reqObject.put(JSONKEYS.SESSIONID, (Utilities.getSharedString("SESSIONID", ctx)));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", ctx))) {
                reqObject.put(JSONKEYS.CUSTOMERID, (Utilities.getSharedString("CUSTOMERID", ctx)));
            }
            picupLocObject.put("LAT", LocationInstance.getInstance().getLatitude());
            picupLocObject.put("LONG", LocationInstance.getInstance().getLongitute());
            reqObject.put("PICKUPLOCATION", picupLocObject);
            reqObject.put("STATUS", "0");

            String encrytedString = Utilities.getEncryptedString(reqObject.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);
        } catch (JSONException e) {
            e.printStackTrace();

        }
        return encodedString;
    }

    public static String createGetServicesApiJSONHeader(Context ctx) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        JSONObject deviceinfo = new JSONObject();
        JSONObject customer = new JSONObject();
        try {
            json.put(JSONKEYS.OPCODE, "GETSERVICES");
            deviceinfo.put(JSONKEYS.DEVICETYPE, "ANDROID");
            deviceinfo.put(JSONKEYS.DEVICEID, Secure.getString(ctx.getContentResolver(), Secure.ANDROID_ID));
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", ctx))) {
                customer.put(JSONKEYS.SESSIONSTRING, (Utilities.getSharedString("SESSIONID", ctx)));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", ctx))) {
                customer.put(JSONKEYS.USERID, (Utilities.getSharedString("CUSTOMERID", ctx)));
            }
            json.put(JSONKEYS.CUSTOMER, customer);
            json.put(JSONKEYS.DEVICEINFO, deviceinfo);
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);
        } catch (Exception e) {
        }
        return encodedString;

    }

    public static String createStoreGcmDetailsApiJSONHeader(String regId, Context ctx) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        JSONObject deviceinfo = new JSONObject();
        JSONObject customer = new JSONObject();
        try {
            Log.d("GCM", "id " + regId);
            json.put(JSONKEYS.OPCODE, "STOREGCMDETAILS");
            json.put(JSONKEYS.GCMREGID, regId);
            deviceinfo.put(JSONKEYS.DEVICETYPE, "ANDROID");
            deviceinfo.put(JSONKEYS.DEVICEID, Secure.getString(ctx.getContentResolver(), Secure.ANDROID_ID));
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", ctx))) {
                customer.put(JSONKEYS.SESSIONSTRING, (Utilities.getSharedString("SESSIONID", ctx)));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", ctx))) {
                customer.put(JSONKEYS.USERID, (Utilities.getSharedString("CUSTOMERID", ctx)));
            }
            json.put(JSONKEYS.CUSTOMER, customer);
            json.put(JSONKEYS.DEVICEINFO, deviceinfo);
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);
        } catch (Exception e) {

        }
        return encodedString;
    }

    public static String createResendOTPApiJSONHeader(String string, Activity otpValidationActivity) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        JSONObject deviceinfo = new JSONObject();
        JSONObject customer = new JSONObject();
        try {
            json.put(JSONKEYS.OPCODE, "RESENDOTP");
            deviceinfo.put(JSONKEYS.DEVICETYPE, "ANDROID");
            deviceinfo.put(JSONKEYS.DEVICEID, Secure.getString(otpValidationActivity.getContentResolver(), Secure.ANDROID_ID));

            if (!TextUtils.isEmpty(Utilities.getSharedString("MOBILE", otpValidationActivity))) {
                customer.put(JSONKEYS.PHONE, (Utilities.getSharedString("MOBILE", otpValidationActivity)));
            }
            json.put(JSONKEYS.CUSTOMER, customer);
            json.put(JSONKEYS.DEVICEINFO, deviceinfo);
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);
        } catch (Exception e) {

        }
        return encodedString;
    }

    public static String createCheckPromoCodeApiJSONHeader(String invitecode, Activity otpValidationActivity) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        try {
            json.put(JSONKEYS.OPCODE, "CHECKPROMOCODE");
            json.put(JSONKEYS.INVITECODE, invitecode);
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);
        } catch (Exception e) {

        }
        return encodedString;
    }

    public static String createRegistrationApiJSONHeader(String userName, String password, String phoneNumber, String deviceID, String email, String inviteCode, boolean isFromIndia, Context context) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        JSONObject customer = new JSONObject();
        JSONObject deviceinfo = new JSONObject();
        try {
            json.put(JSONKEYS.OPCODE, "RIGESTERUSER");
            customer.put(JSONKEYS.NAME, userName);
            customer.put(JSONKEYS.PHONE, phoneNumber);
            customer.put(JSONKEYS.LAT, "0.0");
            customer.put(JSONKEYS.EMAIL, email);
            customer.put(JSONKEYS.PASSWORD, Utilities.getMD5(password));
            customer.put(JSONKEYS.LNG, "0.0");
            customer.put(JSONKEYS.ADDRESS1, "");
            customer.put(JSONKEYS.ADDRESS2, "");
            customer.put(JSONKEYS.GENDER, "");
            customer.put(JSONKEYS.INVITECODE, inviteCode);
            if (isFromIndia) {
                customer.put(JSONKEYS.COUNTRYCODE, "IN");
            } else {
                customer.put(JSONKEYS.COUNTRYCODE, "LK");
            }
            deviceinfo.put(JSONKEYS.DEVICETYPE, "ANDROID");
            deviceinfo.put(JSONKEYS.DEVICEID, deviceID);
            if (Utilities.getSharedString("FCM_TOKEN", context) != null && !Utilities.getSharedString("FCM_TOKEN", context).trim().isEmpty()) {
                deviceinfo.put(JSONKEYS.PUSHTOKEN, Utilities.getSharedString("FCM_TOKEN", context));
            } else {
                deviceinfo.put(JSONKEYS.PUSHTOKEN, "");
            }
            json.put(JSONKEYS.CUSTOMER, customer);
            json.put(JSONKEYS.DEVICEINFO, deviceinfo);
            Log.d("Device", "info : " + deviceinfo.toString());
            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);

        } catch (Exception e) {

        }
        return encodedString;
    }

    public static String createLogoutApiJSONHeader(HomeActivity homeActivity) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        JSONObject deviceinfo = new JSONObject();
        JSONObject customer = new JSONObject();
        try {
            json.put(JSONKEYS.OPCODE, "LOGOUTUSER");
            deviceinfo.put(JSONKEYS.DEVICETYPE, "ANDROID");
            deviceinfo.put(JSONKEYS.DEVICEID, Secure.getString(homeActivity.getContentResolver(), Secure.ANDROID_ID));
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", homeActivity))) {
                customer.put(JSONKEYS.SESSIONSTRING, (Utilities.getSharedString("SESSIONID", homeActivity)));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", homeActivity))) {
                customer.put(JSONKEYS.USERID, (Utilities.getSharedString("CUSTOMERID", homeActivity)));
            }

            json.put(JSONKEYS.CUSTOMER, customer);
            json.put(JSONKEYS.DEVICEINFO, deviceinfo);
            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);
        } catch (Exception e) {

        }
        return encodedString;
    }

    @SuppressLint("SimpleDateFormat")
    public static String createMyTripsApiJSONHeader(Activity activity, int offset, int range) {
        String encodedString = "";
        String currentDateandTime = "";
        JSONObject json = new JSONObject();
        try {
                json.put(JSONKEYS.OPCODE, "GETUSERBOOKINGS");
            json.put(JSONKEYS.TRIPTYPE, "-1");

            //			if (!TextUtils.isEmpty(Utilities.getSharedString("LASTSYNCTIME", activity)))``
            //			{
            //				currentDateandTime = Utilities.getSharedString("LASTSYNCTIME", activity);
            //			}
            //			else
            //			{
//			SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy hh:mm:ss a");
//			Calendar mCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
//			mCalendar.add(Calendar.MONTH, -1);
//			currentDateandTime = sdf.format(mCalendar.getTime());
            //}
            //json.put(JSONKEYS.LASTSYNCTIME, currentDateandTime);

            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", activity))) {
                json.put(JSONKEYS.CUSTOMERID, Utilities.getSharedString("CUSTOMERID", activity));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", activity))) {
                json.put(JSONKEYS.SESSIONID, Utilities.getSharedString("SESSIONID", activity));
            }
            json.put("OFFSET", String.valueOf(offset));
            json.put("RANGE", String.valueOf(range));
            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return encodedString;
    }

    public static String createTrackUsersApiJsonHeader(String tripId, Context mContext) {
        JSONObject reqObject = new JSONObject();
        try {
            reqObject.put("OPCODE", "TRACKUSER");
            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", mContext))) {
                reqObject.put("CUSTOMERID", Utilities.getSharedString("CUSTOMERID", mContext));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", mContext))) {
                reqObject.put("SESSIONID", Utilities.getSharedString("SESSIONID", mContext));
            }
            if (!TextUtils.isEmpty(tripId)) {
                reqObject.put("TRIP", tripId);
            } else {
                Utilities.dialogdisplay("No Active Trips", "Currently there is no Active trip ", mContext);
            }

            return URLUTF8Encoder.encode(Utilities.getEncryptedString(reqObject.toString()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";

    }

    public static String createTrackDriverApiJsonHeader(String tripId, Context mContext) {
        JSONObject reqObject = new JSONObject();
        try {
            reqObject.put("OPCODE", "TRACKDRIVER");
            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", mContext))) {
                reqObject.put("CUSTOMERID", Utilities.getSharedString("CUSTOMERID", mContext));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", mContext))) {
                reqObject.put("SESSIONID", Utilities.getSharedString("SESSIONID", mContext));
            }
            if (!TextUtils.isEmpty(tripId)) {
                reqObject.put("TRIP", tripId);
            } else {
                Utilities.dialogdisplay("No Active Trips", "Currently there is no Active trip ", mContext);
            }

            return URLUTF8Encoder.encode(Utilities.getEncryptedString(reqObject.toString()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";

    }

    public static String createChangePasswordApiJSONHeader(Activity activity, String currentPass, String newPass) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        try {
            json.put(JSONKEYS.OPCODE, "UPDATEPASSWORD");
            json.put("PASSWORD", Utilities.getMD5(currentPass));
            json.put("NEWPASSWORD", Utilities.getMD5(newPass));

            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", activity))) {
                json.put(JSONKEYS.CUSTOMERID, Utilities.getSharedString("CUSTOMERID", activity));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", activity))) {
                json.put(JSONKEYS.SESSIONID, Utilities.getSharedString("SESSIONID", activity));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("MOBILE", activity))) {
                json.put("PHONE", Utilities.getSharedString("MOBILE", activity));
            }
            //			//encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return encodedString;
    }

    public static String createNewBookingApiJSONHeader(Activity activity, String userName, String phoneNum, String emailId, String dateTime,
                                                       String sourc, String dest, String selectedCar, boolean isPackageBooking, String bookingSrcLat, String bookingSrcLng, String bookingDstLat, String bookingDstLng,
                                                       boolean bookNow, boolean isOutStation, String packageId, String packageHours, int paymentMode, String countryCode) throws Exception {
        String encodedString = "";
        // getting latitute and longitude value to calculate distance

        JSONObject json = new JSONObject();
        JSONObject customer = new JSONObject();
        JSONObject booking = new JSONObject();
        JSONObject source = new JSONObject();
        JSONObject destination = new JSONObject();
        String approximateDistance = getApproximateDistance(bookingSrcLat, bookingSrcLng, bookingDstLat, bookingDstLng);
        try {
//            if (selectedCar.equalsIgnoreCase("3"))
//                json.put(JSONKEYS.OPCODE, "CREATEAUTOBOOKING");
//            else
            json.put(JSONKEYS.OPCODE, "CREATEUSERBOOKING");

            json.put(JSONKEYS.CUSTOMERID, Utilities.getSharedString("CUSTOMERID", activity));
            json.put(JSONKEYS.SESSIONID, Utilities.getSharedString("SESSIONID", activity));

            json.put(JSONKEYS.BOOKING, booking);

            booking.put(JSONKEYS.CUSTOMER, customer);
            customer.put(JSONKEYS.NAME, userName);
            customer.put(JSONKEYS.PHONE, phoneNum);
            customer.put(JSONKEYS.EMAIL, emailId);

            String bookingdate = Utilities.parseDateFormat(dateTime);
            booking.put(JSONKEYS.BOOKINGDATE, bookingdate);
            booking.put(JSONKEYS.STATUS, "0");
            booking.put(JSONKEYS.DISTANCE, approximateDistance);

            if (bookNow) {
                booking.put("RIGHTNOW", "1");
            } else {
                booking.put("RIGHTNOW", "0");
            }

            booking.put(JSONKEYS.SOURCE, source);
            source.put(JSONKEYS.NAME, sourc);
            source.put(JSONKEYS.LAT, bookingSrcLat);
            source.put(JSONKEYS.LNG, bookingSrcLng);

            booking.put(JSONKEYS.DESTINATION, destination);
            destination.put(JSONKEYS.NAME, dest);
            destination.put(JSONKEYS.LAT, bookingDstLat);
            destination.put(JSONKEYS.LNG, bookingDstLng);

            booking.put(JSONKEYS.CABTYPE, selectedCar);

            if (!TextUtils.isEmpty(packageId)) {
                booking.put("PACKAGEID", packageId);
            }
            if (isPackageBooking) {
                booking.put("FARE", "");
            } else {
                booking.put("FARE", "");
            }

            if (!TextUtils.isEmpty(packageHours)) {
                booking.put("PACKAGEHOURS", packageHours);
            }
            if (paymentMode == 0) {
                booking.put("PAYMENTTYPE", "C");
            } else if (paymentMode == 1) {
                booking.put("PAYMENTTYPE", "VC");
            } else if (paymentMode == 2) {
                booking.put("PAYMENTTYPE", "MC");
            }
            booking.put("CITYCODE", countryCode);

            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return encodedString;
    }

    private static String getApproximateDistance(String bookingSrcLat, String bookingSrcLng, String bookingDstLat, String bookingDstLng) {
        long aprxDist = DistanceCalculator.getInstance().calculateDistance(Double.parseDouble(bookingSrcLat), Double.parseDouble(bookingSrcLng), Double.parseDouble(bookingDstLat), Double.parseDouble(bookingDstLng));
        return String.valueOf(aprxDist);
    }

    public static String createProfileDetailsJSONHeader(Activity activity) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        JSONObject deviceInfo = new JSONObject();
        try {
            json.put("OPCODE", "USERPROFILE");

            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", activity))) {
                json.put("CUSTOMERID", Utilities.getSharedString("CUSTOMERID", activity));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", activity))) {
                json.put("SESSIONID", Utilities.getSharedString("SESSIONID", activity));
            }
            json.put("DEVICEINFO", deviceInfo);
            deviceInfo.put("DEVICETYPE", "ANDROID");
            deviceInfo.put("DEVICEID", Secure.getString(activity.getContentResolver(), Secure.ANDROID_ID));

            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);

        } catch (Exception e) {

        }
        return encodedString;
    }

    public static String createCancelBookingJsonHeader(String tripId, Activity mContext) {
        String encodedString = "";
        JSONObject json = new JSONObject();

        try {
            json.put("OPCODE", "CANCELUSERBOOKING");
            json.put("BOOKINGID", tripId);


            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", mContext))) {
                json.put("CUSTOMERID", Utilities.getSharedString("CUSTOMERID", mContext));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", mContext))) {
                json.put("SESSIONID", Utilities.getSharedString("SESSIONID", mContext));
            }

            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return encodedString;
    }

    public static String createPanicApiJSONHeader(Activity activity) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        JSONObject customerObj = new JSONObject();

        try {
            json.put("OPCODE", "CUSTOMERPANIC");


            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", activity))) {
                json.put("CUSTOMERID", Utilities.getSharedString("CUSTOMERID", activity));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", activity))) {
                json.put("SESSIONID", Utilities.getSharedString("SESSIONID", activity));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("USERNAME", activity))) {

                customerObj.put("DESCRIPTION", "" + Utilities.getSharedString("USERNAME", activity) + " is in trouble");
            }

            if (!TextUtils.isEmpty(Utilities.getSharedString("ACTIVETRIPID", activity))) {
                customerObj.put("TRIP", Utilities.getSharedString("ACTIVETRIPID", activity));
            }

            json.put("CUSTOMER", customerObj);
            if (!TextUtils.isEmpty(LocationInstance.getInstance().currentLocation)) {
                json.put("ADDRESS", LocationInstance.getInstance().currentLocation);
            }
            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return encodedString;

    }

    public static String createBookingDeatailsApiJSONHeader(Activity activity, String bookingId) {
        String encodedString = "";
        JSONObject json = new JSONObject();

        try {
            json.put("OPCODE", "GETUSERBOOKINGDETAIL");
            json.put("BOOKINGID", bookingId);

            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", activity))) {
                json.put("CUSTOMERID", Utilities.getSharedString("CUSTOMERID", activity));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", activity))) {
                json.put("SESSIONID", Utilities.getSharedString("SESSIONID", activity));
            }
            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return encodedString;
    }

    public static String createUpdateProfileApiJSONHeader(String username, String phoneText, String email,
                                                          String eCName1, String eCName2, String eCNumber1, String eCNumber2, Activity activity) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        JSONObject customerObj = new JSONObject();
        JSONObject deviceInfo = new JSONObject();
        JSONArray wellWisherObj = new JSONArray();
        JSONObject eCObj1 = new JSONObject();
        JSONObject eCObj2 = new JSONObject();
        try {
            json.put("OPCODE", "UPDATEUSERPROFILE");

            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", activity))) {
                json.put("CUSTOMERID", Utilities.getSharedString("CUSTOMERID", activity));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", activity))) {
                json.put("SESSIONID", Utilities.getSharedString("SESSIONID", activity));
            }

            deviceInfo.put("DEVICETYPE", "ANDROID");
            deviceInfo.put("DEVICEID", Secure.getString(activity.getContentResolver(), Secure.ANDROID_ID));

            json.put("DEVICEINFO", deviceInfo);

            if (!TextUtils.isEmpty(username)) {
                customerObj.put("NAME", username);
            }
            if (!TextUtils.isEmpty(email)) {
                customerObj.put("EMAIL", email);
            }


            json.put("CUSTOMER", customerObj);
            eCObj1.put("NAME", eCName1);
            eCObj1.put("PHONE", eCNumber1);
            eCObj2.put("NAME", eCName2);
            eCObj2.put("PHONE", eCNumber2);
            wellWisherObj.put(eCObj1);
            wellWisherObj.put(eCObj2);
            json.put("WELLWISHER", wellWisherObj);
            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return encodedString;
    }

    public static String createForgotPasswordApiJSONHeader(String phone, LoginActivity initialActivity) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        try {
            json.put("OPCODE", "FORGOTPASSOWRD");

            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", initialActivity))) {
                json.put("CUSTOMERID", Utilities.getSharedString("CUSTOMERID", initialActivity));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", initialActivity))) {
                json.put("SESSIONID", Utilities.getSharedString("CUSTOMERID", initialActivity));
            }
            if (!TextUtils.isEmpty(phone)) {
                json.put("PHONE", phone);
            }

            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);

        } catch (Exception e) {

        }
        String encrytedString = Utilities.getEncryptedString(json.toString());
        //encoding header
        encodedString = URLUTF8Encoder.encode(encrytedString);
        return encodedString;
    }

    public static String createPackageDetailsApiJSONHeader(Activity activity) {
        // TODO Auto-generated method stub
        String encodedString = "";
        JSONObject json = new JSONObject();
        try {
            json.put("OPCODE", "PACKAGES");
            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);
        } catch (Exception e) {
        }
        String encrytedString = Utilities.getEncryptedString(json.toString());
        //encoding header
        encodedString = URLUTF8Encoder.encode(encrytedString);
        return encodedString;
    }

    public static String createAddFavouritesApiJSONHeader(String favourites, String address, String latitude, String longitude, String placeId, HomeActivity homeActivity) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        JSONObject deviceInfo = new JSONObject();
        JSONObject favouritePlace = new JSONObject();
        JSONObject placeObj = new JSONObject();
        try {
            json.put("OPCODE", "ADDFAVPLACE");

            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", homeActivity))) {
                json.put("CUSTOMERID", Utilities.getSharedString("CUSTOMERID", homeActivity));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", homeActivity))) {
                json.put("SESSIONID", Utilities.getSharedString("SESSIONID", homeActivity));
            }

            deviceInfo.put("DEVICETYPE", "ANDROID");
            deviceInfo.put("DEVICEID", Secure.getString(homeActivity.getContentResolver(), Secure.ANDROID_ID));
            json.put("DEVICEINFO", deviceInfo);

            favouritePlace.put("FAVNAME", favourites);

            placeObj.put("NAME", address);
            placeObj.put("LAT", latitude);
            placeObj.put("LNG", longitude);
            placeObj.put("GEOPLACEID", placeId);
            favouritePlace.put("PLACE", placeObj);

            json.put("FAVPLACE", favouritePlace);

            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return encodedString;

    }

    public static String createAddRecentPlacesApiJSONHeader(String bookingDestinationAdd, String latitude, String longitude, Activity homeActivity) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        JSONObject lastPlace = new JSONObject();
        JSONObject placeObj = new JSONObject();
        try {
            json.put("OPCODE", "ADDRECENTPLACE");

            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", homeActivity))) {
                json.put("CUSTOMERID", Utilities.getSharedString("CUSTOMERID", homeActivity));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", homeActivity))) {
                json.put("SESSIONID", Utilities.getSharedString("SESSIONID", homeActivity));
            }

            placeObj.put("NAME", bookingDestinationAdd);
            placeObj.put("LAT", latitude);
            placeObj.put("LNG", longitude);
            lastPlace.put("PLACE", placeObj);

            json.put("LASTPLACE", lastPlace);

            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return encodedString;

    }

    public static String createUploadUserPhotoJsonHeader(Activity activity) {
        String encrytedString = "";
        JSONObject json = new JSONObject();
        try {
            json.put("OPCODE", "UPLOADUSERPROFILEPIC");

            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", activity))) {
                json.put("CUSTOMERID", Utilities.getSharedString("CUSTOMERID", activity));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", activity))) {
                json.put("SESSIONID", Utilities.getSharedString("SESSIONID", activity));
            }

            json.put("CUSTOMERPIC", "");

            //encrypting header
            encrytedString = Utilities.getEncryptedString(json.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return encrytedString;
    }


    public static String createCheckForTripRatingApiJSONHeader(Activity activity) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        try {
            json.put(JSONKEYS.OPCODE, "CHECKLASTTRIPRATING");

            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", activity))) {
                json.put(JSONKEYS.CUSTOMERID, Utilities.getSharedString("CUSTOMERID", activity));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", activity))) {
                json.put(JSONKEYS.SESSIONID, Utilities.getSharedString("SESSIONID", activity));
            }

            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return encodedString;
    }

    public static String createUpdateTripRatingApiJSONHeader(Activity activity, float tripRating, String comments) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        try {
            json.put(JSONKEYS.OPCODE, "UPDATETRIPRATING");

            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", activity))) {
                json.put(JSONKEYS.CUSTOMERID, Utilities.getSharedString("CUSTOMERID", activity));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", activity))) {
                json.put(JSONKEYS.SESSIONID, Utilities.getSharedString("SESSIONID", activity));
            }
            json.put(JSONKEYS.RATING, tripRating);
            json.put(JSONKEYS.COMMENTS, comments);

            //encrypting header
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return encodedString;
    }

    public static String createGetNEWTokenApiJSONHeader(String amt, String bookingID, Context ctx) {
        String encodedString = "";

        JSONObject reqObject = new JSONObject();
        try {
            reqObject.put("OPCODE", "GETNEWTOKEN");
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", ctx))) {
                reqObject.put(JSONKEYS.SESSIONID, (Utilities.getSharedString("SESSIONID", ctx)));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", ctx))) {
                reqObject.put(JSONKEYS.CUSTOMERID, (Utilities.getSharedString("CUSTOMERID", ctx)));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("MOBILE", ctx))) {
                reqObject.put(JSONKEYS.MOBILE, Utilities.getSharedString("MOBILE", ctx));
            }
            if (!TextUtils.isEmpty(bookingID)) {
                reqObject.put(JSONKEYS.TRIPID, bookingID);
            }
            //reqObject.put(JSONKEYS.SOURCE, source);
            //reqObject.put(JSONKEYS.DESTINATION,destination);
            reqObject.put(JSONKEYS.AMOUNT, amt);

            String encrytedString = Utilities.getEncryptedString(reqObject.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);
        } catch (JSONException e) {
            e.printStackTrace();

        }
        return encodedString;
    }

    public static String getNewSettingsApiJSONHeader(String countryCode, Context ctx) {
        String encodedString = "";

        JSONObject reqObject = new JSONObject();
        try {
            reqObject.put("OPCODE", "GETNEWSETTINGS");
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", ctx))) {
                reqObject.put(JSONKEYS.SESSIONID, (Utilities.getSharedString("SESSIONID", ctx)));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", ctx))) {
                reqObject.put(JSONKEYS.CUSTOMERID, (Utilities.getSharedString("CUSTOMERID", ctx)));
            }
            reqObject.putOpt("COUNTRYCODE", "LK");

            String encrytedString = Utilities.getEncryptedString(reqObject.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);
        } catch (JSONException e) {
            e.printStackTrace();

        }
        return encodedString;
    }

    public static String createUpdateHomeLocationApiJSONHeader(String address, String lat, String lng, String pref1, String pref2, Context ctx) {
        String encodedString = "";

        JSONObject reqObject = new JSONObject();
        try {
            reqObject.put("OPCODE", "UPDATEHOMEADDRESS");
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", ctx))) {
                reqObject.put(JSONKEYS.SESSIONID, (Utilities.getSharedString("SESSIONID", ctx)));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", ctx))) {
                reqObject.put(JSONKEYS.CUSTOMERID, (Utilities.getSharedString("CUSTOMERID", ctx)));
            }
            reqObject.putOpt(JSONKEYS.HOMEADDRESS, address);
            reqObject.putOpt(JSONKEYS.LAT, lat);
            reqObject.putOpt(JSONKEYS.LNG, lng);
            reqObject.putOpt(JSONKEYS.CABPREF1, pref1);
            reqObject.putOpt(JSONKEYS.CABPREF2, pref2);


            String encrytedString = Utilities.getEncryptedString(reqObject.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);
        } catch (JSONException e) {
            e.printStackTrace();

        }
        return encodedString;
    }

    public static String creategetTMHDetailsApiJSONHeader(Context ctx) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        try {
            json.put(JSONKEYS.OPCODE, "GETTAKEMEHOME");
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", ctx))) {
                json.put(JSONKEYS.SESSIONID, (Utilities.getSharedString("SESSIONID", ctx)));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", ctx))) {
                json.put(JSONKEYS.CUSTOMERID, (Utilities.getSharedString("CUSTOMERID", ctx)));
            }
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);
        } catch (Exception e) {

        }
        return encodedString;
    }

    public static String creategetVowCashApiJSONHeader(Context ctx) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        try {
            json.put(JSONKEYS.OPCODE, "CUSTOMERVOWCASH");
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", ctx))) {
                json.put(JSONKEYS.SESSIONID, (Utilities.getSharedString("SESSIONID", ctx)));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", ctx))) {
                json.put(JSONKEYS.CUSTOMERID, (Utilities.getSharedString("CUSTOMERID", ctx)));
            }
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);
        } catch (Exception e) {

        }
        return encodedString;
    }

    public static String creategetCustomerAccountApiJSONHeader(Context ctx) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        try {
            json.put(JSONKEYS.OPCODE, "CUSTOMERACCOUNTDETAIL");
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", ctx))) {
                json.put(JSONKEYS.SESSIONID, (Utilities.getSharedString("SESSIONID", ctx)));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", ctx))) {
                json.put(JSONKEYS.CUSTOMERID, (Utilities.getSharedString("CUSTOMERID", ctx)));
            }
            String encrytedString = Utilities.getEncryptedString(json.toString());
            //encoding header
            encodedString = URLUTF8Encoder.encode(encrytedString);
        } catch (Exception e) {

        }
        return encodedString;
    }

    public static String createReedemCodeJSONHeader(String reedemCode, Context context) {
        String encodedString = "";
        JSONObject json = new JSONObject();
        try {
            json.put(JSONKEYS.OPCODE, "REDEEMPROMOCODE");
            if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", context))) {
                json.put(JSONKEYS.SESSIONID, (Utilities.getSharedString("SESSIONID", context)));
            }
            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", context))) {
                json.put(JSONKEYS.CUSTOMERID, (Utilities.getSharedString("CUSTOMERID", context)));
            }
            json.put(JSONKEYS.PROMOCODE, (reedemCode));
            String encrytedString = Utilities.getEncryptedString(json.toString());
            encodedString = URLUTF8Encoder.encode(encrytedString);
        } catch (Exception e) {

        }
        return encodedString;

    }
}
