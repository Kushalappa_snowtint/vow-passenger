package com.vowcabs.passengerapp.utils;

public interface LocationUpdatesListener 
{
	public void locationChanged(double longitude, double latitude);
    public void displayGPSSettingsDialog();
}
