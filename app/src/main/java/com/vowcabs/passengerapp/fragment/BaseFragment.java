package com.vowcabs.passengerapp.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import com.vowcabs.passengerapp.HomeActivity;


public class BaseFragment extends Fragment
{
	public HomeActivity mActivity;

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);

		if (this.getActivity() instanceof HomeActivity)
			mActivity = (HomeActivity) this.getActivity();
	}

	public boolean onBackPressed() 
	{
		return false;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
	}
}
