package com.vowcabs.passengerapp.Models;

import org.json.JSONException;
import org.json.JSONObject;

public class PackageBooking 
{
	String packageId;
	String packageName;
	String packageType;

	String packageVehicleType;
	String packageHours;
	String packageKms;
	String packagePrice;
	String packageNightPrice;
	String packageExtraKmCharge;
	String packageExtraHourCharge;
	String packageExtraKmNightCharge;
	String packageExtraHourNightCharge;

	public String getPackageId() 
	{
		return packageId;
	}
	public void setPackageId(String packageId) 
	{
		this.packageId = packageId;
	}
	public String getPackageName() 
	{
		return packageName;
	}
	public void setPackageName(String packageName) 
	{
		this.packageName = packageName;
	}
	public String getPackageHours() 
	{
		return packageHours;
	}
	public void setPackageHours(String packageHours) 
	{
		this.packageHours = packageHours;
	}
	public String getPackageKms() 
	{
		return packageKms;
	}
	public void setPackageKms(String packageKms) 
	{
		this.packageKms = packageKms;
	}
	public String getPackagePrice() 
	{
		return packagePrice;
	}
	public void setPackagePrice(String packagePrice) 
	{
		this.packagePrice = packagePrice;
	}
	public String getPackageNightPrice() 
	{
		return packageNightPrice;
	}
	public void setPackageNightPrice(String packageNightPrice)
	{
		this.packageNightPrice = packageNightPrice;
	}
	public String getPackageExtraKmCharge()
	{
		return packageExtraKmCharge;
	}
	public void setPackageExtraKmCharge(String packageExtraKmCharge) 
	{
		this.packageExtraKmCharge = packageExtraKmCharge;
	}
	public String getPackageExtraHourCharge() 
	{
		return packageExtraHourCharge;
	}
	public void setPackageExtraHourCharge(String packageExtraHourCharge) 
	{
		this.packageExtraHourCharge = packageExtraHourCharge;
	}
	public String getPackageExtraKmNightCharge() 
	{
		return packageExtraKmNightCharge;
	}
	public void setPackageExtraKmNightCharge(String packageExtraKmNightCharge) 
	{
		this.packageExtraKmNightCharge = packageExtraKmNightCharge;
	}
	public String getPackageExtraHourNightCharge() 
	{
		return packageExtraHourNightCharge;
	}
	public void setPackageExtraHourNightCharge(String packageExtraHourNightCharge) 
	{
		this.packageExtraHourNightCharge = packageExtraHourNightCharge;
	}
	public String getPackageType() 
	{
		return packageType;
	}
	public void setPackageType(String packageType) 
	{
		this.packageType = packageType;
	}
	public String getPackageVehicleType() 
	{
		return packageVehicleType;
	}
	public void setPackageVehicleType(String packageVehicleType)
	{
		this.packageVehicleType = packageVehicleType;
	}
	public PackageBooking(JSONObject pckgObj) 
	{
		try 
		{
			if (!pckgObj.isNull("PACKAGEID"))
			{
				setPackageId(pckgObj.getString("PACKAGEID"));
			} 
			if (!pckgObj.isNull("PACKAGENAME"))
			{
				setPackageName(pckgObj.getString("PACKAGENAME"));
			}
			if (!pckgObj.isNull("PACKAGEHOURS"))
			{
				setPackageHours(pckgObj.getString("PACKAGEHOURS"));
			}
			if (!pckgObj.isNull("PACKAGEKMS"))
			{
				setPackageKms(pckgObj.getString("PACKAGEKMS"));
			}
			if (!pckgObj.isNull("PACKAGEPRICE"))
			{
				setPackagePrice(pckgObj.getString("PACKAGEPRICE"));
			}
			if (!pckgObj.isNull("PACKAGENIGHTPRICE"))
			{
				setPackageNightPrice(pckgObj.getString("PACKAGENIGHTPRICE"));
			}
			if (!pckgObj.isNull("PACKAGEEXTRAKMCHARGE"))
			{
				setPackageExtraKmCharge(pckgObj.getString("PACKAGEEXTRAKMCHARGE"));
			}
			if (!pckgObj.isNull("PACKAGEEXTRAHOURCHARGE"))
			{
				setPackageExtraHourCharge(pckgObj.getString("PACKAGEEXTRAHOURCHARGE"));
			}
			if (!pckgObj.isNull("PACKAGEEXTRAKMNIGHTCHARGE"))
			{
				setPackageExtraKmNightCharge(pckgObj.getString("PACKAGEEXTRAKMNIGHTCHARGE"));
			}
			if (!pckgObj.isNull("PACKAGEEXTRAHOURNIGHTCHARGE"))
			{
				setPackageExtraHourNightCharge(pckgObj.getString("PACKAGEEXTRAHOURNIGHTCHARGE"));
			}
			if (!pckgObj.isNull("PACKAGETYPE"))
			{
				setPackageType(pckgObj.getString("PACKAGETYPE").trim());
			}
			if (!pckgObj.isNull("PACKAGEVEHICLETYPE"))
			{
				setPackageVehicleType(pckgObj.getString("PACKAGEVEHICLETYPE").trim());
			}

		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
