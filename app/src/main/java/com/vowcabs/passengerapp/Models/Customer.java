package com.vowcabs.passengerapp.Models;

import org.json.JSONException;
import org.json.JSONObject;

public class Customer 
{
	String id;
	String name;
	String phone;
	String email;
	String lat;
	String lng;
	String geoPlaceID;
	String address1;
	String address2;
	String validate;
	String gender;

	public Customer(JSONObject customerObject) 
	{
		try
		{
			if (!customerObject.isNull("ID"))
			{
				setId(customerObject.getString("ID"));
			}
			if (!customerObject.isNull("NAME"))
			{
				setName(customerObject.getString("NAME"));
			}

			if (!customerObject.isNull("PHONE"))
			{
				setPhone(customerObject.getString("PHONE"));
			}
			if (!customerObject.isNull("EMAIL"))
			{
				setEmail(customerObject.getString("EMAIL"));
			}

			if (!customerObject.isNull("LAT"))
			{
				setLat(customerObject.getString("LAT"));
			}
			if (!customerObject.isNull("LNG"))
			{
				setLng(customerObject.getString("LNG"));
			}
			if (!customerObject.isNull("GEOPLACEID"))
			{
				setGeoPlaceID(customerObject.getString("GEOPLACEID"));
			}
			if (!customerObject.isNull("ADDRESS1"))
			{
				setAddress1(customerObject.getString("ADDRESS1"));
			}
			if (!customerObject.isNull("ADDRESS2"))
			{
				setAddress2(customerObject.getString("ADDRESS2"));
			}
			if (!customerObject.isNull("VALIDATE"))
			{
				setValidate(customerObject.getString("VALIDATE"));
			}
			if (!customerObject.isNull("GENDER"))
			{
				setGender(customerObject.getString("GENDER"));
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getGeoPlaceID() {
		return geoPlaceID;
	}

	public void setGeoPlaceID(String geoPlaceID) {
		this.geoPlaceID = geoPlaceID;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getValidate() {
		return validate;
	}

	public void setValidate(String validate) {
		this.validate = validate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}
