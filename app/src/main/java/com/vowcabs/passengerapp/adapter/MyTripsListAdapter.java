package com.vowcabs.passengerapp.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.AppController;
import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.Models.Booking;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.fragment.BookTripFragment;
import com.vowcabs.passengerapp.fragment.TrackFragment;
import com.vowcabs.passengerapp.fragment.TripDetailsSriLankaFragment;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.DateTime;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.util.Calendar.AM;

public class MyTripsListAdapter extends BaseAdapter {
    Activity mcontext;
    List<Booking> mlist;
    LayoutInflater mlayoutInflater = null;
    String tag_cancel_booking = "cancelbooking_req";
    String encodedString;
    ListView mListView;

    public MyTripsListAdapter(Activity activity, ArrayList<Booking> sArray, ListView lv) {
        // TODO Auto-generated constructor stub
        mcontext = activity;
        mlist = sArray;
        mListView = lv;
        mlayoutInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View v = convertView;
        ViewHolder holder;
        final Booking currentBooking = mlist.get(position);
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.mytrip_listview, null);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        if (!TextUtils.isEmpty(currentBooking.getReferenceId())) {
            holder.tripId.setText(currentBooking.getReferenceId());
        }
        if (!TextUtils.isEmpty(currentBooking.getBookingDate())) {
            holder.tripDate.setText(DateTime.changeDateFormat(currentBooking.getBookingDate()));
        }
        if (currentBooking.getmSource() != null && !TextUtils.isEmpty(currentBooking.getmSource().getName())) {
            holder.fromAddress.setText(mlist.get(position).getmSource().getName());
        }
        if (currentBooking.getmDestination() != null && !TextUtils.isEmpty(currentBooking.getmDestination().getName())) {
            holder.toAddress.setText(currentBooking.getmDestination().getName());
        }

        if (!TextUtils.isEmpty(currentBooking.getBookingStatus()) && currentBooking.getBookingStatus().trim().equals("3")) {
            holder.tripStatusIcon.setImageResource(R.drawable.active_bookingicon);
            holder.tripStatusIcon.setVisibility(View.VISIBLE);
            holder.expandArrow.setVisibility(View.VISIBLE);

        } else if (!TextUtils.isEmpty(currentBooking.getBookingStatus()) && currentBooking.getBookingStatus().trim().equals("4")) {
            holder.tripStatusIcon.setImageResource(R.drawable.cancelled_booking);
            holder.expandArrow.setVisibility(View.INVISIBLE);
            holder.tripStatusIcon.setVisibility(View.VISIBLE);
        } else {
            holder.tripStatusIcon.setVisibility(View.INVISIBLE);
            holder.expandArrow.setVisibility(View.INVISIBLE);
        }

        if (!TextUtils.isEmpty(currentBooking.getBookingStatus()) && (currentBooking.getBookingStatus().trim().equals("0") || currentBooking.getBookingStatus().trim().equals("1"))) {
            holder.cancelBooking.setImageResource(R.drawable.cancel_booking);
            holder.cancelBooking.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(currentBooking.getDriverStatus()) && currentBooking.getDriverStatus().equalsIgnoreCase("3")) {
                holder.cancelBooking.setVisibility(View.INVISIBLE);
            }
        } else {
            holder.cancelBooking.setVisibility(View.INVISIBLE);
        }

        holder.cancelBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Trip", "status "+currentBooking.getBookingStatus());
                dialogdisplay(position);
            }

        });

        holder.repeatBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeatBookingDialog(mlist.get(position));
            }

        });
        holder.returnBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnBookingDialog(mlist.get(position));
            }

        });
        holder.llSourseDest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mlist.get(position).getBookingStatus().trim().equals("0")) // incoming booking
                {
                    Utilities.dialogdisplay("Trip Status", "Driver not yet assigned.Please try after some time.", mcontext);
                }
                if (mlist.get(position).getBookingStatus().trim().equals("1") || mlist.get(position).getBookingStatus().trim().equals("2")) {
                    ((HomeActivity) mcontext).pushFragment(new TrackFragment(mlist.get(position)), false, true);
                }
                if (mlist.get(position).getBookingStatus().trim().equals("3")) {
                    ((HomeActivity) mcontext).pushFragment(new TripDetailsSriLankaFragment(mlist.get(position).getBookingId()), false, true);
                } else if (mlist.get(position).getBookingStatus().trim().equals("4")) {
                    Utilities.dialogdisplay("Cancelled Trip", "Your trip has been cancelled as per your request.", mcontext);
                } else if (mlist.get(position).getBookingStatus().trim().equals("5")) {
                    Utilities.dialogdisplay("Missed Trip", "Your trip went missed.", mcontext);
                }
            }

        });

        return v;
    }

    public void dialogdisplay(final int position) {
        final Dialog customDialog = new Dialog(mcontext);
        View dialogView = mcontext.getLayoutInflater().inflate(R.layout.dialog_view, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        TextView message = (TextView) dialogView.findViewById(R.id.tv_DialogMessage);
        title.setText("Cancel Booking");
        message.setText("Are you sure you want to cancel this Booking?");
        Button yes = (Button) dialogView.findViewById(R.id.btn_yes);
        Button no = (Button) dialogView.findViewById(R.id.btn_no);
        no.setVisibility(View.VISIBLE);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
                makeCancelBookingAPI(mlist.get(position), mcontext);
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }

    private void makeCancelBookingAPI(final Booking booking, final Activity mContext) {
        if (mContext == null)
            return;

        if (Utilities.isNetworkAvailable(mContext)) {
            Utilities.showProgressDialog(mContext);
            encodedString = JsonHeaders.createCancelBookingJsonHeader(booking.getBookingId(), mContext);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET, APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(tag_cancel_booking, "Sucess" + response);
                    try {
                        Utilities.hideProgressDialog();
                        if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("000")) {
                            booking.setBookingStatus("4");
                            mListView.invalidate();
                            notifyDataSetChanged();
                            Utilities.dialogdisplay("Cancel Booking", "Your trip has been cancelled as per your request.", mContext);
                        } else {
                            Utilities.dialogdisplay("Cancel Booking", "Booking cannot be cancelled", mContext);

                        }
                    } catch (Exception e) {

                    }

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mListView.invalidate();
                    notifyDataSetChanged();
                    Utilities.hideProgressDialog();
                    Utilities.dialogdisplay("Network Error", "Please Try Again", mContext);
                    VolleyLog.d("Parsing " + tag_cancel_booking, "Error: " + error.getMessage());
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_cancel_booking);
        }

    }

    public void returnBookingDialog(final Booking booking) {
        final Dialog customDialog = new Dialog(mcontext);
        View dialogView = mcontext.getLayoutInflater().inflate(R.layout.dialog_view, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        TextView message = (TextView) dialogView.findViewById(R.id.tv_DialogMessage);
        title.setText("Return Booking");
        message.setText("Do you want to book a return trip?");
        Button yes = (Button) dialogView.findViewById(R.id.btn_yes);
        Button no = (Button) dialogView.findViewById(R.id.btn_no);
        no.setVisibility(View.VISIBLE);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) mcontext).pushFragment(new BookTripFragment("Return", booking.getmDestination().getName(),
                        booking.getmSource().getName(), booking.getmDestination().getLat(), booking.getmDestination().getLng(), booking.getmSource().getLat(), booking.getmSource().getLng()), false, true);
                customDialog.dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }

    public void repeatBookingDialog(final Booking booking) {
        final Dialog customDialog = new Dialog(mcontext);
        View dialogView = mcontext.getLayoutInflater().inflate(R.layout.dialog_view, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        TextView message = (TextView) dialogView.findViewById(R.id.tv_DialogMessage);
        title.setText("Repeat Booking");
        message.setText("Do you want to repeat this trip?");
        Button yes = (Button) dialogView.findViewById(R.id.btn_yes);
        Button no = (Button) dialogView.findViewById(R.id.btn_no);
        no.setVisibility(View.VISIBLE);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) mcontext).pushFragment(new BookTripFragment("Repeat", booking.getmSource().getName(),
                        booking.getmDestination().getName(), booking.getmSource().getLat(), booking.getmSource().getLng(), booking.getmDestination().getLat(), booking.getmDestination().getLng()), false, true);
                customDialog.dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }

}

class ViewHolder {
    TextView tripId, tripDate, fromAddress, toAddress;
    ImageView cancelBooking, tripStatusIcon, expandArrow;
    RelativeLayout returnBooking, repeatBooking;
    LinearLayout llSourseDest;

    ViewHolder(View base) {
        tripId = (TextView) base.findViewById(R.id.currentBookingId);
        tripDate = (TextView) base.findViewById(R.id.currentBookingTime);
        fromAddress = (TextView) base.findViewById(R.id.et_trips_PickUp);
        toAddress = (TextView) base.findViewById(R.id.et_trips_Drop);
        cancelBooking = (ImageView) base.findViewById(R.id.iv_cancelbooking);
        returnBooking = (RelativeLayout) base.findViewById(R.id.rl_returnBooking);
        repeatBooking = (RelativeLayout) base.findViewById(R.id.rl_repeatBooking);
        tripStatusIcon = (ImageView) base.findViewById(R.id.iv_trips_status);
        llSourseDest = (LinearLayout) base.findViewById(R.id.ll_fromTo);
        expandArrow = (ImageView) base.findViewById(R.id.iv_expandArrow);
    }

}
