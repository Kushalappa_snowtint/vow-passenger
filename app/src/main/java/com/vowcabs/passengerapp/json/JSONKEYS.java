package com.vowcabs.passengerapp.json;


public class JSONKEYS 
{
	public static final long BASEFARE_DAY           =  100;
	public static final long BASEFARE_NIGHT         =  180;
	public static final float   BASEKM              =  2.0f;
	public static final long FAREPERKM_DAY          =  50;
	public static final double SERVICETAX           =  5.6;
	public static final long FAREPERKM_NIGHT        =  20;
	public static final String OPCODE               = "OPCODE";
	public static final String NAME                 = "NAME";
	public static final String PHONE                = "PHONE";
	public static final String LAT                  = "LAT";
	public static final String EMAIL                = "EMAIL";
	public static final String PASSWORD             = "PASSWORD";
	public static final String LNG                  = "LNG";
	public static final String ADDRESS1             = "ADDRESS1";
	public static final String ADDRESS2             = "ADDRESS2";
	public static final String GENDER               = "GENDER";
	public static final String DEVICETYPE           = "DEVICETYPE";
	public static final String DEVICEID             = "DEVICEID";
	public static final String PUSHTOKEN            = "PUSHTOKEN";
	public static final String CUSTOMER             = "CUSTOMER";
	public static final String DEVICEINFO           = "DEVICEINFO";
//	public static final String DRIVERID             = "DRIVERID";
	public static final String TRIPTYPE             = "TRIPTYPE";
	public static final String TRIPID               = "TRIPID";
	public static final String LASTSYNCTIME         = "LASTSYNCTIME";
	public static final String CUSTOMERID           = "CUSTOMERID";
	public static final String SESSIONID            = "SESSIONID";
	public static final String BOOKING              = "BOOKING";
	public static final String BOOKINGDATE          = "BOOKINGDATE";
	public static final String STATUS               = "STATUS";
	public static final String SOURCE               = "SOURCE";
	public static final String DESTINATION          = "DESTINATION";
	public static final String CABTYPE              = "CABTYPE";
	public static final String DISTANCE             = "DISTANCE";
	public static final String OTP                  = "OTP";
	public static final String SESSIONSTRING        = "SESSIONSTRING";
	public static final String USERID               = "USERID";
	public static final String BOOKINGID            = "BOOKINGID";
	public static final String INVITECODE           = "INVITECODE";
	public static final String COUNTRYCODE          = "COUNTRYCODE";
	public static final String RATING               = "RATING";
	public static final String COMMENTS             = "COMMENTS";
	public static final String GCMREGID             = "GCMREGID";
	public static final String MOBILE               = "PHONE";
	public static final String AMOUNT               = "AMOUNT";
	public static final String HOMEADDRESS          = "HOMEADDRESS";
    public static final String CABPREF1             = "CABPREF1";
    public static final String CABPREF2             = "CABPREF2";
	public static final String PROMOCODE             = "PROMOCODE";
	public static final String TRIP_ID = "TRIP_ID";
	public static final int CAR_MINI                =  0;
	public static final int CAR_SEDAN               =  1;
	public static final int CAR_PRIME               =  2;
	public static final int CAR_AUTO                =  3;
	
	public static String serviceableArea            = "serviceable_area";

}
