package com.vowcabs.passengerapp.adapter;


import com.vowcabs.passengerapp.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class InviteAdapter extends ArrayAdapter<String> {
	
	private Context context;
	private String[] textViews;
	private int[] imgViews;
	private static LayoutInflater inflater;
	public InviteAdapter(Context context, String[] textViews,int[] imgViews) {
		super(context, R.layout.activity_list,textViews);
		this.context = context;
		this.textViews = textViews;
		this.imgViews = imgViews;
			
	}
	public View getView(int position,View convertView,ViewGroup parent){
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);	
		convertView = inflater.inflate(R.layout.activity_list,null);
		ImageView img = (ImageView) convertView.findViewById(R.id.imgView);
		TextView text = (TextView) convertView.findViewById(R.id.textView);
		text.setText(textViews[position]);
		img.setImageResource(imgViews[position]);
		return convertView;
		
}

}
