package com.vowcabs.passengerapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.Models.FavouritePlace;
import com.vowcabs.passengerapp.Models.Place;
import com.vowcabs.passengerapp.Models.RecentPlace;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.fragment.BookTripFragment;
import com.vowcabs.passengerapp.fragment.HomeFragment;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteAdapter extends BaseAdapter {

    Activity mcontext;
    List<RecentPlace> mRecentlist;
    List<FavouritePlace> mFavouritelist;
    LayoutInflater mlayoutInflater = null;
    ListView mListView;
    boolean isFavourite = false;
    AutoCompleteTextView sourceOrDestinationView;
    BookTripFragment bookFragment;
    String sourceLocation = "";

    public AutoCompleteAdapter(Activity activity, ArrayList<RecentPlace> sArray, ListView lv, AutoCompleteTextView sourceOrDestinationView, BookTripFragment fragment, String source) {
        // TODO Auto-generated constructor stub
        mcontext = activity;
        mRecentlist = sArray;
        mListView = lv;
        isFavourite = false;
        mlayoutInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.sourceOrDestinationView = sourceOrDestinationView;
        this.bookFragment = fragment;
        this.sourceLocation = source;
    }

    public AutoCompleteAdapter(Activity activity, ArrayList<FavouritePlace> sArray, ListView lv, boolean isFav, AutoCompleteTextView sourceOrDestinationView, BookTripFragment fragment, String source) {
        // TODO Auto-generated constructor stub
        mcontext = activity;
        mFavouritelist = sArray;
        mListView = lv;
        mlayoutInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        isFavourite = isFav;
        this.sourceOrDestinationView = sourceOrDestinationView;
        this.bookFragment = fragment;
        this.sourceLocation = source;
    }

    @Override
    public int getCount() {
        if (mFavouritelist != null && isFavourite)
            return mFavouritelist.size();

        if (mRecentlist.size() > 5)
            return 5;
        // TODO Auto-generated method stub
        return mRecentlist.size();
    }

    @Override
    public Object getItem(int position) {
        if (mFavouritelist != null && isFavourite)
            return mFavouritelist.get(position);
        // TODO Auto-generated method stub
        return mRecentlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View v = convertView;
        PlacesHolder holder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.autocomplete_listitem, null);
            holder = new PlacesHolder(v);
            v.setTag(holder);
        } else {
            holder = (PlacesHolder) v.getTag();
        }

        if (!isFavourite) {
            holder.placeName.setText(" " + mRecentlist.get(position).getmPlace().getName());
        } else {
            holder.placeName.setText(" " + mFavouritelist.get(position).getFavouriteTripName());
        }

        holder.placeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFavourite) {
                    loadMap(mRecentlist.get(position).getmPlace());
                } else {
                    loadMap(mFavouritelist.get(position).getmPlace());
                }

            }

        });

        return v;

    }

    private void loadMap(Place getmPlace) {
        try {
            if (sourceOrDestinationView != null && !TextUtils.isEmpty(getmPlace.getName())) {
                sourceOrDestinationView.setText(getmPlace.getName());
                if (bookFragment != null) {
                    bookFragment.sourceDestinationPicker.setVisibility(View.GONE);
                    bookFragment.getGeoLatLngValues(getmPlace.getName(), 1);
                    bookFragment.bookingDestination = getmPlace.getName();
                    bookFragment.sourceDestinationPicker.setVisibility(View.GONE);
                    bookFragment.bookingScreenScroller.setVisibility(View.VISIBLE);
                    if (!TextUtils.isEmpty(sourceLocation) && !TextUtils.isEmpty(getmPlace.getName()))
                        bookFragment.getDistanceApiCall(sourceLocation, getmPlace.getName(), "", false);
                }
                InputMethodManager imm = (InputMethodManager) mcontext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(sourceOrDestinationView.getWindowToken(), 0);
            } else {
                ((HomeActivity) mcontext).pushFragment(new HomeFragment(true, Double.parseDouble(getmPlace.getLatitude()), Double.parseDouble(getmPlace.getLongitude())), false, false);
            }
        } catch (Exception e) {
        }
    }

}


class PlacesHolder {
    TextView placeName;
    LinearLayout placeLayout;

    PlacesHolder(View base) {
        placeName = (TextView) base.findViewById(R.id.tv_placename);
        placeLayout = (LinearLayout) base.findViewById(R.id.ll_place);
    }
}

