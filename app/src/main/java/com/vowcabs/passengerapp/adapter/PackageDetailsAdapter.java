package com.vowcabs.passengerapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vowcabs.passengerapp.Models.PackageBooking;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.fragment.BookTripFragment;
import com.vowcabs.passengerapp.utils.LocationInstance;

import java.util.ArrayList;
import java.util.List;

public class PackageDetailsAdapter extends BaseAdapter
{
	Activity mcontext;
	List<PackageBooking> mlist;
	LayoutInflater mlayoutInflater = null;
	ListView mListView;
	BookTripFragment fragment = null;

	public PackageDetailsAdapter(Activity activity, ArrayList<PackageBooking> sArray, ListView lv, BookTripFragment fragment) 
	{
		// TODO Auto-generated constructor stub
		mcontext = activity;
		mlist = sArray;
		mListView = lv;
		this.fragment = fragment;
		mlayoutInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() 
	{
		// TODO Auto-generated method stub
		return mlist.size();
	}

	@Override
	public Object getItem(int position) 
	{
		// TODO Auto-generated method stub
		return mlist.get(position);
	}

	@Override
	public long getItemId(int position) 
	{
		// TODO Auto-generated method stub
		return position;
	}
	@Override
	public int getViewTypeCount() {
		return getCount();
	}

	@Override
	public int getItemViewType(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		// TODO Auto-generated method stub
		View v = convertView;
		PackageHolder holder;
		if (convertView == null) 
		{
			LayoutInflater li = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = li.inflate(R.layout.package_detials_listitem, null);
			holder = new PackageHolder(v);
			v.setTag(holder);
		} else {
			holder = (PackageHolder) v.getTag();
		}
		holder.tripDuration.setText(mlist.get(position).getPackageName());
		holder.tripFare.setText("Rs."+mlist.get(position).getPackagePrice());
		holder.packageLayout.setOnClickListener(new View.OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				Log.e("Errror", "Title : " + mlist.get(position).getPackageName());
				if (!TextUtils.isEmpty(mlist.get(position).getPackageId()))
				{
					LocationInstance.getInstance().setCurrentSelectedPackage(mlist.get(position).getPackageId());

				}
				if (!TextUtils.isEmpty(mlist.get(position).getPackageName()))
				{
					fragment.setSelectedPackageTitle(mlist.get(position).getPackageName());
				}
				if (!TextUtils.isEmpty(mlist.get(position).getPackagePrice()))
				{
					LocationInstance.getInstance().setSelectedPackageFare(mlist.get(position).getPackagePrice());
				}
			}
		});

		return v;

	}

}

class PackageHolder 
{
	TextView tripDuration, tripFare;
	RelativeLayout packageLayout;

	PackageHolder(View base) 
	{
		tripDuration                  = (TextView) base.findViewById(R.id.tv_duration);
		tripFare                      = (TextView) base.findViewById(R.id.tv_fare);
		packageLayout                 = (RelativeLayout) base.findViewById(R.id.rl_packageLayout);
	}
}

