package com.vowcabs.passengerapp.fragment;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.AppController;
import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.MyApplication;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONObject;

public class ChangePasswordFragment extends HomeBaseFragment {
    String encodedString;
    public final String tag_change_password = "update_password";
    EditText etCurrentPswrd;
    EditText etNewPswrd;
    EditText etConfirmPswrd;
    TextView changePassword;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.change_password, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        etCurrentPswrd = (EditText) getView().findViewById(R.id.et_current_password);
        etNewPswrd = (EditText) getView().findViewById(R.id.et_new_password);
        etConfirmPswrd = (EditText) getView().findViewById(R.id.et_confirm_password);
        changePassword = (TextView) getView().findViewById(R.id.changePass);

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidRequest())
                    changePassword();
            }


        });

    }

    private boolean isValidRequest() {
        boolean isValid = false;
        String currentPass = etCurrentPswrd.getText().toString().trim();
        String etNewPass = etNewPswrd.getText().toString().trim();
        String etConfirmPass = etConfirmPswrd.getText().toString().trim();

        if (!TextUtils.isEmpty(currentPass) && !TextUtils.isEmpty(etNewPass) && !TextUtils.isEmpty(etConfirmPass)) {
            if (!currentPass.equals(etNewPass) && etNewPass.equals(etConfirmPass)) {
                isValid = true;
            } else if (currentPass.equals(etNewPass)) {
                Utilities.dialogdisplay("Invalid Entry", "New Password should not be same as Current Password", getActivity());
            } else if (!etNewPass.equals(etConfirmPass)) {
                Utilities.dialogdisplay("Invalid Entry", "New Password and  Confirm Password should be same.", getActivity());
            }
        } else if (TextUtils.isEmpty(currentPass)) {
            Utilities.dialogdisplay("Invalid Entry", "Current Password filed should not be empty.", getActivity());
        } else if (TextUtils.isEmpty(etNewPass)) {
            Utilities.dialogdisplay("Invalid Entry", "New Password filed should not be empty.", getActivity());
        } else if (TextUtils.isEmpty(etConfirmPass)) {
            Utilities.dialogdisplay("Invalid Entry", "Confirm Password filed should not be empty.", getActivity());
        }

        return isValid;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).updateHeaderText("Change Password", mActivity);
        MyApplication.getInstance().trackScreenView("Change Password Screen");
    }


    public void changePassword() {
        try {
            if (Utilities.isNetworkAvailable(getActivity())) {
                Utilities.showProgressDialog(getActivity());
                encodedString = JsonHeaders.createChangePasswordApiJSONHeader(getActivity(), etCurrentPswrd.getText().toString(), etNewPswrd.getText().toString());
                JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                        APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d("Success", response.toString());
                        try {
                            Utilities.hideProgressDialog();
                            if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("000")) {
                                dialogdisplay("Password Updated", "Your password has been changed succesfully.");
                            } else if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("122")) {
                                Utilities.dialogdisplay("Invalid Credentials", "Your old password is invalid.Please try agian with valid crenditials.", getActivity());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utilities.hideProgressDialog();
                        Utilities.dialogdisplay("Network Error", "Please Try Again", getActivity());
                        VolleyLog.d("Parsing ", "Error: " + error.getMessage());

                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjReq, tag_change_password);
            }
        } catch (Exception e) {
        }
    }

    public void dialogdisplay(String tilte, String Message) {
        final Dialog customDialog = new Dialog(getActivity());
        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_view, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        TextView message = (TextView) dialogView.findViewById(R.id.tv_DialogMessage);
        title.setText(tilte);
        message.setText(Message);
        Button yes = (Button) dialogView.findViewById(R.id.btn_yes);
        Button no = (Button) dialogView.findViewById(R.id.btn_no);
        yes.setText("Ok");
        no.setVisibility(View.GONE);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
                customDialog.dismiss();
            }
        });
        customDialog.show();

    }

}
