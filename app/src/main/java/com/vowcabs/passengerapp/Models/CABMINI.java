package com.vowcabs.passengerapp.Models;

import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sumanth on 07/09/16.
 */
public class CABMINI {
    public String vehicleName;
    public String vehicleCategory;
    public String rMODTIME;
    public String baseKm;
    public String nightBaseKm;
    public String baseFare;
    public String nightBaseFare;
    public String waitingCharge;
    public String nightWaitingCharge;
    public String farePerKm;
    public String nightFarePerKm;


    public String getrMODTIME() {
        return rMODTIME;
    }

    public void setrMODTIME(String rMODTIME) {
        this.rMODTIME = rMODTIME;
    }

    public String getBaseKm() {
        return baseKm;
    }

    public void setBaseKm(String baseKm) {
        this.baseKm = baseKm;
    }

    public String getNightBaseKm() {
        return nightBaseKm;
    }

    public void setNightBaseKm(String nightBaseKm) {
        this.nightBaseKm = nightBaseKm;
    }

    public String getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(String baseFare) {
        this.baseFare = baseFare;
    }

    public String getNightBaseFare() {
        return nightBaseFare;
    }

    public void setNightBaseFare(String nightBaseFare) {
        this.nightBaseFare = nightBaseFare;
    }

    public String getFarePerKm() {
        return farePerKm;
    }

    public void setFarePerKm(String farePerKm) {
        this.farePerKm = farePerKm;
    }

    public String getNightFarePerKm() {
        return nightFarePerKm;
    }

    public void setNightFarePerKm(String nightFarePerKm) {
        this.nightFarePerKm = nightFarePerKm;
    }

    public String getWaitingCharge() {
        return waitingCharge;
    }

    public void setWaitingCharge(String waitingCharge) {
        this.waitingCharge = waitingCharge;
    }

    public String getNightWaitingCharge() {
        return nightWaitingCharge;
    }

    public void setNightWaitingCharge(String nightWaitingCharge) {
        this.nightWaitingCharge = nightWaitingCharge;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getVehicleCategory() {
        return vehicleCategory;
    }

    public void setVehicleCategory(String vehicleCategory) {
        this.vehicleCategory = vehicleCategory;
    }

    public void setCabInfo(JSONObject cabInfo) {
        try {
            if (!cabInfo.isNull("VehicleName") && !TextUtils.isEmpty(cabInfo.getString("VehicleName"))) {
                setVehicleName(cabInfo.getString("VehicleName").trim());
            }
            if (!cabInfo.isNull("VehicleCategory") && !TextUtils.isEmpty(cabInfo.getString("VehicleCategory"))) {
                setVehicleCategory(cabInfo.getString("VehicleCategory").trim());
            }
            if (!cabInfo.isNull("BaseKm") && !TextUtils.isEmpty(cabInfo.getString("BaseKm"))) {
                setBaseKm(cabInfo.getString("BaseKm").trim());
            }
            if (!cabInfo.isNull("NightBaseKm") && !TextUtils.isEmpty(cabInfo.getString("NightBaseKm"))) {
                setNightBaseKm(cabInfo.getString("NightBaseKm").trim());
            }
            if (!cabInfo.isNull("BaseFare") && !TextUtils.isEmpty(cabInfo.getString("BaseFare"))) {
                setBaseFare(cabInfo.getString("BaseFare").trim());
            }
            if (!cabInfo.isNull("NightBaseFare") && !TextUtils.isEmpty(cabInfo.getString("NightBaseFare"))) {
                setNightBaseFare(cabInfo.getString("NightBaseFare").trim());
            }
            if (!cabInfo.isNull("FarePerKm") && !TextUtils.isEmpty(cabInfo.getString("FarePerKm"))) {
                setFarePerKm(cabInfo.getString("FarePerKm").trim());
            }
            if (!cabInfo.isNull("NightFarePerKm") && !TextUtils.isEmpty(cabInfo.getString("NightFarePerKm"))) {
                setNightFarePerKm(cabInfo.getString("NightFarePerKm").trim());
            }
            if (!cabInfo.isNull("WaitingCharge") && !TextUtils.isEmpty(cabInfo.getString("WaitingCharge"))) {
                setWaitingCharge(cabInfo.getString("WaitingCharge").trim());
            }
            if (!cabInfo.isNull("NightWaitingCharge") && !TextUtils.isEmpty(cabInfo.getString("NightWaitingCharge"))) {
                setNightWaitingCharge(cabInfo.getString("NightWaitingCharge").trim());
            }
            if (!cabInfo.isNull("RMODTIME") && !TextUtils.isEmpty(cabInfo.getString("RMODTIME"))) {
                setrMODTIME(cabInfo.getString("RMODTIME").trim());
            }

        } catch (JSONException e) {
        }
    }

}
