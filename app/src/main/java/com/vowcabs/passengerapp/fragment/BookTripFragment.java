package com.vowcabs.passengerapp.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.AppController;
import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.Models.FavouritePlace;
import com.vowcabs.passengerapp.Models.PackageBooking;
import com.vowcabs.passengerapp.Models.RecentPlace;
import com.vowcabs.passengerapp.MyApplication;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.adapter.AutoCompleteAdapter;
import com.vowcabs.passengerapp.adapter.GooglePlacesAutocompleteAdapter;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.encryption.URLUTF8Encoder;
import com.vowcabs.passengerapp.json.JSONKEYS;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.DateTime;
import com.vowcabs.passengerapp.utils.DateTimePicker.OnDateTimeSetListener;
import com.vowcabs.passengerapp.utils.DistanceCalculator;
import com.vowcabs.passengerapp.utils.LocationInstance;
import com.vowcabs.passengerapp.utils.SimpleDateTimePicker;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.R.id.message;

@SuppressLint("ValidFragment")
public class BookTripFragment extends BaseFragment implements OnClickListener {
    AutoCompleteTextView sourceLocationText;
    AutoCompleteTextView destinationLocationText;
    EditText userName;
    EditText phoneNumber;
    EditText emailId;
    TextView dateTime;
    String selectedDateTime = "";
    ImageView timeImage;
    ImageView contactsIcon;
    public String bookingSource;
    public String bookingDestination;
    String bookingSrcLat;
    String bookingSrcLng;
    String bookingDstLat;
    String bookingDstLng;
    float estimatedDistance = -1;
    public ScrollView bookingScreenScroller;
    RelativeLayout passengerDetails;
    RelativeLayout dateTimeLayout;
    LinearLayout accountDetails;
    TextView carTypeSpinner;
    TextView travellerDetails;
    TextView createBooking;
    TextView paymentOptions;
    boolean isBookNow = false;
    boolean isReturnBooking = false;
    boolean isRepeatBooking = false;
    boolean isOutStaion = false;
    boolean isTakeMeHome = false;
    RelativeLayout carTypeLayout;
    PackageBooking selectedPackage;
    private String tag_get_packages = "getpackages_obj";
    String encodedString;
    AlertDialog builder;
    boolean isValidTripDetails = false;
    String carType[] = {"Mini", "Sedan", "Prime", "Auto"};
    ArrayList<PackageBooking> packageDetailsListMini = null;
    ArrayList<PackageBooking> packageDetailsListSedan = null;
    ArrayList<PackageBooking> packageDetailsListPrime = null;
    ArrayList<PackageBooking> packageDetailsListAuto = null;
    CheckBox packageSelection, airportBooking, returnTripSelection;
    TextView packageSelectionName = null;
    RelativeLayout rlPackageDetails, rlAirportPickup, rlOutstation;
    ArrayList<String> airportFare;
    String packageId = "0";
    String packageHours = "";
    String packageEstimateFare = "";
    int selectedPaymetMode = -1; // default option Cash

    public LinearLayout sourceDestinationPicker;
    private String tag_req_recentplaces = "jobj_req_recentplaces";
    ListView recentPlacesListView;
    Activity mActivity = null;
    GooglePlacesAutocompleteAdapter mGooglePlacesAutocompleteAdapter = null;
    ArrayList<RecentPlace> placesList = new ArrayList<RecentPlace>();
    ListView favouritePlacesListView;
    ArrayList<FavouritePlace> favouritePlacesList = new ArrayList<FavouritePlace>();
    AutoCompleteTextView autoCompView = null;
    private RadioButton paymentCash = null;
    private RadioButton paymentVowCash = null;
    private RadioButton paymentmCash = null;
    AlertDialog paymentSelectionDialog;
    String approximateFare = "";


    public BookTripFragment(boolean isBookNow, boolean isoutStaion, double srcLat, double srcLng) {
        this.isBookNow = isBookNow;
        this.isOutStaion = isoutStaion;
        this.bookingSource = LocationInstance.getInstance().getCurrentLocation();
        this.bookingSrcLat = String.valueOf(srcLat);
        this.bookingSrcLng = String.valueOf(srcLng);
    }

    public BookTripFragment(String isReturnOrRepeatBooking, String source, String destination, String srcLat, String srcLng, String dstLat, String dstLng) {
        this.bookingSource = source;
        this.bookingDestination = destination;
        this.bookingSrcLat = srcLat;
        this.bookingSrcLng = srcLng;
        this.bookingDstLat = dstLat;
        this.bookingDstLng = dstLng;

        if (!TextUtils.isEmpty(source) && !TextUtils.isEmpty(destination)) {
            getDistanceApiCall(source, destination, "", false);
        }
        if (isReturnOrRepeatBooking.equalsIgnoreCase("Return")) {
            this.isReturnBooking = true;
        } else if (isReturnOrRepeatBooking.equalsIgnoreCase("Repeat")) {
            this.isRepeatBooking = true;
        } else if (isReturnOrRepeatBooking.equalsIgnoreCase("TAKEMEHOME")) {
            this.isTakeMeHome = true;
            this.isBookNow = true;
        }
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        ((HomeActivity) getActivity()).topBar.setVisibility(View.GONE);
        MyApplication.getInstance().trackScreenView("Book Trip screen");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Add your code here
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.booktrip, container, false);
        bookingScreenScroller = (ScrollView) view.findViewById(R.id.sv_bookingScreen);
        userName = (EditText) view.findViewById(R.id.user_name);
        phoneNumber = (EditText) view.findViewById(R.id.phone_number);
        emailId = (EditText) view.findViewById(R.id.email);
        passengerDetails = (RelativeLayout) view.findViewById(R.id.passenger_details);
        accountDetails = (LinearLayout) view.findViewById(R.id.userdetail_layout);
        contactsIcon = (ImageView) view.findViewById(R.id.profile_page);
        timeImage = (ImageView) view.findViewById(R.id.time_image);
        dateTimeLayout = (RelativeLayout) view.findViewById(R.id.rl_date_time);
        dateTime = (TextView) view.findViewById(R.id.date_time);
        carTypeSpinner = (TextView) view.findViewById(R.id.cartype_spinner);
        carTypeLayout = (RelativeLayout) view.findViewById(R.id.car_typeLayout);
        travellerDetails = (TextView) view.findViewById(R.id.tv_traveller_details);
        createBooking = (TextView) view.findViewById(R.id.processes_button);
        paymentOptions = (TextView) view.findViewById(R.id.payment_Options);
        packageSelection = (CheckBox) view.findViewById(R.id.cb_packagetype);
        airportBooking = (CheckBox) view.findViewById(R.id.cb_airport);
        returnTripSelection = (CheckBox) view.findViewById(R.id.cb_outstation);
        packageSelectionName = (TextView) view.findViewById(R.id.tv_packagetype);
        rlPackageDetails = (RelativeLayout) view.findViewById(R.id.rl_package_details);
        rlOutstation = (RelativeLayout) view.findViewById(R.id.rl_outstation);
        rlAirportPickup = (RelativeLayout) view.findViewById(R.id.rl_aiport_pickup);
        sourceDestinationPicker = (LinearLayout) view.findViewById(R.id.ll_source_destination);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        sourceLocationText = (AutoCompleteTextView) getView().findViewById(R.id.act_Source);
        destinationLocationText = (AutoCompleteTextView) getView().findViewById(R.id.act_destination);
        sourceLocationText.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item_autocomplete, sourceLocationText));
        //destinationLocationText.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item_autocomplete,destinationLocationText));
        sourceLocationText.setOnItemClickListener(sourceLocationListener);
        sourceLocationText.setOnItemClickListener(destinationLocationListener);
        sourceLocationText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (getActivity() == null)
                    return false;
                sourceLocationText.setFocusable(true);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(autoCompView, InputMethodManager.SHOW_IMPLICIT);
                return false;
            }
        });
        destinationLocationText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (getActivity() == null)
                    return false;

                sourceDestinationPicker.setVisibility(View.VISIBLE);
                bookingScreenScroller.setVisibility(View.GONE);
                autoCompView = (AutoCompleteTextView) getView().findViewById(R.id.d_autoCompleteTextView);
                favouritePlacesListView = (ListView) getView().findViewById(R.id.lv_favouritePlaces);
                recentPlacesListView = (ListView) getView().findViewById(R.id.lv_recentPlaces);
                getRecentPlaces();
                mGooglePlacesAutocompleteAdapter = new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item_autocomplete, destinationLocationText);
                autoCompView.setAdapter(mGooglePlacesAutocompleteAdapter);
                autoCompView.setText("");
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(autoCompView, InputMethodManager.SHOW_IMPLICIT);
                autoCompView.setOnItemClickListener(destinationLocationListener);
                return false;
            }
        });
        passengerDetails.setOnClickListener(this);
        dateTimeLayout.setOnClickListener(this);
        //carTypeLayout.setOnClickListener(this);
        createBooking.setOnClickListener(this);
        paymentOptions.setOnClickListener(this);
        packageSelection.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (airportBooking.isChecked()) {
                    packageSelection.setChecked(false);
                    return;
                }
                if (isChecked) {

                    if (packageDetailsListMini != null && packageDetailsListPrime != null && packageDetailsListPrime != null && packageDetailsListAuto != null) {
                        showPackageSelectionDialog();
                    } else {
                        Utilities.showProgressDialog(getActivity());
                        getPackageDetailsApiCall();
                    }

                } else {
                    packageSelectionName.setText("Choose Package");
                    packageId = "0";
                    packageHours = "";
                    LocationInstance.getInstance().setCurrentSelectedPackage("");
                }
            }
        });
        returnTripSelection.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (isOutStaion) {
                        showOutStationDialog();
                    }
                }
            }
        });
//        airportBooking.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (packageSelection.isChecked()) {
//                    airportBooking.setChecked(false);
//                    return;
//                }
//                if (isChecked) {
//                    showAirpotPickupDialog();
//                } else {
//                    sourceLocationText.setText("");
//                    bookingSrcLat = "";
//                    bookingSrcLng = "";
//                }
//
//            }
//        });

        loadUIFromPersistantData();

        if (isTakeMeHome && isBookNow) {
            dateTime.setText("Earliest Available");
            rlPackageDetails.setVisibility(View.VISIBLE);
            sourceLocationText.setText(bookingSource);
            destinationLocationText.setText(bookingDestination);
            initiateBooking();
        }

        if (isBookNow) {
            dateTime.setText("Earliest Available");
            rlPackageDetails.setVisibility(View.VISIBLE);

            if (LocationInstance.getInstance().getSelectedCar() == 3) {
                rlPackageDetails.setVisibility(View.GONE);
                rlAirportPickup.setVisibility(View.GONE);
                carTypeLayout.setVisibility(View.GONE);
            }
        } else {
            dateTime.setText("Select Date & Time");
        }
        if (isOutStaion) {
            dateTime.setText("Select Date & Time");
            //dateTime.setText("Earliest Available (2 Hours from now.)");
            rlPackageDetails.setVisibility(View.GONE);
            rlAirportPickup.setVisibility(View.GONE);
            paymentOptions.setVisibility(View.GONE);
            rlOutstation.setVisibility(View.VISIBLE);
            selectedPaymetMode = 0;
        }

        getPackageDetailsApiCall();

        if (isOutStaion)
            Utilities.dialogdisplay("OutStation Booking", "Now book OutStation with Vow and get a cab with in 2 Hours.", getActivity());
    }
//
//
//    private void setBookingButtonEnabled(boolean value)
//    {
//        bookingRequestInitiated = value;
//        //+Enable / Disable the button of Booking//
//
//    }

    private void loadUIFromPersistantData() {
        try {
            if (!TextUtils.isEmpty(Utilities.getSharedString("EMAILID", getActivity()))) {
                emailId.setText(Utilities.getSharedString("EMAILID", getActivity()));
            }

            if (!TextUtils.isEmpty(Utilities.getSharedString("MOBILE", getActivity()))) {
                phoneNumber.setText(Utilities.getSharedString("MOBILE", getActivity()));
            }

            if (!TextUtils.isEmpty(Utilities.getSharedString("USERNAME", getActivity()))) {
                userName.setText(Utilities.getSharedString("USERNAME", getActivity()));
            }
            carTypeSpinner.setText(carType[LocationInstance.getInstance().getSelectedCar()]);
            if (!TextUtils.isEmpty(Utilities.getSharedString("USERNAME", getActivity()))
                    && !TextUtils.isEmpty(Utilities.getSharedString("MOBILE", getActivity()))) {
                travellerDetails.setText(" " + Utilities.getSharedString("USERNAME", getActivity())
                        + "(" + Utilities.getSharedString("MOBILE", getActivity()) + ")");
            }

            if (!isRepeatBooking && !isReturnBooking && !TextUtils.isEmpty(bookingSource)) {
                sourceLocationText.setText(bookingSource);
            } else {
                sourceLocationText.setText(bookingSource);
            }

            if (isRepeatBooking || isReturnBooking) {
                destinationLocationText.setText(bookingDestination);
                SimpleDateTimePicker.make("Choose pickup time", new Date(), listener, getFragmentManager(), isOutStaion).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    OnDateTimeSetListener listener = new OnDateTimeSetListener() {
        @Override
        public void DateTimeSet(Date date) {
            // This is the DateTime class we created earlier to handle the conversion
            // of Date to String Format of Date String Format to Date object
            DateTime mDateTime = new DateTime(date);
            // Show in the LOGCAT the selected Date and Time
            // Log.v("TEST_TAG", "Date and Time selected: " + mDateTime.getDateString());
            //
            selectedDateTime = mDateTime.getDateString();
            dateTime.setText(mDateTime.getDateString());

        }
    };

    OnItemClickListener sourceLocationListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String str = (String) parent.getItemAtPosition(position);
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(destinationLocationText.getWindowToken(), 0);
            bookingSource = str;
            getGeoLatLngValues(str, 0);
        }
    };

    OnItemClickListener destinationLocationListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String address = (String) parent.getItemAtPosition(position);
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(destinationLocationText.getWindowToken(), 0);
            bookingDestination = address;
            getGeoLatLngValues(address, 1);
            if (sourceDestinationPicker != null)
                sourceDestinationPicker.setVisibility(View.GONE);

            if (bookingScreenScroller != null)
                bookingScreenScroller.setVisibility(View.VISIBLE);


            if (destinationLocationText != null)
                destinationLocationText.setText(address);
            if (!TextUtils.isEmpty(bookingSource) && !TextUtils.isEmpty(bookingDestination)) {
                getDistanceApiCall(bookingSource, bookingDestination, "", false);
            }
        }
    };

    @Override
    public void onClick(View v) {
        if (v == passengerDetails) {
            if (accountDetails.getVisibility() == View.GONE) {
                accountDetails.setVisibility(View.VISIBLE);
                travellerDetails.setText(" Traveller Details");
                contactsIcon.setImageResource(R.drawable.right_arrow);
            } else {
                if (accountDetails.getVisibility() == View.VISIBLE) {
                    accountDetails.setVisibility(View.GONE);
                    travellerDetails.setText(" " + userName.getText().toString() + " "
                            + "(" + phoneNumber.getText().toString() + ")");
                    contactsIcon.setImageResource(R.drawable.expand_arrow);
                }
            }
        } else if (v == dateTimeLayout) {
            if (!isBookNow) {
                SimpleDateTimePicker.make("Choose pickup time", new Date(), listener, getFragmentManager(), isOutStaion).show();
            }
        } else if (v == carTypeLayout) {
            showCarSelectionDialog();
        } else if (v == createBooking) {
            initiateBooking();
        } else if (v == paymentOptions) {
            if (checkValidDetails()) {
                showPaymentSelectionDialog();
            }
        }
    }

    public void initiateBooking() {
        if (checkValidDetails())
        {
            if(validatePaymentOption()) {

                if (!bookingRequestInitiated) {
                    constructCreateBookingApi();
                }
            }
        }
    }

    static Dialog customDialog = null;

    private void showPackageSelectionDialog() {

        if (customDialog != null && customDialog.isShowing())
            customDialog.dismiss();

        customDialog = new Dialog(getActivity());
        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.package_selection_dialog, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        final TextView estimatedFare = (TextView) dialogView.findViewById(R.id.tv_estimatedValue);
        TextView okButton = (TextView) dialogView.findViewById(R.id.tv_ok_button);

        //ListView packageListView = (ListView) dialogView.findViewById(R.id.packages_list);
        title.setText("Choose Package");
        final NumberPicker np;
        np = (NumberPicker) dialogView.findViewById(R.id.numberPicker);

        np.setFormatter(new NumberPicker.Formatter() {

            @Override
            public String format(int value) {
                // TODO Auto-generated method stub
                return value + " Hours";
            }
        });

        np.setMinValue(2);
        np.setMaxValue(10);
        np.setWrapSelectorWheel(false);
        np.invalidate();
        try {
            Method method = np.getClass().getDeclaredMethod("changeValueByOne", boolean.class);
            method.setAccessible(true);
            method.invoke(np, true);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }


        Utilities.setNumberPickerTextColor(np, getActivity().getResources().getColor(R.color.custom_black));

        try {
            if (LocationInstance.getInstance().getSelectedCar() == 0) {
                selectedPackage = packageDetailsListMini.get(0);
            } else if (LocationInstance.getInstance().getSelectedCar() == 1) {
                selectedPackage = packageDetailsListSedan.get(0);
            } else if (LocationInstance.getInstance().getSelectedCar() == 2) {
                selectedPackage = packageDetailsListPrime.get(0);
            } else if (LocationInstance.getInstance().getSelectedCar() == 3) {
                selectedPackage = packageDetailsListAuto.get(0);
            }
        } catch (Exception e) {
        }

        try {
            int farePerHour = Integer.valueOf(selectedPackage.getPackagePrice()) / Integer.valueOf(selectedPackage.getPackageHours());
            estimatedFare.setText("Rs. " + farePerHour * 2);
            packageEstimateFare = String.valueOf(farePerHour * 2);
        } catch (Exception e) {
        }
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // TODO Auto-generated method stub
                String Old = "Old Value : ";
                String New = "New Value : ";

                try {
                    int farePerHour = Integer.valueOf(selectedPackage.getPackagePrice()) / Integer.valueOf(selectedPackage.getPackageHours());
                    estimatedFare.setText("Rs. " + farePerHour * newVal);
                    packageEstimateFare = String.valueOf(farePerHour * newVal);
                } catch (Exception e) {
                }

            }
        });

        okButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (np != null) {
                    packageHours = "" + np.getValue(); //+ " Hours";
                }
                if (!TextUtils.isEmpty(selectedPackage.getPackageId())) {
                    packageId = selectedPackage.getPackageId();
                    packageSelectionName.setText(np.getValue()+" Hours  "+estimatedFare.getText());

                }
                if (customDialog != null && customDialog.isShowing())
                    customDialog.dismiss();


            }
        });

        customDialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                //Log.d("Dialog ", "Cancelled");
                packageSelection.setChecked(false);
                packageHours = "";
                packageEstimateFare = "";

            }
        });
        customDialog.show();
    }

    public void showOutStationDialog() {
        if (customDialog != null && customDialog.isShowing())
            customDialog.dismiss();
        customDialog = new Dialog(getActivity());
        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.outstation_selection_dialog, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        TextView okButton = (TextView) dialogView.findViewById(R.id.tv_ok_button);

        okButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customDialog != null && customDialog.isShowing())
                    customDialog.dismiss();
            }
        });

        title.setText("Choose Number of Days");
        NumberPicker nPicker = (NumberPicker) dialogView.findViewById(R.id.np_Picker);
        nPicker.setMinValue(1);
        nPicker.setMaxValue(10);
        nPicker.setWrapSelectorWheel(false);

        Utilities.setNumberPickerTextColor(nPicker, getActivity().getResources().getColor(R.color.custom_black));
        customDialog.show();
    }

//    private void showAirpotPickupDialog() {
//        if (LocationInstance.getInstance() == null)
//            return;
//
//        if (LocationInstance.getInstance().getCountryCode().equalsIgnoreCase("lk")) {
//            bookingSrcLat = BaseConstants.colomboAirportLat;
//            bookingSrcLng = BaseConstants.colomboAirportLng;
//            aiport = BaseConstants.colomboAirport;
//        } else {
//            bookingSrcLat = BaseConstants.mangaloreAirportLat;
//            bookingSrcLng = BaseConstants.mangaloreAirportLng;
//            aiport = BaseConstants.mangaloreAirport;
//        }
//        builder = new AlertDialog.Builder(getActivity())
//                .setTitle("Airport Booking")
//                .setMessage("Airport Pickup up to City" + " " + airportFare.get(LocationInstance.getInstance().getSelectedCar()) + " +Tax")
//                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        sourceLocationText.setText(aiport);
//                    }
//
//                })
//                .setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        builder.dismiss();
//                        airportBooking.setChecked(false);
//                    }
//                }).create();
//        builder.setOnCancelListener(new OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                airportBooking.setChecked(false);
//                bookingSrcLat = "";
//                bookingSrcLng = "";
//                aiport = "";
//            }
//        });
//
//        builder.show();
//    }


    public void setSelectedPackageTitle(final String selectedTitle) {
        packageSelectionName.setText(selectedTitle);
        if (customDialog != null)
            customDialog.dismiss();
    }

    private boolean checkValidDetails() {
        isValidTripDetails = false;
        boolean isValidSource = false;
        boolean isValidDestination = false;
        boolean isValidDate = false;
        boolean isValidName = false;
        boolean isValidMobile = false;
        boolean isValidEmail = false;
        boolean isValidPaymentMode = false;

        String message = "";

        if (sourceLocationText != null && !TextUtils.isEmpty(sourceLocationText.getText().toString())) {
            isValidSource = true;
        } else if (!TextUtils.isEmpty(bookingSource)) {
            isValidSource = true;
        } else {
            message = message + "Enter Source Location\n";
        }

        if (destinationLocationText != null && !TextUtils.isEmpty(destinationLocationText.getText().toString())) {
            isValidDestination = true;
        } else if (!TextUtils.isEmpty(bookingDestination)) {
            isValidDestination = true;
        } else {
            message = message + "Enter Destination Location\n";
        }

        if (isBookNow) {
            isValidDate = true;
        } else if (!isBookNow && !TextUtils.isEmpty(selectedDateTime) && isValidDate(selectedDateTime)) {
            isValidDate = true;
        } else {
            message = message + "Enter Booking  Date\n";
        }

        if (!TextUtils.isEmpty(Utilities.getSharedString("EMAILID", getActivity())) || !TextUtils.isEmpty(emailId.getText().toString())) {
            isValidEmail = true;
        } else {
            message = message + "Enter Valid Email\n";
        }

        if (!TextUtils.isEmpty(Utilities.getSharedString("MOBILE", getActivity())) || !TextUtils.isEmpty(phoneNumber.getText().toString())) {
            isValidMobile = true;
        } else {
            message = message + "Enter Mobile Number\n";
        }

        if (!TextUtils.isEmpty(Utilities.getSharedString("USERNAME", getActivity())) || !TextUtils.isEmpty(userName.getText().toString())) {
            isValidName = true;
        } else {
            message = message + "Enter Name of the Customer\n";
        }

       /* if (selectedPaymetMode != -1) {
            isValidPaymentMode = true;
        } else {
            message = message + "Select Payment Mode\n";
        }
*/
        if (isValidSource && isValidEmail && isValidName && isValidDestination && isValidMobile && isValidDate) {
            isValidTripDetails = true;
        } else {
            Utilities.dialogdisplay("Invalid Input", message, getActivity());
        }


        return isValidTripDetails;
    }


    public boolean validatePaymentOption()
    {

        isValidTripDetails = false;
        boolean isValidPaymentMode = false;

        String message = "";


        if (selectedPaymetMode != -1) {
            isValidPaymentMode = true;
        } else {
            message = message + "Select Payment Mode\n";
        }
        if (isValidPaymentMode) {
            isValidTripDetails = true;
        } else {
            Utilities.dialogdisplay("Invalid Input", message, getActivity());
        }
        return isValidTripDetails;
    }



    public boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm aa |  c, dd LLL yyyy");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }

    public void showCarSelectionDialog() {
        builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_DARK)
                .setTitle(R.string.alert_dialog_select_car)
                .setSingleChoiceItems(carType, LocationInstance.getInstance().getSelectedCar(), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                /* User clicked on a radio button do some stuff */
                        //Log.d("Selected Position onClick" , whichButton+"");
                        carTypeSpinner.setText(carType[whichButton]);
                        LocationInstance.getInstance().setSelectedCar(whichButton);
                        builder.cancel();
                    }
                }).create();
        builder.show();
    }

    Response.Listener<JSONObject> sourceApiListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            //Log.d("Geocoded results", response.toString());
            try {
                Utilities.hideProgressDialog();
                JSONArray results = response.getJSONArray("results");
                JSONObject resultsObject = results.getJSONObject(0);
                JSONObject geometryObject = resultsObject.getJSONObject("geometry");
                JSONObject locationObject = geometryObject.getJSONObject("location");

                bookingSrcLat = locationObject.getString("lat");
                bookingSrcLng = locationObject.getString("lng");
                //String placeId = resultsObject.getString("place_id");

            } catch (Exception e) {
                e.printStackTrace();
                Utilities.hideProgressDialog();
                Utilities.dialogdisplay("Network Error", "Please Try Again", getActivity());
            }
        }
    };

    Response.Listener<JSONObject> destinationApiListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            //Log.d("Geocoded results", response.toString());
            try {
                Utilities.hideProgressDialog();
                JSONArray results = response.getJSONArray("results");
                JSONObject resultsObject = results.getJSONObject(0);
                JSONObject geometryObject = resultsObject.getJSONObject("geometry");
                JSONObject locationObject = geometryObject.getJSONObject("location");

                bookingDstLat = locationObject.getString("lat");
                bookingDstLng = locationObject.getString("lng");

                //String placeId = resultsObject.getString("place_id");

            } catch (Exception e) {
                e.printStackTrace();
                Utilities.hideProgressDialog();
                Utilities.dialogdisplay("Network Error", "Please Try Again", getActivity());
            }
        }
    };

    public void getGeoLatLngValues(String address, final int textType) {
        Response.Listener<JSONObject> listener = null;
        if (textType == 0)
            listener = sourceApiListener;
        else
            listener = destinationApiListener;

        try {
            if (Utilities.isNetworkAvailable(getActivity())) {
                Utilities.showProgressDialog(getActivity());
                String finalUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=" +
                        address.replace(" ", "%20") +
                        "&key=" + getResources().getString(R.string.google_places_server_key);
                JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                        finalUrl, null, listener, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utilities.hideProgressDialog();
                        Utilities.dialogdisplay("Network Error", "Please Try Again", getActivity());
                        VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjReq, "geo_coding_api");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void constructCreateBookingApi() {
        if (getActivity() == null)
            return;

        String bookingTime = "";
        if (isBookNow)  // adding 15 mins to the booking time.
        {
            Calendar calNow = Calendar.getInstance();
            calNow.add(Calendar.MINUTE, 15);
            DateTime mDateTime = new DateTime(
                    calNow.get(Calendar.YEAR),
                    calNow.get(Calendar.MONTH),
                    calNow.get(Calendar.DAY_OF_MONTH),
                    calNow.get(Calendar.HOUR_OF_DAY),
                    calNow.get(Calendar.MINUTE)
            );
            bookingTime = mDateTime.getDateString();
        } else {
            bookingTime = selectedDateTime;
        }

        if (!TextUtils.isEmpty(bookingSrcLat) && !TextUtils.isEmpty(bookingSrcLng) && !TextUtils.isEmpty(bookingDstLat) && !TextUtils.isEmpty(bookingDstLng)) {
            try {

                float tripDistance;
                if (estimatedDistance != -1) {
                    tripDistance = estimatedDistance;
                } else {
                    tripDistance = DistanceCalculator.getInstance().calculateDistance(Double.parseDouble(bookingSrcLat), Double.parseDouble(bookingSrcLng),
                            Double.parseDouble(bookingDstLat), Double.parseDouble(bookingDstLng));
                }
                encodedString = JsonHeaders.createNewBookingApiJSONHeader(getActivity(), userName.getText().toString(),
                        phoneNumber.getText().toString(), emailId.getText().toString(),
                        bookingTime, sourceLocationText.getText().toString(),
                        destinationLocationText.getText().toString(), String.valueOf(LocationInstance.getInstance().getSelectedCar()), packageSelection.isChecked(), bookingSrcLat,
                        bookingSrcLng, bookingDstLat, bookingDstLng, isBookNow, isOutStaion, packageId, packageHours, selectedPaymetMode,  LocationInstance.getInstance().getCountryCode());
                if (!bookingRequestInitiated) {
                    if (!Utilities.isPointInRegion(Double.parseDouble(bookingSrcLat), Double.parseDouble(bookingSrcLng))) {
                        Utilities.dialogdisplay("Service Not Available", "Sorry for the inconvenience, Currently our services are not available in your area, please choose a different Pick Up Location.", getActivity());
                        bookingRequestInitiated = false;
                    } else if (tripDistance > 40000 && tripDistance < 100000 && !packageSelection.isChecked()) {
                        Utilities.dialogdisplay("Package", "For selected drop location please select Package Booking.", getActivity());
                        bookingRequestInitiated = false;
                    } else if (!isOutStaion && tripDistance > 100000) {
                        Utilities.dialogdisplay("OutStaion", "For selected drop location please select Outstation Booking.", getActivity());
                        bookingRequestInitiated = false;
                    } else {
                        if (isTakeMeHome) {

                            bookingRequestInitiated = true;
                            new BookTrip(encodedString, false).execute();
                        } else {
                            if (selectedPaymetMode == 2) {
                                BookTrip newBooking = new BookTrip(encodedString, true);
                                Utilities.displayMobitelBookingDialog(newBooking, getActivity());
                            } else {
                                bookingRequestInitiated = true;
                                new BookTrip(encodedString, false).execute();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                bookingRequestInitiated = false;
            }
        } else if (TextUtils.isEmpty(bookingSrcLat) && TextUtils.isEmpty(bookingSrcLng)) {
            Utilities.dialogdisplay("Invalid Source", "Please select a valid source location.", getActivity());
            bookingRequestInitiated = false;
        } else if (TextUtils.isEmpty(bookingDstLat) && TextUtils.isEmpty(bookingDstLng)) {
            Utilities.dialogdisplay("Invalid Destination", "Please select a valid destination location.", getActivity());
            bookingRequestInitiated = false;
        }
    }

    boolean bookingRequestInitiated = false;


    private void getPackageDetailsApiCall() {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            encodedString = JsonHeaders.createPackageDetailsApiJSONHeader(getActivity());
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET, APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    packageDetailsListMini = new ArrayList<PackageBooking>();
                    packageDetailsListSedan = new ArrayList<PackageBooking>();
                    packageDetailsListPrime = new ArrayList<PackageBooking>();
                    packageDetailsListAuto = new ArrayList<PackageBooking>();
                    airportFare = new ArrayList<String>();

                    try {
                        if (response.getJSONArray("PACKAGES").length() > 0) {
                            for (int index = 0; index < response.getJSONArray("PACKAGES").length(); index++) {
                                PackageBooking newPackage = new PackageBooking(response.getJSONArray("PACKAGES").getJSONObject(index));

                                if (newPackage.getPackageVehicleType().equals("0")) {
                                    if (newPackage.getPackageType().equals("R")) {
                                        packageDetailsListMini.add(newPackage);
                                    } else if (!TextUtils.isEmpty(newPackage.getPackageId()) && newPackage.getPackageType().equals("OS") && isOutStaion && LocationInstance.getInstance().getSelectedCar() == 0) {
                                        packageId = newPackage.getPackageId();
                                    }

                                } else if (newPackage.getPackageVehicleType().equals("1")) {

                                    if (newPackage.getPackageType().equals("R")) {
                                        packageDetailsListSedan.add(newPackage);
                                    } else if (!TextUtils.isEmpty(newPackage.getPackageId()) && newPackage.getPackageType().equals("OS") && isOutStaion && LocationInstance.getInstance().getSelectedCar() == 1) {
                                        packageId = newPackage.getPackageId();
                                    }
                                } else if (newPackage.getPackageVehicleType().equals("2")) {
                                    if (newPackage.getPackageType().equals("R")) {
                                        packageDetailsListPrime.add(newPackage);
                                    } else if (!TextUtils.isEmpty(newPackage.getPackageId()) && newPackage.getPackageType().equals("OS") && isOutStaion && LocationInstance.getInstance().getSelectedCar() == 2) {
                                        packageId = newPackage.getPackageId();
                                    }
                                } else if (newPackage.getPackageVehicleType().equals("3")) {
                                    if (newPackage.getPackageType().equals("R")) {
                                        packageDetailsListAuto.add(newPackage);
                                    } else if (!TextUtils.isEmpty(newPackage.getPackageId()) && newPackage.getPackageType().equals("OS") && isOutStaion && LocationInstance.getInstance().getSelectedCar() == 3) {
                                        packageId = newPackage.getPackageId();
                                    }
                                }
                            }
                        }
                        if (packageSelection != null && packageSelection.isChecked()) {
                            Utilities.hideProgressDialog();
                            showPackageSelectionDialog();
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (packageSelection != null && packageSelection.isChecked()) {
                        Utilities.dialogdisplay("Network Error", "Please Try Again", getActivity());
                        VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                    }
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_get_packages);
        }
    }

    public class BookTrip extends AsyncTask<Void, Void, String> {
        String encodedUrl = "";
        boolean isMobitelPayment = false;

        BookTrip(String encodeString, boolean ismCash) {
            encodedUrl = encodeString;
            isMobitelPayment = ismCash;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utilities.showProgressDialog(getActivity());
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                HttpURLConnection connection = null;
                OutputStream os = null;
                InputStream is = null;

                URL url = new URL(APIConstant.BASE_URL + encodedString);
                //Log.d("Request code", APIConstant.BASE_URL + encodedString.toString());

                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setConnectTimeout(60000);
                connection.setReadTimeout(60000); /* server milliseconds */
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestProperty("http.keepAlive", "false");
                connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");


                os = new BufferedOutputStream(connection.getOutputStream());
                os.flush();

                // do somehting with response
                int responseCode = -1;
                responseCode = connection.getResponseCode();
                responseCode = -1;

                is = connection.getInputStream();
                responseString = readStream(is);
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
                bookingRequestInitiated = false;
            }
            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Utilities.hideProgressDialog();

            try {
                JSONObject response = new JSONObject(result);
                String bookingId = "";

                if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("000")) {

                    if (isMobitelPayment) {
                        if (!TextUtils.isEmpty(approximateFare)) {

                            try {
                                if (!response.isNull("BOOKING") && !TextUtils.isEmpty(response.getJSONObject("BOOKING").getString("ID"))) {
                                    bookingId = response.getJSONObject("BOOKING").getString("ID");
                                }
                            } catch (JSONException e) {
                                Utilities.dialogdisplay("Booking Not Processed", "Your Booking can not be processed Right Now. Please try after some time.", getActivity());
                            }
                            Utilities.makeGetTokenApi(approximateFare, bookingId, getActivity());
                        } else {
                            getDistanceApiCall(bookingSource, bookingDestination, bookingId, true);
                        }
                    } else {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(destinationLocationText.getWindowToken(), 0);
                        bookingRequestInitiated = false;
                        ((HomeActivity) getActivity()).pushFragment(new MyTripsFragment(), false, false);
                    }
                    Utilities.makeAddRecentPlacesApi(sourceLocationText.getText().toString(), getActivity());
                    Utilities.makeAddRecentPlacesApi(destinationLocationText.getText().toString(), getActivity());
                } else {
                    Utilities.dialogdisplay("Booking Not Processed", "Your Booking can not be processed Right Now. Please try after some time.", getActivity());
                }
            } catch (Exception e) {
                bookingRequestInitiated = false;
                Utilities.dialogdisplay("Booking Not Processed", "Your Booking can not be processed Right Now. Please try after some time.", getActivity());
                e.printStackTrace();
            }
        }
    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        String result = null;

        try {
            reader = new BufferedReader(new InputStreamReader(in));
            result = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    bookingRequestInitiated = false;
                    VolleyLog.d("Parsing tag_create_booking_obj", "Error: " + e.getMessage());
                    //mCurrentSelectedView = null;
                    Utilities.hideProgressDialog();
                    Utilities.dialogdisplay("Network Error", "Please Try Again", getActivity());
                }
            }
        }
        return result;
    }

    private void getRecentPlaces() {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            JSONObject reqObject = new JSONObject();
            JSONObject deviceObject = new JSONObject();
            try {
                reqObject.put("OPCODE", "GETUSERPLACES");
                if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", getActivity()))) {
                    reqObject.put("CUSTOMERID", Utilities.getSharedString("CUSTOMERID", getActivity()));
                }
                if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", getActivity()))) {
                    reqObject.put("SESSIONID", Utilities.getSharedString("SESSIONID", getActivity()));
                }
                reqObject.put("CITYNAME", "bangalore");
                deviceObject.put("DEVICETYPE", "ANDROID");
                deviceObject.put(JSONKEYS.DEVICEID, Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID));
                reqObject.put("DEVICEINFO", deviceObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String encodedString = URLUTF8Encoder.encode(Utilities.getEncryptedString(reqObject.toString()));
            // Log.d("RecentPlaces", APIConstant.BASE_URL.toString() + encodedString);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>()

            {
                @Override
                public void onResponse(JSONObject response) {

                    try {
                        placesList = new ArrayList<RecentPlace>();
                        if (!response.isNull("RECENTPLACE") && response.getJSONArray("RECENTPLACE").length() > 0) {
                            JSONArray recentPlacesArray = response.getJSONArray("RECENTPLACE");
                            for (int index = 0; index < recentPlacesArray.length(); index++) {
                                JSONObject recentPlace = recentPlacesArray.getJSONObject(index);
                                RecentPlace recentplaceObj = new RecentPlace(recentPlace);
                                placesList.add(recentplaceObj);
                            }
                            //Log.d("Recent Places Count", "" + placesList.size());
                        }

                        favouritePlacesList = new ArrayList<FavouritePlace>();
                        if (!response.isNull("FAVPLACE") && response.getJSONArray("FAVPLACE").length() > 0) {
                            JSONArray favPlacesArray = response.getJSONArray("FAVPLACE");
                            for (int index = 0; index < favPlacesArray.length(); index++) {
                                JSONObject favPlace = favPlacesArray.getJSONObject(index);
                                FavouritePlace favplaceObj = new FavouritePlace(favPlace);
                                favouritePlacesList.add(favplaceObj);
                            }
                        }
                        AutoCompleteAdapter adapter = new AutoCompleteAdapter(getActivity(), placesList, recentPlacesListView, destinationLocationText, BookTripFragment.this, bookingSource);
                        recentPlacesListView.setAdapter(adapter);
                        AutoCompleteAdapter favAdapter = new AutoCompleteAdapter(getActivity(), favouritePlacesList, favouritePlacesListView, true, destinationLocationText, BookTripFragment.this, bookingSource);
                        favouritePlacesListView.setAdapter(favAdapter);
                        Utilities.hideProgressDialog();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    favouritePlacesList = new ArrayList<FavouritePlace>();
                    placesList = new ArrayList<RecentPlace>();
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_req_recentplaces);
        }

    }

    public void showPaymentSelectionDialog() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.payment_dialog, null);
        dialogBuilder.setView(dialogView);
        paymentSelectionDialog = dialogBuilder.create();
        paymentSelectionDialog.show();

        paymentCash = (RadioButton) dialogView.findViewById(R.id.radioBtn_cash);
        paymentVowCash = (RadioButton) dialogView.findViewById(R.id.radioBtn_vowCash);
        paymentmCash = (RadioButton) dialogView.findViewById(R.id.radioBtn_mCash);
        TextView estimatedFare = (TextView) dialogView.findViewById(R.id.tv_estimatedFareValue);
        TextView vowCash = (TextView) dialogView.findViewById(R.id.tv_vowCash);
        getVowCash(vowCash);
        LinearLayout llmCash = (LinearLayout) dialogView.findViewById(R.id.llmCash);
        View mCashDivider = (View) dialogView.findViewById(R.id.view_mcash);
        TextView okButton = (TextView) dialogView.findViewById(R.id.tv_oKbutton);

        if (!isOutStaion && (packageSelection.isChecked() || !isBookNow)) {
            llmCash.setVisibility(View.GONE);
            mCashDivider.setVisibility(View.GONE);
        } else {
            llmCash.setVisibility(View.VISIBLE);
            mCashDivider.setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(approximateFare))
            estimatedFare.setText(" Rs " + approximateFare);

        if (packageSelection.isChecked())
            estimatedFare.setText(" Rs " + packageEstimateFare);

        if (selectedPaymetMode == 0)
            paymentCash.setChecked(true);
        else if (selectedPaymetMode == 1)
            paymentVowCash.setChecked(true);
        else if (selectedPaymetMode == 2)
            paymentmCash.setChecked(true);

        paymentCash.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedPaymetMode = 0;
                    paymentOptions.setText("Pay by Cash");
                    paymentCash.setChecked(true);
                    paymentVowCash.setChecked(false);
                    paymentmCash.setChecked(false);
                }
            }
        });
        paymentVowCash.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedPaymetMode = 1;
                    paymentOptions.setText("Pay by VowCash");
                    paymentVowCash.setChecked(true);
                    paymentCash.setChecked(false);
                    paymentmCash.setChecked(false);
                }
            }
        });
        paymentmCash.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedPaymetMode = 2;
                    paymentOptions.setText("Pay by mCash");
                    paymentmCash.setChecked(true);
                    paymentVowCash.setChecked(false);
                    paymentCash.setChecked(false);
                }
            }
        });
        okButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPaymetMode == 0) {
                    paymentOptions.setText("Pay by Cash");
                }
                paymentSelectionDialog.dismiss();
            }
        });

    }

    public void getDistanceApiCall(String source, String destination, final String bookingId, final boolean ismPay) {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            Utilities.showProgressDialog(getActivity());

            try {
                String finalUrl = "https://maps.googleapis.com/maps/api/distancematrix/json?&origins=" + URLEncoder.encode(source, "UTF-8")
                        + "&destinations=" + URLEncoder.encode(destination, "UTF-8") +
                        //"&components=country:lk"+
                        "&key=" + getResources().getString(R.string.google_places_server_key);

                JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET, finalUrl, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.has("rows")) {
                                JSONArray rows = response.getJSONArray("rows");
                                JSONArray elements = new JSONArray(rows.getJSONObject(0).getString("elements"));
                                JSONObject distanceObj = elements.getJSONObject(0).getJSONObject("distance");
                                String distance = distanceObj.getString("value");
                                try {

                                    float distanceInKMS = Float.valueOf(distance) * 0.001f;
                                    estimatedDistance = Float.valueOf(distance);

                                    if (distanceInKMS <= 100.0) {

                                        float estimatedFare = Utilities.getApproximateFare(distanceInKMS);
                                        //float estimatedFare = Utilities.roundoff((distanceInKMS * 50.0f) + (((distanceInKMS * 50.0f) * LocationInstance.getInstance().getToleranceAmount()) / 100));
                                        approximateFare = String.valueOf(estimatedFare);
                                    } else {
                                        approximateFare = "";
                                    }
                                } catch (Exception e) {
                                }

                                if (ismPay)
                                    Utilities.makeGetTokenApi(approximateFare, bookingId, getActivity());
                            }

                        } catch (JSONException e) {
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ISSUE with", "Google distance matrix api");
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjReq, "distance_matrix");
            } catch (Exception e) {
                Log.e("ISSUE with", "Google distance matrix api");
            }
        }
    }

    private void getVowCash(final TextView vowCash) {
        try {
            if (getActivity() == null)
                return;

            if (Utilities.isNetworkAvailable(getActivity())) {
                String encodedString = JsonHeaders.creategetVowCashApiJSONHeader(getActivity());

                JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                        APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!TextUtils.isEmpty(response.getString("RESPONSECODE")) && response.getString("RESPONSECODE").equalsIgnoreCase("000")
                                    && !TextUtils.isEmpty(response.getString("AMOUNT"))) {
                                final String amt = response.getString("AMOUNT");
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        vowCash.setText("Vow Cash("+amt+")");
                                    }
                                });
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {


                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjReq, "tag_get_vowcash");
            }
        } catch (Exception e) {
        }
    }

}
