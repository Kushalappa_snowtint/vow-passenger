package com.vowcabs.passengerapp.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.LaunchActivity;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.json.JSONKEYS;

import org.json.JSONObject;

/**
 * Created by Sumanth on 27/10/16.
 */

public class FCMCallbackService extends FirebaseMessagingService {
    private static final String TAG = "FCMCallbackService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From:" + remoteMessage.getFrom());
      /*  if (remoteMessage.getNotification() != null) {
            sendNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), "DRASIGN");
        }*/

        if(!remoteMessage.getData().isEmpty()){
            Log.d(TAG, "From:" + remoteMessage.getData());
            sendNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), remoteMessage.getData().get("server_key"));
        }
    }

    private void sendNotification(String title, String body, String msgType) {
        try {
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            Intent intent;
            PendingIntent pendingIntent;
            JSONObject jsonObject = new JSONObject(msgType);
            if(!jsonObject.getString("code").isEmpty() &&  !jsonObject.getString("trip_id").isEmpty()){
                Log.d("Notification", "inside if");
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(JSONKEYS.TRIP_ID, jsonObject.getString("trip_id"));
                pendingIntent = PendingIntent.getActivity(this, 0, intent,
                        PendingIntent.FLAG_ONE_SHOT);
            }else{
                Log.d("Notification", "inside else");
                intent = new Intent(this, LaunchActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pendingIntent = PendingIntent.getActivity(this, 0, intent,
                        PendingIntent.FLAG_ONE_SHOT);
            }

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setAutoCancel(true)
                    .setSmallIcon(getNotificationIcon())
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                    .setSound(defaultSoundUri)
                    .setColor(getColor(this, R.color.custom_orange))
                    .setContentIntent(pendingIntent);


            NotificationManager notificationManager = (NotificationManager)
                    getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, builder.build());

        } catch (Exception e) {
        }
    }

    private final int getNotificationIcon() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return R.drawable.ic_notify_transparent;
        } else {
            return R.drawable.vow_notify_small;
        }
    }

    private final int getColor(Context context, int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }
}
