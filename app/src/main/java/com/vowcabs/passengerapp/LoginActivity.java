package com.vowcabs.passengerapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.ErrorReporter;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends Activity implements OnClickListener {
    TextView loginButton;
    EditText mobilenumber;
    EditText password;
    TextView forgetPassword;
    View mSelectedView = null;
    String encodedString;
    String deviceId;
    String message = "";
    private String tag_json_obj = "jobj_req";
    private String tag_forgotpassword_obj = "jforgotpassword_req";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.initialscreen);

        try {
            ErrorReporter errReporter = new ErrorReporter();
            errReporter.Init(this);
            errReporter.CheckErrorAndSendMail(this);
        } catch (Exception e) {
        }

        initializeUI();

        if (Utilities.getSharedString("CUSTOMERID", this).length() > 0 && Utilities.getSharedString("SESSIONID", this).length() > 0) {
            launchHomeScreen();
        }
    }

    public void initializeUI() {
        loginButton = (TextView) findViewById(R.id.loginButton);
        mobilenumber = (EditText) findViewById(R.id.et_phone_number);
        password = (EditText) findViewById(R.id.et_password);
        forgetPassword = (TextView) findViewById(R.id.forget_pass);
        deviceId = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
        loginButton.setOnClickListener(this);
        forgetPassword.setOnClickListener(this);

        mobilenumber.setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocus() {
                mobilenumber.setHint("");
            }

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
            }
        });

    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        Utilities.hideProgressDialog();
    }


    boolean isValidDetails() {
        boolean isValid = false;
        String passwordText = password.getText().toString();
        String phoneText = mobilenumber.getText().toString().trim();

        String phoneNumberPattern = "^[0-9]*$";
        boolean isValidPassword = false;
        boolean isValidPhone = false;

        if (phoneText.matches(phoneNumberPattern)) {
            isValidPhone = true;
        } else {
            message = "Enter Valid Phone Number\n";
        }

        if (passwordText.length() != 0 && passwordText != null) {
            isValidPassword = true;
        }
        if (passwordText.length() == 0 || phoneText.length() == 0) {
            message = "Mobile/Password Field is Empty\n ";
        }

        if (isValidPhone && isValidPassword) {
            isValid = true;
        } else {
            dialogdisplay("Invalid Input", message);
        }
        return isValid;
    }


    private void makeLoginAPICall() {
        if (Utilities.isNetworkAvailable(this)) {
            Utilities.showProgressDialog(this);
            encodedString = JsonHeaders.createLoginApiJSONHeader(deviceId, mobilenumber.getText().toString(), password.getText().toString(), this);
            //Log.d("Login api",(APIConstant.BASE_URL+encodedString).toString());


            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>()

            {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Success", response.toString());
                    try {
                        mSelectedView = null;
                        if (!response.isNull("RESPONSECODE") &&  response.getString("RESPONSECODE").equals("000")) {
                            storePersistantData(response);
                            mobilenumber.setText("");
                            password.setText("");
                            launchHomeScreen();
                            finish();
                            Utilities.hideProgressDialog();
                        } else if (!response.isNull("RESPONSECODE") &&  response.getString("RESPONSECODE").equals("101")) {
                            dialogdisplay("Invalid Input", "Please check the mobile number and password and try again");
                            Utilities.hideProgressDialog();

                        } else if (!response.isNull("RESPONSECODE") &&  response.getString("RESPONSECODE").equals("105")) {
                            displayOtpValidationDialog();
                        } else if (!response.isNull("RESPONSECODE") &&  response.getString("RESPONSECODE").equals("999")) {
                            Utilities.hideProgressDialog();
                        } else {
                            Utilities.hideProgressDialog();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mSelectedView = null;
                    Utilities.hideProgressDialog();
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                    dialogdisplay("Network Error", "Please Try Again");

                }
            });
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } else {
            mSelectedView = null;
        }
    }

    protected void displayOtpValidationDialog() {
        final Dialog customDialog = new Dialog(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_view, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        TextView message = (TextView) dialogView.findViewById(R.id.tv_DialogMessage);
        title.setText("Your Mobile Number is not validated");
        message.setText("Please Validate");
        Button yes = (Button) dialogView.findViewById(R.id.btn_yes);
        Button no = (Button) dialogView.findViewById(R.id.btn_no);
        yes.setText("Ok");
        no.setVisibility(View.GONE);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, OTPValidationActivity.class);
                intent.putExtra("PhoneNumber", mobilenumber.getText().toString());
                startActivity(intent);
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }

    @Override
    public void onClick(View v) {
        if (v == loginButton) {
            if (mSelectedView != null)
                return;
            boolean isValide = isValidDetails();
            if (isValide) {
                mSelectedView = loginButton;
                makeLoginAPICall();
            }
        }

        if (v == forgetPassword) {
            if (mobilenumber.getText().toString().length() > 0) {
                makeForgotPasswordApiCall();
            } else {
                dialogdisplay("Mobile Field is Empty", "Please enter your Registered Mobile Number.");
            }
        }
    }

    private void storePersistantData(JSONObject response) {
        try {
            if (!response.isNull("SESSIONID"))
                Utilities.putSharedString("SESSIONID", response.getString("SESSIONID"), LoginActivity.this);


            if (!response.isNull("CUSTOMER")) {
                JSONObject customerObj = response.getJSONObject("CUSTOMER");
                if (!customerObj.isNull("ID") && !TextUtils.isEmpty(customerObj.getString("ID"))) {
                    Utilities.putSharedString("CUSTOMERID", customerObj.getString("ID"), LoginActivity.this);
                }
                if (!customerObj.isNull("EMAIL") && !TextUtils.isEmpty(customerObj.getString("EMAIL"))) {
                    Utilities.putSharedString("EMAILID", customerObj.getString("EMAIL"), LoginActivity.this);
                }
                if (!customerObj.isNull("NAME") && !TextUtils.isEmpty(customerObj.getString("NAME"))) {
                    Utilities.putSharedString("USERNAME", customerObj.getString("NAME"), LoginActivity.this);
                }
                if (!customerObj.isNull("PHONE") && !TextUtils.isEmpty(customerObj.getString("PHONE"))) {
                    Utilities.putSharedString("MOBILE", customerObj.getString("PHONE"), LoginActivity.this);
                }
                if (!customerObj.isNull("BONUSAMT") && !TextUtils.isEmpty(customerObj.getString("BONUSAMT"))) {
                    Utilities.putSharedString("BONUSAMT", customerObj.getString("BONUSAMT"), LoginActivity.this);
                }
                if (!customerObj.isNull("ID") && !TextUtils.isEmpty(customerObj.getString("ID"))) {
                    Utilities.putSharedString("CUSTOMERID", customerObj.getString("ID"), LoginActivity.this);
                }
            }

            if (!response.isNull("INVITECODE"))
                Utilities.putSharedString("INVITECODE", response.getString("INVITECODE"), LoginActivity.this);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void launchHomeScreen() {
        Intent intent = new Intent(LoginActivity.this, RatingBarActivity.class);
        startActivity(intent);
    }
    public void dialogdisplay(String tilte, String Message) {
        final Dialog customDialog = new Dialog(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_view, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        TextView message = (TextView) dialogView.findViewById(R.id.tv_DialogMessage);
        title.setText(tilte);
        message.setText(Message);
        Button yes = (Button) dialogView.findViewById(R.id.btn_yes);
        Button no = (Button) dialogView.findViewById(R.id.btn_no);
        yes.setText("Ok");
        no.setVisibility(View.GONE);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedView = null;
                customDialog.dismiss();
            }
        });
        customDialog.show();

    }

    boolean isFirstTime = false;

    private void makeForgotPasswordApiCall() {
        if (Utilities.isNetworkAvailable(this)) {
            if (!isFirstTime) {
                String encodedString = JsonHeaders.createForgotPasswordApiJSONHeader(mobilenumber.getText().toString().trim(), this);
                Utilities.showProgressDialog(this);

                isFirstTime = true;
                JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                        APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Success", response.toString());
                        try {
                            isFirstTime = false;
                            Utilities.hideProgressDialog();
                            if (response.getString("RESPONSECODE").equals("000")) {
                                dialogdisplay("Password Sent", "Please Check Your Inbox For New Password.");
                            } else if (response.getString("RESPONSECODE").equals("101")) {
                                dialogdisplay("User Not Exist", "Please Register.");
                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        isFirstTime = false;
                        VolleyLog.d("Parsing", "Error: " + error.getMessage());
                        Utilities.hideProgressDialog();
                        dialogdisplay("Network Error", "Please Try Again");

                    }
                });
                jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                        0,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(jsonObjReq, tag_forgotpassword_obj);
            }
        } else {
            dialogdisplay("Oops!!", "Please check your Internet Connection");
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            Utilities.dismissProgressDialog();
        } catch (Exception e) {
        }

    }
}


