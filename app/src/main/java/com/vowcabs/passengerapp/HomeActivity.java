
package com.vowcabs.passengerapp;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.adapter.SideBoardAdapter;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.fragment.AboutFragment;
import com.vowcabs.passengerapp.fragment.BaseFragment;
import com.vowcabs.passengerapp.fragment.GooglePlacesAutoCompleteFragment;
import com.vowcabs.passengerapp.fragment.HomeFragment;
import com.vowcabs.passengerapp.fragment.InviteFragment;
import com.vowcabs.passengerapp.fragment.MyTripsFragment;
import com.vowcabs.passengerapp.fragment.PanicFragment;
import com.vowcabs.passengerapp.fragment.RateCardFragment;
import com.vowcabs.passengerapp.fragment.TakeMeHomeFragment;
import com.vowcabs.passengerapp.fragment.TrackFragment;
import com.vowcabs.passengerapp.fragment.UpdateProfileFragment;
import com.vowcabs.passengerapp.fragment.VowCashFragment;
import com.vowcabs.passengerapp.json.JSONKEYS;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.ErrorReporter;
import com.vowcabs.passengerapp.utils.LocationInstance;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Stack;

import static com.vowcabs.passengerapp.api.APIConstant.PROFILEPIC_URL;

public class HomeActivity extends FragmentActivity implements OnClickListener, OnItemClickListener {
    HomeFragment homefragment;
    GooglePlacesAutoCompleteFragment searchPlacesFragment;
    Stack<Fragment> mFragments = new Stack<Fragment>();
    final String tag_image_updateprofile = "image_profilepic";
    DrawerLayout drawerLayout;
    public RelativeLayout topBar;
    private String tag_json_obj = "logout_req";
    private String tag_json_favorites = "favourites_req";
    ImageView menuBar;
    ListView listview;
    public TextView userName;
    public TextView mobileNumber;
    public TextView headerText;
    RelativeLayout profileUpdateLayout;
    public static RelativeLayout menuLayoutDummy;
    public static TextView updateProfileText;
    //ImageView closeButton;
    public ImageView searchIcon;
    public ImageView favouritesIcon;
    public ImageView profileIcon;
    ImageView feedbackIcon;
    ImageView rateUsIcon;
    String encodedString;
    TextView title;
    TextView message, noServiceText;
    AlertDialog builder;
    Dialog customDialog = null;
    String favourites[] = {"HOME", "OFFICE", "OTHERS"};
    SideBoardAdapter mSideBoardAdapter;
    Fragment currentSelected = null;
    int currentSelectedFavourites = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homeactivity);

        try {
            ErrorReporter errReporter = new ErrorReporter();
            errReporter.Init(this);
            errReporter.CheckErrorAndSendMail(this);
        } catch (Exception e) {
        }

        menuBar = (ImageView) findViewById(R.id.menu_button);
        profileIcon = (ImageView) findViewById(R.id.profileicon);
        listview = (ListView) findViewById(R.id.sideboardlistview);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        headerText = (TextView) findViewById(R.id.headertext);
        searchIcon = (ImageView) findViewById(R.id.search_icon);
        feedbackIcon = (ImageView) findViewById(R.id.iv_feedback);
        rateUsIcon = (ImageView) findViewById(R.id.iv_rate_us);
        //closeButton = (ImageView) findViewById(R.id.menu_close_button);
        topBar = (RelativeLayout) findViewById(R.id.top_bar);
        favouritesIcon = (ImageView) findViewById(R.id.iv_favorites);
        userName = (TextView) findViewById(R.id.userName);
        mobileNumber = (TextView) findViewById(R.id.mobileNumber);
        profileUpdateLayout = (RelativeLayout) findViewById(R.id.rl_profile_details);
        menuLayoutDummy = (RelativeLayout) findViewById(R.id.menu_button_dummy);
        updateProfileText = (TextView) findViewById(R.id.updateProfileTextView);
        noServiceText = (TextView) findViewById(R.id.tv_no_service);
        if (!TextUtils.isEmpty(Utilities.getSharedString("USERNAME", HomeActivity.this))) {
            userName.setText(Utilities.getSharedString("USERNAME", HomeActivity.this));
        }

        if (!TextUtils.isEmpty(Utilities.getSharedString("MOBILE", HomeActivity.this))) {
            mobileNumber.setText(Utilities.getSharedString("MOBILE", HomeActivity.this));
        }

        mSideBoardAdapter = new SideBoardAdapter(this);
        listview.setAdapter(mSideBoardAdapter);

        homefragment = new HomeFragment();
        searchPlacesFragment = new GooglePlacesAutoCompleteFragment();
        pushFragment(homefragment, false, true);
        menuBar.setOnClickListener(this);
        listview.setOnItemClickListener(this);
        headerText.setOnClickListener(this);
        searchIcon.setOnClickListener(this);
        favouritesIcon.setOnClickListener(this);
        feedbackIcon.setOnClickListener(this);
        rateUsIcon.setOnClickListener(this);
        profileUpdateLayout.setOnClickListener(this);
        closeSideBoard();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString(JSONKEYS.TRIP_ID) != null) {
                Bundle args = new Bundle();
                args.putString(JSONKEYS.TRIP_ID, getIntent().getExtras().getString(JSONKEYS.TRIP_ID));
                TrackFragment fragment = new TrackFragment();
                fragment.setArguments(args);
                FragmentManager manager = getFragmentManager();
                FragmentTransaction ft = manager.beginTransaction();
                ft.replace(R.id.stack_container, fragment);
                ft.commit();
                if (mFragments.size() < 2) {
                    mFragments.push(fragment);
                }
            }
        }
    }

    public void pushFragment(Fragment fragment, boolean shouldAnimate, boolean shouldAdd) {
        if (shouldAdd && mFragments.size() < 2) {
            mFragments.push(fragment);
        }

        FragmentManager manager = getFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();

        ft.replace(R.id.stack_container, fragment);
        ft.commit();
    }

    public Fragment getCurrentFragment() {
        return mFragments.peek();
    }

    public void popFragment() {
        searchIcon.setOnClickListener(this);
        headerText.setOnClickListener(this);
        updateHeaderText("Getting Pickup Location...", this);

        Fragment fragment = mFragments.elementAt(mFragments.size() - 2);
        mFragments.pop();
        FragmentManager manager = getFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        //		ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        ft.replace(R.id.stack_container, fragment);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        try {
            if (((BaseFragment) mFragments.lastElement()).onBackPressed() == false) {
                if (mFragments.size() == 1) {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    if (((BaseFragment) mFragments.lastElement() instanceof MyTripsFragment)) {
                        mSideBoardAdapter.setSelectedPosition(0);
                    }
                    popFragment();
                }
            }
        } catch (Exception e) {
        }
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void closeSideBoard() {
        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void showSideBoard() {
        drawerLayout.openDrawer(Gravity.LEFT);
    }

    public boolean isSliderOpen() {
        return drawerLayout.isDrawerOpen(Gravity.LEFT);
    }


    @Override
    public void onClick(View v) {
        if (v == menuBar) {
            showSideBoard();
        } else if (v == headerText && ((BaseFragment) mFragments.lastElement() instanceof HomeFragment)) {
            pushFragment(searchPlacesFragment, false, true);
        } else if (v == searchIcon) {
            pushFragment(searchPlacesFragment, false, true);
        } else if (v == favouritesIcon) {
            showFavouritesDialog();
        } else if (v == feedbackIcon) {
            shareFeedback();
        } else if (v == rateUsIcon) {
            rateThisApp();
        } else if (v == profileUpdateLayout) {
            pushFragment(new UpdateProfileFragment(), false, true);
            searchIcon.setVisibility(View.GONE);
            favouritesIcon.setVisibility(View.GONE);
            closeSideBoard();
        }
    }

    public void updateHeaderText(String inputText, Context ctx) {
        headerText.setText(inputText);
    }

    public void display(int position) {
        if (position != 0) {
            headerText.setOnClickListener(null);
            searchIcon.setOnClickListener(null);
            searchIcon.setVisibility(View.GONE);
            favouritesIcon.setVisibility(View.GONE);
        }
        Fragment fragmentItem = null;
        switch (position) {
            case 0:
                if (mFragments.size() > 1)
                    popFragment();

                break;
            case 1:
                fragmentItem = new MyTripsFragment();
                break;

            case 2:
                fragmentItem = new InviteFragment();
                break;
            case 3:
                fragmentItem = new VowCashFragment();
                break;

            case 4:
                fragmentItem = new RateCardFragment();
                break;

            case 5:
                fragmentItem = new TakeMeHomeFragment();
                break;
            case 6:
                fragmentItem = new PanicFragment();
                break;

            case 7:
                fragmentItem = new AboutFragment();
                break;

            case 8:

                if (LocationInstance.getInstance().getCountryCode().equalsIgnoreCase("lk")) {
                    initiateCalltoCustomerCare("1345");
                } else {
                    initiateCalltoCustomerCare("+919886039847");
                }
                break;
            case 9:
                showLogoutAlert();
                break;

            default:
                break;
        }
        closeSideBoard();
        if (fragmentItem != null) {
            if (currentSelected == fragmentItem)
                return;

            currentSelected = fragmentItem;
            pushFragment(fragmentItem, false, true);
        }
    }

    private void initiateCalltoCustomerCare(String phoneNumber) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + phoneNumber));
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        } catch (Exception e) {
        }
    }

    private void shareFeedback() {
        try {
            Intent sharingIntent = new Intent(Intent.ACTION_SENDTO);
            sharingIntent.setData(Uri.parse("mailto:"));
            sharingIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"info@vowcabs.com"});
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback for Vow Cabs");
            startActivity(sharingIntent);
        } catch (Exception e) {
        }

    }

    private void rateThisApp() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.vowcabs.passengerapp")));
        } catch (Exception e) {
        }
    }


    private void showLogoutAlert() {
        dialogdisplay();
    }

    public void dialogdisplay() {
        if (customDialog != null) {
            customDialog = null;
        }
        customDialog = new Dialog(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_view, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        message = (TextView) dialogView.findViewById(R.id.tv_DialogMessage);
        title.setText("Logout");
        message.setText("Are You Sure?");
        Button yes = (Button) dialogView.findViewById(R.id.btn_yes);
        Button no = (Button) dialogView.findViewById(R.id.btn_no);
        no.setVisibility(View.VISIBLE);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeLogoutApiCall();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
                favouritesIcon.setVisibility(View.VISIBLE);
            }
        });
        customDialog.show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mSideBoardAdapter.setSelectedPosition(position);
        display(position);
    }

    public void setTopBarVisibity(int input) {
        topBar.setVisibility(input);
    }

    protected void onStop() {
        super.onStop();

        if (customDialog != null) {
            customDialog.dismiss();
            customDialog = null;
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        setNoServiceTextVisibility(View.GONE);
    }

    public void makeLogoutApiCall() {
        try {
            if (Utilities.isNetworkAvailable(this)) {
                encodedString = JsonHeaders.createLogoutApiJSONHeader(this);
                Utilities.showProgressDialog(this);
                JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                        APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d("Success", response.toString());
                        try {
                            Utilities.hideProgressDialog();
                            if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("000")) {
                                Utilities.removeSharedValueForKey("NAME", HomeActivity.this);
                                Utilities.removeSharedValueForKey("PHONE", HomeActivity.this);
                                Utilities.removeSharedValueForKey("ECNAME1", HomeActivity.this);
                                Utilities.removeSharedValueForKey("ECNAME2", HomeActivity.this);
                                Utilities.removeSharedValueForKey("ECNUMBER1", HomeActivity.this);
                                Utilities.removeSharedValueForKey("ECNUMBER2", HomeActivity.this);
                                Utilities.removeSharedValueForKey("EMAILID", HomeActivity.this);
                                Utilities.removeSharedValueForKey("USERNAME", HomeActivity.this);
                                Utilities.removeSharedValueForKey("MOBILE", HomeActivity.this);
                                Utilities.removeSharedValueForKey("CUSTOMERID", HomeActivity.this);
                                Utilities.removeSharedValueForKey("SESSIONID", HomeActivity.this);

                                Intent mIntent = new Intent(HomeActivity.this, LaunchActivity.class);
                                mIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                ;
                                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(mIntent);

                            } else if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("101")) {
                                // Session is not handled.
                                Utilities.removeSharedValueForKey("NAME", HomeActivity.this);
                                Utilities.removeSharedValueForKey("PHONE", HomeActivity.this);
                                Utilities.removeSharedValueForKey("ECNAME1", HomeActivity.this);
                                Utilities.removeSharedValueForKey("ECNAME2", HomeActivity.this);
                                Utilities.removeSharedValueForKey("ECNUMBER1", HomeActivity.this);
                                Utilities.removeSharedValueForKey("ECNUMBER2", HomeActivity.this);
                                Utilities.removeSharedValueForKey("EMAILID", HomeActivity.this);
                                Utilities.removeSharedValueForKey("USERNAME", HomeActivity.this);
                                Utilities.removeSharedValueForKey("MOBILE", HomeActivity.this);
                                Utilities.removeSharedValueForKey("CUSTOMERID", HomeActivity.this);
                                Utilities.removeSharedValueForKey("SESSIONID", HomeActivity.this);

                                Intent mIntent = new Intent(HomeActivity.this, LaunchActivity.class);
                                mIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                ;
                                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(mIntent);
                                //Utilities.dialogdisplay("Already Logged Out", "Please try after some time.", HomeActivity.this);
                            } else if (response.getString("RESPONSECODE").equals("999")) {
                                Utilities.dialogdisplay("Network Error", "Please try after some time.", HomeActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utilities.hideProgressDialog();
                        Utilities.dialogdisplay("Network Error", "Please Try Again", HomeActivity.this);
                        VolleyLog.d("Parsing ", "Error: " + error.getMessage());

                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
            }
        } catch (Exception e) {
        }
    }

    private void getGeoPlaceId() {
        try {
            if (Utilities.isNetworkAvailable(this)) {
                Utilities.showProgressDialog(HomeActivity.this);
                String finalUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
                        LocationInstance.getInstance().getLatitude() + "," + LocationInstance.getInstance().getLongitute() +
                        "&key=" + getResources().getString(R.string.google_places_server_key);
                JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                        finalUrl, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d("Success", response.toString());
                        try {
                            Utilities.hideProgressDialog();
                            JSONArray results = response.getJSONArray("results");
                            JSONObject resultsObject = results.getJSONObject(0);
                            String placeId = resultsObject.getString("place_id");
                            makeAddFavouritsApi(currentSelectedFavourites, placeId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            currentSelectedFavourites = 0;
                            Utilities.hideProgressDialog();
                            Utilities.dialogdisplay("Network Error", "Please Try Again", HomeActivity.this);
                        }
                    }

                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utilities.hideProgressDialog();
                        currentSelectedFavourites = 0;
                        Utilities.dialogdisplay("Network Error", "Please Try Again", HomeActivity.this);
                        VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjReq, "geo_placeid_api");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void makeAddFavouritsApi(int whichButton, String placeId) {
        if (Utilities.isNetworkAvailable(this)) {
            try {
                Utilities.showProgressDialog(HomeActivity.this);
                encodedString = JsonHeaders.createAddFavouritesApiJSONHeader(favourites[whichButton], LocationInstance.getInstance().getCurrentLocation(),
                        String.valueOf(LocationInstance.getInstance().getLatitude()), String.valueOf(LocationInstance.getInstance().getLongitute()), placeId, this);
                JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                        APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Success", response.toString());
                        try {
                            Utilities.hideProgressDialog();
                            if (response.getString("RESPONSECODE").equals("000")) {
                                currentSelectedFavourites = 0;
                                Utilities.dialogdisplay("Success", "The selected location has been added successfully to your favourites.", HomeActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        currentSelectedFavourites = 0;
                        Utilities.hideProgressDialog();
                        Utilities.dialogdisplay("Network Error", "Please Try Again", HomeActivity.this);
                        VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_favorites);
            } catch (Exception e) {
            }

        }
    }

    public void showFavouritesDialog() {
        builder = new AlertDialog.Builder(this)
                .setIconAttribute(android.R.attr.alertDialogIcon)
                .setTitle("Add to Favourites")
                .setSingleChoiceItems(favourites, 0, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                /* User clicked on a radio button do some stuff */
                        currentSelectedFavourites = whichButton;

                    }
                })
                .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                /* User clicked Yes so do some stuff */
                        getGeoPlaceId();
                        builder.dismiss();
                    }

                })
                .setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                /* User clicked No so do some stuff */
                    }
                }).create();

        builder.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            Utilities.dismissProgressDialog();
            finish();
        } catch (Exception e) {
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", HomeActivity.this)))
                new GetMyProfilePicture().execute(PROFILEPIC_URL + Utilities.getSharedString("CUSTOMERID", HomeActivity.this) + ".png");
        } catch (Exception e) {
        }


    }

    private class GetMyProfilePicture extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... urls) {
            Bitmap map = null;
            for (String url : urls) {
                map = downloadImage(url);
            }
            return map;
        }

        // Sets the Bitmap returned by doInBackground
        @Override
        protected void onPostExecute(Bitmap result) {
            try {
                if (result != null)
                    profileIcon.setImageBitmap(Utilities.makeCircularBitmap(result, 80, 80));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Creates Bitmap from InputStream and returns it
        private Bitmap downloadImage(String url) {
            Bitmap bitmap = null;
            InputStream stream = null;
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize = 1;

            try {
                stream = getHttpConnection(url);
                bitmap = BitmapFactory.decodeStream(stream, null, bmOptions);
                if (stream != null)
                    stream.close();

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return bitmap;
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestMethod("GET");
                httpConnection.connect();

                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }
    }

    public void setNoServiceTextVisibility(int showOrHide) {
        if (noServiceText != null) {
            noServiceText.setVisibility(showOrHide);
        }
    }

}
