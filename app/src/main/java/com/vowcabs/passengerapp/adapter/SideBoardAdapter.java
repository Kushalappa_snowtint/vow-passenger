package com.vowcabs.passengerapp.adapter;


import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vowcabs.passengerapp.R;

import java.util.ArrayList;

public class SideBoardAdapter extends BaseAdapter
{
	ArrayList<SideBoardItem> itemList;
	Context context;

	int selectedPosition = -1;

	public class SideBoardItem
	{
		private String title;
		private int drawableEnabledImage;
		private int drawableSelectedImage;

		public SideBoardItem(String imageTitle,int drawableEnabledImage, int drawableSelectedImage) 
		{
			this.drawableEnabledImage = drawableEnabledImage;
			this.drawableSelectedImage = drawableSelectedImage;
			this.title = imageTitle;
		}

		public String getTitle()
		{
			return title;
		}

		public void setTitle(String title)
		{
			this.title = title;
		}

		public int getDrawableEnabledImage() 
		{
			return drawableEnabledImage;
		}

		public void setDrawableEnabledImage(int drawableEnabledImage)
		{
			this.drawableEnabledImage = drawableEnabledImage;
		}

		public int getDrawableSelectedImage()
		{
			return drawableSelectedImage;
		}

		public void setDrawableSelectedImage(int drawableSelectedImage)
		{
			this.drawableSelectedImage = drawableSelectedImage;
		}
	}
	private ArrayList<SideBoardItem> initializeSideBoardItems()
	{
		itemList=new ArrayList<SideBoardAdapter.SideBoardItem>();
		SideBoardItem bookTripItem=new SideBoardItem("Book Trip",R.drawable.book_trip_icon, R.drawable.book_trip_pressed_icon);
		SideBoardItem myTripItem=new SideBoardItem("MyTrips",R.drawable.my_trip_icon, R.drawable.my_trip_pressed_icon);
		SideBoardItem inviteEarnItem=new SideBoardItem("Offers",R.drawable.offers_tag, R.drawable.offers_tag_pressed);
		SideBoardItem vowCashItem=new SideBoardItem("Vow Cash",R.drawable.vowcash_icon, R.drawable.vowcash_icon_pressed);
		SideBoardItem rateCardItem=new SideBoardItem("Fare Chart",R.drawable.fare_chart_icon, R.drawable.fare_chart_icon_pressed);
		SideBoardItem takeMeHomeItem=new SideBoardItem("Take Me Home",R.drawable.tmh_icon, R.drawable.tmh_icon_pressed);
		SideBoardItem panicItem=new SideBoardItem("Panic",R.drawable.panic_icon, R.drawable.panic_pressed);
		SideBoardItem aboutItem=new SideBoardItem("About Us",R.drawable.about_icon, R.drawable.about_pressed_icon);
		SideBoardItem calltobookItem=new SideBoardItem("Call to Book",R.drawable.callsupport_icon, R.drawable.callsupport_pressed_icon);
		SideBoardItem logoutItem=new SideBoardItem("Logout",R.drawable.logout_icon, R.drawable.logout_pressed);

		itemList.add(bookTripItem);
		itemList.add(myTripItem);
		itemList.add(inviteEarnItem);
		itemList.add(vowCashItem);
		itemList.add(rateCardItem);
		itemList.add(takeMeHomeItem);
		itemList.add(panicItem);
		itemList.add(aboutItem);
		itemList.add(calltobookItem);
		itemList.add(logoutItem);

		return itemList;

	}
	public SideBoardAdapter(Context context) {

		this.itemList = initializeSideBoardItems();
		this.context = context;
	}

	@Override
	public int getViewTypeCount() {
		return getCount();
	}

	@Override
	public int getItemViewType(int position) {
		return position;
	}
	@Override

	public int getCount() 
	{
		return itemList.size();
	}

	@Override
	public SideBoardItem getItem(int position) {
		return itemList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		View view;
		ViewHolder holder;
		LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if(convertView == null) {
			view = mInflater.inflate(R.layout.sideboarditemlayout, parent, false);
			holder = new ViewHolder();
			holder.image = (ImageView)view.findViewById(R.id.sideboard_image);
			holder.name = (TextView)view.findViewById(R.id.sideboard_name);
			view.setTag(holder);
		} 
		else 
		{ 
			view = convertView;
			holder = (ViewHolder)view.getTag();
		}
		
		SideBoardItem item = itemList.get(position);
		holder.name.setText(item.getTitle());

		StateListDrawable states = new StateListDrawable();
		states.addState(new int[] {android.R.attr.state_selected}, context.getResources().getDrawable(item.getDrawableSelectedImage()));
		states.addState(new int[] {android.R.attr.state_enabled}, context.getResources().getDrawable(item.getDrawableEnabledImage()));
		holder.image.setImageDrawable(states);

		if (selectedPosition == position)
		{
			holder.image.setSelected(true);
			holder.name.setTextColor(context.getResources().getColor(R.color.custom_orange));
		}
		else
		{
			holder.image.setSelected(false);
			holder.name.setTextColor(context.getResources().getColor(R.color.custom_grey));
		}
		return view;
	}

	private class ViewHolder 
	{
		public ImageView image;
		public TextView name;
	}

	public void setSelectedPosition(int position)
	{
		selectedPosition = position;
		notifyDataSetChanged();
	}

}
