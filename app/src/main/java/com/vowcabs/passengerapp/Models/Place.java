package com.vowcabs.passengerapp.Models;

import org.json.JSONException;
import org.json.JSONObject;

public class Place 
{

	public String id;
	public String geoPlaceId;
	public String latitude;
	public String longitude;
	public String name;


	public String getId() {
		return id;
	}
	public void setId(String id) 
	{
		this.id = id;
	}
	public String getGeoPlaceId() 
	{
		return geoPlaceId;
	}
	public void setGeoPlaceId(String geoPlaceId) 
	{
		this.geoPlaceId = geoPlaceId;
	}
	public String getLatitude() 
	{
		return latitude;
	}
	public void setLatitude(String latitude) 
	{
		this.latitude = latitude;
	}
	public String getLongitude()
	{
		return longitude;
	}
	public void setLongitude(String longitude)
	{
		this.longitude = longitude;
	}
	public String getName() 
	{
		return name;
	}
	public void setName(String name) 
	{
		this.name = name;
	}

	public Place(JSONObject placeObj) 
	{
		try {
			if(!placeObj.isNull("ID"))
			{
				setId(placeObj.getString("ID"));
			} 
			if(!placeObj.isNull("LAT"))
			{
				setLatitude(placeObj.getString("LAT"));
			} 
			if(!placeObj.isNull("LNG"))
			{
				setLongitude(placeObj.getString("LNG"));
			} 
			if(!placeObj.isNull("GEOPLACEID"))
			{
				setGeoPlaceId(placeObj.getString("GEOPLACEID"));
			} 
			if(!placeObj.isNull("NAME"))
			{
				setName(placeObj.getString("NAME"));
			} 

		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
