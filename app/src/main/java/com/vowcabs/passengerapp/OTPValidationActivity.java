package com.vowcabs.passengerapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

public class OTPValidationActivity extends Activity implements OnClickListener {
    Button resend;
    Button proceed;
    EditText inputPassword;
    private String tag_validateotp_obj = "validateotp_req";
    private String tag_resendotp_obj = "resendotp_req";
    String encodedString;
    String phoneNumber;
    boolean isOtpValidated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.WHITE));
        } catch (Exception e) {
        }

        setContentView(R.layout.otp_dialog);

        if (!TextUtils.isEmpty(getIntent().getStringExtra("PhoneNumber"))) {
            phoneNumber = getIntent().getStringExtra("PhoneNumber");
        }

        inputPassword = (EditText) findViewById(R.id.et_input);
        resend = (Button) findViewById(R.id.btn_resend);
        proceed = (Button) findViewById(R.id.btn_proceed);
        resend.setOnClickListener(this);
        proceed.setOnClickListener(this);
        setFinishOnTouchOutside(false);

    }

    @Override
    public void onClick(View v) {
        if (v == resend) {
            makeResendOTPApiCall();
        } else if (v == proceed) {
            if (!TextUtils.isEmpty(inputPassword.getText().toString())) {
                makeValidateOtpApiCall();
            } else {
                Utilities.dialogdisplay("OTP Field Empty", "Please enter otp number sent to your inbox and click on proceed.", this);
            }
        }
    }

    public void makeValidateOtpApiCall() {
        if (Utilities.isNetworkAvailable(this)) {
            Utilities.showProgressDialog(OTPValidationActivity.this);
            if (!TextUtils.isEmpty(inputPassword.getText().toString())) {
                encodedString = JsonHeaders.createValidateOtpApiJSONHeader(inputPassword.getText().toString(), phoneNumber, this);
            }
            isOtpValidated = true;
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    isOtpValidated = false;
                    Log.d("Success", response.toString());

                    try {
                        Utilities.hideProgressDialog();
                        if (!response.isNull("RESPONSECODE") &&  response.getString("RESPONSECODE").equals("000")) {

                            if (!response.isNull("SESSIONID") &&  response.has("SESSIONID"))
                                Utilities.putSharedString("SESSIONID", response.getString("SESSIONID"), OTPValidationActivity.this);

                            if (!response.isNull("CUSTOMER") &&  response.has("CUSTOMER"))
                                Utilities.putSharedString("CUSTOMERID", response.getJSONObject("CUSTOMER").getString("ID"), OTPValidationActivity.this);

                            startActivity(new Intent(OTPValidationActivity.this, HomeActivity.class));
                            finish();
                        } else
                            Utilities.dialogdisplay("Invalid OTP", "Please enter valid otp", OTPValidationActivity.this);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    isOtpValidated = false;
                    Utilities.hideProgressDialog();
                    Utilities.dialogdisplay("Network Error", "Please Try Again", OTPValidationActivity.this);
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());

                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_validateotp_obj);
        }
    }

    private void makeResendOTPApiCall() {
        if (Utilities.isNetworkAvailable(this)) {
            Utilities.showProgressDialog(OTPValidationActivity.this);
            encodedString = JsonHeaders.createResendOTPApiJSONHeader(inputPassword.getText().toString(), this);
            isOtpValidated = true;
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    isOtpValidated = false;
                    Utilities.hideProgressDialog();
//                    Log.d("Success", response.toString());
                    Utilities.dialogdisplay("Password Sent", "One Time Password sent to your mobile number", OTPValidationActivity.this);

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    isOtpValidated = false;
                    Utilities.hideProgressDialog();
                    Utilities.dialogdisplay("Network Error", "Please Try Again", OTPValidationActivity.this);
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());

                }
            });
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_resendotp_obj);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utilities.dismissProgressDialog();
    }

}
