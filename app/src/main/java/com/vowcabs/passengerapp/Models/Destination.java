package com.vowcabs.passengerapp.Models;

import org.json.JSONException;
import org.json.JSONObject;

public class Destination 
{

	String id;
	String lat;
	String lng;
	String geoPlaceId;
	String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getGeoPlaceId() {
		return geoPlaceId;
	}

	public void setGeoPlaceId(String geoPlaceId) {
		this.geoPlaceId = geoPlaceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Destination(JSONObject destinationObj) throws JSONException 
	{
		if (!destinationObj.isNull("ID"))
		{
			setId(destinationObj.getString("ID"));
		}
		if (!destinationObj.isNull("LAT"))
		{
			setLat(destinationObj.getString("LAT"));
		}
		if (!destinationObj.isNull("LNG"))
		{
			setLng(destinationObj.getString("LNG"));
		}
		if (!destinationObj.isNull("GEOPLACEID"))
		{
			setGeoPlaceId(destinationObj.getString("GEOPLACEID"));
		}
		if (!destinationObj.isNull("NAME"))
		{
			setName(destinationObj.getString("NAME"));
		}
	}

}
