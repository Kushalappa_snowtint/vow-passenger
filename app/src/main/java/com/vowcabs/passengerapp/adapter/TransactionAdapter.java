package com.vowcabs.passengerapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vowcabs.passengerapp.Models.VowTransaction;
import com.vowcabs.passengerapp.R;

import java.util.List;

/**
 * Created by avinash on 26/7/16.
 */
public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.MyViewHolder> {

    List<VowTransaction> transaction_list;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView debitorcredit;
        public TextView transDesc, transAmount, dateTime;

        public MyViewHolder(View itemView) {
            super(itemView);
            debitorcredit = (ImageView) itemView.findViewById(R.id.iv_transaction);
            transDesc = (TextView) itemView.findViewById(R.id.tv_desc);
            dateTime = (TextView) itemView.findViewById(R.id.tv_date);
        }
    }

    public TransactionAdapter(List<VowTransaction> transaction_list) {
        this.transaction_list = transaction_list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        VowTransaction details = transaction_list.get(position);

        if (!TextUtils.isEmpty(details.getBalance()))
            holder.transDesc.setText(details.getDesc());

        if (!TextUtils.isEmpty(details.getDate()))
            holder.dateTime.setText(details.getDate());

        if (!TextUtils.isEmpty(details.getTransactionType()) && details.getTransactionType().equalsIgnoreCase("d")) {
            holder.debitorcredit.setImageResource(R.drawable.orange_arrow);
        } else {
            holder.debitorcredit.setImageResource(R.drawable.greenarrow);
        }
    }

    @Override
    public int getItemCount() {
        return transaction_list.size();
    }


}
