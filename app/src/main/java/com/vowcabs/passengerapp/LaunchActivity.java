package com.vowcabs.passengerapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.vowcabs.passengerapp.utils.Utilities;

public class LaunchActivity extends Activity implements OnClickListener {
    TextView sigUpText;
    TextView siginText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launch_screen);

//        if (!TextUtils.isEmpty(Utilities.readDataFromFile(BaseConstants.sdCardPath+"/Services.json"))) {
//           // Log.e("Services JSON", Utilities.readDataFromFile("Services.json"));
//        } else {
//            Utilities.makeGetServicesApi(this);
//        }

        if (Utilities.getSharedString("CUSTOMERID", this).length() > 0 && Utilities.getSharedString("SESSIONID", this).length() > 0)
            launchHomeScreen();

        sigUpText = (TextView) findViewById(R.id.signup_button);
        siginText = (TextView) findViewById(R.id.signin_button);
        sigUpText.setOnClickListener(this);
        siginText.setOnClickListener(this);
    }

    private void launchHomeScreen() {
        Intent intent = new Intent(LaunchActivity.this, RatingBarActivity.class);
        startActivity(intent);
    }


    @Override
    public void onClick(View v) {
        if (v == siginText) {
            startActivity(new Intent(LaunchActivity.this, LoginActivity.class));
        }
        if (v == sigUpText) {
            startActivity(new Intent(LaunchActivity.this, RegistrationActivity.class));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            Utilities.dismissProgressDialog();
        } catch (Exception e) {
        }
    }

}
