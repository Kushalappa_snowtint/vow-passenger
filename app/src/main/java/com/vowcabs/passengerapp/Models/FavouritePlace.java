package com.vowcabs.passengerapp.Models;

import org.json.JSONException;
import org.json.JSONObject;

public class FavouritePlace {

    public String id;
    public String favouriteTripName;

    Place mPlace;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Place getmPlace() {
        return mPlace;
    }

    public void setmPlace(Place mPlace) {
        this.mPlace = mPlace;
    }

    public String getFavouriteTripName() {
        return favouriteTripName;
    }

    public void setFavouriteTripName(String favouriteTripName) {
        this.favouriteTripName = favouriteTripName;
    }


    public FavouritePlace(JSONObject placeObj) {
        try {
            if (!placeObj.isNull("ID")) {
                setId(placeObj.getString("ID"));
            }

            if (!placeObj.isNull("PLACE")) {
                mPlace = new Place(placeObj.getJSONObject("PLACE"));
                setmPlace(mPlace);
            }
            if (!placeObj.isNull("FAVTRIPNAME")) {
                setFavouriteTripName(placeObj.getString("FAVTRIPNAME"));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}
