package com.vowcabs.passengerapp.Models;

import org.json.JSONException;
import org.json.JSONObject;

public class AutoSuggestedPlace 
{

	private String description;
	private String placeId;

	public AutoSuggestedPlace(JSONObject jsonObject)
	{
		try 
		{
			setDescription(jsonObject.getString("description"));
			setPlaceId(jsonObject.getString("place_id"));
		} 
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String getDescription() 
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public String getPlaceId()
	{
		return placeId;
	}
	public void setPlaceId(String placeId) 
	{
		this.placeId = placeId;
	}

}
