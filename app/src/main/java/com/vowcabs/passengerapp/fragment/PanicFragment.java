package com.vowcabs.passengerapp.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.AppController;
import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.MyApplication;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.LocationInstance;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

public class PanicFragment extends HomeBaseFragment implements OnClickListener {
    ImageView panicButton;
    RelativeLayout rlAmbulance;
    RelativeLayout rlSos;
    RelativeLayout rlPolice;
    RelativeLayout rlNationalHelp;
    LinearLayout llIndia, llLanka;
    private String tag_panic_api = "tag_panic";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.panic_fragment, container, false);
        return rootView;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        panicButton = (ImageView) getView().findViewById(R.id.panic_button);
        llIndia     = (LinearLayout) getView().findViewById(R.id.ll_in);
        llLanka     = (LinearLayout) getView().findViewById(R.id.ll_lk);

        if (LocationInstance.getInstance().getCountryCode().equalsIgnoreCase("LK")) {
            llIndia.setVisibility(View.GONE);
            llLanka.setVisibility(View.VISIBLE);
            rlAmbulance = (RelativeLayout) getView().findViewById(R.id.rl_ambulance_icon);
            rlSos = (RelativeLayout) getView().findViewById(R.id.rl_sos_icon);
            rlPolice = (RelativeLayout) getView().findViewById(R.id.rl_police_icon);
            rlNationalHelp = (RelativeLayout) getView().findViewById(R.id.rl_national_help);

        } else {
            llIndia.setVisibility(View.VISIBLE);
            llLanka.setVisibility(View.GONE);
            rlAmbulance = (RelativeLayout) getView().findViewById(R.id.rl_ambulance_icon_in);
            rlSos = (RelativeLayout) getView().findViewById(R.id.rl_sos_icon_in);
            rlPolice = (RelativeLayout) getView().findViewById(R.id.rl_police_icon_in);
            rlNationalHelp = (RelativeLayout) getView().findViewById(R.id.rl_national_help);
        }
        rlAmbulance.setOnClickListener(this);
        rlSos.setOnClickListener(this);
        rlPolice.setOnClickListener(this);
        rlNationalHelp.setOnClickListener(this);
        panicButton.setOnClickListener(this);

        ((HomeActivity) getActivity()).updateHeaderText("Panic", mActivity);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).updateHeaderText("Panic", mActivity);
        MyApplication.getInstance().trackScreenView("Panic Screen");
    }

    private void makePanicApiCall() {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            Utilities.showProgressDialog(getActivity());
            String encodedString = JsonHeaders.createPanicApiJSONHeader(getActivity());
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //Log.d("Success", response.toString());
                    try {
                        Utilities.hideProgressDialog();
                        if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("000")) {
                            dialogdisplay("Stay Calm", "Your location has been shared with your emergency contacts. You can also call Ambulance & Police from your App for further assistance.");
                        } else if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("101")) {
                            dialogdisplay("No Emergency Contact Added", "Please update your SOS contacts in Profile.");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utilities.hideProgressDialog();
                    Utilities.dialogdisplay("Network Error", "Please Try Again", getActivity());
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());

                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_panic_api);
        }
    }

    public void dialogdisplay(String tilte, String Message) {
        final Dialog customDialog = new Dialog(getActivity());
        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_view, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        TextView message = (TextView) dialogView.findViewById(R.id.tv_DialogMessage);
        title.setText(tilte);
        message.setText(Message);
        Button yes = (Button) dialogView.findViewById(R.id.btn_yes);
        Button no = (Button) dialogView.findViewById(R.id.btn_no);
        no.setVisibility(View.GONE);
        yes.setText("Ok");
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }

    @Override
    public void onClick(View v) {
        if (v == panicButton) {
            if ((!TextUtils.isEmpty(Utilities.getSharedString("ECNAME1", getActivity()))) || (!TextUtils.isEmpty(Utilities.getSharedString("ECNAME2", getActivity())))
                    && (Utilities.getSharedString("ECNAME1", getActivity()).length() > 0 || Utilities.getSharedString("ECNAME2", getActivity()).length() > 0)) {
                makePanicApiCall();
            } else {
                Utilities.dialogdisplay("No Emergency Contact Added", "Please update your SOS contacts in Profile.", getActivity());
            }
        }
        if (v == rlAmbulance) {
            if (LocationInstance.getInstance().getCountryCode().equalsIgnoreCase("LK"))
                initiateCall("110");
            else
                initiateCall("108");
        }
        if (v == rlSos) {
            if (LocationInstance.getInstance().getCountryCode().equalsIgnoreCase("LK"))
                initiateCall("118");
            else
                initiateCall("112");
        }
        if (v == rlPolice) {
            if (LocationInstance.getInstance().getCountryCode().equalsIgnoreCase("LK"))
                initiateCall("119");
            else
                initiateCall("100");
        }
        if (v == rlNationalHelp) {
            initiateCall("1919");
        }

    }

    private void initiateCall(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
