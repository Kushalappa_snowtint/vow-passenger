package com.vowcabs.passengerapp.Models;

import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by avinash on 26/7/16.
 */
public class VowTransaction {

    private String paymentId, paymentType, amount, date, balance, transactionType, desc;

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setData(JSONObject transaction) {
        try {
            if (!transaction.isNull("ID") && !TextUtils.isEmpty(transaction.getString("ID"))) {

                setPaymentId(transaction.getString("ID").trim());
            }
            if (!transaction.isNull("PAYMENTTYPE") && !TextUtils.isEmpty(transaction.getString("PAYMENTTYPE"))) {

                setPaymentType(transaction.getString("ID").trim());
            }
            if (!transaction.isNull("Amount") && !TextUtils.isEmpty(transaction.getString("Amount"))) {

                setAmount(transaction.getString("Amount").trim());
            }
            if (!transaction.isNull("DATETIME") && !TextUtils.isEmpty(transaction.getString("DATETIME"))) {

                setDate(transaction.getString("DATETIME").trim());
            }
            if (!transaction.isNull("Balance") && !TextUtils.isEmpty(transaction.getString("Balance"))) {

                setBalance(transaction.getString("Balance").trim());
            }
            if (!transaction.isNull("TRANSACTIONTYPE") && !TextUtils.isEmpty(transaction.getString("TRANSACTIONTYPE"))) {

                setTransactionType(transaction.getString("TRANSACTIONTYPE").trim());
            }
            if (!transaction.isNull("DESC") && !TextUtils.isEmpty(transaction.getString("DESC"))) {

                setDesc(transaction.getString("DESC").trim());
            }

        } catch (JSONException e) {
        }
    }


}
