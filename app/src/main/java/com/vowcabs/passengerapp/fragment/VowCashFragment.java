package com.vowcabs.passengerapp.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.AppController;
import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.VowCashTransactionsActivity;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.LocationInstance;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

public class VowCashFragment extends HomeBaseFragment implements View.OnClickListener {

    TextView avilabelBalance, terms;
    Button addCash;
    Button vowCashTransactions;
    Button btnReedemCash;
    //ImageView vowTrasactions;
    String tag_get_vowcash = "tag_get_vowcash";
    String tag_reedem = "tag_reedem";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.vow_cash, container, false);
        avilabelBalance = (TextView) rootView.findViewById(R.id.currentBalance);
        terms = (TextView) rootView.findViewById(R.id.tv_termsUsed1);
        addCash = (Button) rootView.findViewById(R.id.btn_addCash);
        vowCashTransactions = (Button) rootView.findViewById(R.id.btn_vowTransaction);
        //vowTrasactions  = (ImageView) rootView.findViewById(R.id.iv_vowcash_trans);
        btnReedemCash = (Button)rootView.findViewById(R.id.btn_redeem);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).updateHeaderText("VOW CASH", mActivity);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).updateHeaderText("VOW CASH", mActivity);
        addCash.setOnClickListener(this);
        terms.setOnClickListener(this);
        vowCashTransactions.setOnClickListener(this);
        btnReedemCash.setOnClickListener(this);
        getVowCash();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_addCash:
                try {
                    ((HomeActivity) getActivity()).pushFragment(new VowCashPaymetSelectionFragment(), false, false);
                } catch (Exception e) {
                }

                break;
            case R.id.tv_termsUsed1:
                try {
                    if (LocationInstance.getInstance().getCountryCode().equalsIgnoreCase("LK")) {
                        ((HomeActivity) getActivity()).pushFragment(new WebUrlViewFragment("http://lk.vowcabs.com/terms.html"), false, false);
                    } else {
                        ((HomeActivity) getActivity()).pushFragment(new WebUrlViewFragment("http://vowcabs.com/terms.html"), false, false);
                    }
                } catch (Exception e) {
                }
                break;
            case R.id.btn_vowTransaction:
                startActivity(new Intent(getActivity(), VowCashTransactionsActivity.class));
                break;
            case R.id.btn_redeem:
                showRedeemDialog();
        }

    }

    private void showRedeemDialog() {
        if(getActivity() != null){
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_promo_code);
            dialog.setCancelable(false);
            final EditText editText = (EditText) dialog.findViewById(R.id.et_redeem_code);
            Button submit = (Button)dialog.findViewById(R.id.btn_submit);
            Button cancel = (Button)dialog.findViewById(R.id.btn_cancel);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!TextUtils.isEmpty(editText.getText().toString())){
                        dialog.dismiss();
                        Utilities.showProgressDialog(getActivity());
                        sendReedemCodeToServer(editText.getText().toString());
                    }else{
                        dialog.dismiss();
                    }
                }
            });
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    private void sendReedemCodeToServer(String reedemCode) {
        if(getActivity() != null){
            if(Utilities.isNetworkAvailable(getActivity())){
                String encodedString = JsonHeaders.createReedemCodeJSONHeader(reedemCode, getActivity());
                JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                        APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Utilities.hideProgressDialog();
                        try{
                            if (!TextUtils.isEmpty(response.getString("RESPONSECODE")) && response.getString("RESPONSECODE").equalsIgnoreCase("000")){
                                getVowCash();
                            }
                            Utilities.dialogdisplay("Promo code", response.getString("MESSAGE"), getActivity());
                        }catch (JSONException e){
                            Log.d("Exception", e.toString());
                        }

                        Log.d("Reedem", "response "+response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Utilities.hideProgressDialog();
                        Log.d("Reedem", "response "+error.toString());
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjReq, tag_reedem);
            }
        }
    }

    private void getVowCash() {
        try {
            if (getActivity() == null)
                return;

            if (Utilities.isNetworkAvailable(getActivity())) {
                String encodedString = JsonHeaders.creategetVowCashApiJSONHeader(getActivity());

                JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                        APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!TextUtils.isEmpty(response.getString("RESPONSECODE")) && response.getString("RESPONSECODE").equalsIgnoreCase("000")
                                    && !TextUtils.isEmpty(response.getString("AMOUNT"))) {
                                final String amt = response.getString("AMOUNT");
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (avilabelBalance != null)
                                            avilabelBalance.setText(amt);
                                    }
                                });
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {


                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjReq, tag_get_vowcash);
            }
        } catch (Exception e) {
        }
    }
}
