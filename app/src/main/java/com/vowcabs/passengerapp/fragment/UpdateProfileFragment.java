package com.vowcabs.passengerapp.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.AppController;
import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.MyApplication;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.vowcabs.passengerapp.api.APIConstant.PROFILEPIC_URL;

public class UpdateProfileFragment extends BaseFragment implements OnClickListener {
    String tag_getprofile_details_obj = "getuserprofile_req";

    Bitmap mUserPhoto;
    public static final int PICK_FROM_CAMERA = 1;
    public static final int PICK_FROM_GALLERY = 2;
    //TODO
    ImageView mProfilePhoto, proifleBg;
    ImageView plusIcon;
    RelativeLayout mPasswardRL;
    LinearLayout mMbllayout;
    LinearLayout mNameLayout;
    boolean isPersonalInfo = false;

    TextView mMblNuTV;
    TextView mImageLogo;
    TextView emailId;
    EditText userName;
    EditText mobileNumber;
    EditText emerencyContactName1;
    EditText emerencyContactName2;
    ;
    EditText emerencyContactNumber1;
    ;
    EditText emerencyContactNumber2;
    EditText confirmPassword;
    String username;
    String phoneText;
    String email;
    String eCName1;
    String eCName2;
    String eCNumber1;
    String eCNumber2;
    String emailPattern = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@" + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+";
    //TextView  updateProfile,updateSos;
    RelativeLayout rlPersonalInfo, rlSosInfo;
    ScrollView svSosSInfo, svPersonalInfo;
    private static String tag_json_updateprofile = "updateprofile_req";
    boolean isMobileNumberChanged = false;
    boolean isEmailChanged = false;
    private Uri mImageCaptureUri;
    private File outPutFile = null;
    private static final int CAMERA_CODE = 101, GALLERY_CODE = 201, CROPING_CODE = 301;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.update_profile, container,
                false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //TODO
        mProfilePhoto = (ImageView) getView().findViewById(R.id.profilePhoto);
        proifleBg = (ImageView) getView().findViewById(R.id.profile_bgcircle);
        plusIcon = (ImageView) getView().findViewById(R.id.plus_icon);
        mImageLogo = (TextView) getView().findViewById(R.id.ImageLogo);
        mMblNuTV = (TextView) getView().findViewById(R.id.mblNuTV);
        mNameLayout = (LinearLayout) getView().findViewById(R.id.namelayout);
        mMbllayout = (LinearLayout) getView().findViewById(R.id.mbllayout);
        mPasswardRL = (RelativeLayout) getView().findViewById(R.id.passwardRL);
        emailId = (TextView) getView().findViewById(R.id.email);
        userName = (EditText) getView().findViewById(R.id.editname);
        mobileNumber = (EditText) getView().findViewById(R.id.mobilenumber);
        emerencyContactName1 = (EditText) getView().findViewById(R.id.emergencyContact1);
        emerencyContactName2 = (EditText) getView().findViewById(R.id.emergencyContactName2);
        emerencyContactNumber1 = (EditText) getView().findViewById(R.id.emergencyNumber1);
        emerencyContactNumber2 = (EditText) getView().findViewById(R.id.emergencyNumber2);
        rlPersonalInfo = (RelativeLayout) getView().findViewById(R.id.rl_personalnfo);
        rlSosInfo = (RelativeLayout) getView().findViewById(R.id.rl_soslnfo);
        svPersonalInfo = (ScrollView) getView().findViewById(R.id.sv_personalInfo);
        svSosSInfo = (ScrollView) getView().findViewById(R.id.sv_sosInfo);
        //updateProfile                = (TextView) getView().findViewById(R.id.tv_updateProfile);
        //updateSos                    = (TextView) getView().findViewById(R.id.update_sos);
        //mPasswEditText.setTransformationMethod(new PasswordTransformationMethod());
        ((HomeActivity) getActivity()).updateHeaderText("Update Profile", mActivity);
        mProfilePhoto.setOnClickListener(this);
        mPasswardRL.setOnClickListener(this);
        rlPersonalInfo.setOnClickListener(this);
        rlSosInfo.setOnClickListener(this);
        HomeActivity.menuLayoutDummy.setOnClickListener(this);

        outPutFile = new File(android.os.Environment.getExternalStorageDirectory(), "temp.png");

        //TODO
        populatePersistantData();
        getUserProfile();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).updateHeaderText("Update Profile", mActivity);
        HomeActivity.menuLayoutDummy.setVisibility(View.VISIBLE);
        plusIcon.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", mActivity)))
            new GetMyProfilePic().execute(PROFILEPIC_URL + Utilities.getSharedString("CUSTOMERID", getActivity()) + ".png");
        MyApplication.getInstance().trackScreenView("Update profile Screen");
        if (!TextUtils.isEmpty(Utilities.getSharedString("EMAIL", getActivity()))) {
            emailId.setText(Utilities.getSharedString("EMAIL", getActivity()));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        HomeActivity.menuLayoutDummy.setVisibility(View.INVISIBLE);
    }


    private void getUserProfile() {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {

            String encodedString = JsonHeaders
                    .createProfileDetailsJSONHeader(getActivity());
            Log.d("User Profile url", APIConstant.BASE_URL + encodedString);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                    com.android.volley.Request.Method.GET, APIConstant.BASE_URL
                    + encodedString, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            updateUserDetails(response);
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    System.out.println(error.getMessage());
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq,
                    tag_getprofile_details_obj);
        }
    }

    private void updateUserDetails(JSONObject response) {
        Log.d("Profile", "response : " + response.toString());
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            try {
                if (!response.isNull("RESPONSECODE")) {
                    JSONArray wellwishersArray = response.getJSONArray("WELLWISHER");
                    JSONObject wellWisher;
                    if (wellwishersArray.length() == 2) {
                        wellWisher = (JSONObject) wellwishersArray.get(0);
                        emerencyContactName1.setText(wellWisher.getString("NAME"));
                        emerencyContactNumber1.setText(wellWisher.getString("PHONE"));

                        wellWisher = (JSONObject) wellwishersArray.get(1);
                        emerencyContactName2.setText(wellWisher.getString("NAME"));
                        emerencyContactNumber2.setText(wellWisher.getString("PHONE"));
                    } else if (wellwishersArray.length() == 1) {
                        wellWisher = (JSONObject) wellwishersArray.get(0);
                        emerencyContactName1.setText(wellWisher.getString("NAME"));
                        emerencyContactNumber1.setText(wellWisher.getString("PHONE"));
                    }
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == HomeActivity.menuLayoutDummy) {
            if (isValidDetails())
                makeUpdateProfileApiCall();
        }

        if (v == mProfilePhoto) {
            //selectPhoto();
            selectImageOption();
        }
        if (v == mPasswardRL) {
            ((HomeActivity) getActivity()).pushFragment(new ChangePasswordFragment(), false, true);
        }

        if (v == rlPersonalInfo) {
            svPersonalInfo.setVisibility(View.VISIBLE);
            svSosSInfo.setVisibility(View.GONE);

            rlPersonalInfo.setBackgroundColor(getActivity().getResources().getColor(R.color.dark_orange));
            rlSosInfo.setBackgroundColor(getActivity().getResources().getColor(R.color.custom_orange));

        }
        if (v == rlSosInfo) {
            svSosSInfo.setVisibility(View.VISIBLE);
            svPersonalInfo.setVisibility(View.GONE);
            rlPersonalInfo.setBackgroundColor(getActivity().getResources().getColor(R.color.custom_orange));
            rlSosInfo.setBackgroundColor(getActivity().getResources().getColor(R.color.dark_orange));
        }
        //		if (v == updateProfile)
        //		{
        //			checkMobileNumberUpdation();
        //
        //			if (isValidDetails())
        //				makeUpdateProfileApiCall();
        //		}
        //		if (v == updateSos)
        //		{
        //			if (isValidDetails())
        //				makeUpdateProfileApiCall();
        //		}


    }

    private void checkMobileNumberUpdation() {
        if (!TextUtils.isEmpty(Utilities.getSharedString("MOBILE", getActivity())) && !TextUtils.isEmpty(mobileNumber.getText().toString())) {
            if (!Utilities.getSharedString("MOBILE", getActivity()).equals(mobileNumber.getText().toString())) {
                isMobileNumberChanged = true;
            }
        }

    }

    private void checkEmailUpdation() {
        if (!TextUtils.isEmpty(Utilities.getSharedString("EMAIL", getActivity())) && !TextUtils.isEmpty(emailId.getText().toString())) {
            if (!Utilities.getSharedString("EMAIL", getActivity()).equals(emailId.getText().toString())) {
                isEmailChanged = true;
            }
        }
    }

    private void changeProfileEditableMode() {
        HomeActivity.updateProfileText.setText("Save");
        userName.setEnabled(true);
        mobileNumber.setEnabled(true);
        emailId.setEnabled(true);
        plusIcon.setVisibility(View.VISIBLE);

        emerencyContactName1.setEnabled(true);
        emerencyContactName2.setEnabled(true);
    }


    boolean isValidDetails() {
        boolean isValid = false;
        username = userName.getText().toString().trim();
        email = emailId.getText().toString().trim();
        phoneText = mobileNumber.getText().toString().trim();
        eCName1 = emerencyContactName1.getText().toString().trim();
        eCName2 = emerencyContactName2.getText().toString().trim();
        eCNumber1 = emerencyContactNumber1.getText().toString().trim();
        eCNumber2 = emerencyContactNumber2.getText().toString().trim();
        //		String passwordText = mPasswEditText.getText().toString().trim();
        //		String confirmPasswordText=confirmPassword.getText().toString();
        String phoneNumberPattern = "^[0-9]*$";
        String message = "";
        boolean isValidName = false;
        boolean isValidPhone = false;
        boolean isValidEmail = false;
        boolean isValidEC = false;


        Matcher matcherObj = Pattern.compile(emailPattern).matcher(email);
        if (!TextUtils.isEmpty(email) && matcherObj.matches()) {
            isValidEmail = true;
        } else {
            message = message + "Please Enter Valid Email Id\n";
        }
        boolean isValidECName1 = false;
        boolean isValidECName2 = false;
        boolean isValidECNumber1 = false;
        boolean isValidECNumber2 = false;
        boolean isValidConfirmPassword = false;
        //boolean isValidPassword=false;

        //		if(passwordText.length() != 0 &&passwordText!= null)
        //		{
        //			isValidPassword=true;
        //		}
        //		else
        //		{
        //			message=message +"Password field is Empty\n";
        //		}
        //		if(confirmPasswordText.length() != 0 &&confirmPasswordText!= null)
        //		{
        //			isValidConfirmPassword=true;
        //		}
        //		else
        //		{
        //			message=message +"Confirm password field is Empty\n";
        //
        //		}
        //		if(passwordText.compareTo(confirmPasswordText)!=0)
        //		{
        //			isValidConfirmPassword=false;
        //			message=message + "Password and Confirm Password are different\n";
        //
        //		}
//        if (!(userName.getText().toString().length() == 0)
//                && userName.getText().toString() != null) {
//            isValidName = true;
//        } else {
//            message = message + "Name field is Empty\n";
//
//        }
//        if (phoneText.matches(phoneNumberPattern) && phoneText.length() == 10 || phoneText.length() == 9) {
//            isValidPhone = true;
//
//        } else {
//            message = message + "Please Enter Valid Mobile Number\n";
//        }

        if (!(emerencyContactNumber1.getText().toString().equalsIgnoreCase(Utilities.getSharedString("MOBILE", getActivity()))
                || emerencyContactNumber2.getText().toString().equalsIgnoreCase(Utilities.getSharedString("MOBILE", getActivity())))) {
            isValidEC = true;

        } else {
            message = message + "Well wisher mobile number should be different from customer mobile number.\n";
        }


        if (!(emerencyContactName1.getText().toString().length() == 0)
                && emerencyContactName1.getText().toString() != null) {
            isValidECName1 = true;
        } else {
            message = message + "Emergency ContactName1 field is Empty\n";

        }
        if (!(emerencyContactName2.getText().toString().length() == 0)
                && emerencyContactName2.getText().toString() != null) {
            isValidECName2 = true;
        } else {
            message = message + "Emergency ContactName2 field is Empty\n";

        }

        if (!(emerencyContactNumber1.getText().toString().length() == 0)
                && emerencyContactNumber1.getText().toString() != null) {
            isValidECNumber1 = true;
        } else {
            message = message + "Emergency ContactNumber1 field is Empty\n";

        }
        if (!(emerencyContactNumber2.getText().toString().length() == 0)
                && emerencyContactNumber2.getText().toString() != null) {
            isValidECNumber2 = true;
        } else {
            message = message + "Emergency ContactNumber2 field is Empty\n";

        }

        if (!(emerencyContactNumber1.getText().toString().length() < 9)) {
            isValidECNumber1 = true;
        } else {
            isValidECNumber1 = false;
            message = message + "Emergency ContactNumber1 is invalid\n";
        }

        if (!(emerencyContactNumber2.getText().toString().length() < 9)) {
            isValidECNumber2 = true;
        } else {
            isValidECNumber2 = false;
            message = message + "Emergency ContactNumber2 is invalid\n";
        }


        if (isValidEC && isValidECName1 && isValidECName2 && isValidECNumber1 && isValidECNumber2) {
            isValid = true;
        } else {
            dialogdisplay("INVALID INPUT", message);
        }

        return isValid;
    }

    public void dialogdisplay(String tilte, String Message) {
        final Dialog customDialog = new Dialog(getActivity());
        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_view, null);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(dialogView);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialogView.findViewById(R.id.tv_DialogTitle);
        TextView message = (TextView) dialogView.findViewById(R.id.tv_DialogMessage);
        title.setText(tilte);
        message.setText(Message);
        Button yes = (Button) dialogView.findViewById(R.id.btn_yes);
        Button no = (Button) dialogView.findViewById(R.id.btn_no);
        yes.setText("Ok");
        no.setVisibility(View.GONE);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });
        customDialog.show();

    }

    private void makeUpdateProfileApiCall() {
        if (getActivity() == null)
            return;

        // Assuming the API call is successful.
        if (!TextUtils.isEmpty(eCName1)) {
            Utilities.putSharedString("ECNAME1", eCName1, getActivity());
        }
        if (!TextUtils.isEmpty(eCNumber1)) {
            Utilities.putSharedString("ECNUMBER1", eCNumber1, getActivity());
        }
        if (!TextUtils.isEmpty(eCName2)) {
            Utilities.putSharedString("ECNAME2", eCName2, getActivity());
        }
        if (!TextUtils.isEmpty(eCNumber2)) {
            Utilities.putSharedString("ECNUMBER2", eCNumber2, getActivity());
        }
        if (!TextUtils.isEmpty(emailId.getText().toString().trim())) {
            Utilities.putSharedString("EMAIL", emailId.getText().toString().trim(), getActivity());
        }


        if (Utilities.isNetworkAvailable(getActivity())) {
            String encodedString = JsonHeaders.createUpdateProfileApiJSONHeader(username, phoneText, emailId.getText().toString()
                    , eCName1, eCName2, eCNumber1, eCNumber2, getActivity());
            Utilities.showProgressDialog(getActivity());
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET, APIConstant.BASE_URL + encodedString, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Success", response.toString());
                            try {
                                Utilities.hideProgressDialog();
                                // All these variables are getting flushed after
                                // this HTTP GET API call.

                                if (!response.isNull("RESPONSECODE") && !(response.getString("RESPONSECODE")
                                        .equals("000"))) {
                                    Utilities.removeSharedValueForKey(
                                            "ECNAME1", getActivity());
                                    Utilities.removeSharedValueForKey(
                                            "ECNUMBER1", getActivity());
                                    Utilities.removeSharedValueForKey(
                                            "ECNAME2", getActivity());
                                    Utilities.removeSharedValueForKey(
                                            "ECNUMBER2", getActivity());
                                }
                                if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("000")) {
                                    Utilities.putSharedString("USERNAME", username, getActivity());
                                    Utilities.putSharedString("MOBILE", phoneText, getActivity());
                                    isMobileNumberChanged = false;
                                }
                                if (!TextUtils.isEmpty(Utilities.getSharedString("USERNAME", getActivity()))) {
                                    ((HomeActivity) getActivity()).userName.setText(Utilities.getSharedString("USERNAME", getActivity()));
                                }

                                if (!TextUtils.isEmpty(Utilities.getSharedString("MOBILE", getActivity()))) {
                                    ((HomeActivity) getActivity()).mobileNumber.setText(Utilities.getSharedString("MOBILE", getActivity()));
                                }
                                if (isMobileNumberChanged) {
                                    dialogdisplay("Mobile Number Update", "Mobile number has been updated successfully.");
                                } else {
                                    dialogdisplay("Profile Update", "Your profile has been updated successfully .");
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utilities.hideProgressDialog();
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                    dialogdisplay("Network Error", "Please Try Again");
                    // Remove Shared Keys
                    Utilities.removeSharedValueForKey("ECNAME1",
                            getActivity());
                    Utilities.removeSharedValueForKey("ECNUMBER1",
                            getActivity());
                    Utilities.removeSharedValueForKey("ECNAME2",
                            getActivity());
                    Utilities.removeSharedValueForKey("ECNUMBER2",
                            getActivity());
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq,
                    tag_json_updateprofile);
        }
    }

    private void populatePersistantData() {
        if (!TextUtils.isEmpty(Utilities.getSharedString("EMAILID", getActivity()))) {
            emailId.setText(Utilities.getSharedString("EMAILID", getActivity()));
        }
        if (!TextUtils.isEmpty(Utilities.getSharedString("USERNAME", getActivity()))) {
            userName.setText(Utilities.getSharedString("USERNAME", getActivity()));
        }

        if (!TextUtils.isEmpty(Utilities.getSharedString("MOBILE", getActivity()))) {
            mobileNumber.setText(Utilities.getSharedString("MOBILE", getActivity()));
        }
        if (!TextUtils.isEmpty(Utilities.getSharedString("ECNAME1", getActivity()))) {
            emerencyContactName1.setText(Utilities.getSharedString("ECNAME1", getActivity()));
        }
        if (!TextUtils.isEmpty(Utilities.getSharedString("ECNUMBER1", getActivity()))) {
            emerencyContactNumber1.setText(Utilities.getSharedString("ECNUMBER1", getActivity()));
        }
        if (!TextUtils.isEmpty(Utilities.getSharedString("ECNAME2", getActivity()))) {
            emerencyContactName2.setText(Utilities.getSharedString("ECNAME2", getActivity()));
        }
        if (!TextUtils.isEmpty(Utilities.getSharedString("ECNUMBER2", getActivity()))) {
            emerencyContactNumber2.setText(Utilities.getSharedString("ECNUMBER2", getActivity()));
        }
    }
    //	private void selectPhoto()
    //	{
    //		final CharSequence[] items = { getResources().getString(R.string.TAKE_PHOTO),getResources().getString(R.string.CHOOSE_PHOTO_TEXT), "Cancel"};
    //
    //		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    //		builder.setCancelable(false);
    //		builder.setTitle( getResources().getString(R.string.ADD_PHOTO));
    //		builder.setItems(items, new DialogInterface.OnClickListener()
    //		{
    //			@Override
    //			public void onClick(DialogInterface dialog, int item)
    //			{
    //				if (items[item].equals("Take Photo"))
    //				{
    //					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    //					intent.putExtra(MediaStore.EXTRA_OUTPUT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString());
    //					// ******** code for crop image
    //					intent.putExtra("crop", "true");
    //					intent.putExtra("aspectX", 0);
    //					intent.putExtra("aspectY", 0);
    //					intent.putExtra("outputX", 150);
    //					intent.putExtra("outputY", 150);
    //
    //					try
    //					{
    //						intent.putExtra("return-data", true);
    //						startActivityForResult(intent, PICK_FROM_CAMERA);
    //					}
    //					catch (ActivityNotFoundException e)
    //					{
    //						// Do nothing for now
    //						e.printStackTrace();
    //					}
    //				}
    //				else if (items[item].equals("Choose Photo"))
    //				{
    //					try
    //					{
    //						Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    //						intent.putExtra("crop", "true");
    //						intent.putExtra("aspectX", 1);
    //						intent.putExtra("aspectY", 1);
    //						intent.putExtra("outputX", 150);
    //						intent.putExtra("outputY", 150);
    //						intent.putExtra("scale", true);
    //						startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_GALLERY);
    //					}
    //					catch (ActivityNotFoundException e)
    //					{
    //						// Do nothing for now
    //					}
    //				}
    //				else if (items[item].equals("Cancel"))
    //				{
    //					dialog.dismiss();
    //				}
    //			}
    //		});
    //		builder.show();
    //	}


    //	@Override
    //	public void onActivityResult(int requestCode, int resultCode, Intent data)
    //	{
    //		super.onActivityResult(requestCode, resultCode, data);
    //		mImageCaptureUri = data.getData();
    //
    //		mUserPhoto = null;
    //
    //		if (requestCode == PICK_FROM_CAMERA)
    //		{
    //			if (resultCode == getActivity().RESULT_OK)
    //			{
    //				if(data == null)
    //					return;
    //
    //				mUserPhoto = data.getParcelableExtra("data");
    //			}
    //		}
    //		else if (requestCode == PICK_FROM_GALLERY)
    //		{
    //			if (resultCode == getActivity().RESULT_OK)
    //			{
    //				if (data == null || data.getData() == null)
    //					return;
    //
    //				try
    //				{
    //					outPutFile = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
    //					doCrop();
    //					//mUserPhoto = BitmapFactory.decodeFile(getGalleryImagePath(data.getData()));
    //				}
    //				catch (Exception e)
    //				{
    //					e.printStackTrace();
    //				}
    //			}
    //		}
    //
    //		//		if (mUserPhoto != null)
    //		//		{
    //		//			try
    //		//			{
    //		//				int size = Utilities.getDIP(getActivity(), 50);
    //		//				mUserPhoto = Bitmap.createScaledBitmap(mUserPhoto, size, size, true);
    //		//				uploadUserProfilePhoto(mUserPhoto);
    //		//			}
    //		//			catch (Exception e)
    //		//			{
    //		//				e.printStackTrace();
    //		//			}
    //		//		}
    //
    //	}

    private void selectImageOption() {
        final CharSequence[] items =
                {"Capture Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Capture Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp1.png");
                    mImageCaptureUri = Uri.fromFile(f);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                    startActivityForResult(intent, CAMERA_CODE);

                } else if (items[item].equals("Choose from Gallery")) {
                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, GALLERY_CODE);

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_CODE && resultCode == Activity.RESULT_OK && null != data) {
            mImageCaptureUri = data.getData();
            System.out.println("Gallery Image URI : " + mImageCaptureUri);
            CropingIMG();

        } else if (requestCode == CAMERA_CODE && resultCode == Activity.RESULT_OK) {

            System.out.println("Camera Image URI : " + mImageCaptureUri);
            CropingIMG();
        } else if (requestCode == CROPING_CODE) {
            try {
                if (outPutFile.exists()) {
                    Bitmap photo = decodeFile(outPutFile);
                    int size = Utilities.getDIP(getActivity(), 50);
                    mUserPhoto = Bitmap.createScaledBitmap(photo, size, size, true);
                    uploadUserProfilePhoto(mUserPhoto);

                    //mProfilePhoto.setImageBitmap(photo);
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Error while save image", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void CropingIMG() {
        final ArrayList cropOptions = new ArrayList();

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        List list = getActivity().getPackageManager().queryIntentActivities(intent, 0);
        int size = list.size();
        if (size == 0) {
            Toast.makeText(getActivity(), "Cann't find image croping app", Toast.LENGTH_SHORT).show();
            return;
        } else {
            intent.setData(mImageCaptureUri);
            intent.putExtra("outputX", 512);
            intent.putExtra("outputY", 512);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);


            //Create output file here
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outPutFile));
            Intent i = new Intent(intent);
            ResolveInfo res = (ResolveInfo) list.get(0);
            i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            startActivityForResult(i, CROPING_CODE);
        }
    }

    private Bitmap decodeFile(File f) {
        try {
            // decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 216;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

    public String getGalleryImagePath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        String path = null;
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            path = cursor.getString(column_index);
            cursor.close();
        }
        return path;
    }

    String encodedImage = null;

    private void uploadUserProfilePhoto(Bitmap photo) {
        if (photo == null)
            return;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.PNG, 80, baos);
        byte[] byteArrayImage = baos.toByteArray();
        encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

        if (TextUtils.isEmpty(encodedImage))
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            makeUploadPhotoApi(encodedImage);
        }
    }

    private void makeUploadPhotoApi(String encodedImage) {
        Log.d("image", "upoload check");
        final String encodedUrl = JsonHeaders.createUploadUserPhotoJsonHeader(getActivity());
        Utilities.showProgressDialog(getActivity());
        Map<String, String> params = new HashMap<String, String>();
        params.put("DATA1", encodedUrl);
        params.put("DATA2", Utilities.getSharedString("CUSTOMERID", getActivity()));
        params.put("DATA3", encodedImage);
        params.put("DATA4", Utilities.getSharedString("SESSIONID", getActivity()));

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, "http://test.passenger.vowcabs.com:1220/imageUpload.aspx", new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Success", response.toString());
                        try {
                            if (response.getString("RESPONSECODE").equals("000")) {
                                new GetMyProfilePic().execute(PROFILEPIC_URL + Utilities.getSharedString("CUSTOMERID", getActivity()) + ".png");
                                dialogdisplay("Profile Update", "Profile picture has been updated successfully.");
                            }

                            Utilities.hideProgressDialog();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utilities.hideProgressDialog();
                        dialogdisplay("Network Error", "Please Try Again");
                    }
                });
        AppController.getInstance().addToRequestQueue(request, tag_json_updateprofile);
    }

    /*private void makeUploadPhotoApi(final String encodedImageString) {
        final String encodedUrl = JsonHeaders.createUploadUserPhotoJsonHeader(encodedImageString, getActivity());
        Utilities.showProgressDialog(getActivity());
        StringRequest jsonObjReq = new StringRequest(com.android.volley.Request.Method.POST, "http://test.passenger.vowcabs.com:1220/imageUpload.aspx?",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (getActivity() == null)
                            return;

                        String profilePicUrl = "";
                        Log.d("Success", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("RESPONSECODE").equals("000")) {
                                profilePicUrl = jsonObject.getString("BASEURL") + jsonObject.getString("USERPIC");
                                Utilities.putSharedString("USERPROFILEPICURL", profilePicUrl, getActivity());
                                new GetMyProfilePic().execute(profilePicUrl);
                                dialogdisplay("Profile Update", "Profile picture has been updated successfully.");
                            }

                            Utilities.hideProgressDialog();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.hideProgressDialog();
                dialogdisplay("Network Error", "Please Try Again");
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("DATA1", encodedUrl);
                params.put("DATA2", Utilities.getSharedString("CUSTOMERID", getActivity()));
                params.put("DATA3", Utilities.getEncryptedString(encodedImageString));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_updateprofile);*/


      /*  RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        String URL = "http://test.passenger.vowcabs.com:1220/imageUpload.aspx?";
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("DATA1", "Zs%2bQoYVoXkCR9VN72pauSSOtV9onvcH9ezFLDu9Bt5uoz80dYoK%2bm2OLdEVmttnMH6srjer9MK%2fB0MqFX9w%2b3verMl6X9DX7cyfAKHddjz8%3d");
            jsonBody.put("DATA2", Utilities.getSharedString("CUSTOMERID", getActivity()));
            jsonBody.put("DATA3", encodedImageString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = jsonBody.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("VOLLEY", response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };

        requestQueue.add(stringRequest);*/

    //}

    private class GetMyProfilePic extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... urls) {
            Bitmap map = null;
            for (String url : urls) {
                map = downloadImage(url);
            }
            return map;
        }

        // Sets the Bitmap returned by doInBackground
        @Override
        protected void onPostExecute(Bitmap result) {
            try {
                mProfilePhoto.setImageBitmap(Utilities.makeCircularBitmap(result, 280, 280));
                ((HomeActivity) getActivity()).profileIcon.setImageBitmap(Utilities.makeCircularBitmap(result, 80, 80));
                Drawable drawable = getResources().getDrawable(R.drawable.outercircle);
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                if (bitmap != null) {
                    proifleBg.setImageBitmap(Utilities.makeCircularBitmap(bitmap, 320, 320));
                    plusIcon.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Creates Bitmap from InputStream and returns it
        private Bitmap downloadImage(String url) {
            Bitmap bitmap = null;
            InputStream stream = null;
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize = 1;

            try {
                stream = getHttpConnection(url);
                bitmap = BitmapFactory.decodeStream(stream, null, bmOptions);
                if (stream != null)
                    stream.close();

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return bitmap;
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestMethod("GET");
                httpConnection.connect();

                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }
    }

    /*public void makeUploadPhotoApi(final String encodedImageString)  throws UnsupportedEncodingException {
        final String encodedUrl = JsonHeaders.createUploadUserPhotoJsonHeader(encodedImageString, getActivity());

        String data = URLEncoder.encode("DATA1", "UTF-8") + "=" + URLEncoder.encode(encodedUrl, "UTF-8");
        data += "&" + URLEncoder.encode("DATA2", "UTF-8") + "=" + URLEncoder.encode(Utilities.getSharedString("CUSTOMERID", getActivity()), "UTF-8");
        data += "&" + URLEncoder.encode("DATA3", "UTF-8") + "=" + URLEncoder.encode(encodedImageString, "UTF-8");
        String text = "";
        BufferedReader reader = null;
        try {
            URL url = new URL("http://test.passenger.vowcabs.com:1220/imageUpload.aspx?");
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                // Append server response in string
                sb.append(line + "\n");
            }
            text = sb.toString();
        } catch (Exception ex) {
            Log.d("Exception ", "exe "+ex.toString());

        } finally {
            try {

                reader.close();
            } catch (Exception ex) {
            }
        }
        Log.d("Image", "repsonse "+ text);
    }*/

}



