package com.vowcabs.passengerapp.Models;

import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class Driver 
{
	private String driverId;

	private String driverName;
	private double driverLAT;
	private double driverLONG;
	private String driverStatus;
	private String driverIMEI;
	private String driverDlNumber;
	private String driverCarId;
	private long distanceFromPickupLoc;
	private String driverLastUpdatedTime;
	private String driverCabType;
	private String driverValidate;
	private String driverPhone;



	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public double getDriverLAT() {
		return driverLAT;
	}
	public void setDriverLAT(double driverLAT) {
		this.driverLAT = driverLAT;
	}
	public double getDriverLONG() {
		return driverLONG;
	}
	public void setDriverLONG(double driverLONG) {
		this.driverLONG = driverLONG;
	}
	public String getDriverStatus() {
		return driverStatus;
	}
	public void setDriverStatus(String driverStatus) {
		this.driverStatus = driverStatus;
	}
	public String getDriverIMEI() {
		return driverIMEI;
	}
	public void setDriverIMEI(String driverIMEI) {
		this.driverIMEI = driverIMEI;
	}
	public String getDriverDlNumber() {
		return driverDlNumber;
	}
	public void setDriverDlNumber(String driverDlNumber) {
		this.driverDlNumber = driverDlNumber;
	}
	public String getDriverCarId() {
		return driverCarId;
	}
	public void setDriverCarId(String driverCarId) {
		this.driverCarId = driverCarId;
	}
	public String getDriverLastUpdatedTime() {
		return driverLastUpdatedTime;
	}
	public void setDriverLastUpdatedTime(String driverLastUpdatedTime) {
		this.driverLastUpdatedTime = driverLastUpdatedTime;
	}
	public String getDriverCabType() {
		return driverCabType;
	}
	public void setDriverCabType(String driverCabType) {
		this.driverCabType = driverCabType;
	}
	public String getDriverValidate() {
		return driverValidate;
	}
	public void setDriverValidate(String driverValidate) {
		this.driverValidate = driverValidate;
	}
	public String getDriverPhone() {
		return driverPhone;
	}
	public void setDriverPhone(String driverPhone) {
		this.driverPhone = driverPhone;
	}
	public long getDistanceFromPickupLoc() 
	{
		return distanceFromPickupLoc;
	}

	public void setDistanceFromPickupLoc(long distanceFromPickupLoc) 
	{
		this.distanceFromPickupLoc = distanceFromPickupLoc;
	}

	public Driver(JSONObject serverResponse)
	{
		try 
		{
			if(!serverResponse.isNull("LAT") && !TextUtils.isEmpty(serverResponse.getString("LAT")))
			{
				setDriverLAT(Double.parseDouble(serverResponse.getString("LAT")));
			}
			if(!serverResponse.isNull("LNG") && !TextUtils.isEmpty(serverResponse.getString("LNG")))
			{
				setDriverLONG(Double.parseDouble(serverResponse.getString("LNG")));
			}
			if(!serverResponse.isNull("NAME") && !TextUtils.isEmpty(serverResponse.getString("NAME")))
			{
				setDriverName(serverResponse.getString("NAME"));
			}
//			if(!serverResponse.isNull("LAT") && !TextUtils.isEmpty(serverResponse.getString("LAT")) &&
//					!serverResponse.isNull("LNG") && !TextUtils.isEmpty(serverResponse.getString("LNG")))
//			{
//				setDistanceFromPickupLoc(DistanceCalculator.getInstance().calculateDistance(Double.valueOf(serverResponse.getString("LAT")), 
//						Double.valueOf(serverResponse.getString("LNG")), LocationInstance.getInstance().getLatitude(),
//						LocationInstance.getInstance().getLongitute()));
//			}

		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}

	}

}
