package com.vowcabs.passengerapp.fragment;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.AppController;
import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.Models.Booking;
import com.vowcabs.passengerapp.MyApplication;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.adapter.MyTripsListAdapter;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyTripsFragment extends HomeBaseFragment implements OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    TextView pastTrip;
    TextView futureTrip;
    TextView presentTrip;
    TextView noTrips;
    View rootView = null;
    String mCurrenType = null;
    String encodedString;
    String tag_getmytrips_obj = "getmytrips_req";
    ArrayList<Booking> mybookingsList = new ArrayList<Booking>();
    ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    MyTripsListAdapter listAdapter;
    boolean flag_loading = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.mytripscreen, container, false);
        } else {
            ViewGroup oldParent = (ViewGroup) rootView.getParent();
            if (oldParent != null)
                oldParent.removeView(rootView);
        }
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        listView = (ListView) getView().findViewById(R.id.mytrips_list);
        noTrips = (TextView) getView().findViewById(R.id.no_trips);
        swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_refresh_layout);

        ((HomeActivity) getActivity()).updateHeaderText("My Trips", mActivity);
        ((HomeActivity) getActivity()).favouritesIcon.setVisibility(View.GONE);


        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);

                                        getMyTrips(0, 20);
                                    }
                                }
        );

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (totalItemCount != 0 && visibleItemCount % 20 == 0) {
                    if (flag_loading == false) {
                        flag_loading = true;
                        getMyTrips(visibleItemCount, 20);
                    }
                }
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) mActivity).topBar.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).updateHeaderText("My Trips", mActivity);

        MyApplication.getInstance().trackScreenView("My Trips Screen");
    }

    void setSelectedCurrentType(String inType) {
        deselectAll();
        if (inType.equals("completed")) {
            selectTabButton(pastTrip, true, R.drawable.my_trip_pressed_icon);
        } else if (inType.equals("progress")) {
            selectTabButton(presentTrip, true, R.drawable.my_trip_pressed_icon);
        } else if (inType.equals("forthcoming")) {
        }
        mCurrenType = inType;
    }

    @Override
    public void onClick(View v) {
        if (v == pastTrip) {
            mCurrenType = "completed";
        } else if (v == presentTrip) {
            mCurrenType = "progress";
        } else if (v == futureTrip) {
            mCurrenType = "forthcoming";
        }
        setSelectedCurrentType(mCurrenType);
    }

    void selectTabButton(TextView textButton, boolean select, int backgroundcolor) {
        if (textButton != null) {
            if (select) {
                textButton.setBackgroundResource(backgroundcolor);
            } else {
                textButton.setBackgroundDrawable(null);
            }
        }
    }

    void deselectAll() {
        selectTabButton(pastTrip, false, R.drawable.my_trip_icon);
    }

    public void getMyTrips(int offset, int range) {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            Utilities.showProgressDialog(getActivity());
            // showing refresh animation before making http call
            swipeRefreshLayout.setRefreshing(true);

            encodedString = JsonHeaders.createMyTripsApiJSONHeader(getActivity(), offset, range);
            //Log.d("My trips url", APIConstant.BASE_URL + encodedString.toString());
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET, APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //Log.d(tag_getmytrips_obj, "Sucess" +response);
                    try {
                        parseJsonResponse(response);
                        //storeLastSyncTime(response);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    // stopping swipe refresh
                    swipeRefreshLayout.setRefreshing(false);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // stopping swipe refresh
                    swipeRefreshLayout.setRefreshing(false);
                    Utilities.hideProgressDialog();
                    VolleyLog.d("Parsing " + tag_getmytrips_obj, "Error: " + error.getMessage());
                    Utilities.dialogdisplay("Network Error", "Please Try Again", getActivity());
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_getmytrips_obj);
        }

    }


    private void parseJsonResponse(JSONObject response) {
        try {
            if (getActivity() == null)
                return;
//		if (response.getJSONArray("BOOKINGS").length() > 0 || response.getJSONArray("AUTOBOOKINGS").length() > 0)
            if (!response.isNull("BOOKINGS") && response.getJSONArray("BOOKINGS").length() > 0) {
                JSONArray bookingsArray = response.getJSONArray("BOOKINGS");
                mybookingsList.clear();
                for (int index = 0; index < bookingsArray.length(); index++) {
                    JSONObject booking = bookingsArray.getJSONObject(index);
                    Booking bookingObj = new Booking(booking);
                    mybookingsList.add(bookingObj);
                }
//			JSONArray autoBookingsArray= response.getJSONArray("AUTOBOOKINGS");
//			for (int index =0 ; index< autoBookingsArray.length(); index ++)
//			{
//				JSONObject autoBooking = autoBookingsArray.getJSONObject(index);
//				Booking autobookingObj = new Booking(autoBooking);
//				mybookingsList.add(autobookingObj);
//			}
                //Log.d("My Trips Count", ""+mybookingsList.size());
            } else {
                Utilities.dialogdisplay("No Trips", "There is no current trips.", getActivity());
                //noTrips.setVisibility(View.VISIBLE);
            }
            displayListView();
        } catch (JSONException e) {
        } catch (Exception e) {
        }

    }

    public void displayListView() {

        listAdapter = new MyTripsListAdapter(getActivity(), mybookingsList, listView);
        listView.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
        Utilities.hideProgressDialog();

    }


    private void storeLastSyncTime(JSONObject response) {
        if (getActivity() == null)
            return;

        try {
            if (response != null && !response.isNull("LASTSYNCTIME") && !TextUtils.isEmpty(response.getString("LASTSYNCTIME"))) {
                Utilities.putSharedString("LASTSYNCTIME", response.getString("LASTSYNCTIME"), getActivity());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRefresh() {

        getMyTrips(0, 20);
    }
}