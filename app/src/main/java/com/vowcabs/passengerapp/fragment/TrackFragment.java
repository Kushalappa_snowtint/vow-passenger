package com.vowcabs.passengerapp.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vowcabs.passengerapp.AppController;
import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.Models.Booking;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.json.JSONKEYS;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.LatLngInterpolator;
import com.vowcabs.passengerapp.utils.LocationInstance;
import com.vowcabs.passengerapp.utils.MarkerAnimation;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static com.vowcabs.passengerapp.R.id.trip;

@SuppressLint("ValidFragment")
public class TrackFragment extends HomeBaseFragment implements OnClickListener, OnMapReadyCallback, OnCameraChangeListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private GoogleMap googleMap;
    static View mRootView = null;

    private String tag_req_trackuser = "jobj_req_trackuser";
    private String tag_req_trackdriver = "jobj_req_trackdriver";
    private LocationManager locationManager;
    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;
    TextView driverName;
    TextView driverNumber;
    TextView driverCarDetails;
    TextView driverCarNumber;
    TextView callDriver;
    TextView shareLocation;
    TextView estimatedText;
    String activeDriverNumber = "";
    String trackUrl = "";
    String tripId = "";
    Marker driverMarker;
    double driverLatitude = 0.0;
    double driverLongitude = 0.0;
    double driverPreviousLat = 0.0;
    double driverPreviousLng = 0.0;
    float headingAngle = 0.0f;
    double pickupLatitude;
    double pickupLongitude;
    LatLng destLatLng;
    LinearLayout driverLoc;
    TimerTask tTask;
    Timer animationTimer;
    TimerTask etaTask;
    Timer etaTimer;
    Marker passengerLoc;
    String estimatedTime;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    public static final float DEFAULT_ZOOM_LEVEL = 17.0f;
    private int tripDriverStatus = 0;
    int markerIcon = R.drawable.mini_marker;
    ArrayList<LatLng> tripPreviousLatLng = new ArrayList<>();


    public TrackFragment() {

    }

    public TrackFragment(Booking referenceId) {
        this.tripId = referenceId.getBookingId();

        if (!TextUtils.isEmpty(referenceId.getmDestination().getLat().trim()) && !TextUtils.isEmpty(referenceId.getmDestination().getLng().trim())) {
            destLatLng = new LatLng(Double.parseDouble(referenceId.getmDestination().getLat().trim()), Double.parseDouble(referenceId.getmDestination().getLng().trim()));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView != null) {
            ViewGroup parent = (ViewGroup) mRootView.getParent();
            if (parent != null)
                parent.removeView(mRootView);
        }
        try {
            mRootView = inflater.inflate(R.layout.track_fragment, container, false);
        } catch (InflateException e) {
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tripId = bundle.getString(JSONKEYS.TRIP_ID, "");
        }


        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        driverName = (TextView) getView().findViewById(R.id.tv_driver_name);
        driverNumber = (TextView) getView().findViewById(R.id.tv_driver_number);
        driverCarDetails = (TextView) getView().findViewById(R.id.tv_car_details);
        driverCarNumber = (TextView) getView().findViewById(R.id.tv_driver_car_number);
        callDriver = (TextView) getView().findViewById(R.id.tv_callDriver);
        shareLocation = (TextView) getView().findViewById(R.id.tv_shareLocation);
        estimatedText = (TextView) getView().findViewById(R.id.tv_estimateTime);
        driverLoc = (LinearLayout) getView().findViewById(R.id.ll_locationMarker);
        callDriver.setOnClickListener(this);
        shareLocation.setOnClickListener(this);
        //driverLoc.setOnClickListener(this);
        // Loading map
        initilizeMap();
        makeTrackUserApiCall(true);

        try {
            ((HomeActivity) getActivity()).searchIcon.setVisibility(View.GONE);

            if (LocationInstance.getInstance().selectedCar == 0) {
                markerIcon = R.drawable.mini_marker;
            } else if (LocationInstance.getInstance().selectedCar == 1) {
                markerIcon = R.drawable.sedan_marker;
            } else if (LocationInstance.getInstance().selectedCar == 2) {
                markerIcon = R.drawable.prime_marker;
            } else if (LocationInstance.getInstance().selectedCar == 3) {
                markerIcon = R.drawable.auto_marker;
            }

        } catch (Exception e) {
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    private void makeTrackUserApiCall(final boolean showProgress) {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            String encodedString = JsonHeaders.createTrackUsersApiJsonHeader(tripId, getActivity());

            if (showProgress) {
                Utilities.showProgressDialog(getActivity());
            }

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Success", response.toString());
                    Utilities.hideProgressDialog();

                    try {
                        if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("000")) {
                            if (!response.isNull("TRACKURL") && !TextUtils.isEmpty(response.getString("TRACKURL"))) {
                                trackUrl = response.getString("TRACKURL");
                            }
                            if (!response.isNull("TRIPDETAIL")) {
                                if (!showProgress)
                                    return;

                                for (int index = 0; index < response.getJSONArray("TRIPDETAIL").length(); index++) {
                                    //Log.e("Trip Status", response.getJSONArray("TRIPDETAIL").getJSONObject(index).getString("TRIPSTATUS"));
                                    if (response.getJSONArray("TRIPDETAIL").getJSONObject(index).getString("TRIPSTATUS").equals("1")) {
                                        pickupLatitude = Double.valueOf(response.getJSONArray("TRIPDETAIL").getJSONObject(index).getString("LAT"));
                                        pickupLongitude = Double.valueOf(response.getJSONArray("TRIPDETAIL").getJSONObject(index).getString("LNG"));
                                        ((HomeActivity) getActivity()).updateHeaderText("Driver On the way", mActivity);
                                    }
                                }
                            }
                            if (!response.isNull("DRIVER")) {
                                if (!TextUtils.isEmpty(response.getJSONObject("DRIVER").getString("NAME"))) {
                                    driverName.setText(response.getJSONObject("DRIVER").getString("NAME"));
                                }
                                if (!TextUtils.isEmpty(response.getJSONObject("DRIVER").getString("PHONE"))) {
                                    activeDriverNumber = response.getJSONObject("DRIVER").getString("PHONE");
                                    driverNumber.setText(response.getJSONObject("DRIVER").getString("PHONE"));
                                }

                                if (!TextUtils.isEmpty(response.getJSONObject("DRIVER").getString("CARID"))) {
                                    driverCarNumber.setText(response.getJSONObject("DRIVER").getString("CARID"));
                                }

                                if (!TextUtils.isEmpty(response.getJSONObject("DRIVER").getString("CABMANUFACTURER"))) {
                                    driverCarDetails.setText(response.getJSONObject("DRIVER").getString("CABMANUFACTURER"));
                                }
                                if (!TextUtils.isEmpty(response.getJSONObject("DRIVER").getString("LAT"))) {
                                    driverLatitude = Double.valueOf(response.getJSONObject("DRIVER").getString("LAT"));
                                }
                                if (!TextUtils.isEmpty(response.getJSONObject("DRIVER").getString("LNG"))) {
                                    driverLongitude = Double.valueOf(response.getJSONObject("DRIVER").getString("LNG"));
                                }
                            }
                            if (!response.isNull("DRIVERSTATUSONBOOKING") && response.getString("DRIVERSTATUSONBOOKING").trim().equalsIgnoreCase("3")) {
                                tripDriverStatus = 3;  // Meter Start Triggered
                                ((HomeActivity) getActivity()).updateHeaderText("On Board", mActivity);
                            }
                        } else {
                            Utilities.dialogdisplay("Track Info", "Driver not yet Assined.Please try after some time", getActivity());
                        }
                        if (driverLatitude != 0.0 && driverLongitude != 0.0)
                            moveGoogleMapsCameraPositon(new LatLng(driverLatitude, driverLongitude));

                        getEstimatedTime();
                        startMarkerAnimation();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utilities.hideProgressDialog();
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                    Utilities.dialogdisplay("Network Error", "Please Try Again", getActivity());

                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_req_trackuser);
        }

    }

    private void makeTrackDriverApiCall() {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            String encodedString = JsonHeaders.createTrackUsersApiJsonHeader(tripId, getActivity());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Success", response.toString());
                    try {
                        if (!response.isNull("DRIVER")) {
                            if (!TextUtils.isEmpty(response.getJSONObject("DRIVER").getString("LAT")) && !response.getJSONObject("DRIVER").getString("LAT").equalsIgnoreCase("0.0")) {
                                try {
                                    driverLatitude = Double.parseDouble(response.getJSONObject("DRIVER").getString("LAT"));
                                } catch (NumberFormatException e) {
                                }
                            }
                            if (!TextUtils.isEmpty(response.getJSONObject("DRIVER").getString("LNG")) && !response.getJSONObject("DRIVER").getString("LNG").equalsIgnoreCase("0.0")) {
                                try {
                                    driverLongitude = Double.parseDouble(response.getJSONObject("DRIVER").getString("LNG"));
                                } catch (NumberFormatException e) {
                                }
                            }
                            if (!TextUtils.isEmpty(response.getJSONObject("DRIVER").getString("HEADINGANGLE")) && !response.getJSONObject("DRIVER").getString("HEADINGANGLE").equalsIgnoreCase("0.0")) {
                                try {
                                    headingAngle = Float.parseFloat(response.getJSONObject("DRIVER").getString("HEADINGANGLE"));
                                } catch (NumberFormatException e) {
                                }

                            }
                            moverMarkerPosition();
                        }

                        if (!response.isNull("DRIVERSTATUSONBOOKING") && response.getString("DRIVERSTATUSONBOOKING").trim().equalsIgnoreCase("3")) {
                            tripDriverStatus = 3;  // Meter Start Triggered
                            try {
                                ((HomeActivity) getActivity()).updateHeaderText("On Board", mActivity);
                            } catch (Exception e) {
                            }
                        }

                        if (!response.isNull("DRIVERSTATUSONBOOKING") && response.getString("DRIVERSTATUSONBOOKING").trim().equalsIgnoreCase("4")) {
                            tripDriverStatus = 4;  // Meter End Triggered
                            try {
                                if (!TextUtils.isEmpty(tripId)) {
                                    ((HomeActivity) getActivity()).pushFragment(new TripDetailsSriLankaFragment(tripId), false, true);
                                }
                                ((HomeActivity) getActivity()).updateHeaderText("Trip Completed", mActivity);
                            } catch (Exception e) {
                            }
                        }
                    } catch (JSONException e) {
                    }

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_req_trackdriver);
        }
    }

    private void initilizeMap() {
        if (getActivity() == null)
            return;

        int status = com.google.android.gms.common.GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

        // Showing status
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, mActivity, requestCode);
            dialog.show();
        } else {
            getMapFragment().getMapAsync(this);
        }
    }

    private MapFragment getMapFragment() {
        FragmentManager fm = null;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            fm = getFragmentManager();
        } else {
            fm = getChildFragmentManager();
        }

        return (MapFragment) fm.findFragmentById(R.id.tracking_mapView);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.googleMap = map;

        // Enabling MyLocation Layer of Google Map
        googleMap.setMyLocationEnabled(true);
        googleMap.setOnCameraChangeListener(this);
        buildGoogleApiClient();

        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    protected synchronized void buildGoogleApiClient() {
        try {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            Utilities.locationChecker(mGoogleApiClient, getActivity());
        } catch (Exception e) {
        }
    }

    private void moveGoogleMapsCameraPositon(LatLng myLaLn) {
        CameraPosition camPos = new CameraPosition.Builder().target(myLaLn).zoom(DEFAULT_ZOOM_LEVEL).bearing(0).tilt(0).build();
        CameraUpdate camUpd3 = CameraUpdateFactory.newCameraPosition(camPos);
        googleMap.moveCamera(camUpd3);
        googleMap.animateCamera(camUpd3);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            initilizeMap();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCameraChange(CameraPosition arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onLocationChanged(Location location) {
        // TODO Auto-generated method stub
        try {
            // latitude and longitude
//				latitude  = location.getLatitude();
//				longitude = location.getLongitude();
//				LatLng myLaLn = new LatLng(latitude, longitude);
            //moveGoogleMapsCameraPositon(myLaLn);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == callDriver) {
            if (activeDriverNumber.length() > 0)
                initiateCalltoCustomerCare(activeDriverNumber);
        }
        if (v == shareLocation) {
            shareTrackDetails(trackUrl);
        }

        if (v == driverLoc) {
//			if (driverLatLng == null)
//				return;
//
//			googleMap.moveCamera( CameraUpdateFactory.newLatLngZoom(driverLatLng , 16.0f));
        }
    }

    private void initiateCalltoCustomerCare(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void shareTrackDetails(String trackDetails) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(trackDetails)));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        try {
            Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                onLocationChanged(mLastLocation);
                //Log.e("onConnected :", "" + mLastLocation);
            }
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(600); //6 seconds
            mLocationRequest.setFastestInterval(600); //6 seconds
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            mLocationRequest.setSmallestDisplacement(1000F);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, TrackFragment.this);
        } catch (Exception e) {
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        // TODO Auto-generated method stub

    }

    private void startMarkerAnimation() {
        if (animationTimer != null) {
            animationTimer.cancel();
            animationTimer = null;
        }

        animationTimer = new Timer();

        tTask = new TimerTask() {

            @Override
            public void run() {
                if (getActivity() == null)
                    return;

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        makeTrackDriverApiCall();

                    }
                });
            }
        };
        animationTimer.scheduleAtFixedRate(tTask, 5000, 5000);
    }

    private void addDrivermarker() {
        if (googleMap != null) {
            driverMarker = googleMap.addMarker(new MarkerOptions()
                    .flat(true)
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.mini_marker))
                    .anchor(0.5f, 0.5f)
                    .rotation(headingAngle)
                    .position(new LatLng(driverLatitude, driverLongitude)));
        }
    }

    private void moverMarkerPosition() {
        if (getActivity() == null)
            return;

        LatLng sourceLaLn = null, destinationLaLn = null;
        if (driverLatitude != 0.0 && driverLongitude != 0.0) {

            if (driverPreviousLat != 0.0 && driverPreviousLng != 0.0) {
                sourceLaLn = new LatLng(driverPreviousLat, driverPreviousLng);
                destinationLaLn = new LatLng(driverLatitude, driverLongitude);

                if (!tripPreviousLatLng.contains(destinationLaLn)) {
                    removeDriverMarker();
                    driverMarker = googleMap.addMarker(new MarkerOptions()
                            .flat(true)
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.mini_marker))
                            .anchor(0.5f, 0.5f)
                            .position(sourceLaLn));
                    LatLngInterpolator mLatLngInterpolator = new LatLngInterpolator.Spherical();
                    MarkerAnimation.animateMarker(driverMarker, sourceLaLn, destinationLaLn, mLatLngInterpolator, headingAngle);
                    tripPreviousLatLng.add(destinationLaLn);
                    startETATimer();

                }
            }

            driverPreviousLat = driverLatitude;
            driverPreviousLng = driverLongitude;
        }

//
//
//            myLaLn = new LatLng(driverLatitude, driverLongitude);
//
//            if (tripPreviousLatLng != null && !tripPreviousLatLng.contains(myLaLn))
//            {
//                removeDriverMarker();
//                driverMarker = googleMap.addMarker(new MarkerOptions()
//                        .flat(true)
//                        .icon(BitmapDescriptorFactory
//                                .fromResource(R.drawable.mini_marker))
//                        .anchor(0.5f, 0.5f)
//                        .position(myLaLn));
//
//                LatLng finalPosition = new LatLng(driverLatitude, driverLongitude);
//                LatLngInterpolator mLatLngInterpolator = new LatLngInterpolator.Spherical();
//
//
//                Log.e("PREVIOUS LAT :", ""+myLaLn);
//                Log.e("CURRENT LAT :", ""+finalPosition);
//               // Log.e("DRIVER MARKER  :", ""+driverMarker);
//
//                MarkerAnimation.animateMarker(driverMarker, finalPosition, mLatLngInterpolator);
//
//                driverPreviousLat = driverLatitude;
//                driverPreviousLng = driverLongitude;
//                tripPreviousLatLng.add(myLaLn);
//
//            }
//            if (tripCurrentLatLng != null && !tripCurrentLatLng.contains(new LatLng(driverLatitude, driverLongitude)))
//                driverPreviousLat = driverLatitude;
//                driverPreviousLng = driverLongitude;
//
//                //tripCurrentLatLng.add(new LatLng(driverLatitude, driverLongitude));
//
//                myLaLn = new LatLng(driverPreviousLat, driverPreviousLng);
//                tripPreviousLatLng.add(myLaLn);
//
//            }
//        else if (startingPointLatLng!= null && startingPointLatLng.latitude != driverLatitude && startingPointLatLng.longitude != driverLongitude)
//          {
//           driverPreviousLat = driverLatitude;
//           driverPreviousLng = driverLongitude;
//          }
    }

    public void removeDriverMarker() {
        if (driverMarker != null) {
            driverMarker.remove();
            driverMarker = null;
        }
    }

    private void startETATimer() {
        if (etaTimer != null) {
            etaTimer.cancel();
            etaTimer = null;
        }

        etaTimer = new Timer();

        etaTask = new TimerTask() {
            @Override
            public void run() {
                if (getActivity() == null)
                    return;

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getEstimatedTime();
                    }
                });
            }
        };
        etaTimer.schedule(etaTask, 8000);
    }

    void getEstimatedTime() {
        LatLng sourceLatLng = new LatLng(driverLatitude, driverLongitude);
        LatLng destinationLatLng = null;
        if (tripDriverStatus == 3) {
            destinationLatLng = destLatLng;  // Driver started the trip and navigating to destination.
        } else {
            destinationLatLng = new LatLng(pickupLatitude, pickupLongitude);  //Driver accepted the trip and navigating to pickup location.
        }

        if (sourceLatLng != null && destinationLatLng != null) {
            String url = Utilities.getDirectionsUrl(sourceLatLng, destinationLatLng, getActivity());
            makeGoogleMapsDirectionsApi(url, sourceLatLng, destinationLatLng);
        }
    }

    private void makeGoogleMapsDirectionsApi(String url, final LatLng sourceLatLng, final LatLng destinLatLng) {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    estimatedTime = Utilities.calculateEstimatedTravelTime(response);
                    if (googleMap != null) {
                        googleMap.clear();

                        if (destinLatLng != null)

                        {
                            passengerLoc = googleMap.addMarker(new MarkerOptions()
                                    .flat(true)
                                    .icon(BitmapDescriptorFactory.defaultMarker())
                                    .anchor(0.5f, 0.5f)
                                    .position(destinLatLng));
                        }
                        estimatedText.setText("ETA : " + estimatedTime);

                        if (tripDriverStatus != 3 && estimatedTime.startsWith("1 min")) {
                            ((HomeActivity) getActivity()).updateHeaderText("Driver is Arriving", mActivity);
                        }

//                        passengerLoc = googleMap.addMarker(new MarkerOptions().position(new LatLng(pickupLatitude, pickupLongitude)).title("")
//                                .snippet("").icon(BitmapDescriptorFactory.fromBitmap(Utilities.writeTextOnDrawable(R.drawable.eta_background, estimatedTime, getActivity()))));
                        // PathJSONParser.drawPath(response.toString(),googleMap);
                        addDrivermarker();
                        // moveGoogleMapsCameraPositon(sourceLatLng);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                    Utilities.hideProgressDialog();
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, "googlemaps_directions_api_track");
        }
    }
}
