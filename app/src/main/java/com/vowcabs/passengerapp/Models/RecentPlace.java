package com.vowcabs.passengerapp.Models;

import org.json.JSONException;
import org.json.JSONObject;

public class RecentPlace 
{
	public String id;
	public String customerId;
	Place mPlace;


	public String getId() 
	{
		return id;
	}
	public void setId(String id) 
	{
		this.id = id;
	}
	public String getCustomerId() 
	{
		return customerId;
	}
	public void setCustomerId(String customerId) 
	{
		this.customerId = customerId;
	}

	public Place getmPlace() 
	{
		return mPlace;
	}
	public void setmPlace(Place mPlace)
	{
		this.mPlace = mPlace;
	}

	public RecentPlace(JSONObject placeObj) 
	{
		try {
			if(!placeObj.isNull("ID"))
			{
				setId(placeObj.getString("ID"));
			} 

			if(!placeObj.isNull("PLACE"))
			{
				mPlace = new Place(placeObj.getJSONObject("PLACE"));
				setmPlace(mPlace);
			}
			if(!placeObj.isNull("CUSTOMERID"))
			{
				setCustomerId(placeObj.getString("CUSTOMERID"));
			}
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



}
