package com.vowcabs.passengerapp.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.AppController;
import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.json.JSONKEYS;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.BaseConstants;
import com.vowcabs.passengerapp.utils.DateTime;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

@SuppressLint("ValidFragment")
public class TripDetailsFragment extends HomeBaseFragment
{
	String bookingID ;
	private TextView greetingMsg, tripId, tripDate, tripFare, sourceLoc, destinationLoc, totalFare, extraKMS,extraKMSFare, combinedFare, serviceTax;
	static View mRootView = null;
	LinearLayout tripBill;

	private String tag_bookings_details = "tag_bookingdetails";
	public TripDetailsFragment(String bookingid)
	{
		bookingID = bookingid;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) 
	{
		if (mRootView != null) 
		{
			ViewGroup parent = (ViewGroup) mRootView.getParent();
			if (parent != null)
				parent.removeView(mRootView);
		}
		try 
		{
			mRootView = inflater.inflate(R.layout.trip_details_screen, container, false);
		}
		catch (InflateException e) 
		{
			/* map is already there, just return view as it is */
		}
		return mRootView;
	}
	@Override
	public void onResume() 
	{
		super.onResume();
		((HomeActivity) getActivity()).updateHeaderText("Trip Details ", mActivity);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) 
	{
		super.onActivityCreated(savedInstanceState);
		greetingMsg     = (TextView) getView().findViewById(R.id.greeting_msg);
		tripId          = (TextView) getView().findViewById(R.id.trip_number);
		tripFare        = (TextView) getView().findViewById(R.id.tv_price_numericals);
		tripDate        = (TextView) getView().findViewById(R.id.tv_date);
		sourceLoc       = (TextView) getView().findViewById(R.id.tv_from);
		destinationLoc  = (TextView) getView().findViewById(R.id.tv_to);
		totalFare       = (TextView) getView().findViewById(R.id.tv_totalfare);
		extraKMS        = (TextView) getView().findViewById(R.id.tv_extraKms);
		extraKMSFare    = (TextView) getView().findViewById(R.id.tv_extraKmsFare);
		combinedFare    = (TextView) getView().findViewById(R.id.tv_total);
		//serviceTax      = (TextView) getView().findViewById(R.id.tv_serviceTax);
		tripBill        = (LinearLayout) getView().findViewById(R.id.ll_trip_details);
		makeBookingDetailsApi();
	}
	private void makeBookingDetailsApi()
	{
		if (getActivity() == null)
			return;

		if (Utilities.isNetworkAvailable(getActivity()))
		{
			Utilities.showProgressDialog(getActivity());
			String encodedString  = JsonHeaders.createBookingDeatailsApiJSONHeader(getActivity(), bookingID);
			Log.d("Tripdetails String",(APIConstant.BASE_URL+encodedString).toString());
			JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
					APIConstant.BASE_URL +encodedString, null, new Response.Listener<JSONObject>()
					{
				@Override
				public void onResponse(JSONObject response)
				{
					System.out.println(response.toString());
					Log.d("Success", response.toString());
					try 
					{
						Utilities.hideProgressDialog();
						if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("000"))
						{
							JSONObject bookings = response.getJSONObject("BOOKING");

							//String bookingid = bookings.getString("BOOKINGID");
							String referenceid = bookings.getString("REFERENCEID");
							tripId.setText(referenceid);

							JSONObject customer = (JSONObject) bookings.get("CUSTOMER");
							//String customerID = customer.getString("ID");

							if(!TextUtils.isEmpty(customer.getString("NAME")))
							{
								greetingMsg.setText("Hi, "+ customer.getString("NAME")+" thanks for riding with us");
							}
							tripDate.setText(DateTime.changeDateFormat(bookings.getString("BOOKINGDATE")));
							//String phoneNumber=customer.getString("PHONE");
							//String email=customer.getString("EMAIL");

							JSONObject destination=bookings.getJSONObject("DESTINATION");
							String destination1=destination.getString("NAME");
							destinationLoc.setText(destination1);
							JSONObject source=bookings.getJSONObject("SOURCE");
							String source1=source.getString("NAME");
							sourceLoc.setText(source1);

                            if(!TextUtils.isEmpty(bookings.getString("TOTALFARE").trim()))
                            {
                                tripFare.setText("Rs." + removeLeadingZeros(bookings.getString("TOTALFARE").trim()));
                                totalFare.setText("Rs." + removeLeadingZeros(bookings.getString("TOTALFARE").trim()));
                            }


							if(!TextUtils.isEmpty(bookings.getString("TRAVELKM")))
							{
								float extraKms = 0;
								if (!TextUtils.isEmpty(bookings.getString("TRAVELKM").trim()) && Float.parseFloat(removeLeadingZeros(bookings.getString("TRAVELKM").trim())) >JSONKEYS.BASEKM)
								{
									float distanceInKms = Float.parseFloat(removeLeadingZeros(bookings.getString("TRAVELKM").trim()));
									extraKms = distanceInKms - JSONKEYS.BASEKM;
								}
								extraKMS.setText(String.valueOf(Utilities.roundoff(extraKms)));
								extraKMSFare.setText("Rs."+String.valueOf(Utilities.roundoff(extraKms*JSONKEYS.FAREPERKM_DAY)));
								float combineFare =JSONKEYS.BASEFARE_DAY +(extraKms*JSONKEYS.FAREPERKM_DAY);
								combinedFare.setText("Rs."+String.valueOf(Utilities.roundoff(combineFare)));
//								double serviceTaxAmount =  ((combineFare*JSONKEYS.SERVICETAX)/100 );
//								String convertedTaxAmount =String.format("%.2f",serviceTaxAmount);
//								serviceTax.setText("Rs."+String.valueOf(convertedTaxAmount));
							}
							/*if(bookings.getJSONObject("DRIVER") !=null)
							{

											JSONObject driver=bookings.getJSONObject("DRIVER");
											

											String name=driver.getString("NAME");
											String cabType=driver.getString("CABMANUFACTURER");
											String dlno=driver.getString("DLNO");
							}
*/
						}
						else if (response.getString("RESPONSECODE").equals("101"))
						{

						}
					} 
					catch (JSONException e)
					{
						e.printStackTrace();
					}
				}

					}, new Response.ErrorListener() 
					{
						@Override
						public void onErrorResponse(VolleyError error)
						{
							Utilities.hideProgressDialog();
							Utilities.dialogdisplay("Network Error", "Please Try Again", getActivity());
							VolleyLog.d("Parsing ", "Error: " + error.getMessage());

						}
					});
			AppController.getInstance().addToRequestQueue(jsonObjReq, tag_bookings_details);
		}

	}

	private static String removeLeadingZeros(String str) {
		try {
            if(Double.parseDouble(str)==0)
                return new String("0");
            else
            {
                int i=0;
                for(i=0; i<str.length(); i++)
                {
                    if(str.charAt(i)!='0')
                        break;
                }
                return str.substring(i, str.length());
            }
		} catch (Exception ex) {
			// whatever
		}
		return str;
	}

	public void sendEmail()
	{
		Date now = new Date();
		android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
		try
		{
			// image naming and path  to include sd card  appending name you choose for file
			String mPath = BaseConstants.sdCardPath + "/" + "invoice_"+bookingID + ".jpg";

			// create bitmap screen capture
            tripBill.setDrawingCacheEnabled(true);
			Bitmap bitmap = Bitmap.createBitmap(tripBill.getDrawingCache());
            tripBill.setDrawingCacheEnabled(false);

			File imageFile = new File(mPath);

			FileOutputStream outputStream = new FileOutputStream(imageFile);
			String myFile = imageFile.getAbsolutePath();
			System.out.println("Image path: "+ myFile);
			int quality = 100;
			bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
			outputStream.flush();
			outputStream.close();

			sendScreenshot(myFile);
		} catch (Throwable e) {
			// Several error may come out with file handling or OOM
			e.printStackTrace();
		}
	}
    private void sendScreenshot(String image)
    {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/jpeg");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Invoice");
        intent.putExtra(Intent.EXTRA_TEXT, "VowCabs");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+image));
        startActivity(Intent.createChooser(intent,"Sending email"));
    }

}
