package com.vowcabs.passengerapp.Models;

import org.json.JSONException;
import org.json.JSONObject;

public class Source 
{

	String id;
	String lat;
	String lng;
	String geoPlaceId;
	String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getGeoPlaceId() {
		return geoPlaceId;
	}

	public void setGeoPlaceId(String geoPlaceId) {
		this.geoPlaceId = geoPlaceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Source(JSONObject sourceObj) throws JSONException 
	{
		if (!sourceObj.isNull("ID"))
		{
			setId(sourceObj.getString("ID"));
		}
		if (!sourceObj.isNull("LAT"))
		{
			setLat(sourceObj.getString("LAT"));
		}
		if (!sourceObj.isNull("LNG"))
		{
			setLng(sourceObj.getString("LNG"));
		}
		if (!sourceObj.isNull("GEOPLACEID"))
		{
			setGeoPlaceId(sourceObj.getString("GEOPLACEID"));
		}
		if (!sourceObj.isNull("NAME"))
		{
			setName(sourceObj.getString("NAME"));
		}
	}

}
