package com.vowcabs.passengerapp.utils;

import android.os.Environment;

import java.io.File;

/**
 * Created by Sumanth on 26/04/16.
 */
public class BaseConstants {
    public static final String PROJECT_NUMBER = "551777436016";  // gcm project number

    public static final String colomboAirport = "Bandaranaike International Airport";
    public static String colomboAirportLat = "7.1817";
    public static String colomboAirportLng = "79.8841";

    public static final String mangaloreAirport = "Bandaranaike International Airport";
    public static String mangaloreAirportLat = "12.950573076280232";
    public static String mangaloreAirportLng = "74.87242598086596";
    public static final File sdCardPath = new File(Environment.getExternalStorageDirectory() + "/VOWCABS");
}
