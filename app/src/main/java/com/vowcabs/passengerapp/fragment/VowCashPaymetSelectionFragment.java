package com.vowcabs.passengerapp.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.utils.Utilities;

/**
 * Created by Sumanth on 11/08/16.
 */
@SuppressLint("ValidFragment")
public class VowCashPaymetSelectionFragment extends HomeBaseFragment implements View.OnClickListener {
    ImageView backButton;
    Button payButton;
    Button amt1000, amt1500, amt2000;
    EditText amtInput;
    RadioButton creditCard, mobitelCash;
    int selectedPayOption = 0;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.payment, container, false);
        payButton = (Button) rootView.findViewById(R.id.btn_pay);
        backButton = (ImageView) rootView.findViewById(R.id.iv_back_arrow);
        amtInput = (EditText) rootView.findViewById(R.id.et_inputAmt);
        amt1000 = (Button) rootView.findViewById(R.id.btn_amount1000);
        amt1500 = (Button) rootView.findViewById(R.id.btn_amount1500);
        amt2000 = (Button) rootView.findViewById(R.id.btn_amount2000);
        creditCard = (RadioButton) rootView.findViewById(R.id.rb_creditCard);
        mobitelCash = (RadioButton) rootView.findViewById(R.id.rb_mobitelCash);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).updateHeaderText("Vow Cash", mActivity);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).updateHeaderText("Vow Cash", mActivity);
        amtInput.setText(amt1000.getText().toString());
        amt1000.setOnClickListener(this);
        amt1500.setOnClickListener(this);
        amt2000.setOnClickListener(this);
        backButton.setOnClickListener(this);
        payButton.setOnClickListener(this);

        creditCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedPayOption = 0;
                    creditCard.setChecked(true);
                    mobitelCash.setChecked(false);
                    setDefalultCashOptions();
                }
            }
        });
        mobitelCash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedPayOption = 1;
                    mobitelCash.setChecked(true);
                    creditCard.setChecked(false);
                    setDefalultCashOptions();
                }
            }
        });
    }

    public void setDefalultCashOptions() {
        if (selectedPayOption == 0) {
            amt1000.setText("Rs.1000");
            amt1500.setText("Rs.1500");
            amt2000.setText("Rs.2000");
        } else if (selectedPayOption == 1) {
            amt1000.setText("Rs.100");
            amt1500.setText("Rs.200");
            amt2000.setText("Rs.500");
        }
        amtInput.setText(amt1000.getText().toString());
    }

    @Override
    public void onClick(View v) {
        if (getActivity() == null)
            return;

        switch (v.getId()) {
            case R.id.btn_amount1000:
                amtInput.setText(amt1000.getText().toString());
                break;
            case R.id.btn_amount1500:
                amtInput.setText(amt1500.getText().toString());
                break;
            case R.id.btn_amount2000:
                amtInput.setText(amt2000.getText().toString());
                break;
            case R.id.btn_pay:

                try {
                    if (selectedPayOption == 0) {


                        if (!TextUtils.isEmpty(amtInput.getText().toString()) && Integer.valueOf(amtInput.getText().toString().replace("Rs.", "")) >= 1000) {
                            int amount = Integer.valueOf(amtInput.getText().toString().replace("Rs.", ""));
                            ((HomeActivity) getActivity()).pushFragment(
                                    new WebUrlViewFragment(APIConstant.SAMPATHBANK_PAYMENT_URL + amount + "&ID=" + Utilities.getSharedString("CUSTOMERID", getActivity())), false, false);
                        } else {
                            Utilities.dialogdisplay("Invalid Input", " Minimum amount 1000 is required to add VowCash.", getActivity());
                        }

                    }
// else if (selectedPayOption == 1) {
//                        if (!TextUtils.isEmpty(amtInput.getText().toString()) && Integer.valueOf(amtInput.getText().toString().replace("Rs.", "")) >= 100) {
//                            Utilities.makeGetTokenApi(amtInput.getText().toString().replace("Rs.", ""),getActivity());
//                        } else {
//                            Utilities.dialogdisplay("Invalid Input", " Minimum amount 100 is required to add VowCash.", getActivity());
//                        }
//
//
//                    }
                } catch (NumberFormatException e) {
                    Utilities.dialogdisplay("Invalid Input", " Please enter only numeric value.", getActivity());
                }
                break;
            case R.id.iv_back_arrow:

                ((HomeActivity) getActivity()).pushFragment(new VowCashFragment(), false, false);
                break;
        }
    }


}
