package com.vowcabs.passengerapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.InviteScreen;
import com.vowcabs.passengerapp.MyApplication;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.utils.Utilities;

public class InviteFragment extends HomeBaseFragment
{

	Button inviteFriends;
	TextView promoCode, avilabelBalance;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) 
	{
		View rootView=inflater.inflate(R.layout.invite_friend, container,false);
		return rootView;
	}
	@Override
	public void onResume() 
	{
		super.onResume();
		((HomeActivity) getActivity()).updateHeaderText("Invite & Earn",mActivity);
		MyApplication.getInstance().trackScreenView("Offers Screen");
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		inviteFriends     = (Button) getView().findViewById(R.id.button_inviteFriend);
		promoCode         = (TextView) getView().findViewById(R.id.tv_promoCode);
		//avilabelBalance   = (TextView) getView().findViewById(R.id.tv_balance);
		if (!TextUtils.isEmpty(Utilities.getSharedString("INVITECODE", getActivity())))
			promoCode.setText(Utilities.getSharedString("INVITECODE", getActivity()));

//		if (!TextUtils.isEmpty(Utilities.getSharedString("BONUSAMT", getActivity())))
//			avilabelBalance.setText("Balance : "+Utilities.getSharedString("BONUSAMT", getActivity()));

		/* inviteFriends.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v)
			{
				if (!TextUtils.isEmpty(Utilities.getSharedString("INVITECODE", getActivity())))
				{
					String shareBody = "I have an Vow Coupon worth ₹100 for you. Sign up with my code "+" "+Utilities.getSharedString("INVITECODE", getActivity())  + " to avail the coupon and ride for free.\n Have a great ride! Download: http://tinyurl.com/q5mnajx";
					Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
					sharingIntent.setType("text/plain");
					sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "₹100 off your first Vow ride!");
					sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
					startActivity(Intent.createChooser(sharingIntent, "Share your code"));
				}
			}
		}); */
		
		inviteFriends.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(),InviteScreen.class);
				startActivity(i);
				
			}
		});
	}
}
