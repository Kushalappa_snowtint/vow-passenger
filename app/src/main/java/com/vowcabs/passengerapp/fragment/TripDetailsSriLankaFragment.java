package com.vowcabs.passengerapp.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.AppController;
import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.json.JSONKEYS;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.BaseConstants;
import com.vowcabs.passengerapp.utils.DateTime;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

@SuppressLint("ValidFragment")
public class TripDetailsSriLankaFragment extends HomeBaseFragment {
    String bookingID;
    private TextView greetingMsg, packageDetails, cashPayment, tvOnlinePayment, onlinePayment, totalFare, fare, tripId, tripDate, sourceLoc, destinationLoc, totalKm, totalWaitTime, tvDesc;
    static View mRootView = null;
    LinearLayout tripBill;
    RelativeLayout rl_totalKm, rl_waitTime, rl_onlinePayment;

    private String tag_bookings_details_srilanka = "tag_bookingdetails_srilanka";

    public TripDetailsSriLankaFragment(String bookingid) {
        bookingID = bookingid;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView != null) {
            ViewGroup parent = (ViewGroup) mRootView.getParent();
            if (parent != null)
                parent.removeView(mRootView);
        }
        try {
            mRootView = inflater.inflate(R.layout.trip_details_screen_srilanka, container, false);
        } catch (InflateException e) {
        }
        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).updateHeaderText("Trip Details ", mActivity);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        greetingMsg = (TextView) getView().findViewById(R.id.greeting_msg);
        tripId = (TextView) getView().findViewById(R.id.trip_number);
        packageDetails = (TextView) getView().findViewById(R.id.package_details);
        cashPayment = (TextView) getView().findViewById(R.id.cashPayment);
        onlinePayment = (TextView) getView().findViewById(R.id.onlinePayment);
        tvOnlinePayment = (TextView) getView().findViewById(R.id.tv_onlinePayment);
        totalFare = (TextView) getView().findViewById(R.id.totalFare);
        tripDate = (TextView) getView().findViewById(R.id.tv_date);
        sourceLoc = (TextView) getView().findViewById(R.id.tv_from);
        destinationLoc = (TextView) getView().findViewById(R.id.tv_to);
        fare = (TextView) getView().findViewById(R.id.tv_price_numericals);
        totalKm = (TextView) getView().findViewById(R.id.totalKm);
        totalWaitTime = (TextView) getView().findViewById(R.id.waitTime);
        tripBill = (LinearLayout) getView().findViewById(R.id.ll_trip_details);
        rl_totalKm = (RelativeLayout) getView().findViewById(R.id.rl_totalKm);
        rl_waitTime = (RelativeLayout) getView().findViewById(R.id.rl_waitTime);
        rl_onlinePayment = (RelativeLayout) getView().findViewById(R.id.rl_onlinePayment);
        tvDesc = (TextView) getView().findViewById(R.id.tv_taxiOperator);
        tvDesc.setMovementMethod(new ScrollingMovementMethod());
        makeBookingDetailsApi();
    }

    private void makeBookingDetailsApi() {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            Utilities.showProgressDialog(getActivity());
            String encodedString = JsonHeaders.createBookingDeatailsApiJSONHeader(getActivity(), bookingID);
            //Log.d("Tripdetails String",(APIConstant.BASE_URL+encodedString).toString());
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //Log.d("Success", response.toString());
                    try {
                        Utilities.hideProgressDialog();
                        if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("000")) {
                            if (!response.isNull("BOOKING")) {
                                JSONObject bookings = response.getJSONObject("BOOKING");
                                //String bookingid = bookings.getString("BOOKINGID");
                                if (!bookings.isNull("REFERENCEID") && !TextUtils.isEmpty(bookings.getString("REFERENCEID"))) {
                                    tripId.setText(bookings.getString("REFERENCEID"));
                                }
                                if (!bookings.isNull("BOOKINGDATE") && !TextUtils.isEmpty(bookings.getString("BOOKINGDATE"))) {
                                    tripDate.setText(DateTime.changeDateFormat(bookings.getString("BOOKINGDATE")));
                                }

                                if (!bookings.isNull("CUSTOMER")) {
                                    JSONObject customer = (JSONObject) bookings.get("CUSTOMER");
                                    //String customerID = customer.getString("ID");

                                    if (!customer.isNull("NAME") && !TextUtils.isEmpty(customer.getString("NAME"))) {
                                        greetingMsg.setText("Hi, " + customer.getString("NAME") + " thanks for riding with us");
                                    }
                                }

                                if (!bookings.isNull("DESTINATION")) {
                                    JSONObject destination = bookings.getJSONObject("DESTINATION");
                                    if (!destination.isNull("NAME")) {
                                        destinationLoc.setText(destination.getString("NAME"));
                                    }
                                }
                                if (!bookings.isNull("SOURCE")) {
                                    JSONObject source = bookings.getJSONObject("SOURCE");
                                    if (!source.isNull("NAME"))
                                        sourceLoc.setText(source.getString("NAME"));
                                }
                                if (!bookings.isNull("TOTALFARE") && !TextUtils.isEmpty(bookings.getString("TOTALFARE").trim())) {
                                    try {
                                        fare.setText("Rs." + removeLeadingZeros(bookings.getString("TOTALFARE").trim()));
                                        totalFare.setText("Rs." + removeLeadingZeros(bookings.getString("TOTALFARE").trim()));
                                    } catch (Exception e) {
                                    }
                                }

                                if (!bookings.isNull("TRIPPAYMENT")) {

                                    JSONObject tripPayment = bookings.getJSONObject("TRIPPAYMENT");

                                    if (bookings.isNull("PACKAGEID"))
                                        return;

                                    if (bookings.getString("PACKAGEID").equals("0") || bookings.getString("PACKAGEID").trim().equals("")) {
                                        packageDetails.setText("Local");
                                        if (!bookings.isNull("TRAVELKM"))
                                            totalKm.setText(bookings.getString("TRAVELKM"));

                                        if (!bookings.isNull("WAITINGTIME"))
                                            totalWaitTime.setText(bookings.getString("WAITINGTIME"));


                                        if (!tripPayment.isNull("PAYMENTTYPE") && tripPayment.getString("PAYMENTTYPE").trim().equalsIgnoreCase("c")) {
                                            if (!tripPayment.isNull("CASHAMOUNT"))
                                                cashPayment.setText("Rs." + tripPayment.getString("CASHAMOUNT"));

                                            rl_onlinePayment.setVisibility(View.GONE);

                                        } else if (!tripPayment.isNull("PAYMENTTYPE") && tripPayment.getString("PAYMENTTYPE").trim().equalsIgnoreCase("vc")) {

                                            if (!tripPayment.isNull("CASHAMOUNT"))
                                                cashPayment.setText("Rs." + tripPayment.getString("CASHAMOUNT"));

                                            if (!tripPayment.isNull("ONLINEPAYMENTAMT"))
                                                onlinePayment.setText(tripPayment.getString("ONLINEPAYMENTAMT"));

                                            tvOnlinePayment.setText("Paid through vow Cash");

                                        } else if (!tripPayment.isNull("PAYMENTTYPE") && tripPayment.getString("PAYMENTTYPE").trim().equalsIgnoreCase("mc")) {

                                            if (!tripPayment.isNull("CASHAMOUNT"))
                                                cashPayment.setText("Rs." + tripPayment.getString("CASHAMOUNT"));

                                            tvOnlinePayment.setText("Paid through mCash");

                                            try {

                                                double paidMCash = Double.parseDouble(tripPayment.getString("ONLINEPAYMENTAMT"));
                                                double refundedMCash = Double.parseDouble(tripPayment.getString("REFUNDAMOUNT"));
                                                double mcashPaid = paidMCash - refundedMCash;
                                                onlinePayment.setText("Rs." + Utilities.roundOff(mcashPaid));

                                            } catch (NumberFormatException e) {
                                            } catch (Exception e) {
                                            }
                                        }
                                    } else if (bookings.getString("PACKAGEID").equals("1") || bookings.getString("PACKAGEID").equals("2") || bookings.getString("PACKAGEID").equals("3") || bookings.getString("PACKAGEID").equals("4")) {

                                        if (!bookings.isNull("PACKAGEHOURS") && !TextUtils.isEmpty(bookings.getString("PACKAGEHOURS").trim()))
                                            packageDetails.setText(bookings.getString("PACKAGEHOURS") + " hrs");
                                        rl_totalKm.setVisibility(View.GONE);
                                        rl_waitTime.setVisibility(View.GONE);


                                        if (!tripPayment.isNull("PAYMENTTYPE") && !TextUtils.isEmpty(tripPayment.getString("PAYMENTTYPE").trim())) {
                                            if (!tripPayment.isNull("PAYMENTTYPE") && tripPayment.getString("PAYMENTTYPE").trim().equalsIgnoreCase("c")) {
                                                if (!tripPayment.isNull("CASHAMOUNT"))
                                                    cashPayment.setText("Rs." + tripPayment.getString("CASHAMOUNT"));

                                                rl_onlinePayment.setVisibility(View.GONE);

                                            } else if (!tripPayment.isNull("PAYMENTTYPE") && tripPayment.getString("PAYMENTTYPE").trim().equalsIgnoreCase("vc")) {
                                                if (!tripPayment.isNull("CASHAMOUNT"))
                                                    cashPayment.setText("Rs." + tripPayment.getString("CASHAMOUNT"));

                                                if (!tripPayment.isNull("ONLINEPAYMENTAMT"))
                                                    onlinePayment.setText("Rs." + tripPayment.getString("ONLINEPAYMENTAMT"));

                                                tvOnlinePayment.setText("Paid through vow Cash");

                                            } else if (!tripPayment.isNull("PAYMENTTYPE") && tripPayment.getString("PAYMENTTYPE").trim().equalsIgnoreCase("mc")) {
                                                if (!tripPayment.isNull("CASHAMOUNT"))
                                                    cashPayment.setText("Rs." + tripPayment.getString("CASHAMOUNT"));
                                                tvOnlinePayment.setText("Paid through mCash");
                                                try {

                                                    double paidMCash = Double.parseDouble(tripPayment.getString("ONLINEPAYMENTAMT"));
                                                    double refundedMCash = Double.parseDouble(tripPayment.getString("REFUNDAMOUNT"));
                                                    double mcashPaid = paidMCash - refundedMCash;
                                                    onlinePayment.setText("Rs." + Utilities.roundOff(mcashPaid));

                                                } catch (NumberFormatException e) {
                                                } catch (Exception e) {
                                                }

                                            }
                                        }
                                    } else if (bookings.getString("PACKAGEID").equals("7") || bookings.getString("PACKAGEID").equals("8") || bookings.getString("PACKAGEID").equals("9")) {
                                        packageDetails.setText("OutStation");
                                        rl_totalKm.setVisibility(View.GONE);
                                        rl_totalKm.setVisibility(View.GONE);
                                        if (!tripPayment.isNull("PAYMENTTYPE") && tripPayment.getString("PAYMENTTYPE").trim().equalsIgnoreCase("c")) {
                                            if (!tripPayment.isNull("CASHAMOUNT"))
                                                cashPayment.setText("Rs." + tripPayment.getString("CASHAMOUNT"));
                                            rl_onlinePayment.setVisibility(View.GONE);

                                        } else if (!tripPayment.isNull("PAYMENTTYPE") && tripPayment.getString("PAYMENTTYPE").trim().equalsIgnoreCase("vc")) {
                                            if (!tripPayment.isNull("CASHAMOUNT"))
                                                cashPayment.setText("Rs." + tripPayment.getString("CASHAMOUNT"));

                                            if (!tripPayment.isNull("ONLINEPAYMENTAMT"))
                                                onlinePayment.setText(tripPayment.getString("ONLINEPAYMENTAMT"));
                                            tvOnlinePayment.setText("Paid through vow Cash");

                                        } else if (!tripPayment.isNull("PAYMENTTYPE") && tripPayment.getString("PAYMENTTYPE").trim().equalsIgnoreCase("mc")) {
                                            if (!tripPayment.isNull("CASHAMOUNT"))
                                                cashPayment.setText("Rs." + tripPayment.getString("CASHAMOUNT"));

                                            tvOnlinePayment.setText("Paid through mCash");
                                            try {

                                                double paidMCash = Double.parseDouble(tripPayment.getString("ONLINEPAYMENTAMT"));
                                                double refundedMCash = Double.parseDouble(tripPayment.getString("REFUNDAMOUNT"));
                                                double mcashPaid = paidMCash - refundedMCash;
                                                onlinePayment.setText("Rs." + Utilities.roundOff(mcashPaid));

                                            } catch (NumberFormatException e) {
                                            } catch (Exception e) {
                                            }


                                        }
                                    }

                                    if (!bookings.isNull("TRAVELKM") && !TextUtils.isEmpty(bookings.getString("TRAVELKM"))) {
                                        try {
                                            float extraKms = 0;
                                            if (!TextUtils.isEmpty(bookings.getString("TRAVELKM").trim()) && Float.parseFloat(removeLeadingZeros(bookings.getString("TRAVELKM").trim())) > JSONKEYS.BASEKM) {
                                                float distanceInKms = Float.parseFloat(removeLeadingZeros(bookings.getString("TRAVELKM").trim()));

                                            }
                                        } catch (Exception e) {
                                        }
//                                else
//                                {
//                                    float distanceInKms = Float.parseFloat(removeLeadingZeros(bookings.getString("DISTANCE").trim()));
//                                    extraKms = distanceInKms - JSONKEYS.BASEKM;
//                                }
//                                totalKm.setText(String.valueOf(Utilities.roundoff(extraKms)));
//                                totalKm.setText(String.valueOf(Utilities.roundoff(extraKms*JSONKEYS.FAREPERKM_DAY)));

//								double serviceTaxAmount =  ((combineFare*JSONKEYS.SERVICETAX)/100 );
//								String convertedTaxAmount =String.format("%.2f",serviceTaxAmount);
//								serviceTax.setText("Rs."+String.valueOf(convertedTaxAmount));
                                    }

                                }
                            }

                        } else if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("101")) {
                            Utilities.dialogdisplay("Network Issue", "Please Try Again", getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utilities.hideProgressDialog();
                    Utilities.dialogdisplay("Network Error", "Please Try Again", getActivity());
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());

                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_bookings_details_srilanka);
        }

    }

    private static String removeLeadingZeros(String str) {
        try {
            if (Double.parseDouble(str) == 0)
                return new String("0");
            else {
                int i = 0;
                for (i = 0; i < str.length(); i++) {
                    if (str.charAt(i) != '0')
                        break;
                }
                return str.substring(i, str.length());
            }
        } catch (Exception ex) {
            // whatever
        }
        return str;
    }

    public void sendEmail() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = BaseConstants.sdCardPath + "/" + "invoice_" + bookingID + ".jpg";

            // create bitmap screen capture
            tripBill.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(tripBill.getDrawingCache());
            tripBill.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            String myFile = imageFile.getAbsolutePath();
            System.out.println("Image path: " + myFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            sendScreenshot(myFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
    }

    private void sendScreenshot(String image) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/jpeg");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Invoice");
        intent.putExtra(Intent.EXTRA_TEXT, "VowCabs");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + image));
        startActivity(Intent.createChooser(intent, "Sending email"));
    }

}
