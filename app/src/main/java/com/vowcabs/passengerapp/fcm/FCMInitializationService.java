package com.vowcabs.passengerapp.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.vowcabs.passengerapp.utils.Utilities;

/**
 * Created by Sumanth on 27/10/16.
 */

public class FCMInitializationService extends FirebaseInstanceIdService {
    private static final String TAG = "FCMService";

    @Override
    public void onTokenRefresh() {
        String fcmToken = FirebaseInstanceId.getInstance().getToken();

        Log.d(TAG, "FCM Token:" + fcmToken);
        Utilities.putSharedString("FCM_TOKEN", fcmToken, this);
        Utilities.sendRegistrationIdToServer(fcmToken, FCMInitializationService.this);
        //Save or send FCM registration token
    }
}
