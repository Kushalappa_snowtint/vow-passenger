package com.vowcabs.passengerapp.utils;

import android.content.Intent;
import android.graphics.drawable.Drawable;

public class CropingOption
{

    public Intent appIntent;
    public Drawable icon;
    public CharSequence title;

    public CropingOption()
    {
    }
}
