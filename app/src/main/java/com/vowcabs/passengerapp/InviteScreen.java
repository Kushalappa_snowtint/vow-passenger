package com.vowcabs.passengerapp;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vowcabs.passengerapp.adapter.InviteAdapter;
import com.vowcabs.passengerapp.utils.CustomListView;
import com.vowcabs.passengerapp.utils.Utilities;

import java.util.List;

public class InviteScreen extends Activity {
    private TextView textView1, textView2, textView3, textView4;
    private ImageView imgView;
    private CustomListView listView1, listView2;

    public static String[] textdesc1 = {"WhatsApp", "Messenger", "SMS",
            "Email"};
    public static int[] imgdesc1 = {R.drawable.whatsapp, R.drawable.fb,
            R.drawable.sms, R.drawable.email};
    public static String[] textdesc2 = {"Twitter", "Facebook"};
    public static int[] imgdesc2 = {R.drawable.twitter, R.drawable.facebook};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);
        textView1 = (TextView) findViewById(R.id.textView1);
        textView2 = (TextView) findViewById(R.id.textView2);
        textView3 = (TextView) findViewById(R.id.textView3);
        textView4 = (TextView) findViewById(R.id.textView4);
        imgView = (ImageView) findViewById(R.id.imgView);
        listView1 = (CustomListView) findViewById(R.id.listView1);
        listView2 = (CustomListView) findViewById(R.id.listView2);
        InviteAdapter adapter1 = new InviteAdapter(InviteScreen.this, textdesc1, imgdesc1);
        InviteAdapter adapter2 = new InviteAdapter(InviteScreen.this, textdesc2, imgdesc2);
        listView1.setAdapter(adapter1);
        listView2.setAdapter(adapter2);

        try {
            setListViewHeightBasedOnChildren(listView1);
            setListViewHeightBasedOnChildren(listView2);
        } catch (Exception e) {
        }


        listView1.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                switch (position) {
                    case 0:
                        Intent whatsappintent = new Intent(Intent.ACTION_SEND);
                        whatsappintent.setType("text/plain");
                        whatsappintent.setPackage("com.whatsapp");
                        whatsappintent.putExtra(Intent.EXTRA_TEXT,
                                "Hi I have an Vow Coupon worth Rs.100 for you.Sign up with my code " + Utilities.getSharedString("INVITECODE", InviteScreen.this) + " to avail the coupon and ride for free.Have a great ride! Download : http://tinyurl.com/q5mnajx ");
                        try {
                            startActivity(whatsappintent);
                        } catch (Exception e) {
                            Toast.makeText(InviteScreen.this, "Please install whatsapp",
                                    Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 1:
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT,
                                "Hi I have an vow Coupon worth Rs.100 for you.Sign up with my code " + Utilities.getSharedString("INVITECODE", InviteScreen.this) + " to avail the coupon and ride for free.Have a great ride! Download : http://tinyurl.com/q5mnajx ");
                        sendIntent.setType("text/plain");
                        sendIntent.setPackage("com.facebook.orca");
                        try {
                            startActivity(sendIntent);
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(InviteScreen.this,
                                    "Please Install Facebook Messenger",
                                    Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 2:
                        Intent smsIntent = new Intent(Intent.ACTION_SEND);
                        smsIntent.putExtra(Intent.EXTRA_TEXT,
                                "Hi I have an vow Coupon worth Rs.100 for you.Sign up with my code " + Utilities.getSharedString("INVITECODE", InviteScreen.this) + " to avail the coupon and ride for free.Have a great ride! Download : http://tinyurl.com/q5mnajx ");
                        smsIntent.setType("vnd.android-dir/mms-sms");
                        try {
                            startActivity(smsIntent);
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(),
                                    "Unable to send sms", Toast.LENGTH_SHORT)
                                    .show();

                        }
                        break;
                    case 3:
                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.setData(Uri.parse("mailto:"));
                        emailIntent.setType("text/plain");
                        emailIntent.putExtra(Intent.EXTRA_TEXT,
                                "Hi I have an vow Coupon worth Rs.100 for you.Sign up with my code " + Utilities.getSharedString("INVITECODE", InviteScreen.this) + " to avail the coupon and ride for free.Have a great ride! Download : http://tinyurl.com/q5mnajx ");
                        try {
                            startActivity(emailIntent);
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(),
                                    "Unable to send email", Toast.LENGTH_SHORT)
                                    .show();
                        }
                        break;

                }
            }
        });
        listView2.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {

                switch (position) {
                    case 0:
                        Intent tweetIntent = new Intent(Intent.ACTION_SEND);
                        tweetIntent.putExtra(Intent.EXTRA_TEXT,
                                "Hi I have an vow Coupon worth Rs.100 for you.Sign up with my code " + Utilities.getSharedString("INVITECODE", InviteScreen.this) + " to avail the coupon and ride for free.Have a great ride! Download : http://tinyurl.com/q5mnajx ");
                        tweetIntent.setType("text/plain");

                        PackageManager packManager = getPackageManager();
                        List<ResolveInfo> resolvedInfoList = packManager
                                .queryIntentActivities(tweetIntent,
                                        PackageManager.MATCH_DEFAULT_ONLY);

                        boolean resolved = false;
                        for (ResolveInfo resolveInfo : resolvedInfoList) {
                            if (resolveInfo.activityInfo.packageName
                                    .startsWith("com.twitter.android")) {
                                tweetIntent.setClassName(
                                        resolveInfo.activityInfo.packageName,
                                        resolveInfo.activityInfo.name);
                                resolved = true;
                                break;
                            }
                        }
                        if (resolved) {
                            startActivity(tweetIntent);
                        } else {
                            Toast.makeText(InviteScreen.this, "Twitter app isn't found",
                                    Toast.LENGTH_LONG).show();
                        }
                        break;
                    case 1:

                        Intent shareIntent = new Intent(
                                android.content.Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                                "Hi I have an vow Coupon worth Rs.100 for you.Sign up with my code " + Utilities.getSharedString("INVITECODE", InviteScreen.this) + " to avail the coupon and ride for free.Have a great ride! Download : http://tinyurl.com/q5mnajx ");

                        PackageManager pm = getPackageManager();
                        List<ResolveInfo> activityList = pm.queryIntentActivities(
                                shareIntent, 0);
                        for (final ResolveInfo app : activityList) {
                            if ((app.activityInfo.name).contains("facebook")) {
                                try {
                                    final ActivityInfo activity = app.activityInfo;
                                    final ComponentName name = new ComponentName(
                                            activity.applicationInfo.packageName,
                                            activity.name);
                                    shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                                            "Hi I have an vow Coupon worth ₹100 for you.Sign up with my code " + Utilities.getSharedString("INVITECODE", InviteScreen.this) + " to avail the coupon and ride for free.Have a great ride! Download : http://tinyurl.com/q5mnajx ");
                                    shareIntent
                                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                                    | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                                    shareIntent.setComponent(name);
                                    startActivity(shareIntent);
                                } catch (Exception e) {
                                    Toast.makeText(InviteScreen.this,
                                            "Make sure Facebook app is installed",
                                            Toast.LENGTH_SHORT).show();
                                }
                                break;
                            }

                        }
                        break;

                }
            }

        });

    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
				View.MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, listView);
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
						ViewGroup.LayoutParams.WRAP_CONTENT));

			view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utilities.dismissProgressDialog();
    }

}
