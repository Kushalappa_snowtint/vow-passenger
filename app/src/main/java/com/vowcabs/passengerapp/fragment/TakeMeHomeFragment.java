package com.vowcabs.passengerapp.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.vowcabs.passengerapp.AppController;
import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.LocationInstance;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Sumanth on 28/07/16.
 */
public class TakeMeHomeFragment extends HomeBaseFragment implements OnMapReadyCallback, GoogleMap.OnCameraChangeListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private TextView homeAddressText, termsUsed;
    private Button saveHomeAddress;
    private GoogleMap googleMap = null;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude = 0.0;
    private double currentLongitude = 0.0;
    private GetLocationAsync asyncTask;
    private Timer apiTimer;
    private TimerTask task;
    private LatLng changedLatLng;
    private Spinner prefSpinner1;
    private Spinner prefSpinner2;
    private String tag_takeme_home = "tag_takeme_home";
//    private String[] spinnerOption1 = new String[]{"Option 1", "Mini", "Sedan", "Prime"};
//    private String[] spinnerOption2 = new String[]{"Option 2", "Mini", "Sedan", "Prime"};

    private String[] spinnerOption1 = new String[]{"Option 1", "Mini", "Sedan", "TUKTUK"};
    private String[] spinnerOption2 = new String[]{"Option 2", "Mini", "Sedan", "TUKTUK"};
    ArrayAdapter<String> arrayadapter1;
    ArrayAdapter<String> arrayadapter2;
    Map<String, Integer> myMap = new HashMap<String, Integer>();
    private String selectedOption1 = spinnerOption1[0];
    private String selectedOption2 = spinnerOption2[0];

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.takeme_home, container, false);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).updateHeaderText("TAKE ME HOME", mActivity);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        homeAddressText = (TextView) getView().findViewById(R.id.tv_home_address);
        termsUsed = (TextView) getView().findViewById(R.id.tv_termsUsed);
        saveHomeAddress = (Button) getView().findViewById(R.id.textView_save);
        prefSpinner1 = (Spinner) getView().findViewById(R.id.spinner_pref1);
        prefSpinner2 = (Spinner) getView().findViewById(R.id.spinner_pref2);
        initializeMap();

        ((HomeActivity) getActivity()).updateHeaderText("TAKE ME HOME", mActivity);

        myMap.put("Mini", 0);
        myMap.put("Sedan", 1);
        //myMap.put("Prime", 2);
        myMap.put("TUKTUK", 3);


        arrayadapter1 = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item_textview, spinnerOption1) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the second item from Spinner
                    return false;
                } else if (selectedOption2.equalsIgnoreCase(spinnerOption1[position])) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the disable item text color
                    tv.setTextColor(Color.GRAY);
                } else if (selectedOption2.equalsIgnoreCase(spinnerOption1[position])) {
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        arrayadapter2 = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item_textview, spinnerOption2) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the second item from Spinner
                    return false;
                } else if (selectedOption1.equalsIgnoreCase(spinnerOption2[position])) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the disable item text color
                    tv.setTextColor(Color.GRAY);
                } else if (selectedOption1.equalsIgnoreCase(spinnerOption2[position])) {
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        saveHomeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() == null)
                    return;

                if (!TextUtils.isEmpty(homeAddressText.getText().toString()) && currentLatitude != 0.0 && currentLongitude != 0.0) {
                    if (selectedOption1.equalsIgnoreCase(spinnerOption1[0]) || selectedOption2.equalsIgnoreCase(spinnerOption2[0])) {
                        Utilities.dialogdisplay("Select Prefered Cab", "Please select your preferrence of cabs.", getActivity());
                    } else {

                        makeUpdateHomeAddressApi(homeAddressText.getText().toString(), String.valueOf(currentLatitude), String.valueOf(currentLongitude),
                                String.valueOf(myMap.get(selectedOption1)), String.valueOf(myMap.get(selectedOption2)));
                    }
                } else {
                    Utilities.dialogdisplay("Error", "Sorry, We are unable to fetch your home location", getActivity());
                }
            }
        });

        termsUsed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LocationInstance.getInstance().getCountryCode().equalsIgnoreCase("LK")) {
                    ((HomeActivity) getActivity()).pushFragment(new WebUrlViewFragment("http://lk.vowcabs.com/terms.html"), false, false);
                } else {
                    ((HomeActivity) getActivity()).pushFragment(new WebUrlViewFragment("http://vowcabs.com/terms.html"), false, false);
                }
            }
        });
        prefSpinner1.setAdapter(arrayadapter1);
        prefSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (selectedOption2.equalsIgnoreCase(spinnerOption1[position])) {
//                    Utilities.dialogdisplay("Already Selected", "Please choose different option.", getActivity());
//                } else {
                selectedOption1 = spinnerOption1[position];
                arrayadapter1.notifyDataSetChanged();
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

        prefSpinner2.setAdapter(arrayadapter2);
        prefSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (selectedOption1.equalsIgnoreCase(spinnerOption2[position])) {
//                    Utilities.dialogdisplay("Already Selected", "Please choose different option.", getActivity());
//
//                } else {
                selectedOption2 = spinnerOption2[position];
                arrayadapter2.notifyDataSetChanged();
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getSaveTMHDetails() {
        if (LocationInstance.getInstance().getTmhInfo() != null && !TextUtils.isEmpty(LocationInstance.getInstance().getTmhInfo().toString())) {
            try {
                double lat = Double.valueOf(LocationInstance.getInstance().getTmhInfo().getString("LAT"));
                double lng = Double.valueOf(LocationInstance.getInstance().getTmhInfo().getString("LNG"));
                moveGoogleMapsCameraPositon(new LatLng(lat, lng));
                int index1 = Integer.valueOf(LocationInstance.getInstance().getTmhInfo().getString("CABPREF1"));
                int index2 = Integer.valueOf(LocationInstance.getInstance().getTmhInfo().getString("CABPREF2"));

                if (index1 <= spinnerOption1.length)
                    if ((index1 + 1) == spinnerOption1.length) {
                        prefSpinner1.setSelection(index1);
                    } else {
                        prefSpinner1.setSelection(index1 + 1);
                    }

                if (index2 <= spinnerOption2.length) {
                    if ((index2 + 1) == spinnerOption2.length) {
                        prefSpinner2.setSelection(index2);
                    } else {
                        prefSpinner2.setSelection(index2 + 1);
                    }
                }


            } catch (Exception e) {
            }
        } else {
            Utilities.getTMHDetails(getActivity());
        }
    }


    private void initializeMap() {
        if (getActivity() == null)
            return;

        int status = com.google.android.gms.common.GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

        // Showing status
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, mActivity, requestCode);
            dialog.show();
        } else {
            getMapFragment().getMapAsync(this);
        }
    }

    private MapFragment getMapFragment() {
        FragmentManager fm = null;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            fm = getFragmentManager();
        } else {
            fm = getChildFragmentManager();
        }

        return (MapFragment) fm.findFragmentById(R.id.mapView);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.googleMap = map;

        // Enabling MyLocation Layer of Google Map
        googleMap.setMyLocationEnabled(true);
        googleMap.setOnCameraChangeListener(this);
        buildGoogleApiClient();

        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();

        getSaveTMHDetails();

    }

    protected synchronized void buildGoogleApiClient() {
        try {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            Utilities.locationChecker(mGoogleApiClient, getActivity());
        } catch (Exception e) {
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        // Log.e("onLocationChanged :", ""+location);
        try {
            LatLng myLaLn = new LatLng(location.getLatitude(), location.getLongitude());
            if (LocationInstance.getInstance().getTmhInfo() == null) {
                moveGoogleMapsCameraPositon(myLaLn);
            }
            //  updateCurrentLocation(myLaLn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void moveGoogleMapsCameraPositon(LatLng myLaLn) {
        CameraPosition camPos = new CameraPosition.Builder().target(myLaLn).zoom(15).bearing(0).tilt(0).build();
        CameraUpdate camUpd3 = CameraUpdateFactory.newCameraPosition(camPos);
        googleMap.moveCamera(camUpd3);
        googleMap.animateCamera(camUpd3);

    }

    @Override
    public void onConnected(Bundle arg0) {
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            onLocationChanged(mLastLocation);
            Log.e("onConnected :", "" + mLastLocation);
        }
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(400); //60 seconds
        mLocationRequest.setFastestInterval(400); //50 seconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setSmallestDisplacement(1000F);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, TakeMeHomeFragment.this);
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        MapFragment f = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.mapView);
        if (f != null)
            getFragmentManager().beginTransaction().remove(f).commit();
    }

    @Override
    public void onCameraChange(CameraPosition arg0) {
        changedLatLng = arg0.target;
        if (apiTimer != null) {
            apiTimer.cancel();
            apiTimer = null;
        }
        apiTimer = new Timer();
        task = new TimerTask() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateCurrentLocation(changedLatLng);
                        }
                    });
                }
            }
        };
        apiTimer.schedule(task, 1500);
    }

    class GetLocationAsync extends AsyncTask<String, Void, String[]> {
        String[] currentAddress = new String[3];

        public GetLocationAsync(double latitud, double longitud) {
            // TODO Auto-generated constructor stub
            currentLatitude = latitud;
            currentLongitude = longitud;
        }

        @Override
        protected void onPreExecute() {
            Fragment currentFragment = ((HomeActivity) mActivity).getCurrentFragment();
            homeAddressText.setText("Getting Home Address...");
        }

        @Override
        protected String[] doInBackground(String... params) {
            if (mActivity == null)
                return currentAddress;
            // transforming lat/long values in to street name.
            Geocoder gCoder = new Geocoder(mActivity);
            String strAdd = "";
            ArrayList<Address> addresses;
            try {
                addresses = (ArrayList<Address>) gCoder.getFromLocation(currentLatitude, currentLongitude, 1);
                if (addresses != null && addresses.size() > 0) {
                    Address returnedAddress = addresses.get(0);

                    StringBuilder strReturnedAddress = new StringBuilder("");

                    for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                    }
                    strAdd = strReturnedAddress.toString();

                    currentAddress[2] = strAdd;
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getAddress();
                    }
                });
            }
            return currentAddress;

        }

        @Override
        protected void onPostExecute(String[] returnedAddress) {
            //Log.e("onPostExecute :", ""+ returnedAddress);

            if (mActivity == null)
                return;
            try {
                homeAddressText.setText(returnedAddress[2]);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getAddress() {
        try {
            if (asyncTask != null)
                asyncTask.cancel(true);

            if (Utilities.isNetworkAvailable(getActivity())) {
                String finalUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + currentLatitude + "," + currentLongitude +
                        //URLEncoder.encode(selectedAddress,"UTF-8")
                        //"&components=country:lk"+
                        "&key=" + getResources().getString(R.string.google_places_server_key);
                JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                        finalUrl, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d("Success", response.toString());
                        try {
                            JSONArray results = response.getJSONArray("results");
                            JSONObject resultsObject = results.getJSONObject(0);
                            String formattedAddress = resultsObject.getString("formatted_address");

                            try {
                                homeAddressText.setText(formattedAddress);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Utilities.hideProgressDialog();
                            //Utilities.dialogdisplay("Network Error", "Please try again",InstantBookingActivity.this);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utilities.hideProgressDialog();
                        Utilities.dialogdisplay("Location not found", "Sorry we are unable to trace your location.", getActivity());
                        VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjReq, "geo_placeid_api");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateCurrentLocation(LatLng latLng) {
        try {
            if (Utilities.isNetworkAvailable(mActivity)) {
                asyncTask = new GetLocationAsync(latLng.latitude, latLng.longitude);
                asyncTask.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void makeUpdateHomeAddressApi(String address, String lat, String lng, String pref1, String pref2) {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            Utilities.showProgressDialog(getActivity());
            String encodedString = JsonHeaders.createUpdateHomeLocationApiJSONHeader(address, lat, lng, pref1, pref2, getActivity());
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Success", response.toString());
                    try {
                        Utilities.hideProgressDialog();
                        if (!response.isNull("RESPONSECODE") && response.getString("RESPONSECODE").equals("000")) {
                            Utilities.getTMHDetails(getActivity());
                            Utilities.dialogdisplay("Sucess", "Your TMH information has been Updated Successfully.", getActivity());
                        } else {
                            Utilities.dialogdisplay("Error", "Some thing went wrong, Please try again.", getActivity());
                        }

                    } catch (JSONException e) {
                    }

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utilities.hideProgressDialog();
                    Utilities.dialogdisplay("Network Error", "Please Try Again", getActivity());
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());

                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_takeme_home);
        }

    }
}
