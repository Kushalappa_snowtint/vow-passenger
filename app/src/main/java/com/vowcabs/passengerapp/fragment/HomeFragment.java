package com.vowcabs.passengerapp.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.test.mock.MockPackageManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vowcabs.passengerapp.AppController;
import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.Models.Driver;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.json.JSONKEYS;
import com.vowcabs.passengerapp.json.JsonHeaders;
import com.vowcabs.passengerapp.utils.DistanceCalculator;
import com.vowcabs.passengerapp.utils.LocationInstance;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends HomeBaseFragment implements OnClickListener, OnMapReadyCallback, OnCameraChangeListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private GoogleMap googleMap = null;
    TextView bookNow;
    TextView bookLater;
    TextView outStation;
    ImageView carMiniIcon;
    ImageView carSedanIcon;
    ImageView carPrimeIcon;
    ImageView autoIcon;
    ImageView carMiniSelected;
    ImageView carSedanSelected;
    ImageView carPrimeSelected;
    ImageView autoSelected;
    TextView carMiniETA;
    TextView carSedanETA;
    TextView carPrimeETA;
    TextView autoETA;
    TextView carMiniTitle;
    TextView carSedanTitle;
    TextView carPrimeTitle;
    TextView autoTitle;
    ImageView takeMeHome;
    View mCurrentSelectedCar = null;
    RelativeLayout mCurrentSelectedLayout = null;
    RelativeLayout rlMini;
    RelativeLayout rlSedan;
    RelativeLayout rlPrime;
    RelativeLayout rlAuto;
    Marker availableDriverMarkers;
    LinearLayout markerLayout;
    static View mRootView = null;
    boolean isDataLoaded = false;
    double latitude;
    double longitude;
    boolean allowBooking = true;
    boolean hasLatLngValues = false;
    ArrayList<TextView> selectedCarType = new ArrayList<TextView>();
    private String tag_req_nearbydrivers = "jobj_req_nearbydriverdrivers";
    Activity mActivity = null;
    Timer apiTimer;
    TimerTask task;
    Dialog customDialog;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    private GetLocationAsync asyncTask;
    private static final int REQUEST_CODE_PERMISSION = 24444444;


    //CameraPositon
    LatLng markerLatLng;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.homefragment, container, false);
        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            ((HomeActivity) mActivity).topBar.setVisibility(View.VISIBLE);
            ((HomeActivity) mActivity).searchIcon.setVisibility(View.VISIBLE);
            ((HomeActivity) mActivity).favouritesIcon.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        try {

            if (asyncTask != null)
                asyncTask.cancel(true);
        } catch (Exception e) {
        }
        super.onPause();
    }

    @SuppressLint("ValidFragment")
    public HomeFragment(boolean haveLatLngValues, double lat, double lng) {
        hasLatLngValues = haveLatLngValues;
        latitude = lat;
        longitude = lng;
    }

    public HomeFragment() {
        hasLatLngValues = false;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = getActivity();
        try {
            bookNow = (TextView) mRootView.findViewById(R.id.booknow);
            bookLater = (TextView) mRootView.findViewById(R.id.booklater);
            outStation = (TextView) mRootView.findViewById(R.id.outStation);
            carMiniETA = (TextView) mRootView.findViewById(R.id.tv_mini_time);
            carSedanETA = (TextView) mRootView.findViewById(R.id.tv_sedan_time);
            carPrimeETA = (TextView) mRootView.findViewById(R.id.tv_prime_time);
            autoETA = (TextView) mRootView.findViewById(R.id.tv_auto_time);
            carMiniTitle = (TextView) mRootView.findViewById(R.id.tv_mini_title);
            carSedanTitle = (TextView) mRootView.findViewById(R.id.tv_sedan_title);
            carPrimeTitle = (TextView) mRootView.findViewById(R.id.tv_prime_title);
            autoTitle = (TextView) mRootView.findViewById(R.id.tv_auto_title);
            takeMeHome = (ImageView) mRootView.findViewById(R.id.iv_takeMeHome);
            carMiniIcon = (ImageView) mRootView.findViewById(R.id.car_mini);
            carSedanIcon = (ImageView) mRootView.findViewById(R.id.car_sedan);
            carPrimeIcon = (ImageView) mRootView.findViewById(R.id.car_prime);
            autoIcon = (ImageView) mRootView.findViewById(R.id.car_auto);
            carMiniSelected = (ImageView) mRootView.findViewById(R.id.iv_mini_bg);
            carSedanSelected = (ImageView) mRootView.findViewById(R.id.iv_sedan_bg);
            carPrimeSelected = (ImageView) mRootView.findViewById(R.id.iv_prime_bg);
            autoSelected = (ImageView) mRootView.findViewById(R.id.iv_auto_bg);
            markerLayout = (LinearLayout) mRootView.findViewById(R.id.locationMarker);
            rlMini = (RelativeLayout) mRootView.findViewById(R.id.rl_mini);
            rlSedan = (RelativeLayout) mRootView.findViewById(R.id.rl_sedan);
            rlPrime = (RelativeLayout) mRootView.findViewById(R.id.rl_prime);
            rlAuto = (RelativeLayout) mRootView.findViewById(R.id.rl_auto);

            checkRunTimePermissions();
            initializeMap();
            Utilities.getTMHDetails(getActivity());

            bookNow.setOnClickListener(this);
            bookLater.setOnClickListener(this);
            outStation.setOnClickListener(this);
            markerLayout.setOnClickListener(this);
            rlMini.setOnClickListener(this);
            rlSedan.setOnClickListener(this);
            rlPrime.setOnClickListener(this);
            rlAuto.setOnClickListener(this);
            takeMeHome.setOnClickListener(this);
            deselectAllCars();
            setBackgroundView(carMiniSelected, R.drawable.car_selection);
            mCurrentSelectedCar = carMiniSelected;
            mCurrentSelectedLayout = rlMini;
            LocationInstance.getInstance().setSelectedCar(0);
            selectedImageView(carMiniIcon, R.drawable.mini_pressed);
            selectedTextView(carMiniETA, getResources().getColor(R.color.custom_orange));
            selectedTextView(carMiniTitle, getResources().getColor(R.color.custom_orange));
            selectedCarType.add(carMiniETA);
            selectedCarType.add(carSedanETA);
            selectedCarType.add(carPrimeETA);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void checkRunTimePermissions()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
            String[] mPermission = {
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CALL_PHONE,
            };

            try {
                if (ActivityCompat.checkSelfPermission(getActivity(), mPermission[0])
                        != MockPackageManager.PERMISSION_GRANTED ||
                        ActivityCompat.checkSelfPermission(getActivity(), mPermission[1])
                                != MockPackageManager.PERMISSION_GRANTED ) {

                    ActivityCompat.requestPermissions(getActivity(),
                            mPermission, REQUEST_CODE_PERMISSION);

// If any permission are not allowed by user, this condition will execute every time, else your else part will work
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            if (grantResults.length == 3 &&
                    grantResults[0] == MockPackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == MockPackageManager.PERMISSION_GRANTED &&
                    grantResults[2] == MockPackageManager.PERMISSION_GRANTED ) {
            }
        }
    }

    /**
     * function to load map. If map is not created it will create it for you
     */
    private void initializeMap() {
        if (getActivity() == null)
            return;

        int status = com.google.android.gms.common.GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

        // Showing status
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, mActivity, requestCode);
            dialog.show();
        } else {
            getMapFragment().getMapAsync(this);
        }
    }

    private MapFragment getMapFragment() {
        FragmentManager fm = null;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            fm = getFragmentManager();
        } else {
            fm = getChildFragmentManager();
        }

        return (MapFragment) fm.findFragmentById(R.id.map);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.googleMap = map;

        // Enabling MyLocation Layer of Google Map
        googleMap.setMyLocationEnabled(true);
        googleMap.setOnCameraChangeListener(this);
        buildGoogleApiClient();

        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    protected synchronized void buildGoogleApiClient() {
        try {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            Utilities.locationChecker(mGoogleApiClient, getActivity());
        } catch (Exception e) {
        }
    }

    private void getNearByDriversApiCall() {
        try {
            if (getActivity() == null)
                return;

            if (Utilities.isNetworkAvailable(getActivity())) {
                String encodedString = JsonHeaders.createGetNearByDriverApiJSONHeader(getActivity());

                JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                        APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d(response.toString(), "Sucess: " + response.toString());
                        calculateEstimatedTime(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (getActivity() == null)
                            return;

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (!isDataLoaded) {
                                    Utilities.dialogdisplay("Network Error", "Something went wrong. We are trying to fix the issue. Please bear with us!", getActivity());
                                }

                                //VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                                Utilities.hideProgressDialog();
                            }
                        });

                    }
                });
                jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                        0,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(jsonObjReq, tag_req_nearbydrivers);
            }
        } catch (Exception e) {
        }
    }

    ArrayList<Driver> allAvialableDrivers = new ArrayList<Driver>();
    ArrayList<Driver> miniAvailableDrivers = new ArrayList<Driver>();
    ArrayList<Driver> sedanAvailableDrivers = new ArrayList<Driver>();
    ArrayList<Driver> primeAvailableDrivers = new ArrayList<Driver>();
    ArrayList<Driver> autoAvailableDrivers = new ArrayList<Driver>();

    private Driver nearestMiniDriver = null;
    private Driver nearestSedanDriver = null;
    private Driver nearestPrimeDriver = null;
    private Driver nearestAutoDriver = null;

    private void calculateEstimatedTime(JSONObject response) {
        try {
            allAvialableDrivers.clear();
            miniAvailableDrivers.clear();
            sedanAvailableDrivers.clear();
            primeAvailableDrivers.clear();
            autoAvailableDrivers.clear();

            nearestAutoDriver = null;
            nearestMiniDriver = null;
            nearestSedanDriver = null;
            nearestPrimeDriver = null;


            if (!response.isNull("DRIVERS") && response.getJSONArray("DRIVERS").length() > 0) {
                if (googleMap != null)
                    googleMap.clear();

                for (int index = 0; index < response.getJSONArray("DRIVERS").length(); index++) {
                    Driver driverObject = new Driver(response.getJSONArray("DRIVERS").getJSONObject(index));
                    long distanceFromPickupLoc = DistanceCalculator.getInstance().calculateDistance(Double.valueOf(driverObject.getDriverLAT()),
                            Double.valueOf(driverObject.getDriverLONG()), markerLatLng.latitude,
                            markerLatLng.longitude);
                    driverObject.setDistanceFromPickupLoc(distanceFromPickupLoc);
                    allAvialableDrivers.add(driverObject);

                    if (response.getJSONArray("DRIVERS").getJSONObject(index).getString("CABTYPE").equals("0")) {
                        Driver miniDriver = new Driver(response.getJSONArray("DRIVERS").getJSONObject(index));
                        miniAvailableDrivers.add(miniDriver);

                        if (nearestMiniDriver == null || nearestMiniDriver.getDistanceFromPickupLoc() > driverObject.getDistanceFromPickupLoc())
                            nearestMiniDriver = driverObject;
                    } else if (response.getJSONArray("DRIVERS").getJSONObject(index).getString("CABTYPE").equals("1")) {
                        Driver sedanDriver = new Driver(response.getJSONArray("DRIVERS").getJSONObject(index));
                        sedanAvailableDrivers.add(sedanDriver);

                        if (nearestSedanDriver == null || nearestSedanDriver.getDistanceFromPickupLoc() > driverObject.getDistanceFromPickupLoc())
                            nearestSedanDriver = driverObject;

                    } else if (response.getJSONArray("DRIVERS").getJSONObject(index).getString("CABTYPE").equals("2")) {
                        Driver primeDriver = new Driver(response.getJSONArray("DRIVERS").getJSONObject(index));
                        primeAvailableDrivers.add(primeDriver);

                        if (nearestPrimeDriver == null || nearestPrimeDriver.getDistanceFromPickupLoc() > driverObject.getDistanceFromPickupLoc())
                            nearestPrimeDriver = driverObject;
                    } else if (response.getJSONArray("DRIVERS").getJSONObject(index).getString("CABTYPE").equals("3")) {
                        Driver autoDriver = new Driver(response.getJSONArray("DRIVERS").getJSONObject(index));
                        autoAvailableDrivers.add(autoDriver);

                        if (nearestAutoDriver == null || nearestAutoDriver.getDistanceFromPickupLoc() > driverObject.getDistanceFromPickupLoc())
                            nearestAutoDriver = driverObject;
                    }
                }


                if (LocationInstance.getInstance().selectedCar == 0) {
                    addMarkersToMap(miniAvailableDrivers, R.drawable.mini_marker);
                } else if (LocationInstance.getInstance().selectedCar == 1) {
                    addMarkersToMap(sedanAvailableDrivers, R.drawable.sedan_marker);
                } else if (LocationInstance.getInstance().selectedCar == 2) {
                    addMarkersToMap(primeAvailableDrivers, R.drawable.prime_marker);
                } else if (LocationInstance.getInstance().selectedCar == 3) {
                    addMarkersToMap(autoAvailableDrivers, R.drawable.auto_marker);
                }


                if (miniAvailableDrivers.size() > 0 && nearestMiniDriver != null) {
                    getNearByDriver(nearestMiniDriver.getDriverLAT(), nearestMiniDriver.getDriverLONG(), "mini");
                } else {
                    Utilities.setETA(carMiniETA, "", false);
                }
                if (sedanAvailableDrivers.size() > 0 && nearestSedanDriver != null) {
                    getNearByDriver(nearestSedanDriver.getDriverLAT(), nearestSedanDriver.getDriverLONG(), "sedan");
                } else {
                    Utilities.setETA(carSedanETA, "", false);
                }
                if (primeAvailableDrivers.size() > 0 && nearestPrimeDriver != null) {
                    getNearByDriver(nearestPrimeDriver.getDriverLAT(), nearestPrimeDriver.getDriverLONG(), "prime");
                } else {
                    Utilities.setETA(carPrimeETA, "", false);
                }
                if (autoAvailableDrivers.size() > 0 && nearestAutoDriver != null) {
                    getNearByDriver(nearestAutoDriver.getDriverLAT(), nearestAutoDriver.getDriverLONG(), "auto");
                } else {
                    Utilities.setETA(autoETA, "", true);
                }

            } else {
                clearCabDrivers();
            }
            bookNow.setClickable(true);
            if (!isDataLoaded) {
                Utilities.hideProgressDialog();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        isDataLoaded = true;
    }

    private void getNearByDriver(double driverLAT, double driverLONG, String cabType) {
        if (getActivity() == null)
            return;

        LatLng sourceLatLng = new LatLng(driverLAT, driverLONG);
        LatLng destinationLatLng = new LatLng(markerLatLng.latitude, markerLatLng.longitude);
        String url = Utilities.getDirectionsUrl(sourceLatLng, destinationLatLng, getActivity());
        if (cabType.equalsIgnoreCase("mini")) {
            makeGoogleMapsDirectionsApiForMini(url);
        }
        if (cabType.equalsIgnoreCase("sedan")) {
            makeGoogleMapsDirectionsApiForSedan(url);
        }
        if (cabType.equalsIgnoreCase("prime")) {
            makeGoogleMapsDirectionsApiForPrime(url);
        }
        if (cabType.equalsIgnoreCase("auto")) {
            makeGoogleMapsDirectionsApiForAuto(url);
        }

    }

    private void makeGoogleMapsDirectionsApiForMini(String url) {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    String miniETA = Utilities.calculateEstimatedTravelTime(response);
                    Log.d("ETA Mini", "Sucess: " + miniETA);
                    Utilities.setETA(carMiniETA, miniETA, false);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                    Utilities.hideProgressDialog();
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, "googlemaps_directions_api_mini");
        }

    }

    private void makeGoogleMapsDirectionsApiForSedan(String url) {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    String sedanETA = Utilities.calculateEstimatedTravelTime(response);
                    Log.d("ETA Sedan", "Sucess: " + sedanETA);
                    Utilities.setETA(carSedanETA, sedanETA, false);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utilities.hideProgressDialog();
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, "googlemaps_directions_api_sedan");
        }

    }

    private void makeGoogleMapsDirectionsApiForPrime(String url) {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    String primeETA = Utilities.calculateEstimatedTravelTime(response);
                    Log.d("ETA Prime", "Sucess: " + primeETA);
                    Utilities.setETA(carPrimeETA, primeETA, false);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utilities.hideProgressDialog();
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, "googlemaps_directions_api_prime");
        }

    }

    private void makeGoogleMapsDirectionsApiForAuto(String url) {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                    url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    String autoEta = Utilities.calculateEstimatedTravelTime(response);
                    Log.d("ETA Auto", "Sucess: " + autoETA);
                    Utilities.setETA(autoETA, autoEta, true);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utilities.hideProgressDialog();
                    VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, "googlemaps_directions_api_auto");
        }
    }

    @Override
    public void onClick(View v) {
        if (((HomeActivity) mActivity) != null && ((HomeActivity) mActivity).isSliderOpen())
            return;


        allowBooking = true;
        String cabType = "";
        if (v == bookNow) {

            if (TextUtils.isEmpty(LocationInstance.getInstance().getCurrentLocation())) {
                Utilities.dialogdisplay(cabType + "Please Wait", "We are trying to find your pickup location.", getActivity());
                return;
            }
            if (bookNow.isClickable() == false) {
                Utilities.dialogdisplay(cabType + "Please Wait", "We are trying to find nearest driver to you pickup location.", getActivity());
            }

            if (mCurrentSelectedCar == carMiniSelected && miniAvailableDrivers.isEmpty()) {
                cabType = "Mini Cab";
                allowBooking = false;
            } else if (mCurrentSelectedCar == carSedanSelected && sedanAvailableDrivers.isEmpty()) {
                cabType = "Sedan Cab";
                allowBooking = false;
            } else if (mCurrentSelectedCar == carPrimeSelected && primeAvailableDrivers.isEmpty()) {
                cabType = "Prime Cab";
                allowBooking = false;
            } else if (mCurrentSelectedCar == autoSelected && autoAvailableDrivers.isEmpty()) {
                cabType = "Auto";
                allowBooking = false;
            }

            if (allowBooking) {
                ((HomeActivity) mActivity).pushFragment(new BookTripFragment(true, false, latitude, longitude), false, true);
            } else {
                Utilities.dialogdisplay(cabType + " Not Available", "At present your choice of cab is not available. We are working to increase our fleet. You can choose another cab or try again later. Sorry for the inconvenience.", getActivity());
            }

        } else if (v == bookLater) {

            if (LocationInstance.getInstance().selectedCar == 3) {
                return;
            }

            if (TextUtils.isEmpty(LocationInstance.getInstance().getCurrentLocation())) {
                Utilities.dialogdisplay(cabType + "Please Wait", "We are trying to find your pickup location.", getActivity());
                return;
            }

            ((HomeActivity) mActivity).pushFragment(new BookTripFragment(false, false, latitude, longitude), false, true);
        } else if (v == outStation) {

            if (LocationInstance.getInstance().selectedCar == 3) {
                return;
            }

            if (TextUtils.isEmpty(LocationInstance.getInstance().getCurrentLocation())) {
                Utilities.dialogdisplay(cabType + "Please Wait", "We are trying to find your pickup location.", getActivity());
                return;
            }

            if (mCurrentSelectedCar == autoSelected) {
                Utilities.clickableAndChangeColor(outStation, R.color.lite_orange, getActivity(), false);
            } else {
                ((HomeActivity) mActivity).pushFragment(new BookTripFragment(false, true, latitude, longitude), false, true);
            }
        } else if (v == rlMini) {
            if (mCurrentSelectedLayout == v) {
                showFareChart(R.layout.activity_mini_pressed);
                return;
            }
            addMarkersToMap(miniAvailableDrivers, R.drawable.mini_marker);
            startAnimation(carMiniSelected, carMiniIcon, R.drawable.mini_pressed, rlMini);
            deselectAllCars();
            LocationInstance.getInstance().setSelectedCar(JSONKEYS.CAR_MINI);
            selectedTextView(carMiniETA, getResources().getColor(R.color.custom_orange));
            selectedTextView(carMiniTitle, getResources().getColor(R.color.custom_orange));
            setBackgroundView(carMiniSelected, R.drawable.car_selection);
            mCurrentSelectedCar = carMiniSelected;
            mCurrentSelectedLayout = rlMini;
            adjustGoogleMapZoomLevel();
            disableAutoBooking();
        } else if (v == rlSedan) {
            if (mCurrentSelectedLayout == v) {
                showFareChart(R.layout.activity_sedan_pressed);
                return;
            }
            addMarkersToMap(sedanAvailableDrivers, R.drawable.sedan_marker);
            deselectAllCars();
            startAnimation(carSedanSelected, carSedanIcon, R.drawable.sedan_pressed, rlSedan);
            LocationInstance.getInstance().setSelectedCar(JSONKEYS.CAR_SEDAN);
            selectedTextView(carSedanETA, getResources().getColor(R.color.custom_orange));
            selectedTextView(carSedanTitle, getResources().getColor(R.color.custom_orange));
            setBackgroundView(carSedanSelected, R.drawable.car_selection);
            mCurrentSelectedCar = carSedanSelected;
            mCurrentSelectedLayout = rlSedan;
            adjustGoogleMapZoomLevel();
            disableAutoBooking();
        } else if (v == rlPrime) {
            if (mCurrentSelectedLayout == v) {
                showFareChart(R.layout.activity_prime_pressed);
                return;
            }
            addMarkersToMap(primeAvailableDrivers, R.drawable.prime_marker);
            deselectAllCars();
            startAnimation(carPrimeSelected, carPrimeIcon, R.drawable.prime_pressed, rlPrime);
            LocationInstance.getInstance().setSelectedCar(JSONKEYS.CAR_PRIME);
            selectedTextView(carPrimeETA, getResources().getColor(R.color.custom_orange));
            selectedTextView(carPrimeTitle, getResources().getColor(R.color.custom_orange));
            setBackgroundView(carPrimeSelected, R.drawable.car_selection);
            mCurrentSelectedCar = carPrimeSelected;
            mCurrentSelectedLayout = rlPrime;
            adjustGoogleMapZoomLevel();
            disableAutoBooking();
        } else if (v == rlAuto) {
            addMarkersToMap(autoAvailableDrivers, R.drawable.auto_marker);
            deselectAllCars();
            startAnimation(autoSelected, autoIcon, R.drawable.autoicon_pressed, rlAuto);
            LocationInstance.getInstance().setSelectedCar(JSONKEYS.CAR_AUTO);
            selectedTextView(autoETA, getResources().getColor(R.color.custom_orange));
            selectedTextView(autoTitle, getResources().getColor(R.color.custom_orange));
            setBackgroundView(autoSelected, R.drawable.car_selection);
            mCurrentSelectedCar = autoSelected;
            mCurrentSelectedLayout = rlAuto;
            enableAutoBooking();
        } else if (v == takeMeHome) {
            if (LocationInstance.getInstance().getTmhInfo() != null && !TextUtils.isEmpty(LocationInstance.getInstance().getTmhInfo().toString())) {
                try {
                    // Log.e("TMH DETAILS", LocationInstance.getInstance().getTmhInfo().toString());
                    if (!LocationInstance.getInstance().getTmhInfo().isNull("HOMEADDRESS") && !TextUtils.isEmpty(LocationInstance.getInstance().getTmhInfo().getString("HOMEADDRESS"))
                            && !LocationInstance.getInstance().getTmhInfo().isNull("LAT") && !TextUtils.isEmpty(LocationInstance.getInstance().getTmhInfo().getString("LAT"))
                            && !LocationInstance.getInstance().getTmhInfo().isNull("LNG") && !TextUtils.isEmpty(LocationInstance.getInstance().getTmhInfo().getString("LNG"))
                            && !LocationInstance.getInstance().getTmhInfo().isNull("CABPREF1") && !TextUtils.isEmpty(LocationInstance.getInstance().getTmhInfo().getString("CABPREF1"))
                            && !LocationInstance.getInstance().getTmhInfo().isNull("CABPREF2") && !TextUtils.isEmpty(LocationInstance.getInstance().getTmhInfo().getString("CABPREF2"))) {

                        String tmhPref1 = LocationInstance.getInstance().getTmhInfo().getString("CABPREF1").trim();
                        String tmhPref2 = LocationInstance.getInstance().getTmhInfo().getString("CABPREF2").trim();

                        if ((tmhPref1.equalsIgnoreCase("0") || tmhPref2.equalsIgnoreCase("0")) && !miniAvailableDrivers.isEmpty()) {
                            LocationInstance.getInstance().setSelectedCar(JSONKEYS.CAR_MINI);
                            createBooking();
                            return;
                        }
                        if ((tmhPref1.equalsIgnoreCase("1") || tmhPref2.equalsIgnoreCase("1")) && !sedanAvailableDrivers.isEmpty()) {
                            LocationInstance.getInstance().setSelectedCar(JSONKEYS.CAR_SEDAN);
                            createBooking();
                            return;
                        }
                        if ((tmhPref1.equalsIgnoreCase("3") || tmhPref2.equalsIgnoreCase("3")) && !autoAvailableDrivers.isEmpty()) {
                            LocationInstance.getInstance().setSelectedCar(JSONKEYS.CAR_AUTO);
                            createBooking();
                            return;
                        } else {
                            Utilities.dialogdisplay("Prefered Cab Not Available", "At present your choice of cab is not available. We are working to increase our fleet. " +
                                    "You can choose another cab or try again later. Sorry for the inconvenience.", getActivity());
                        }
                    }

                } catch (Exception e) {
                }
            } else {
                Utilities.dialogdisplay("TMH info not available.", "At present your home address details are not available. Please update your details.", getActivity());
            }
        }
    }

    public void createBooking() {
        try {
            BookTripFragment bookingFragemnt = new BookTripFragment("TAKEMEHOME", LocationInstance.getInstance().getCurrentLocation(), LocationInstance.getInstance().getTmhInfo().getString("HOMEADDRESS"),
                    String.valueOf(LocationInstance.getInstance().getLatitude()),
                    String.valueOf(LocationInstance.getInstance().getLongitute()), LocationInstance.getInstance().getTmhInfo().getString("LAT"),
                    LocationInstance.getInstance().getTmhInfo().getString("LNG"));
            ((HomeActivity) mActivity).pushFragment(bookingFragemnt, false, true);
        } catch (Exception e) {
        }
    }

    private void startAnimation(ImageView target, final ImageView selectedCar, final int selectedCarImage, RelativeLayout targetLayout) {
        Animation anim = null;

        anim = new TranslateAnimation(mCurrentSelectedCar.getX(), target.getX(), mCurrentSelectedCar.getY(), target.getY());

        if (anim != null) {
            anim.setDuration(1200);
            anim.setAnimationListener(new AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    selectedImageView(selectedCar, selectedCarImage);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }
            });
            mCurrentSelectedCar.startAnimation(anim);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        removeMapView();
    }


    private void moveGoogleMapsCameraPositon(LatLng myLaLn) {
        CameraPosition camPos = new CameraPosition.Builder().target(myLaLn).zoom(15).bearing(0).tilt(0).build();
        CameraUpdate camUpd3 = CameraUpdateFactory.newCameraPosition(camPos);
        googleMap.moveCamera(camUpd3);
        googleMap.animateCamera(camUpd3);
        updateCurrentLocation(myLaLn);

    }

    private void adjustGoogleMapZoomLevel() {
        String currentSelectedCarETA = selectedCarType.get(LocationInstance.getInstance().selectedCar).getText().toString();
        if (!currentSelectedCarETA.equals("No Cabs") && currentSelectedCarETA.length() > 0) {
            String arr[] = currentSelectedCarETA.trim().split("[a-zA-Z ]+");
            int sum = 0;
            for (int i = 0; i < arr.length; i++)
                sum += Integer.parseInt(arr[i]);
            if (sum > 15) {
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(13), 100, null);
            } else if (sum > 10) {
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(14), 100, null);
            }
        }
    }

    public void updateCurrentLocation(LatLng latLng) {
        if (markerLayout != null)
            markerLayout.setVisibility(View.VISIBLE);

        try {
            if (Utilities.isNetworkAvailable(mActivity)) {
                //Log.e("updateCurrentLocation: ", "GetLocationAsync :"+latLng);

                asyncTask = new GetLocationAsync(latLng.latitude, latLng.longitude);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) // Above Api Level 13
                {
                    asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    asyncTask.execute();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCameraChange(CameraPosition arg0) {
        if (mActivity == null)
            return;

        markerLatLng = arg0.target;
        if (apiTimer != null) {
            apiTimer.cancel();
            apiTimer = null;
        }
        apiTimer = new Timer();
        task = new TimerTask() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            bookNow.setClickable(false);
                            LatLng changedLatLng = markerLatLng;

                            if (!Utilities.isPointInRegion(markerLatLng.latitude, markerLatLng.longitude)) {
                                ((HomeActivity) mActivity).setNoServiceTextVisibility(View.VISIBLE);
                                disableBooking();
                            } else {
                                ((HomeActivity) mActivity).setNoServiceTextVisibility(View.GONE);
                                getNearByDriverTimer();
                                enableBooking();
                                clearCabDrivers();
                            }
                            LocationInstance.getInstance().setLatitude(markerLatLng.latitude);
                            LocationInstance.getInstance().setLongitute(markerLatLng.longitude);
                            LocationInstance.getInstance().sHasLatLngChanged = true;
                            updateCurrentLocation(changedLatLng);
                        }

                    });
                }
            }
        };
        apiTimer.schedule(task, 1500);
    }

    void getNearByDriverTimer() {

        Timer getDriverTimer = new Timer();
        TimerTask getDriverTask = new TimerTask() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    if (Utilities.isPointInRegion(markerLatLng.latitude, markerLatLng.longitude)) {
                        getNearByDriversApiCall();
                    }
                }
            }
        };
        getDriverTimer.scheduleAtFixedRate(getDriverTask, 0, 30000);
    }

    class GetLocationAsync extends AsyncTask<String, Void, String[]> {
        String[] currentAddress = new String[3];

        public GetLocationAsync(double latitud, double longitud) {
            // TODO Auto-generated constructor stub
            latitude = latitud;
            longitude = longitud;
        }

        @Override
        protected void onPreExecute() {
            Fragment currentFragment = ((HomeActivity) mActivity).getCurrentFragment();
            if (currentFragment != null && currentFragment instanceof HomeFragment) {
                ((HomeActivity) mActivity).updateHeaderText("Getting Pickup Location...", mActivity);
                LocationInstance.getInstance().setCurrentLocation("");
            }
        }

        @Override
        protected String[] doInBackground(String... params) {
            if (mActivity == null)
                return currentAddress;
            // transforming lat/long values in to street name.
            Geocoder gCoder = new Geocoder(mActivity);
            String strAdd = "";
            ArrayList<Address> addresses;
            try {
                addresses = (ArrayList<Address>) gCoder.getFromLocation(latitude, longitude, 1);
                if (addresses != null && addresses.size() > 0) {
                    Address returnedAddress = addresses.get(0);
                    if (!TextUtils.isEmpty(returnedAddress.getCountryCode()) && !LocationInstance.getInstance().getCountryCode().equalsIgnoreCase(returnedAddress.getCountryCode())) {
                        getNewSettingsApi(JsonHeaders.getNewSettingsApiJSONHeader(returnedAddress.getCountryCode(), mActivity));
                        currentAddress[0] = returnedAddress.getCountryCode();
                        LocationInstance.getInstance().setCountryCode(currentAddress[0]);
                    }
//					if (!TextUtils.isEmpty(returnedAddress.getLocality()))
//					{
//						currentAddress[1] =returnedAddress.getLocality();
//					}
                    StringBuilder strReturnedAddress = new StringBuilder("");

                    for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                    }
                    strAdd = strReturnedAddress.toString();
                    //Toast.makeText(mActivity, strAdd, Toast.LENGTH_LONG).show();

                    currentAddress[2] = strAdd;
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getAddress();
                    }
                });

            }
            return currentAddress;

        }

        @Override
        protected void onPostExecute(String[] returnedAddress) {
            //Log.e("onPostExecute :", ""+ returnedAddress);
            if (mActivity == null)
                return;

            try {
                if (((HomeActivity) mActivity).getCurrentFragment() == HomeFragment.this || hasLatLngValues) {
                    ((HomeActivity) mActivity).updateHeaderText(returnedAddress[2], mActivity);
                }
                LocationInstance.getInstance().setCurrentLocation(returnedAddress[2]);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getAddress() {
        try {
            if (asyncTask != null)
                asyncTask.cancel(true);

            if (Utilities.isNetworkAvailable(getActivity())) {
                String finalUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + LocationInstance.getInstance().getLatitude() + "," + LocationInstance.getInstance().getLongitute() +
                        //URLEncoder.encode(selectedAddress,"UTF-8")
                        //"&components=country:lk"+
                        "&key=" + getResources().getString(R.string.google_places_server_key);
                JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                        finalUrl, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d("Success", response.toString());
                        try {
                            JSONArray results = response.getJSONArray("results");
                            JSONObject resultsObject = results.getJSONObject(0);
                            String formattedAddress = resultsObject.getString("formatted_address");


                            try {
                                if (((HomeActivity) mActivity).getCurrentFragment() == HomeFragment.this) {
                                    ((HomeActivity) mActivity).updateHeaderText(formattedAddress, mActivity);
                                }
                                LocationInstance.getInstance().setCurrentLocation(formattedAddress);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (resultsObject.has("address_components")) {
                                JSONArray addressComponentsArray = resultsObject.getJSONArray("address_components");
                                for (int j = 0; j < addressComponentsArray.length(); j++) {
                                    JSONObject typesObject = addressComponentsArray.getJSONObject(j);

                                    if (typesObject.has("types")) {
                                        JSONArray typesArray = typesObject.getJSONArray("types");
                                        for (int x = 0; x < typesArray.length(); x++) {
                                            String type = typesArray.getString(x);
                                            if (type.equalsIgnoreCase("country")) {

                                                String countryCode = typesObject.getString("short_name");
                                                //Log.e("countryCode :", countryCode);
                                                if (!TextUtils.isEmpty(countryCode) && !LocationInstance.getInstance().getCountryCode().equalsIgnoreCase(countryCode)) {
                                                    getNewSettingsApi(JsonHeaders.getNewSettingsApiJSONHeader(countryCode, mActivity));
                                                    LocationInstance.getInstance().setCountryCode(countryCode);
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Utilities.hideProgressDialog();
                            //Utilities.dialogdisplay("Network Error", "Please try again",InstantBookingActivity.this);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utilities.hideProgressDialog();
                        Utilities.dialogdisplay("Location not found", "Sorry we are unable to trace your location.", getActivity());
                        VolleyLog.d("Parsing ", "Error: " + error.getMessage());
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjReq, "geo_placeid_api");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void selectedImageView(ImageView iv, int imageId) {
        if (iv != null) {
            iv.setImageResource(imageId);
        }
    }

    void deselectAllCars() {
        selectedImageView(carMiniIcon, R.drawable.mini_normal);
        selectedImageView(carSedanIcon, R.drawable.sedan_normal);
        selectedImageView(carPrimeIcon, R.drawable.prime_normal);
        selectedImageView(autoIcon, R.drawable.auto_icon);
        selectedTextView(carMiniETA, Color.WHITE);
        selectedTextView(carSedanETA, Color.WHITE);
        selectedTextView(carPrimeETA, Color.WHITE);
        selectedTextView(autoETA, Color.WHITE);
        selectedTextView(carMiniTitle, Color.WHITE);
        selectedTextView(carSedanTitle, Color.WHITE);
        selectedTextView(carPrimeTitle, Color.WHITE);
        selectedTextView(autoTitle, Color.WHITE);
        setBackgroundView(carMiniSelected, 0);
        setBackgroundView(carSedanSelected, 0);
        setBackgroundView(carPrimeSelected, 0);
        setBackgroundView(autoSelected, 0);
    }

    private void selectedTextView(TextView selectedView, int color) {
        //selectedView.setTextColor(color);
    }

    private void setBackgroundView(ImageView selectedImage, int background) {
        selectedImage.setImageResource(background);
    }

    private void disableBooking() {
        Utilities.clickableAndChangeColor(bookNow, R.color.lite_orange, getActivity(), false);
        Utilities.clickableAndChangeColor(bookLater, R.color.lite_orange, getActivity(), false);
        Utilities.clickableAndChangeColor(outStation, R.color.lite_orange, getActivity(), false);
    }

    private void enableBooking() {
        Utilities.clickableAndChangeColor(bookNow, R.color.custom_orange, getActivity(), true);
        Utilities.clickableAndChangeColor(bookLater, R.color.custom_orange, getActivity(), true);
        Utilities.clickableAndChangeColor(outStation, R.color.custom_orange, getActivity(), true);
    }

    private void disableAutoBooking() {
        if (Utilities.isPointInRegion(latitude, longitude)) {
            Utilities.clickableAndChangeColor(bookNow, R.color.custom_orange, getActivity(), true);
            Utilities.clickableAndChangeColor(bookLater, R.color.custom_orange, getActivity(), true);
            Utilities.clickableAndChangeColor(outStation, R.color.custom_orange, getActivity(), true);
        }
    }

    private void enableAutoBooking() {
        if (Utilities.isPointInRegion(latitude, longitude)) {
            Utilities.clickableAndChangeColor(bookLater, R.color.lite_orange, getActivity(), false);
            Utilities.clickableAndChangeColor(outStation, R.color.lite_orange, getActivity(), false);
        }
    }

    private void addMarkersToMap(ArrayList<Driver> selectedCabDrivers, int marker) {
        int markerIcon = marker;
//
        if (googleMap != null)
            googleMap.clear();

        if (allAvialableDrivers == null || allAvialableDrivers.size() <= 0)
            return;

        if (selectedCabDrivers.equals(autoAvailableDrivers))
            markerIcon = R.drawable.auto_marker;


        for (int i = 0; i < selectedCabDrivers.size(); i++) {
            LatLng ll = new LatLng(selectedCabDrivers.get(i).getDriverLAT(), selectedCabDrivers.get(i).getDriverLONG());
            availableDriverMarkers = googleMap.addMarker(new MarkerOptions().position(ll).title("")
                    .snippet("").icon(BitmapDescriptorFactory.fromResource(markerIcon)));
        }
        //Log.d("Driver Markers Count :", ""+selectedCabDrivers.size());
    }

    private void clearCabDrivers() {
        if (googleMap != null)
            googleMap.clear();
        Utilities.setETA(carMiniETA, "", false);
        Utilities.setETA(carSedanETA, "", false);
        Utilities.setETA(carPrimeETA, "", false);
        Utilities.setETA(autoETA, "", false);
    }

    @Override
    public void onConnected(Bundle arg0) {

        double latToLoad = 0.00;
        double lngToLoad = 0.00;

        LatLng mySavedLaLn = new LatLng(LocationInstance.getInstance().getLatitude(), LocationInstance.getInstance().getLongitute());

        if (hasLatLngValues) {
            latToLoad = latitude;
            lngToLoad = longitude;
        } else if (mySavedLaLn.latitude != 0.00 && mySavedLaLn.longitude != 0.00) {
            latToLoad = mySavedLaLn.latitude;
            lngToLoad = mySavedLaLn.longitude;
        } else {
            Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {

                latToLoad = mLastLocation.getLatitude();
                lngToLoad = mLastLocation.getLongitude();

            }
        }
        moveGoogleMapsCameraPositon(new LatLng(latToLoad, lngToLoad));
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        // TODO Auto-generated method stub

    }

    void showFareChart(int selectedLayout) {
//		customDialog = new Dialog(getActivity());
//		LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(HomeActivity.LAYOUT_INFLATER_SERVICE);
//		View dialogView = inflater.inflate(selectedLayout,null);
//		customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		customDialog.setContentView(dialogView);
//		customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//		customDialog.show();
    }

    private void getNewSettingsApi(String encodedString) {
        if (getActivity() == null)
            return;

        if (Utilities.isNetworkAvailable(getActivity())) {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET, APIConstant.BASE_URL + encodedString, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        if (response.toString() != null && !response.isNull("Cabs")) {
                            LocationInstance.getInstance().setCabDetails(response);
                        }
                        if (response.toString() != null && !response.isNull("AMOUNTTOLERENCE") && !TextUtils.isEmpty(response.getString("AMOUNTTOLERENCE"))) {
                            LocationInstance.getInstance().setToleranceAmount(Float.valueOf(response.getString("AMOUNTTOLERENCE")));
                        }
                    } catch (JSONException e) {
                    } catch (NumberFormatException e) {
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, "get_newsettings");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        removeMapView();
    }

    void removeMapView() {
        try {
            ((HomeActivity) mActivity).setNoServiceTextVisibility(View.GONE);
            if (asyncTask != null)
                asyncTask.cancel(true);


            MapFragment f = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
            // Log.e("onDestroyView ", "MapFragment " + f);
            if (f != null)
                getActivity().getFragmentManager().beginTransaction().remove(f).commit();

            // Log.e("onDestroyView ", "MapFragment Rmoved " + f);


        } catch (Exception e) {
        }

    }

}