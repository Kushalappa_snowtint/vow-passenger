package com.vowcabs.passengerapp.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vowcabs.passengerapp.AppController;
import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.Models.FavouritePlace;
import com.vowcabs.passengerapp.Models.RecentPlace;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.adapter.AutoCompleteAdapter;
import com.vowcabs.passengerapp.adapter.GooglePlacesAutocompleteAdapter;
import com.vowcabs.passengerapp.api.APIConstant;
import com.vowcabs.passengerapp.encryption.URLUTF8Encoder;
import com.vowcabs.passengerapp.json.JSONKEYS;
import com.vowcabs.passengerapp.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

public class GooglePlacesAutoCompleteFragment extends HomeBaseFragment implements OnItemClickListener
{
	private String tag_req_recentplaces = "jobj_req_recentplaces";
	ListView recentPlacesListView;
	Activity mActivity = null;
	GooglePlacesAutocompleteAdapter mGooglePlacesAutocompleteAdapter = null;
	ArrayList<RecentPlace> placesList=new ArrayList<RecentPlace>();
	ListView favouritePlacesListView;
	ArrayList<FavouritePlace> favouritePlacesList=new ArrayList<FavouritePlace>();
	AutoCompleteTextView autoCompView = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.google_places_autocomplete, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) 
	{
		super.onActivityCreated(savedInstanceState);

		mActivity = getActivity();
		autoCompView = (AutoCompleteTextView) getView().findViewById(R.id.autoCompleteTextView);
		favouritePlacesListView = (ListView) getView().findViewById(R.id.lv_favouritePlaces);
		recentPlacesListView = (ListView) getView().findViewById(R.id.lv_recentPlaces);
		getRecentPlaces();
		mGooglePlacesAutocompleteAdapter = new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item_autocomplete, null);
		autoCompView.setAdapter(mGooglePlacesAutocompleteAdapter);
		autoCompView.setText("");
		autoCompView.setOnItemClickListener(this);

	}
	public void onResume() 
	{
		super.onResume();
		((HomeActivity) getActivity()).setTopBarVisibity(View.GONE);
		if (autoCompView != null)
			autoCompView.setText("");
	}

	private void getRecentPlaces() 
	{
		if (getActivity() == null)
			return;

		if (Utilities.isNetworkAvailable(getActivity()))
		{
			JSONObject reqObject = new JSONObject();
			JSONObject deviceObject = new JSONObject();	
			try
			{
				reqObject.put("OPCODE", "GETUSERPLACES");
				if (!TextUtils.isEmpty(Utilities.getSharedString("CUSTOMERID", getActivity())))
				{
					reqObject.put("CUSTOMERID", Utilities.getSharedString("CUSTOMERID", getActivity()));	
				}
				if (!TextUtils.isEmpty(Utilities.getSharedString("SESSIONID", getActivity())))
				{
					reqObject.put("SESSIONID", Utilities.getSharedString("SESSIONID", getActivity()));	
				}
				reqObject.put("CITYNAME", "bangalore");
				deviceObject.put("DEVICETYPE", "ANDROID");
				deviceObject.put(JSONKEYS.DEVICEID, Secure.getString(getActivity().getContentResolver(),Secure.ANDROID_ID));
				reqObject.put("DEVICEINFO", deviceObject);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			String encodedString  = URLUTF8Encoder.encode(Utilities.getEncryptedString(reqObject.toString()));
			//Log.d("RecentPlaces",APIConstant.BASE_URL.toString()+encodedString);
			JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
					APIConstant.BASE_URL+encodedString, null, new Response.Listener<JSONObject>()
										
					{
				@Override
				public void onResponse(JSONObject response)
				{

					try
					{
						placesList=new ArrayList<RecentPlace>();
						if (!response.isNull("RECENTPLACE") && response.getJSONArray("RECENTPLACE").length() > 0)
						{
							try {
								JSONArray recentPlacesArray = response.getJSONArray("RECENTPLACE");
								for (int index = 0; index < recentPlacesArray.length(); index++) {
									JSONObject recentPlace = recentPlacesArray.getJSONObject(index);
									RecentPlace recentplaceObj = new RecentPlace(recentPlace);
									placesList.add(recentplaceObj);
								}
							}catch (Exception e){}
							//Log.d("Recent Places Count", ""+placesList.size());
						
						}

						favouritePlacesList = new ArrayList<FavouritePlace>();
						if(!response.isNull("FAVPLACE") && response.getJSONArray("FAVPLACE").length() > 0)
						{
							JSONArray favPlacesArray= response.getJSONArray("FAVPLACE");
							for (int index =0 ; index< favPlacesArray.length(); index ++)
							{
								JSONObject favPlace = favPlacesArray.getJSONObject(index);
								FavouritePlace favplaceObj = new FavouritePlace(favPlace);
								favouritePlacesList.add(favplaceObj);
							}
						}
						AutoCompleteAdapter adapter=new AutoCompleteAdapter(getActivity(),placesList, recentPlacesListView,null,null,"");
						recentPlacesListView.setAdapter(adapter);
						AutoCompleteAdapter favAdapter=new AutoCompleteAdapter(getActivity(),favouritePlacesList, favouritePlacesListView,true,null,null,"");
						favouritePlacesListView.setAdapter(favAdapter);
						Utilities.hideProgressDialog();
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
				} 

					}, new Response.ErrorListener() 
					{
						@Override
						public void onErrorResponse(VolleyError error)
						{
							favouritePlacesList = new ArrayList<FavouritePlace>();
							placesList=new ArrayList<RecentPlace>();
							VolleyLog.d("Parsing ", "Error: " + error.getMessage());
						}
					});
			AppController.getInstance().addToRequestQueue(jsonObjReq, tag_req_recentplaces);
		}
	}

	public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) 
	{
		String str = (String) adapterView.getItemAtPosition(position);
		makeGeocodingApiCall(str);
	}

	private void makeGeocodingApiCall(String selectedAddress) 
	{
		try 
		{
			if (Utilities.isNetworkAvailable(getActivity()))
			{
				Utilities.showProgressDialog(getActivity());
				String finalUrl ="https://maps.googleapis.com/maps/api/geocode/json?address="+
						URLEncoder.encode(selectedAddress,"UTF-8")+
						//"&components=country:lk"+
						"&key="+getResources().getString(R.string.google_places_server_key);
				JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
						finalUrl, null, new Response.Listener<JSONObject>()
						{
					@Override
					public void onResponse(JSONObject response)
					{
						Log.d("Success", response.toString());
						try 
						{
							Utilities.hideProgressDialog();
							JSONArray results =response.getJSONArray("results");
							JSONObject resultsObject =results.getJSONObject(0);
							JSONObject geometryObject =resultsObject.getJSONObject("geometry");
							JSONObject locationObj = geometryObject.getJSONObject("location");
							((HomeActivity) getActivity()).pushFragment(new HomeFragment(true, Double.parseDouble(locationObj.getString("lat")),Double.parseDouble(locationObj.getString("lng"))), false, false);
						} 
						catch (Exception e)
						{
							e.printStackTrace();
							Utilities.hideProgressDialog();
							Utilities.dialogdisplay("Network Error", "Please Try Again",getActivity());
						}
					}

						}, new Response.ErrorListener() 
						{
							@Override
							public void onErrorResponse(VolleyError error)
							{
								Utilities.hideProgressDialog();
								Utilities.dialogdisplay("Network Error", "Please Try Again",getActivity());
								VolleyLog.d("Parsing ", "Error: " + error.getMessage());
							}
						});
				AppController.getInstance().addToRequestQueue(jsonObjReq, "geo_placeid_api");
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() 
	{
		super.onPause();
		((HomeActivity) getActivity()).setTopBarVisibity(View.VISIBLE);
	}

}
