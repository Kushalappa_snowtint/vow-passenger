package com.vowcabs.passengerapp.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

public class ErrorReporter implements Thread.UncaughtExceptionHandler{

	private String[]	 _recipients	 = new String[] { "sumanth@innate.asia","ravibora@innate.asia"};
	private String	 _subject	 = "Crash Report of Vow Passenger Android";

	String	 VersionName;
	String	 buildNumber;
	String	 PackageName;
	String	 FilePath;
	String	 PhoneModel;
	String	 AndroidVersion;
	String	 Board;
	String	 Brand;
	String	 Device;
	String	 Display;
	String	 FingerPrint;
	String	 Host;
	String	 ID;
	String	 Manufacturer;
	String	 Model;
	String	 Product;
	String	 Tags;
	long	 Time;
	String	 Type;
	String	 User;
	String	 serviceProvider;
	HashMap<String, String>	 CustomParameters	= new HashMap<String, String>();

	private Thread.UncaughtExceptionHandler	PreviousHandler;
	private static ErrorReporter	 S_mInstance;
	private Context	 CurContext;

	public void AddCustomData(String Key, String Value) {
		CustomParameters.put(Key, Value);
	}

	private String CreateCustomInfoString() {
		String CustomInfo = "";
		Iterator iterator = CustomParameters.keySet().iterator();
		while (iterator.hasNext()) {
			String CurrentKey = (String) iterator.next();
			String CurrentVal = CustomParameters.get(CurrentKey);
			CustomInfo += CurrentKey + " = " + CurrentVal + "\n";
		}
		return CustomInfo;
	}

	static ErrorReporter getInstance() {
		if (S_mInstance == null)
			S_mInstance = new ErrorReporter();
		return S_mInstance;
	}

	public void Init(Context context) {
		PreviousHandler = Thread.getDefaultUncaughtExceptionHandler();
		Thread.setDefaultUncaughtExceptionHandler(this);
		CurContext = context;
	}

	public long getAvailableInternalMemorySize() {
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getAvailableBlocks();
		return availableBlocks * blockSize;
	}

	public long getTotalInternalMemorySize() {
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long totalBlocks = stat.getBlockCount();
		return totalBlocks * blockSize;
	}

	void RecoltInformations(Context context) {
		try {
			PackageManager pm = context.getPackageManager();
			PackageInfo pi;
			// Version
			pi = pm.getPackageInfo(context.getPackageName(), 0);
			VersionName = pi.versionName;
			buildNumber = currentVersionNumber(context);
			// Package name
			PackageName = pi.packageName;

			TelephonyManager manager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);

			// Device model
			PhoneModel = android.os.Build.MODEL;
			// Android version
			AndroidVersion = android.os.Build.VERSION.RELEASE;

			Board = android.os.Build.BOARD;
			Brand = android.os.Build.BRAND;
			Device = android.os.Build.DEVICE;
			Display = android.os.Build.DISPLAY;
			FingerPrint = android.os.Build.FINGERPRINT;
			Host = android.os.Build.HOST;
			ID = android.os.Build.ID;
			Model = android.os.Build.MODEL;
			Product = android.os.Build.PRODUCT;
			Tags = android.os.Build.TAGS;
			Time = android.os.Build.TIME;
			Type = android.os.Build.TYPE;
			User = android.os.Build.USER;
			serviceProvider = manager.getNetworkOperatorName();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String CreateInformationString() {
		RecoltInformations(CurContext);

		String ReturnVal = "";
		ReturnVal += "Version : " + VersionName;
		ReturnVal += "\n";
		ReturnVal += "Build Number : "+ buildNumber;
		ReturnVal += "\n";
		ReturnVal += "Package : " + PackageName;
		ReturnVal += "\n";
		ReturnVal += "FilePath : " + FilePath;
		ReturnVal += "\n";
		ReturnVal += "Phone Model" + PhoneModel;
		ReturnVal += "\n";
		ReturnVal += "Android Version : " + AndroidVersion;
		ReturnVal += "\n";
		ReturnVal += "Board : " + Board;
		ReturnVal += "\n";
		ReturnVal += "Brand : " + Brand;
		ReturnVal += "\n";
		ReturnVal += "Device : " + Device;
		ReturnVal += "\n";
		ReturnVal +="Service Provider : " + serviceProvider;
		ReturnVal += "\n";
		ReturnVal += "Display : " + Display;
		ReturnVal += "\n";
		ReturnVal += "Finger Print : " + FingerPrint;
		ReturnVal += "\n";
		ReturnVal += "Host : " + Host;
		ReturnVal += "\n";
		ReturnVal += "ID : " + ID;
		ReturnVal += "\n";
		ReturnVal += "Model : " + Model;
		ReturnVal += "\n";
		ReturnVal += "Product : " + Product;
		ReturnVal += "\n";
		ReturnVal += "Tags : " + Tags;
		ReturnVal += "\n";
		ReturnVal += "Time : " + Time;
		ReturnVal += "\n";
		ReturnVal += "Type : " + Type;
		ReturnVal += "\n";
		ReturnVal += "User : " + User;
		ReturnVal += "\n";
		ReturnVal += "Total Internal memory : " + getTotalInternalMemorySize();
		ReturnVal += "\n";
		ReturnVal += "Available Internal memory : " + getAvailableInternalMemorySize();
		ReturnVal += "\n";

		return ReturnVal;
	}

	public void uncaughtException(Thread t, Throwable e) {
		String Report = "";
		Date CurDate = new Date();
		Report += "Error Report collected on : " + CurDate.toString();
		Report += "\n";
		Report += "\n";
		Report += "Informations :";
		Report += "\n";
		Report += "==============";
		Report += "\n";
		Report += "\n";
		Report += CreateInformationString();

		Report += "Custom Informations :\n";
		Report += "=====================\n";
		Report += CreateCustomInfoString();

		Report += "\n\n";
		Report += "Stack : \n";
		Report += "======= \n";
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		e.printStackTrace(printWriter);
		String stacktrace = result.toString();
		Report += stacktrace;

		Report += "\n";
		Report += "Cause : \n";
		Report += "======= \n";

		// If the exception was thrown in a background thread inside
		// AsyncTask, then the actual exception can be found with getCause
		Throwable cause = e.getCause();
		while (cause != null) {
			cause.printStackTrace(printWriter);
			Report += result.toString();
			cause = cause.getCause();
		}
		printWriter.close();
		Report += "**** End of current Report ***";
		SaveAsFile(Report);
		// SendErrorMail( Report );
		PreviousHandler.uncaughtException(t, e);
	}

	private void SendErrorMail(Context _context, String ErrorContent) {
		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		String subject = _subject;
		String body = "\n\n" + ErrorContent + "\n\n";
		sendIntent.putExtra(Intent.EXTRA_EMAIL, _recipients);
		sendIntent.putExtra(Intent.EXTRA_TEXT, body);
		sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		sendIntent.setType("message/rfc822");
		sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		_context.startActivity(Intent.createChooser(sendIntent, "Title:"));
	}

	private void SaveAsFile(String ErrorContent) {
		try {
			Random generator = new Random();
			int random = generator.nextInt(99999);
			String FileName = "stack-" + random + ".stacktrace";
			FileOutputStream trace = CurContext.openFileOutput(FileName, Context.MODE_PRIVATE);
			trace.write(ErrorContent.getBytes());
			trace.close();
		} catch (Exception e) {
			// ...
		}
	}

	private String[] GetErrorFileList() {
		File dir = new File(FilePath + "/");
		// Try to create the files folder if it doesn't exist
		dir.mkdir();
		// Filter for ".stacktrace" files
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(".stacktrace");
			}
		};
		return dir.list(filter);
	}

	private boolean bIsThereAnyErrorFile() {
		return GetErrorFileList().length > 0;
	}

	public void CheckErrorAndSendMail(Context _context) {
		try {
			FilePath = _context.getFilesDir().getAbsolutePath();
			if (bIsThereAnyErrorFile()) {
				String WholeErrorText = "";

				String[] ErrorFileList = GetErrorFileList();
				int curIndex = 0;
				final int MaxSendMail = 5;
				for (String curString : ErrorFileList) {
					if (curIndex++ <= MaxSendMail) {
						WholeErrorText += "New Trace collected :\n";
						WholeErrorText += "=====================\n ";
						String filePath = FilePath + "/" + curString;
						BufferedReader input = new BufferedReader(new FileReader(filePath));
						String line;
						while ((line = input.readLine()) != null) {
							WholeErrorText += line + "\n";
						}
						input.close();
					}

					// DELETE FILES !!!!
					File curFile = new File(FilePath + "/" + curString);
					curFile.delete();
				}
				SendErrorMail(_context, WholeErrorText);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String currentVersionNumber(Context a) {
		PackageManager pm = a.getPackageManager();
		try {
			PackageInfo pi = pm.getPackageInfo("de.gamedisk.app", PackageManager.GET_SIGNATURES);
			return pi.versionName + (pi.versionCode > 0 ? " (" + pi.versionCode + ")" : "");
		} catch (NameNotFoundException e) {
			return null;
		}
	}
	//	public class GMailSender extends javax.mail.Authenticator {
	//		 
	//		private String mailhost = "smtp.gmail.com";
	//		 
	//		    private String user;
	//		 
	//		    private String password;
	//		 
	//		    private Session session;
	//		 
	//		     
	//		 
	//		    private Multipart _multipart = new MimeMultipart();
	//		 
	//		    static {
	//		 
	//		        Security.addProvider(new com.example.sendmail.JSSEProvider());
	//		 
	//		    }
	//		 
	//		 
	//		 
	//		    public GMailSender(String user, String password) {
	//		 
	//		        this.user = user;
	//		 
	//		        this.password = password;
	//		 
	//		 
	//		 
	//		        Properties props = new Properties();
	//		 
	//		        props.setProperty("mail.transport.protocol", "smtp");
	//		 
	//		        props.setProperty("mail.host", mailhost);
	//		 
	//		        props.put("mail.smtp.auth", "true");
	//		 
	//		        props.put("mail.smtp.port", "465");
	//		 
	//		        props.put("mail.smtp.socketFactory.port", "465");
	//		 
	//		        props.put("mail.smtp.socketFactory.class",
	//		 
	//		                "javax.net.ssl.SSLSocketFactory");
	//		 
	//		        props.put("mail.smtp.socketFactory.fallback", "false");
	//		 
	//		        props.setProperty("mail.smtp.quitwait", "false");
	//		 
	//		 
	//		 
	//		        session = Session.getDefaultInstance(props, this);
	//		 
	//		    }
	//		 
	//		 
	//		 
	//		    protected PasswordAuthentication getPasswordAuthentication() {
	//		 
	//		        return new PasswordAuthentication(user, password);
	//		 
	//		    }
	//		 
	//		 
	//		 
	//		    public synchronized void sendMail(String subject, String body,
	//		 
	//		            String sender, String recipients) throws Exception {
	//		 
	//		        try {
	//		 
	//		            MimeMessage message = new MimeMessage(session);
	//		 
	//		            DataHandler handler = new DataHandler(new ByteArrayDataSource(
	//		 
	//		                    body.getBytes(), "text/plain"));
	//		 
	//		            message.setSender(new InternetAddress(sender));
	//		 
	//		            message.setSubject(subject);
	//		 
	//		            message.setDataHandler(handler);
	//		 
	//		            BodyPart messageBodyPart = new MimeBodyPart();
	//		 
	//		            messageBodyPart.setText(body);
	//		 
	//		            _multipart.addBodyPart(messageBodyPart);
	//		 
	//		 
	//		 
	//		            // Put parts in message
	//		 
	//		            message.setContent(_multipart);
	//		 
	//		            if (recipients.indexOf(',') > 0)
	//		 
	//		                message.setRecipients(Message.RecipientType.TO,
	//		 
	//		                        InternetAddress.parse(recipients));
	//		 
	//		            else
	//		 
	//		                message.setRecipient(Message.RecipientType.TO,
	//		 
	//		                        new InternetAddress(recipients));
	//		 
	//		            Transport.send(message);
	//		 
	//		        } catch (Exception e) {
	//		 
	//		 
	//		 
	//		        }
	//		 
	//		    }
	//		 
	//		 
	//		 
	//		    public void addAttachment(String filename) throws Exception {
	//		 
	//		        BodyPart messageBodyPart = new MimeBodyPart();
	//		 
	//		        DataSource source = new FileDataSource(filename);
	//		 
	//		        messageBodyPart.setDataHandler(new DataHandler(source));
	//		 
	//		        messageBodyPart.setFileName("download image");
	//		 
	//		 
	//		 
	//		        _multipart.addBodyPart(messageBodyPart);
	//		 
	//		    }
	//		 
	//		 
	//		 
	//		    public class ByteArrayDataSource implements DataSource {
	//		 
	//		        private byte[] data;
	//		 
	//		        private String type;
	//		 
	//		         
	//		 
	//		 
	//		 
	//		        public ByteArrayDataSource(byte[] data, String type) {
	//		 
	//		            super();
	//		 
	//		            this.data = data;
	//		 
	//		            this.type = type;
	//		 
	//		        }
	//		 
	//		 
	//		 
	//		        public ByteArrayDataSource(byte[] data) {
	//		 
	//		            super();
	//		 
	//		            this.data = data;
	//		 
	//		        }
	//		 
	//		 
	//		 
	//		 
	//		 
	//		        public void setType(String type) {
	//		 
	//		            this.type = type;
	//		 
	//		        }
	//		 
	//		 
	//		 
	//		        public String getContentType() {
	//		 
	//		            if (type == null)
	//		 
	//		                return "application/octet-stream";
	//		 
	//		            else
	//		 
	//		                return type;
	//		 
	//		        }
	//		 
	//		 
	//		 
	//		        public InputStream getInputStream() throws IOException {
	//		 
	//		            return new ByteArrayInputStream(data);
	//		 
	//		        }
	//		 
	//		 
	//		 
	//		        public String getName() {
	//		 
	//		            return "ByteArrayDataSource";
	//		 
	//		        }
	//		 
	//		 
	//		 
	//		        public OutputStream getOutputStream() throws IOException {
	//		 
	//		            throw new IOException("Not Supported");
	//		 
	//		        }
	//		 
	//		    }
	//		 
	//		}
	//	public final class JSSEProvider extends Provider {
	//		 
	//		 
	//		 
	//		public JSSEProvider() {
	//		 
	//		super("HarmonyJSSE", 1.0, "Harmony JSSE Provider");
	//		 
	//		        AccessController
	//		 
	//		                .doPrivileged(new java.security.PrivilegedAction<Void>() {
	//		 
	//		                    public Void run() {
	//		 
	//		                        put("SSLContext.TLS",
	//		 
	//		                                "org.apache.harmony.xnet.provider.jsse.SSLContextImpl");
	//		 
	//		                        put("Alg.Alias.SSLContext.TLSv1", "TLS");
	//		 
	//		                        put("KeyManagerFactory.X509",
	//		 
	//		                                "org.apache.harmony.xnet.provider.jsse.KeyManagerFactoryImpl");
	//		 
	//		                        put("TrustManagerFactory.X509",
	//		 
	//		                                "org.apache.harmony.xnet.provider.jsse.TrustManagerFactoryImpl");
	//		 
	//		                        return null;
	//		 
	//		                    }
	//		 
	//		                });
	//		 
	//		    }
	//		 
	//		}
	//	
	//	 new Thread(new Runnable() {
	//		 
	//         public void run() {
	//
	//             try {
	//
	//                 GMailSender sender = new GMailSender(
	//
	//                         "ravi.sharma@oodlestechnologies.com",
	//
	//                         "Can't disclose, enter your password and your email");
	//
	//
	//
	//                 sender.addAttachment(Environment.getExternalStorageDirectory().getPath()+"/image.jpg");
	//
	//                 sender.sendMail("Test mail", "This mail has been sent from android app along with attachment",
	//
	//                         "ravi.sharma@oodlestechnologies.com",
	//
	//                         "ravisharmabpit@gmail.com");
	//
	//                  
	//
	//                  
	//
	//                  
	//
	//                  
	//
	//             } catch (Exception e) {
	//
	//                 Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_LONG).show();
	//
	//                  
	//
	//             }
	//
	//         }
	//
	//     }).start();
	//
	// }
	//
	//});


}
