package com.vowcabs.passengerapp.Models;

import org.json.JSONException;
import org.json.JSONObject;

public class Booking 
{
	String bookingId;
	String referenceId;
	Customer mCustomer;
	Source mSource;
	Destination mDestination;
	String totalFare;
	String userBooking;
	String travelKM;
	String waitingTime;
	String waitingFare;
	String bookingDate;
	String returnDate;
	String description;
	String cabType;
	String fare;
	String bookingStatus;
	String totalTime;
	String driverStatus;
	String distance;

	public Booking()
	{

	}
	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public Customer getmCustomer() {
		return mCustomer;
	}

	public void setmCustomer(Customer mCustomer) {
		this.mCustomer = mCustomer;
	}

	public Source getmSource() {
		return mSource;
	}

	public void setmSource(Source mSource) {
		this.mSource = mSource;
	}

	public Destination getmDestination() {
		return mDestination;
	}

	public void setmDestination(Destination mDestination) {
		this.mDestination = mDestination;
	}

	public String getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}

	public String getUserBooking() {
		return userBooking;
	}

	public void setUserBooking(String userBooking) {
		this.userBooking = userBooking;
	}

	public String getTravelKM() {
		return travelKM;
	}

	public void setTravelKM(String travelKM) {
		this.travelKM = travelKM;
	}

	public String getWaitingTime() {
		return waitingTime;
	}

	public void setWaitingTime(String waitingTime) {
		this.waitingTime = waitingTime;
	}

	public String getWaitingFare() {
		return waitingFare;
	}

	public void setWaitingFare(String waitingFare) {
		this.waitingFare = waitingFare;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFare() {
		return fare;
	}

	public void setFare(String fare) {
		this.fare = fare;
	}

	public String getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public String getTotalTime() {
		return totalTime;
	}
	public String getCabType() {
		return cabType;
	}

	public void setCabType(String cabType) {
		this.cabType = cabType;
	}

	public void setTotalTime(String totalTime) {
		this.totalTime = totalTime;
	}

	public String getDriverStatus() {
		return driverStatus;
	}

	public void setDriverStatus(String driverStatus) {
		this.driverStatus = driverStatus;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public Booking(JSONObject bookingObj) 
	{
		try {
			if (!bookingObj.isNull("BOOKINGID"))
			{
				setBookingId(bookingObj.getString("BOOKINGID"));
			}
			if (!bookingObj.isNull("REFERENCEID"))
			{
				setReferenceId(bookingObj.getString("REFERENCEID"));
			}
			if (!bookingObj.isNull("CUSTOMER"))
			{
				mCustomer = new Customer(bookingObj.getJSONObject("CUSTOMER"));
				setmCustomer(mCustomer);
			}
			if (!bookingObj.isNull("DESTINATION"))
			{
				mDestination = new Destination(bookingObj.getJSONObject("DESTINATION"));
				setmDestination(mDestination);
			}
			if (!bookingObj.isNull("SOURCE"))
			{
				mSource = new Source(bookingObj.getJSONObject("SOURCE"));
				setmSource(mSource);
			}
			if (!bookingObj.isNull("TOTALFARE"))
			{
				setTotalFare(bookingObj.getString("TOTALFARE"));
			}
			if (!bookingObj.isNull("USERBOOKING"))
			{
				setUserBooking(bookingObj.getString("USERBOOKING"));
			}
			if (!bookingObj.isNull("TRAVELKM"))
			{
				setTravelKM(bookingObj.getString("TRAVELKM"));
			}
			if (!bookingObj.isNull("WAITINGTIME"))
			{
				setWaitingTime(bookingObj.getString("WAITINGTIME"));
			}
			if (!bookingObj.isNull("WAITINGFARE"))
			{
				setWaitingFare(bookingObj.getString("WAITINGFARE"));
			}
			if (!bookingObj.isNull("BOOKINGDATE"))
			{
				setBookingDate(bookingObj.getString("BOOKINGDATE"));
			}
			if (!bookingObj.isNull("RETURNDATE"))
			{
				setReturnDate(bookingObj.getString("RETURNDATE"));
			}
			if (!bookingObj.isNull("DESCRIPTION"))
			{
				setDescription(bookingObj.getString("DESCRIPTION"));
			}
			if (!bookingObj.isNull("FARE"))
			{
				setFare(bookingObj.getString("FARE"));
			}
			if (!bookingObj.isNull("CABTYPE"))
			{
				setCabType(bookingObj.getString("CABTYPE"));
			}
			if (!bookingObj.isNull("BOOKINGSTATUS"))
			{
				setBookingStatus(bookingObj.getString("BOOKINGSTATUS"));
			}
			if (!bookingObj.isNull("TOTALTIME"))
			{
				setTotalTime(bookingObj.getString("TOTALTIME"));
			}
			if (!bookingObj.isNull("DRIVERSTATUS"))
			{
				setDriverStatus(bookingObj.getString("DRIVERSTATUS"));
			}
			if (!bookingObj.isNull("DISTANCE"))
			{
				setDistance(bookingObj.getString("DISTANCE"));
			}

		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}

	}

}
