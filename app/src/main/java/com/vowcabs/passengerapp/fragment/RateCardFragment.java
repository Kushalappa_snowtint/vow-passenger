package com.vowcabs.passengerapp.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vowcabs.passengerapp.HomeActivity;
import com.vowcabs.passengerapp.MyApplication;
import com.vowcabs.passengerapp.R;
import com.vowcabs.passengerapp.utils.LocationInstance;

public class RateCardFragment  extends HomeBaseFragment
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
	{
		View rootView=inflater.inflate(R.layout.ratecardscreen, container,false);
		return rootView;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

          try {
              if (!TextUtils.isEmpty(LocationInstance.getInstance().getCountryCode()) && LocationInstance.getInstance().getCountryCode().equalsIgnoreCase("LK")) {
                  ((HomeActivity) mActivity).pushFragment(new WebUrlViewFragment("http://lk.vowcabs.com/farechart.html"), false, false);
              }
			  else
              {
                  ((HomeActivity) mActivity).pushFragment(new WebUrlViewFragment("https://vowcabs.com/farechart.html"), false, false);
              }
			  ((HomeActivity) getActivity()).updateHeaderText("Fare Chart",mActivity);
          }catch (Exception e){}

	}
	@Override
	public void onResume()
	{
		super.onResume();
		((HomeActivity) getActivity()).updateHeaderText("Fare Chart",mActivity);
		MyApplication.getInstance().trackScreenView("Rate card Screen");
	}

}
